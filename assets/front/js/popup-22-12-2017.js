// Inline popups
$('#inline-popups').magnificPopup({
  delegate: 'a',
  removalDelay: 500, //delay removal by X to allow out-animation
  callbacks: {
    beforeOpen: function() {
       this.st.mainClass = this.st.el.attr('data-effect');
       $('#inline-popups a').find('.radio-active-bg').removeClass('radio-active-bg');
       $('#inline-popups a').find('.ghcoption'+this.st.el.attr('option_id')).addClass('radio-active-bg');
    },
    afterClose: function(){
        getAnswer(this.st.el.attr('option_id'),this.st.el.attr('question_id'),this.st.el.attr('current_question'));
    }
  },
  midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
});

// Hinge effect popup
$('a.hinge').magnificPopup({
  mainClass: 'mfp-with-fade',
  removalDelay: 1000, //delay removal by X to allow out-animation
  callbacks: {
    beforeClose: function() {
        this.content.addClass('hinge');
    }, 
    close: function() {
        this.content.removeClass('hinge'); 
    }
  },
  midClick: true
});