function get_class(){
	
		var franchisee_id = $('#franchisee_id').val();
		var school_id = $('#school_id').val();
		var teacher_id = $('#teacher_id').val();
		
		get_no_of_class(franchisee_id,school_id,teacher_id);	
}

function checkUnique(value,FieldCount){
	
	var class_name = value;
	
	if(FieldCount > 1){
		
		for(var i=1; i <= FieldCount; i++){
			if(i==1){
				var prev_field_count =  1;
			}else{
				var prev_field_count = i - 1;
			}
		var prev_class_name = $('input[name="class_name['+prev_field_count+'][name]"]').val();
		var current_class_name = class_name;	
		
		if(current_class_name!='' && prev_class_name!=''){
		if(current_class_name == prev_class_name){
			
			$(".class_name_"+FieldCount).addClass('error');
			$(".class_name_"+FieldCount).text('Class Name already exists, please try another one');	
			return false;	
		}else{
			
			$(".class_name_"+FieldCount).removeClass('error');
			$(".class_name_"+FieldCount).text('');
		}
		
		}
		}
		
	}
	checkUniqueClass(class_name,FieldCount);	
}

