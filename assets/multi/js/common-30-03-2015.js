jQuery(document).ready(function(){
	try{
		jQuery.validator.addMethod("zipcode_validate", function(zip, element) {
			zip = zip.replace(/\s+/g, ""); 
			return this.optional(element) || zip.match(/^\d{5,6}$/);
		}, "Enter valid zipcode");
	} catch(e) {
		
	}
	$('.flashmsgtext').delay(5000).fadeOut('slow');
	$('.flashmsgtext_error').delay(5000).fadeOut('slow');	
});

function IsEmail() {
		email = document.getElementById('email').value;
		var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (regex.test(email)) {
			return true;
		} else {
		document.getElementById('mailmsg').style.display="block";
			return false;
	   }
}
	



// check valid email address
function validateEmail(sEmail) { 
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}


function redirect(page){
		window.location = page;
}

function deleteConfirm(){
	if(confirm('Are you sure you want to delete?')){
		return true;
	}else{
		return false;
	}
}

function cancelConfirm(){
	if(confirm('Are you sure? You want to cancel.')){
		return true;
	}else{
		return false;
	}
}
// Update user status
function update_user_status(user_id,val){ //alert(user_id + '<>'+ val);
	jQuery('#td_'+user_id).html('<img src='+base_url+'images/loading_small.gif>');	
	jQuery.ajax({
		url : base_url+'ajax/update_user_status/'+user_id+'/'+val,
		success : function(data){
			jQuery('#td_'+user_id).html(data);
		},
		error : function(){
			jQuery('#td_'+user_id).html('Error!');
		}			
	});	
}



function changeStatus(id,table,fieldname){
	var id  = id;
	var table = table;
	var fieldname = fieldname;
	var status = status;
	 $('.status-'+id).attr('src',base_url+'images/loading_small.gif');
		$.ajax({
		type: 'POST',
		url: base_url+'ajax/change_status',
		data: { id:id,table:table,fieldname:fieldname,status:status},
		datatype: 'json',
		cache: 'false',
		success: function(response) {
			if(response.trim() == 1){
				$('.status-'+id).attr('src',base_url+'img/icon-active.gif');
			}else{
				$('.status-'+id).attr('src',base_url+'img/icon-deactivate.gif');
			}
		}
	}); 
}

    
function get_school(franchisee_id,school_id){
//alert(franchisee_id + '<>' + school_id);
$('#school_id').html('<img src='+base_url+'images/loading_small.gif>');	
$.ajax({
	type: 'POST',
	url: base_url+'ajax/get_school',
	data: {franchisee_id:franchisee_id,school_id:school_id},
	datatype: 'html',
	cache: 'false',
	success: function(response) {
		if(response){
			$('#school_id').html(response);
		}else{
			//alert('No School Found for selected Franchisee');	
		}
	}
});
}	

function get_teacher(school_id,teacher_id){

$('#teacher_id').html('<img src='+base_url+'images/loading_small.gif>');	
$.ajax({
	type: 'POST',
	url: base_url+'ajax/get_teacher',
	data: {school_id:school_id,teacher_id:teacher_id},
	datatype: 'html',
	cache: 'false',
	success: function(response) {
		if(response){
			$('#teacher_id').html(response);
		}else{
			//alert('No Teacher Found for selected School');	
		}
	}
});
}	

//check count rows of class if exist
function get_no_of_class(franchsiee_id,school_id,teacher_id) {
	//alert(franchsiee_id+','+school_id+','+teacher_id);
	var get_no_of_class = 0;
	$.ajax({
	type: 'POST',
	url: base_url+'ajax/get_no_of_class',
	data: {franchsiee_id:franchsiee_id,school_id:school_id,teacher_id:teacher_id},
	datatype: 'html',
	cache: 'false',
	success: function(response) {
		
		if(response){
			$('#get_num_class').val(response);
		}else{
			$('#get_num_class').val('0');
			//alert('No Teacher Found for selected School');	
		}
	}
	});
}


//check unique class name
function checkUniqueClass(class_name,school_id,FieldCount) {

	$.ajax({
	type: 'POST',
	url: base_url+'ajax/uniqueClass',
	data: {class_name:class_name,school_id:school_id},
	success: function(response) {
		if(response==1){
			$(".class_name_"+FieldCount).addClass('error');
			$(".class_name_"+FieldCount).text('Class Name already exists, please try another one');
		}else{
			
			$(".class_name_"+FieldCount).text('');
			$(".class_name_"+FieldCount).removeClass('error');
		}
	}
	});
}
    
function readURL(input,id) {
   if (input.files && input.files[0]) {
	 var reader = new FileReader();

	 reader.onload = function (e) {
	   $('#'+id)
	   .attr('src', e.target.result)
	   .width(50)
	   .height(50);
	 };

	 reader.readAsDataURL(input.files[0]);
   }
 }
 

function get_franchsiee_schools(franchisee_id){
	 
	$.ajax({
	type: 'POST',
	url: base_url+'ajax/get_school_list',
	data: {franchisee_id:franchisee_id},
	success: function(data) {
		if(data){
			$("#show_all_school").html(data);
			$('#show_all_school').show();
		}else{
			$('#show_all_school').hide();
			alert('No school Found');	
		}
	}
	});
	 
 }
 
 function get_school_teachers(school_id){
	 
	$.ajax({
	type: 'POST',
	url: base_url+'ajax/get_teacher_list',
	data: {school_id:school_id},
	success: function(data) {
		if(data){
			$("#show_all_teacher").html(data);
			$('#show_all_teacher').show();	
		}else{
			$('#show_all_teacher').hide();		
			alert('No teacher Found');
		}
	}
    });
}

function get_school_teachers(school_id){
	$.ajax({
	type: 'POST',
	url: base_url+'ajax/get_teacher_list',
	data: {school_id:school_id},
	success: function(data) {
		if(data){
			$("#show_all_teacher").html(data);
			$('#show_all_teacher').show();	
		}else{
			$('#show_all_teacher').hide();		
			alert('No teacher Found');
		}
	}
    });
}

var company_ids = [];
function get_faclility_list(company_id,selected_id){
    company_ids.push(company_id);
	
    $('.ms-elem-selection ms-selected').click(function(){
        var li_id = $(this).attr('id');
        alert(li_id);
        //company_ids.pop(selected_ids);
    });
	$.ajax({
	type: 'POST',
	url: base_url+'ajax/get_faclility_list',
	data: {company_ids:company_ids},
	success: function(data) {
		if(data){
			//$("#show_all_teacher").html(data);
			//$('#show_all_teacher').show();	
		}else{
			//$('#show_all_teacher').hide();		
			//alert('No teacher Found');
		}
	}
    });
}