// All Child Assessment Report Related Jquery function

// on document ready
$(document).ready(function(){ 
	
	// form validation
    jQuery("#report_form").validate({ //Validation
		submitHandler: function(form) { //Event fired on form submission
			jQuery("#report_form").ajaxSubmit(function(data){ //Ajax Submit
		   		// jQuery("#comments_list").prepend(data); //Result display
				//You can clear the form at this stage if you want
			});
    	}
    });	
	
 });
 
 
// `from div` show/hide condn
function getFromDivCond(val) {
	 
	$('#report_form')[0].reset();
	$('#compare_type').val(val);
	
	$("#child_list").hide();
	$("#assessment_from").hide();
	$("#class_from").hide();
	$("#teacher_from").hide();
	$("#teacher_to").hide();
	$("#class_to").hide();
		
		if(val==4){
			$("#compare_with option[value='1']").removeAttr('disabled');
			$("#compare_with option[value='2']").removeAttr('disabled');
			$("#compare_with option[value='3']").removeAttr('disabled');
		}else if(val==5){
			$("#compare_with option[value='1']").attr('disabled','disabled');
			$("#compare_with option[value='2']").removeAttr('disabled');
			$("#compare_with option[value='3']").removeAttr('disabled');
		}
		else if(val==6){
			$("#compare_with option[value='1']").attr('disabled','disabled');
			$("#compare_with option[value='2']").attr('disabled','disabled');
			$("#compare_with option[value='3']").attr('disabled','disabled');
		}
		else{
			$("#compare_with option[value='1']").removeAttr('disabled');
			$("#compare_with option[value='2']").removeAttr('disabled');
			$("#compare_with option[value='3']").removeAttr('disabled');
		}
		
		if(val==6){
			$("#school_from").hide();
			$("#school_to").hide();
		}else{
			$("#school_from").show();
			$("#school_to").show();	
		}
		
		if(val==3 || val==4 || val==5 || val==6){
			$("#compare_with_div").show();
		}else{
			$("#compare_with_div").hide();
		}
		
		if(val!=1){
			$("#assessment_to").hide();	
		}
		
		if(val==1 || val==2){
			
		 $('input[name=filter2]').prop("checked",false);
		 $('#both').prop("checked",true);	
		}
}

//on from class change
function getFromClass(class_id,from_to_id){

	var type = $('#compare_type').val();
	if(from_to_id==1){
		getFromChildList(class_id,type);
		getFromAssessmentList(class_id,type);
	}
}


function getCompareWith(val) {
	
	$('#school_to').val(0);
	if(val==1 || val==2){
		$("#school_to").show();
	}else {
		$("#school_to").hide();
	}
	$("#teacher_to").hide();
	$("#class_to").hide();
	
}

//show teacher list from school id
//2nd parameter is only for  `from school` OR `to school` 
function getTeachers(school_id,from_to_id){
	 var type = $('#compare_type').val();
	 var compare_with = $('#compare_with').val();
	 
	 $.ajax({
      url: ''+base_url+'ajax/showTeachers',
      type: 'GET',
      dataType: 'json',
      data: {'school_id': school_id},
      success: function(data) {
         
       	  
		  if(from_to_id==1){
			var teacherContent='<label for="teacher_from">Teacher</label>';
		  	teacherContent +='<select id="teacher_from" name="teacher_from" onchange="getClasses(this.value,'+from_to_id+');" class="form-control required">'; 
		  }else{
			  var teacherContent='<label for="teacher_to">Teacher</label>';
			  teacherContent +='<select id="teacher_to" name="teacher_to" onchange="getClasses(this.value,'+from_to_id+');" class="form-control required">'; 
		  }
		  teacherContent +='<option value="">--Select--</option>';		  
		  
        if(!$.isEmptyObject(data['teacher_data'])){
        $.each(data['teacher_data'], function(index, schoolTeachers) {
          //append to table <tr></tr>
            teacherContent +='<option value="'+schoolTeachers.teacher_id+'">'+schoolTeachers.teacher_name+'</option>';
        });
		}
		
		teacherContent += '</select>';
		
		if(from_to_id==1){
		$("#teacher_from").html('');
		$("#teacher_from").append(teacherContent);
			if(type !=5 && type!=6){
				$("#teacher_from").show();
			}else{
				$("#teacher_from").hide();	
			}
		}
		
		if(from_to_id==2){
			$("#teacher_to").html('');
			$("#teacher_to").append(teacherContent);
			
			if(type !=5 && type!=6){
				if(compare_with==1){
					$("#teacher_to").show();
				}else{
					$("#teacher_to").hide();	
				}
			}else{
				$("#teacher_to").hide();	
			}
			
		}
		
      },
    });
	
}

//show class list from teacher id
function getClasses(teacher_id,from_to_id){
	 
	 var type = $('#compare_type').val();
	 
	 $.ajax({
      url: ''+base_url+'ajax/showClasses',
      type: 'GET',
      dataType: 'json',
      data: {'teacher_id': teacher_id},
      success: function(data) {
         
       	 
		  if(from_to_id==1){
			 var classContent='<label for="class_from">Class</label>';
		  	classContent +='<select id="class_from" name="class_from" onchange="getFromClass(this.value,'+from_to_id+');" class="form-control required">'; 
		  }else{
			 var classContent='<label for="class_to">Class</label>';
			classContent +='<select id="class_to" name="class_to" onchange="getFromClass(this.value,'+from_to_id+');" class="form-control required">';  
		  }
		  classContent +='<option value="">--Select--</option>';		  
		  
        if(!$.isEmptyObject(data['class_data'])){
        $.each(data['class_data'], function(index, teacherClasses) {
          //append to table <tr></tr>
            classContent +='<option value="'+teacherClasses.class_id+'">'+teacherClasses.class_name+'</option>';
        });
		}
		classContent += '</select>';
		
		if(from_to_id==1){
			$("#class_from").html('');
			$("#class_from").append(classContent);
			
			if(type !=5 && type!=6){
				$("#class_from").show();
			}else{
				$("#class_from").hide();	
			}
		}
		if(from_to_id==2){
			$("#class_to").html('');
			$("#class_to").append(classContent);
			
			if(type !=5 && type!=6){
				$("#class_to").show();
			}else{
				$("#class_to").hide();	
			}
			
		}
		
			
      },
    });
	
}

//show children from class id 
function getFromChildList(class_id,type){
   $.ajax({
      url: ''+base_url+'ajax/showChildren',
      type: 'GET',
      dataType: 'json',
      data: {'class_id': class_id},
      success: function(data) {
         
       	  var childContent='<label for="child_list">Child</label>';
		  childContent +='<select id="child_list" name="child_list" onchange="getGender(this.value);" class="form-control required">'; 
		  childContent +='<option value="">--Select--</option>';		  
		  
       	  //var childCount = data['child_data_count'];
		  //$("#child_count").val(childCount);
        if(!$.isEmptyObject(data['child_data'])){
        $.each(data['child_data'], function(index, classChild) {
          //append to table <tr></tr>
            childContent +='<option value="'+classChild.child_id+'">'+classChild.child_forename+' '+classChild.child_surname+'</option>';
        });
      }
        
		childContent += '</select>';
		
		$("#child_list").html('');
		$("#child_list").append(childContent);
		
		if(type !=4 && type!=5){
			$("#child_list").show();
		}
			
      },
    });
}



//show assessment `from` class id
function getFromAssessmentList(class_id,type){
   $.ajax({
      url: ''+base_url+'ajax/showAssessment',
      type: 'GET',
      dataType: 'json',
      data: {'class_id': class_id},
      success: function(data) { 
         
       	  var Content='<label for="assessment_from">';
		  if(type==1){
		  	Content +='Assessment 1';
		  }else{
			Content +='Assessment';  
		  }
		  Content +='</label>';
		  Content +='<select id="assessment_from" name="assessment_from" onchange="getAssessmentFromVal(this.value,'+class_id+');" class="form-control required">';
		  Content +='<option value="">--Select--</option>';		  
		  
       	  //var assessmentCount = data['assessment_data_count'];
		  //$("#assessment_count").val(assessmentCount);
        if(!$.isEmptyObject(data['assessment_data'])){
         $.each(data['assessment_data'], function(index, classAssessment) {
          //append to table <tr></tr>
           Content +='<option value="'+classAssessment.assessment_id+'">'+classAssessment.assessment_date+'</option>';
        });
      }
        
		Content += '</select>';
		$("#assessment_from").html('');
		$("#assessment_from").append(Content);
		if(type!=4 && type!=5){
			$("#assessment_from").show();
		}
		
      },
    });
}

//show assessment 2 for type 1 only
// assessment1 :selected assessment_from,to remove from assessment 2 list
function getToAssessmentList(class_id,assessment_from){
   $.ajax({
      url: ''+base_url+'ajax/showAssessment',
      type: 'GET',
      dataType: 'json',
      data: {'class_id': class_id,'assessment':assessment_from},
      success: function(data) { 
         
       	  var Content='<label for="assessment_to">Assessment 2</label>';
		  Content +='<select id="assessment_to" name="assessment_to" class="form-control required">'; 
		  Content +='<option value="">--Select--</option>';		  
		  
       	  //var assessmentCount = data['assessment_data_count'];
		  //$("#assessment_count").val(assessmentCount);
        if(!$.isEmptyObject(data['assessment_data'])){
        $.each(data['assessment_data'], function(index, classAssessment)  {
          //append to table <tr></tr>
           Content +='<option value="'+classAssessment.assessment_id+'">'+classAssessment.assessment_date+'</option>';
        });
       }
		Content += '</select>';
		$("#assessment_to").html('');
		$("#assessment_to").append(Content);
		$("#assessment_to").show();	
      },
    });
}
   
//get assessment from value
function getAssessmentFromVal(assessment_from,class_id){
	
		 var type = $('#compare_type').val();
		 //var class_id = $('#class_from').val();
		 if(type==1){
			 getToAssessmentList(class_id,assessment_from);
		 }
}

// get child gender
function getGender(child_id){
   $.ajax({
      url: ''+base_url+'ajax/getGender',
      type: 'GET',
      dataType: 'json',
      data: {'child_id': child_id},
      success: function(response) {
		  if(response == 'M'){
			 $('#girls').prop("disabled",true);
			 $('#girls').prop("checked",false);
			 $('#boys').prop("disabled",false);
		  }else{
			 $('#girls').prop("disabled",false);
			 $('#boys').prop("disabled",true);
			 $('#boys').prop("checked",false);
		  }
		  
	  },
    });	
		
}

//if same year group is checked - show text of year
//if same age is checked - show text of child age
function showDiv(val){
	if(val=='Y'){
		$('#year_group').show();
		$('#child_age').hide();
	}else if(val=='A'){
		 $('#year_group').hide();
		 $('#child_age').show();
	}else{
		$('#year_group').hide();
		$('#child_age').hide();
	}
	
	
	
}