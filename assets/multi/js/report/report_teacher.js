// All Child Assessment Report Related Jquery function

// on document ready
$(document).ready(function(){ 
	
	// form validation
    jQuery("#report_form").validate({ //Validation
		submitHandler: function(form) { //Event fired on form submission
			jQuery("#report_form").ajaxSubmit(function(data){ //Ajax Submit
		   		// jQuery("#comments_list").prepend(data); //Result display
				//You can clear the form at this stage if you want
			});
    	}
    });	
	
	//on from class change
	$('#class_from').on('change', function() { 
  		 var class_id = $(this).val();
		 var type = $('#compare_type').val();
		 getFromChildList(class_id,type);
		 getFromAssessmentList(class_id,type);
		 
	});
	
 });
 

 
// `from div` show/hide condn
function getFromDivCond(val) {
	 
	$("#compare_with").val("0");
	 //$("#compare_with option[value='0']").props('selected','selected');
	$("select#compare_with option[value='0']").attr("selected","selected");
		if(val==4){
			$("#child_list").hide();
			$("#assessment_from").hide();
			$("#class_from").show();
			$("#compare_with option[value='1']").attr('disabled','disabled');
		}
		else{
			$("#class_from").show();
			$("#child_list").show();
			$("#compare_with option[value='1']").removeAttr('disabled');
		}
		
		if(val==3 || val==4){
			$("#compare_with_div").show();
		}else{
			$("#compare_with_div").hide();
		}
		
		if(val==1){
			
		}else{
			$("#assessment_to").hide();	
		}
		
		if(val==1 || val==2){
			
		 $('input[name=filter2]').prop("checked",false);
		 $('#both').prop("checked",true);	
		}
}


//show children from class id 
function getFromChildList(class_id,type){
   $.ajax({
      url: ''+base_url+'ajax/showChildren',
      type: 'GET',
      dataType: 'json',
      data: {'class_id': class_id},
      success: function(data) {
         
       	  var childContent='<label for="child_list">Child</label>';
		  childContent +='<select id="child_list" name="child_list" onchange="getGender(this.value);" class="form-control required">';
		  childContent +='<option value="">--Select--</option>';		  
		  
       	  //var childCount = data['child_data_count'];
		  //$("#child_count").val(childCount);
        if(!$.isEmptyObject(data['child_data'])){
        $.each(data['child_data'], function(index, classChild) {
          //append to table <tr></tr>
           childContent +='<option value="'+classChild.child_id+'">'+classChild.child_forename+' '+classChild.child_surname+'</option>';
        });
      }
        
		childContent += '</select>';
		
		$("#child_list").html('');
		$("#child_list").append(childContent);
		
		if(type != '4'){
			$("#child_list").show();
		}
			
      },
    });
}



//show assessment `from` class id
function getFromAssessmentList(class_id,type){
   $.ajax({
      url: ''+base_url+'ajax/showAssessment',
      type: 'GET',
      dataType: 'json',
      data: {'class_id': class_id},
      success: function(data) { 
         
       	  var Content='<label for="assessment_from">';
		  if(type==1){
		  	Content +='Assessment 1';
		  }else{
			Content +='Assessment';  
		  }
		  Content +='</label>';
		  Content +='<select id="assessment_from" name="assessment_from" onchange="getAssessmentFromVal(this.value);" class="form-control required">';
		  Content +='<option value="">--Select--</option>';		  
		  
       	  //var assessmentCount = data['assessment_data_count'];
		  //$("#assessment_count").val(assessmentCount);
        if(!$.isEmptyObject(data['assessment_data'])){
        $.each(data['assessment_data'], function(index, classAssessment) {
          //append to table <tr></tr>
           Content +='<option value="'+classAssessment.assessment_id+'">'+classAssessment.assessment_date+'</option>';
        });
      }
        
		Content += '</select>';
		$("#assessment_from").html('');
		$("#assessment_from").append(Content);
		if(type!=4){
			$("#assessment_from").show();
		}
		
      },
    });
}

//show assessment 2 for type 1 only
// assessment1 :selected assessment_from,to remove from assessment 2 list
function getToAssessmentList(class_id,assessment_from){
   $.ajax({
      url: ''+base_url+'ajax/showAssessment',
      type: 'GET',
      dataType: 'json',
      data: {'class_id': class_id,'assessment':assessment_from},
      success: function(data) { 
         
       	  var Content='<label for="assessment_to">Assessment 2</label>';
		  Content +='<select id="assessment_to" name="assessment_to" class="form-control required">';
		  Content +='<option value="">--Select--</option>';		  
		  
       	  //var assessmentCount = data['assessment_data_count'];
		  //$("#assessment_count").val(assessmentCount);
        if(!$.isEmptyObject(data['assessment_data'])){
        $.each(data['assessment_data'], function(index, classAssessment) {
          //append to table <tr></tr>
           Content +='<option value="'+classAssessment.assessment_id+'">'+classAssessment.assessment_date+'</option>';
        });
      }
		Content += '</select>';
		$("#assessment_to").html('');
		$("#assessment_to").append(Content);
		$("#assessment_to").show();	
      },
    });
}
   
//get assessment from value
function getAssessmentFromVal(assessment_from){
	
		 var type = $('#compare_type').val();
		 var class_id = $('#class_from').val();
		 if(type==1){
			 getToAssessmentList(class_id,assessment_from);
		 }
}

// get child gender
function getGender(child_id){
   $.ajax({
      url: ''+base_url+'ajax/getGender',
      type: 'GET',
      dataType: 'json',
      data: {'child_id': child_id},
      success: function(response) {
		  if(response == 'M'){
			 $('#girls').prop("disabled",true);
			 $('#girls').prop("checked",false);
			 $('#boys').prop("disabled",false);
		  }else{
			 $('#girls').prop("disabled",false);
			 $('#boys').prop("disabled",true);
			 $('#boys').prop("checked",false);
		  }
		  
	  },
    });	
		
}

//if same year group is checked - show text of year
//if same age is checked - show text of child age
function showDiv(val){
	if(val=='Y'){
		$('#year_group').show();
		$('#child_age').hide();
	}else if(val=='A'){
		 $('#year_group').hide();
		 $('#child_age').show();
	}else{
		$('#year_group').hide();
		$('#child_age').hide();
	}
	
	
	
}