
// search function
/*$("#search").keyup(function() {
        var search = $(this).val();
        $(".records").show();
		$(".pagination").hide();
        if (search){
			$(".records").not(":contains(" + search + ")").hide();
		}else {
			$(".pagination").show();	
		}
});*/

$(document).ready(function(){
	$('#search').keyup(function() {
		searchTable($(this).val());
	});
});

function searchTable(inputVal){

	var table = $('#tblData');
	table.find('tr').each(function(index, row){
		var allCells = $(row).find('td');
		if(allCells.length > 0) {
			var found = false;
			allCells.each(function(index, td){
				var regExp = new RegExp(inputVal, 'i');
				if(regExp.test($(td).text())) {
					found = true;
					return false;
				}
		});
		if(found == true){
			$(row).show();
			$(".pagination").show();
		}
		else { 
			$(row).hide();
			$(".pagination").hide();
			}
		}
	});
}

// ajax function  
function get_records(page,limit) { 


		jQuery.ajax({
					url: base_url+'test/ajax',
					type:'POST',
					data:{'page':page,'limit':limit},
					dataType: 'html',
					beforeSend: function() {
						  // alert('loader');
                    },
					success:function(data){
						
						//$('#records-div').html('');
						//$('#records-div').html(data);
						//$("#page-limit").val(limit).find("option[value=" + limit +"]").attr('selected', true);
						},
							error:function (request,error) {
                            	alert('Network error has occurred please try again!');
                        	}
		});	
}

function get_records_onpage_limit(limit,territory_id,role_id) { 
	get_records(territory_id,role_id,'0',limit);
}


