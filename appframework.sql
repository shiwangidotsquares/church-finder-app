-- phpMyAdmin SQL Dump
-- version 3.3.8.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 11, 2020 at 01:18 PM
-- Server version: 5.6.34
-- PHP Version: 5.4.45

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `appframework`
--

-- --------------------------------------------------------

--
-- Table structure for table `lss_actions`
--

CREATE TABLE IF NOT EXISTS `lss_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controller_id` int(11) NOT NULL,
  `action` varchar(255) NOT NULL,
  `action_url` varchar(255) DEFAULT '',
  `alias` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `controller_id` (`controller_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=399 ;

--
-- Dumping data for table `lss_actions`
--

INSERT INTO `lss_actions` (`id`, `controller_id`, `action`, `action_url`, `alias`) VALUES
(206, 41, 'index', '/index', 'View Dashboard'),
(207, 42, 'index', '/index', 'View Configuration'),
(208, 42, 'add', '/add', 'Add Configuration'),
(209, 42, 'edit', '/edit', 'Edit Configuration'),
(210, 42, 'delete', '/delete', 'Delete Configuration'),
(211, 42, 'status', '/status', 'Change Status'),
(233, 47, 'index', '/index', 'View Products'),
(234, 47, 'add', '/add', 'Add Product'),
(235, 47, 'edit', '/edit', 'Edit Pages'),
(236, 47, 'delete', '/delete', 'Delete Page'),
(237, 47, 'status', '/status', 'Change Status'),
(242, 50, 'index', '/index', 'View Users'),
(243, 50, 'add', '/add', 'Add User'),
(244, 50, 'edit', '/edit', 'Edit Individuals'),
(246, 50, 'status', '/status', 'Change Status'),
(247, 51, 'index', '/index', 'View Company'),
(248, 51, 'add', '/add', 'Add Company'),
(249, 51, 'edit', '/edit', 'Edit Company'),
(253, 52, 'index', '/index', 'View Staff'),
(254, 52, 'add', '/add', 'Add Staff'),
(255, 52, 'edit', '/edit', 'Edit Staff'),
(256, 54, 'index', '/index', 'View Categories'),
(257, 54, 'add', '/add', 'Add Category'),
(282, 65, 'send_email', '/send_email', 'Send Email'),
(288, 67, 'index', '/index', 'View FAQ'),
(289, 67, 'add', '/add', 'Add FAQ'),
(290, 67, 'edit', '/edit', 'Edit FAQ'),
(293, 69, 'index', '/index', 'View Blog'),
(294, 69, 'edit', '/edit', 'Edit  Blog'),
(297, 69, 'add', 'add', 'Add Blogs'),
(308, 75, 'index', '/index', 'View Roles'),
(309, 75, 'add', '/add', 'Add Role'),
(310, 75, 'edit', '/edit', 'Edit Role'),
(311, 75, 'delete', '/delete', 'Delete Role'),
(312, 75, 'status', '/status', 'Change Status'),
(313, 75, 'assign', '/assign', 'Assign Module'),
(320, 50, 'delete', '/delete', 'Delete Individual'),
(321, 51, 'delete', '/delete', 'Delete Company'),
(322, 51, 'status', '/status', 'Change Status'),
(323, 52, 'delete', '/delete', 'Delete Staff'),
(324, 52, 'status', '/status', 'Change Status'),
(325, 54, 'edit', '/edit', 'Edit Category'),
(326, 54, 'delete', '/delete', 'Delete Category'),
(327, 54, 'status', '/status', 'Change Status'),
(342, 67, 'delete', '/delete', 'Delete FAQ'),
(344, 69, 'delete', '/delete', 'Delete Blog'),
(345, 69, 'status', '/status', 'Change Status'),
(354, 50, 'view', '/view', 'View Info'),
(355, 51, 'view', '/view', 'View Info'),
(356, 52, 'view', '/view', 'View Info'),
(357, 79, 'add', '/add', 'Add Job'),
(358, 79, 'index', '/view', 'View Jobs'),
(359, 77, 'add', '/add', 'Add Event'),
(360, 77, 'index', '/view', 'View Events'),
(361, 78, 'add', '/add', 'Add Directory'),
(362, 78, 'index', '/view', 'View Directory'),
(363, 76, 'add', '/add', 'Add News'),
(364, 76, 'index', '/view', 'View News'),
(365, 79, 'edit', '/edit', 'Edit Jobs'),
(366, 79, 'delete', '/delete', 'Delete Job'),
(367, 79, 'status', '/status', 'Change Status'),
(368, 76, 'edit', '/edit', 'Edit News'),
(369, 76, 'delete', '/delete', 'Delete News'),
(370, 76, 'status', '/status', 'Change Status'),
(371, 77, 'delete', '/delete', 'Delete Event'),
(372, 77, 'edit', '/edit', 'Edit Event'),
(373, 77, 'status', '/status', 'Status Event'),
(374, 81, 'delete', '/delete', 'Delete Survey '),
(375, 81, 'add', '/add', 'Add Survey '),
(376, 81, 'edit', '/edit', 'Edit Survey '),
(377, 81, 'status', '/status', 'Status Survey '),
(378, 82, 'question', '/question', 'Delete Question'),
(379, 82, 'status', '/status', 'Status Question'),
(380, 82, 'add', '/add', 'Add Question'),
(381, 82, 'edit', '/edit', 'Edit Question'),
(382, 81, 'index', '/index', 'View Survey '),
(383, 82, 'index', '/index', 'View Question'),
(384, 83, 'index', '/index', 'Set Permission'),
(385, 80, 'index', '/index', 'View media');

-- --------------------------------------------------------

--
-- Table structure for table `lss_amenity`
--

CREATE TABLE IF NOT EXISTS `lss_amenity` (
  `feature_id` int(11) NOT NULL AUTO_INCREMENT,
  `feature_name` varchar(255) NOT NULL,
  `amenity_created_time` datetime NOT NULL,
  PRIMARY KEY (`feature_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `lss_amenity`
--

INSERT INTO `lss_amenity` (`feature_id`, `feature_name`, `amenity_created_time`) VALUES
(1, 'test hfhfg', '2018-07-11 00:00:00'),
(2, 'kggjg', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `lss_blog`
--

CREATE TABLE IF NOT EXISTS `lss_blog` (
  `blog_id` int(11) NOT NULL AUTO_INCREMENT,
  `org_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `blog_title` varchar(255) NOT NULL,
  `blog_description` text NOT NULL,
  `blog_image` varchar(255) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `blog_is_active` tinyint(4) NOT NULL DEFAULT '1',
  `blog_is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `blog_created_date` datetime NOT NULL,
  `blog_updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`blog_id`),
  KEY `org_id` (`org_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `lss_blog`
--

INSERT INTO `lss_blog` (`blog_id`, `org_id`, `category_id`, `blog_title`, `blog_description`, `blog_image`, `tags`, `blog_is_active`, `blog_is_deleted`, `blog_created_date`, `blog_updated_date`) VALUES
(2, 1, 18, 'test final', '<p>\n	test</p>', '1523542847Status Light_1.png', 'test,blogs', 1, 0, '2018-04-13 08:55:30', '2018-06-20 06:10:09'),
(3, 1, 18, 'test blog 1 1', '<p>\n	blohg 1 1</p>', '1523857687_logo-image-bank-dontblink.jpg', 'blog 1 1', 1, 0, '2018-04-16 06:04:01', '2020-02-14 10:50:26'),
(4, 1, 41, 'blog test', '<p>\n	aer</p>', '1523443342Chrysanthemum.jpg', 'aer', 1, 0, '2018-04-16 06:09:31', '2018-04-16 06:56:18');

-- --------------------------------------------------------

--
-- Table structure for table `lss_business_directories`
--

CREATE TABLE IF NOT EXISTS `lss_business_directories` (
  `business_directories_id` int(10) NOT NULL AUTO_INCREMENT,
  `org_id` int(11) NOT NULL,
  `business_directories_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `business_directories_logo` varchar(255) CHARACTER SET latin1 NOT NULL,
  `business_directories_rating` decimal(1,1) NOT NULL,
  `business_directories_category` int(11) NOT NULL,
  `business_directories_website_url` varchar(100) CHARACTER SET latin1 NOT NULL,
  `business_directories_email_address` varchar(255) CHARACTER SET latin1 NOT NULL,
  `business_directories_phone_number` varchar(20) CHARACTER SET latin1 NOT NULL,
  `business_directories_fax_number` varchar(20) CHARACTER SET latin1 NOT NULL,
  `business_directories_address` varchar(255) CHARACTER SET latin1 NOT NULL,
  `business_directories_city` varchar(255) CHARACTER SET latin1 NOT NULL,
  `business_directories_state` varchar(255) CHARACTER SET latin1 NOT NULL,
  `business_directories_country` int(11) NOT NULL,
  `business_directories_zip_code` varchar(50) CHARACTER SET latin1 NOT NULL,
  `business_directories_google_plus` varchar(255) CHARACTER SET latin1 NOT NULL,
  `business_directories_twitter` varchar(255) CHARACTER SET latin1 NOT NULL,
  `business_directories_facebook` varchar(255) CHARACTER SET latin1 NOT NULL,
  `business_directories_linkedIn` varchar(255) CHARACTER SET latin1 NOT NULL,
  `business_directories_pinterest` varchar(255) CHARACTER SET latin1 NOT NULL,
  `business_directories_instagram` varchar(255) CHARACTER SET latin1 NOT NULL,
  `business_directories_is_active` enum('0','1') CHARACTER SET latin1 NOT NULL,
  `business_directories_is_deleted` enum('0','1') CHARACTER SET latin1 NOT NULL,
  `business_directories_created_time` datetime NOT NULL,
  `business_directories_updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `business_directories_description` text CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`business_directories_id`),
  KEY `org_id` (`org_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `lss_business_directories`
--

INSERT INTO `lss_business_directories` (`business_directories_id`, `org_id`, `business_directories_name`, `business_directories_logo`, `business_directories_rating`, `business_directories_category`, `business_directories_website_url`, `business_directories_email_address`, `business_directories_phone_number`, `business_directories_fax_number`, `business_directories_address`, `business_directories_city`, `business_directories_state`, `business_directories_country`, `business_directories_zip_code`, `business_directories_google_plus`, `business_directories_twitter`, `business_directories_facebook`, `business_directories_linkedIn`, `business_directories_pinterest`, `business_directories_instagram`, `business_directories_is_active`, `business_directories_is_deleted`, `business_directories_created_time`, `business_directories_updated_time`, `business_directories_description`) VALUES
(1, 1, 'Business 1 1', '1523535073Chrysanthemum.jpg', 0.9, 3, '654', '12asd@sd', '2341', 'asd', '234', '234', '234', 0, '234', '645', '654', '465', '465', '654', '654', '1', '0', '2018-04-16 06:12:50', '2018-04-16 06:14:14', '211');

-- --------------------------------------------------------

--
-- Table structure for table `lss_career`
--

CREATE TABLE IF NOT EXISTS `lss_career` (
  `job_id` int(10) NOT NULL AUTO_INCREMENT,
  `org_id` int(11) NOT NULL,
  `jobs_title` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `desire_skills` varchar(255) NOT NULL,
  `desire_experience` int(2) NOT NULL,
  `job_type` int(2) NOT NULL,
  `location` varchar(255) NOT NULL,
  `jobs_description` text NOT NULL,
  `jobs_is_active` enum('0','1') NOT NULL DEFAULT '1',
  `jobs_is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `jobs_created_time` datetime NOT NULL,
  `jobs_updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`job_id`),
  KEY `org_id` (`org_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `lss_career`
--

INSERT INTO `lss_career` (`job_id`, `org_id`, `jobs_title`, `department`, `desire_skills`, `desire_experience`, `job_type`, `location`, `jobs_description`, `jobs_is_active`, `jobs_is_deleted`, `jobs_created_time`, `jobs_updated_time`) VALUES
(1, 1, 'Test Job', '', 'Test Skills ', 2, 2, 'jaipur', '<p>\n	Test Desc</p>\n', '1', '1', '2018-04-13 13:22:14', '2018-04-16 06:54:42'),
(2, 1, 'test job', '', 'asd', 3, 1, 'asd', '<p>\n	asd</p>\n', '1', '1', '2018-04-16 06:46:04', '2018-04-16 06:54:36'),
(3, 1, '        as33', '', '    er\ner\nre3\nre', 5, 2, '    sdf3', '<h3 style="color:blue;">\n	er3</h3>\n', '1', '1', '2018-04-16 06:46:42', '2018-04-16 06:54:23'),
(4, 1, 'test sadd', '', '     dasda', 4, 3, 'dfasf', '<p>\n	safdsfdsafds</p>\n', '1', '0', '2018-04-17 05:17:43', '2018-06-20 10:30:18'),
(5, 1, 'ffs', '', 'fdsff', 2, 2, 'dsff', '<p>\n	fsfsd</p>\n', '1', '0', '2018-06-20 10:07:09', '2018-06-20 10:31:20'),
(6, 1, 'dasda', '', 'dadas', 4, 4, 'dsff', '<p>\n	dasdasdsad</p>\n', '1', '0', '2018-06-20 10:08:51', '2018-06-20 10:08:51');

-- --------------------------------------------------------

--
-- Table structure for table `lss_categories`
--

CREATE TABLE IF NOT EXISTS `lss_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `org_id` int(11) NOT NULL,
  `category_parent_id` int(11) NOT NULL DEFAULT '0',
  `category_name` varchar(255) NOT NULL,
  `category_description` text NOT NULL,
  `category_is_active` tinyint(4) NOT NULL DEFAULT '1',
  `category_is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `category_created_time` datetime NOT NULL,
  `category_updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`),
  KEY `org_id` (`org_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62 ;

--
-- Dumping data for table `lss_categories`
--

INSERT INTO `lss_categories` (`category_id`, `org_id`, `category_parent_id`, `category_name`, `category_description`, `category_is_active`, `category_is_deleted`, `category_created_time`, `category_updated_time`) VALUES
(1, 1, 0, 'test cat', '', 1, 1, '2018-04-10 12:04:27', '2020-02-14 12:14:38'),
(2, 1, 0, 'Business Directory', '', 1, 0, '2018-04-10 12:06:06', '2020-02-14 12:14:38'),
(3, 1, 2, 'Pharmaceutical industry', '', 1, 0, '2018-04-10 12:08:20', '2020-02-14 12:14:38'),
(4, 1, 0, 'User Type', '', 1, 0, '2018-04-10 14:31:55', '2020-02-14 12:14:38'),
(5, 1, 4, 'Staff', '', 1, 1, '2018-04-10 14:32:13', '2020-02-14 12:14:38'),
(6, 1, 4, 'End User', '', 1, 0, '2018-04-10 14:32:20', '2020-02-14 12:14:38'),
(7, 1, 0, 'Buyer', '', 1, 0, '2018-04-10 14:32:26', '2020-02-14 12:14:38'),
(8, 1, 0, 'Seller', '', 1, 0, '2018-04-10 14:32:31', '2020-02-14 12:14:38'),
(9, 1, 0, 'Survey', '', 1, 0, '2018-04-10 15:28:19', '2020-02-14 12:14:38'),
(10, 1, 9, 'ENT ', '', 1, 0, '2018-04-10 15:29:14', '2020-02-14 12:14:38'),
(11, 1, 9, 'Orthopaedics', '', 1, 0, '2018-04-10 15:29:27', '2020-02-14 12:14:38'),
(12, 1, 0, 'Oncology', '', 1, 1, '2018-04-10 15:29:39', '2020-02-14 12:14:38'),
(13, 1, 9, 'Identity Theft', '0', 1, 0, '2018-04-10 15:29:57', '2020-02-14 12:14:38'),
(14, 1, 0, 'Products', '', 1, 0, '2018-04-11 14:03:41', '2020-02-14 12:14:38'),
(15, 1, 14, 'Golf Items', '', 1, 0, '2018-04-11 14:07:15', '2020-02-14 12:14:38'),
(16, 1, 0, 'Blog', '', 1, 0, '2018-04-11 15:01:42', '2020-02-14 12:14:38'),
(17, 1, 16, 'Time Management', '', 1, 0, '2018-04-11 15:06:17', '2020-02-14 12:14:38'),
(18, 1, 16, 'Motivation', '', 1, 0, '2018-04-11 15:06:28', '2020-02-14 12:14:38'),
(28, 1, 0, 'News', '0', 1, 0, '2018-04-12 16:12:38', '2020-02-14 12:14:38'),
(29, 1, 28, 'test', '', 1, 1, '2018-04-13 14:12:30', '2020-02-14 12:14:38'),
(30, 1, 0, 'asdasd 1', '0', 1, 0, '2018-04-13 11:24:09', '2020-02-14 12:14:38'),
(31, 1, 30, 'asd', '', 1, 1, '2018-04-13 11:24:19', '2020-02-14 12:14:38'),
(32, 1, 0, 'ss Lorem Ipsum ', '0', 1, 0, '2018-04-13 11:25:44', '2020-02-14 12:14:38'),
(33, 1, 0, 'abc', '0', 1, 0, '2018-04-13 11:34:53', '2020-02-14 12:14:38'),
(35, 1, 32, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', '', 1, 0, '2018-04-13 11:37:42', '2020-02-14 12:14:38'),
(36, 1, 30, '1', '', 1, 1, '2018-04-13 11:39:53', '2020-02-14 12:14:38'),
(37, 1, 36, '2', '', 1, 1, '2018-04-13 11:39:59', '2020-02-14 12:14:38'),
(38, 1, 37, '3', '', 1, 0, '2018-04-13 11:40:07', '2020-02-14 12:14:38'),
(39, 1, 14, 'product 1', '', 1, 1, '2018-04-13 11:58:46', '2020-02-14 12:14:38'),
(40, 1, 15, 'item 2', '', 1, 0, '2018-04-13 11:58:55', '2020-02-14 12:14:38'),
(41, 1, 16, 'blog test', '', 1, 1, '2018-04-16 06:09:16', '2020-02-14 12:14:38'),
(42, 1, 2, 'directory test', '', 1, 0, '2018-04-16 06:10:21', '2020-02-14 12:14:38'),
(43, 1, 9, 'survey test', '', 1, 1, '2018-04-16 06:17:11', '2020-02-14 12:14:38'),
(44, 1, 30, '2', '', 1, 1, '2018-04-17 06:29:39', '2020-02-14 12:14:38'),
(45, 1, 44, 'dsgdfgdfg', '', 1, 1, '2018-04-17 06:30:56', '2020-02-14 12:14:38'),
(46, 1, 0, 'abcl', '0', 1, 0, '2018-04-17 07:17:58', '2020-02-14 12:14:38'),
(48, 1, 46, 'test', '', 1, 0, '2018-04-23 07:06:05', '2020-02-14 12:14:38'),
(49, 1, 48, 'test2', '', 1, 1, '2018-04-23 07:06:15', '2020-02-14 12:14:38'),
(50, 1, 49, 'test3', '', 1, 1, '2018-04-23 07:06:24', '2020-02-14 12:14:38'),
(51, 1, 0, 'business', '', 1, 0, '2018-07-17 06:28:59', '2020-02-14 12:14:38'),
(52, 1, 46, 'test2', '', 1, 0, '2018-08-08 09:08:41', '2020-02-14 12:14:38'),
(53, 1, 6, 'test', '', 1, 0, '2019-05-16 13:37:52', '2020-02-14 12:14:38'),
(54, 1, 0, 'p1', '', 1, 0, '2019-05-16 13:38:02', '2020-02-14 12:14:38'),
(55, 1, 54, 'pc1', '', 1, 0, '2019-05-16 13:38:10', '2020-02-14 12:14:38'),
(56, 1, 54, 'PC2', '', 1, 0, '2019-05-16 13:38:20', '2020-02-14 12:14:38'),
(57, 1, 56, 'PCC', '', 1, 0, '2019-05-16 13:38:37', '2020-02-14 12:14:38'),
(58, 1, 57, '4th', '', 1, 0, '2019-07-30 11:11:47', '2020-02-14 12:14:38'),
(59, 1, 58, '5th', '', 1, 0, '2019-07-30 11:12:00', '2020-02-14 12:14:38'),
(60, 1, 59, 'ddf', '', 1, 0, '2019-07-30 11:32:51', '2020-02-14 12:14:38'),
(61, 1, 0, 'test', '', 1, 0, '2020-01-02 09:09:41', '2020-02-14 12:14:38');

-- --------------------------------------------------------

--
-- Table structure for table `lss_controllers`
--

CREATE TABLE IF NOT EXISTS `lss_controllers` (
  `controller_id` int(11) NOT NULL AUTO_INCREMENT,
  `controller_name` varchar(255) NOT NULL,
  `controller_url` varchar(255) DEFAULT NULL,
  `controller_alias` varchar(100) NOT NULL,
  `ordering` int(11) NOT NULL,
  `controllers_is_active` enum('0','1') NOT NULL,
  `controller_price` double NOT NULL,
  `controller_description` varchar(255) NOT NULL,
  `controller_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`controller_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=89 ;

--
-- Dumping data for table `lss_controllers`
--

INSERT INTO `lss_controllers` (`controller_id`, `controller_name`, `controller_url`, `controller_alias`, `ordering`, `controllers_is_active`, `controller_price`, `controller_description`, `controller_is_deleted`) VALUES
(41, 'dashboard', '/dashboard', 'Dashboard', 0, '0', 0, '', 0),
(42, 'website_config', '/website_config', 'Configuration', 1, '1', 0, '', 0),
(47, 'product', '/product', 'Manage Products', 6, '0', 0, '', 0),
(50, 'user', '/user', 'Manage Users', 2, '0', 0, '', 0),
(51, 'company', '/company', 'Manage Companies', 3, '1', 0, '', 0),
(52, 'staff', '/staff', 'Manage Staff', 4, '1', 0, '', 0),
(54, 'category', '/category', 'manage Categories', 3, '0', 0, '', 0),
(65, 'message', '/message', 'Send message', 7, '1', 0, '', 0),
(67, 'faq', '/faq', 'Manage FAQ', 9, '0', 0, '', 0),
(69, 'blog', '/blog', 'Manage Blog', 8, '0', 0, '', 0),
(75, 'role', '/role', 'Manage Role', 10, '0', 0, '', 0),
(76, 'news', '/news', 'Manage News', 12, '0', 0, '', 0),
(77, 'event', '/event', 'Manage Event', 13, '0', 0, '', 0),
(78, 'directories', '/directories', 'Manage Business Directory', 14, '0', 0, '', 0),
(79, 'job', '/job', 'Manage Jobs', 15, '0', 0, '', 0),
(80, 'upload', '/upload', 'Manage Media', 3, '0', 0, '', 0),
(81, 'survey', '/survey', 'Manage survey', 16, '0', 0, '', 0),
(82, 'question', '/question', 'Manage Question', 17, '0', 0, '', 0),
(83, 'permission', '/permission', 'Manage Permission', 18, '0', 0, '', 0),
(84, 'price', '/price', 'Manage price', 19, '0', 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lss_country`
--

CREATE TABLE IF NOT EXISTS `lss_country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`country_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=258 ;

--
-- Dumping data for table `lss_country`
--

INSERT INTO `lss_country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Armenia', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Belarus', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D''Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Georgia', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'Korea, Republic of', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People''s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Moldova, Republic of', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Russian Federation', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Tajikistan', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Turkmenistan', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Ukraine', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1),
(252, 'Ascension Island (British)', 'AC', 'ASC', '', 0, 1),
(253, 'Kosovo, Republic of', 'XK', 'UNK', '', 0, 1),
(254, 'Isle of Man', 'IM', 'IMN', '', 0, 1),
(255, 'Tristan da Cunha', 'TA', 'SHN', '', 0, 1),
(256, 'Guernsey', 'GG', 'GGY', '', 0, 1),
(257, 'Jersey', 'JE', 'JEY', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `lss_customer`
--

CREATE TABLE IF NOT EXISTS `lss_customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(250) NOT NULL,
  `email_address` varchar(250) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `pin_code` varchar(250) NOT NULL,
  `company_name` varchar(250) NOT NULL,
  `company_address` varchar(250) NOT NULL,
  `company_url` varchar(250) NOT NULL,
  `phone` varchar(250) NOT NULL,
  `fax` varchar(250) NOT NULL,
  `skype_id` varchar(250) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `status` varchar(250) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `lss_customer`
--

INSERT INTO `lss_customer` (`customer_id`, `client_name`, `email_address`, `address`, `city`, `state`, `pin_code`, `company_name`, `company_address`, `company_url`, `phone`, `fax`, `skype_id`, `is_deleted`, `status`) VALUES
(1, 'test', 'test@gmail.com', '4 ra 5', 'jaipur', 'rajasthan', '323001', 'gfdgfgf', '0', 'https://www.dotsquares.com/', '6864646', 'mbbm', 'mnmnbmnb', 0, '0'),
(2, 'test1', 'test2@gmail.com', '4 ra 5 3', 'jaipur 4', 'rajasthan 5', '323001 6', 'gfdgfgf 7', 'egtry54rtfwettr 8', 'https://www.dotsquares.com/9', '6864646 10', 'mbbm 11', 'mnmnbmnb 12hjhj', 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `lss_employee`
--

CREATE TABLE IF NOT EXISTS `lss_employee` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` bigint(20) NOT NULL,
  `date_of_birth` date NOT NULL,
  `marital_status` enum('0','1') NOT NULL COMMENT '0 for married and 1 for married ',
  `education_qualification` int(2) NOT NULL,
  `permanent_address` text NOT NULL,
  `temporary_address` text NOT NULL,
  `date_of_joining` date NOT NULL,
  `department` int(2) NOT NULL,
  `designation` int(2) NOT NULL,
  `status` enum('0','1') NOT NULL COMMENT '0 for active ',
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `is_deleted` enum('0','1') NOT NULL COMMENT '0 for deleted and 1 for deleted',
  `org_id` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `lss_employee`
--

INSERT INTO `lss_employee` (`id`, `first_name`, `last_name`, `email`, `phone_number`, `date_of_birth`, `marital_status`, `education_qualification`, `permanent_address`, `temporary_address`, `date_of_joining`, `department`, `designation`, `status`, `created_date`, `updated_date`, `is_deleted`, `org_id`) VALUES
(2, 'Ajithgdfgd', 'Singh', 'ajit1.singh@dotsquares.com', 7791010259, '0000-00-00', '1', 1, 'Jaipurfgjngfgn', 'sikarffgnfgnfg', '2018-12-26', 1, 3, '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', 1),
(13, 'test', 'double', 'admin@mailinator.com', 957978987, '2018-06-29', '1', 1, 'gjghjh', 'jghjg', '2018-06-28', 1, 0, '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `lss_event`
--

CREATE TABLE IF NOT EXISTS `lss_event` (
  `event_id` int(10) NOT NULL AUTO_INCREMENT,
  `org_id` int(11) NOT NULL,
  `event_title` varchar(255) NOT NULL,
  `event_description` text NOT NULL,
  `event_date` datetime NOT NULL,
  `event_venue` varchar(255) NOT NULL,
  `event_image_name` varchar(255) NOT NULL,
  `event_is_active` enum('0','1') NOT NULL,
  `event_is_deleted` enum('0','1') NOT NULL,
  `event_created_time` datetime NOT NULL,
  `event_updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`event_id`),
  KEY `org_id` (`org_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `lss_event`
--

INSERT INTO `lss_event` (`event_id`, `org_id`, `event_title`, `event_description`, `event_date`, `event_venue`, `event_image_name`, `event_is_active`, `event_is_deleted`, `event_created_time`, `event_updated_time`) VALUES
(1, 1, 'weekend fun', '<p>\n	t65654645</p>\n', '2018-09-29 00:00:00', 'test', '1523443342Chrysanthemum.jpg', '1', '0', '2018-08-10 06:16:32', '2018-08-10 06:16:32'),
(2, 1, 'Mass Gathering', '<p>\n	<span class="st" style="line-height: 1.4; overflow-wrap: break-word; color: rgb(84, 84, 84); font-family: arial, sans-serif; font-size: small;"><span style="font-weight: bold; color: rgb(106, 106, 106);">Gathering Events</span>&nbsp;is a mobile pop-up bar and caravan bar service for weddings and events. We offer bars hire, bartenders and cocktail bartenders in Brisbane,&nbsp;...</span></p>\n<div>\n	&nbsp;</div>\n', '2019-02-13 00:00:00', 'Community Hall', '', '1', '0', '2019-02-08 11:47:33', '2019-02-08 11:47:33');

-- --------------------------------------------------------

--
-- Table structure for table `lss_faq`
--

CREATE TABLE IF NOT EXISTS `lss_faq` (
  `faq_id` int(11) NOT NULL AUTO_INCREMENT,
  `org_id` int(11) NOT NULL,
  `faq_title` varchar(255) NOT NULL,
  `faq_description` text NOT NULL,
  `faq_ordering` int(11) NOT NULL DEFAULT '0',
  `faq_is_active` tinyint(4) NOT NULL DEFAULT '1',
  `faq_is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `faq_created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`faq_id`),
  KEY `org_id` (`org_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `lss_faq`
--

INSERT INTO `lss_faq` (`faq_id`, `org_id`, `faq_title`, `faq_description`, `faq_ordering`, `faq_is_active`, `faq_is_deleted`, `faq_created_date`) VALUES
(1, 1, 'test', '<p>\r\n	test</p>\r\n', 2, 1, 0, '2018-04-13 08:46:01'),
(2, 1, '1 1', '<p>\n	11</p>\n', 11, 1, 1, '2018-04-16 06:08:04'),
(3, 1, 'asdddddddddddd', '<p>\n	ddddddddddddddddddddddddddddddddddddddddddd</p>\n', 0, 1, 1, '2018-04-16 06:08:41'),
(4, 1, 'jhj', '<p>\r\n	khj</p>\r\n', 0, 1, 1, '2020-02-27 04:59:52');

-- --------------------------------------------------------

--
-- Table structure for table `lss_form`
--

CREATE TABLE IF NOT EXISTS `lss_form` (
  `form_id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) NOT NULL,
  `form_name` varchar(80) NOT NULL,
  `field_title` varchar(60) NOT NULL,
  `field_type` varchar(255) NOT NULL,
  `field_value` varchar(255) NOT NULL,
  PRIMARY KEY (`form_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `lss_form`
--

INSERT INTO `lss_form` (`form_id`, `field_id`, `form_name`, `field_title`, `field_type`, `field_value`) VALUES
(10, 0, 'sadsads', '', '', ''),
(11, 0, 'gdfgfdg', '', '', ''),
(12, 0, 'fsdfdsf', '', '', ''),
(13, 0, 'rtyyrty', '', '', ''),
(14, 0, 'sdfdsf', '', '', ''),
(15, 0, 'hfghgh', '', '', ''),
(16, 0, 'dfgf', '', '', ''),
(17, 0, 'chandan sir', '', '', ''),
(18, 0, 'sadasdsadas', '', '', ''),
(19, 0, 'sfsdfsdfdsfsdfsdfsfsdfsdfdsfsdfdsfdsf', '', '', ''),
(20, 0, '', '', '', ''),
(21, 0, 'fff', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `lss_job`
--

CREATE TABLE IF NOT EXISTS `lss_job` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_title` varchar(255) NOT NULL,
  `job_description` text NOT NULL,
  `client_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `org_id` int(255) NOT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `lss_job`
--

INSERT INTO `lss_job` (`job_id`, `job_title`, `job_description`, `client_id`, `employee_id`, `role`, `start_date`, `end_date`, `is_deleted`, `org_id`) VALUES
(1, 'Business Analyst ', '<p>\n	<span style="font-size:14px;"><span style="font-family: arial, sans-serif;">A requirement of someone who analyzes an organization or&nbsp;</span><b style="font-family: arial, sans-serif; font-size: 16px;">business</b><span style="font-family: arial, sans-serif;">&nbsp;domain (real or hypothetical) and documents its&nbsp;</span><b style="font-family: arial, sans-serif; font-size: 16px;">business</b><span style="font-family: arial, sans-serif;">&nbsp;or processes or systems, assessing the&nbsp;</span><b style="font-family: arial, sans-serif; font-size: 16px;">business</b><span style="font-family: arial, sans-serif;">&nbsp;model or its integration with technology.</span></span></p>\n', 1, 2, 2, '2018-06-29 00:00:00', '2018-07-06 00:00:00', 0, 1),
(2, 'Job Title 1', '<p>\n	Job Description<span style="color: red;">&nbsp; </span></p>\n', 2, 13, 4, '2018-07-06 00:00:00', '2018-07-06 00:00:00', 0, 0),
(3, 'PHP Developer', '<p>\n	Must have skills on Core / Cake PHP coding&nbsp;</p>\n', 2, 2, 3, '2018-06-30 00:00:00', '2018-07-07 00:00:00', 0, 0),
(4, 'Job Title', '<p>\n	rewewrewr</p>\n', 1, 2, 3, '2018-06-29 00:00:00', '2018-07-07 00:00:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `lss_media`
--

CREATE TABLE IF NOT EXISTS `lss_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `org_id` int(11) NOT NULL DEFAULT '0',
  `type` text NOT NULL,
  `filename` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 is deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `lss_media`
--

INSERT INTO `lss_media` (`id`, `org_id`, `type`, `filename`, `status`) VALUES
(4, 1, 'PNG', 'student.PNG', 1),
(5, 0, 'jpg', 'Koala.jpg', 1),
(6, 0, 'jpg', '7.jpg', 1),
(7, 0, 'jpg', 'Hall_Image__43.jpg', 1),
(8, 1, 'jpg', '1581928819photo-1553660585-3721aa906208.jpg', 1),
(9, 0, 'jpg', '1581931387blue_sky_grass_from_the_grass_highdefinition_picture_10_166036.jpg', 0),
(10, 0, 'jpg', '1581931393Hall_Image__23.jpg', 1),
(11, 0, 'jpg', '15819321185.jpg', 0),
(12, 0, 'pdf', '1581932182DB-Structure.pdf', 1),
(13, 0, 'jpg', '1582639220Chrysanthemum.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lss_news`
--

CREATE TABLE IF NOT EXISTS `lss_news` (
  `news_id` int(10) NOT NULL AUTO_INCREMENT,
  `org_id` int(11) NOT NULL,
  `news_title` varchar(255) NOT NULL,
  `news_description` text NOT NULL,
  `news_image_name` varchar(255) NOT NULL,
  `news_is_active` enum('0','1') NOT NULL,
  `news_is_deleted` enum('0','1') NOT NULL,
  `news_created_time` datetime NOT NULL,
  `news_updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`news_id`),
  KEY `org_id` (`org_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `lss_news`
--

INSERT INTO `lss_news` (`news_id`, `org_id`, `news_title`, `news_description`, `news_image_name`, `news_is_active`, `news_is_deleted`, `news_created_time`, `news_updated_time`) VALUES
(1, 1, 'Test News', '<p>\n	test Descrption</p>\n', '152361318018 mb.jpg,1523858404_lake.png', '1', '0', '2018-04-13 13:23:43', '2018-04-17 05:10:50'),
(2, 1, 'news title 11', '<p>\n	desc 11</p>\n', '1523430254Koala.jpg,1523430527Tulips.jpg,1523443342Chrysanthemum.jpg,1523443363Desert.jpg,1523857687_logo-image-bank-dontblink.jpg,1523858414_lake.png,1523859365_index1.jpg', '1', '1', '2018-04-16 06:55:13', '2018-04-16 06:57:54'),
(3, 1, 'Google Map API', '<p style="box-sizing: inherit; margin: 16px 0px; padding: 0px; color: rgb(33, 33, 33); font-family: Roboto, sans-serif; font-size: 16px; background-color: rgb(240, 240, 240);">\n	Build customized, agile experiences that bring the real world to your users with static and dynamic maps, Street View imagery, and 360&deg; views.</p>\n<div>\n	&nbsp;</div>\n', '', '1', '0', '2019-02-08 11:37:41', '2019-02-08 11:37:41');

-- --------------------------------------------------------

--
-- Table structure for table `lss_organisation`
--

CREATE TABLE IF NOT EXISTS `lss_organisation` (
  `org_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `register_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 ="Registered User", 2 = "Guest User"',
  `user_username` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `organisation_name` varchar(255) NOT NULL,
  `user_profile_image` varchar(255) NOT NULL,
  `key_code` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `user_last_login` datetime DEFAULT NULL,
  `user_login_count` int(11) NOT NULL DEFAULT '0',
  `user_user_type` int(10) NOT NULL,
  `google_auth_code` varchar(255) NOT NULL,
  `last_login_ip` varchar(255) NOT NULL,
  `is_term_accepted` tinyint(4) NOT NULL DEFAULT '1',
  `is_auth_requied` tinyint(4) NOT NULL DEFAULT '0',
  `user_is_active` enum('1','0') NOT NULL DEFAULT '1',
  `user_is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL,
  `updated_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `couch_org_id` varchar(45) DEFAULT NULL,
  `couch_org_rev` varchar(45) DEFAULT NULL,
  `couch_blog_id` varchar(45) DEFAULT NULL,
  `couch_blog_rev` varchar(45) DEFAULT NULL,
  `couch_survey_id` varchar(45) DEFAULT NULL,
  `couch_survey_rev` varchar(45) DEFAULT NULL,
  `verification_code` varchar(255) NOT NULL,
  PRIMARY KEY (`org_id`),
  KEY `user_id` (`org_id`),
  KEY `role_id` (`role_id`),
  KEY `user_id_2` (`org_id`),
  KEY `role_id_2` (`role_id`),
  KEY `user_id_3` (`org_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `lss_organisation`
--

INSERT INTO `lss_organisation` (`org_id`, `role_id`, `register_type`, `user_username`, `user_password`, `user_email`, `organisation_name`, `user_profile_image`, `key_code`, `auth_key`, `user_last_login`, `user_login_count`, `user_user_type`, `google_auth_code`, `last_login_ip`, `is_term_accepted`, `is_auth_requied`, `user_is_active`, `user_is_deleted`, `created_time`, `updated_time`, `couch_org_id`, `couch_org_rev`, `couch_blog_id`, `couch_blog_rev`, `couch_survey_id`, `couch_survey_rev`, `verification_code`) VALUES
(1, 0, 1, 'pawanjoshi', 'e10adc3949ba59abbe56e057f20f883e', 'shiwangi.khanduja@dotsquares.com', 'Dotsquares', '1523443653Desert.jpg', '', '', NULL, 0, 5, '', '', 1, 0, '1', '0', '2018-04-13 14:14:42', '2018-04-13 08:44:42', '02d7fee2482d491c85fef92a095b1887', '31-c491ac9ef7df6cdb05781c7c1147b2b3', 'c932a0307e724a9aa0a9e938c3c11562', '31-14776530bd58cf311645d0d9cd954cf9', '3d20358e3fee4319a23bd1eb4fab598f', '31-5a1bbabbd9c577861a5f0bb13f0d0bff', '5e46691199f61');

-- --------------------------------------------------------

--
-- Table structure for table `lss_organisation_info`
--

CREATE TABLE IF NOT EXISTS `lss_organisation_info` (
  `user_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `org_id` int(11) NOT NULL,
  `user_mobile_number` varchar(255) NOT NULL,
  `address_line1` varchar(255) NOT NULL,
  `address_line2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `postcode` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(11) NOT NULL,
  `billing_state` varchar(255) NOT NULL,
  `billing_country` varchar(11) NOT NULL,
  `billing_zipcode` varchar(255) NOT NULL,
  `user_profile_image` varchar(255) NOT NULL,
  `user_referral_code` varchar(255) NOT NULL,
  PRIMARY KEY (`user_info_id`),
  KEY `user_id` (`org_id`),
  KEY `org_id` (`org_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `lss_organisation_info`
--

INSERT INTO `lss_organisation_info` (`user_info_id`, `org_id`, `user_mobile_number`, `address_line1`, `address_line2`, `city`, `region`, `postcode`, `state`, `country`, `billing_state`, `billing_country`, `billing_zipcode`, `user_profile_image`, `user_referral_code`) VALUES
(1, 1, '2345678001', 'test', '', 'test', 'test', 'test', '', '', '', '3', '', '', 'DotsLJBY3H');

-- --------------------------------------------------------

--
-- Table structure for table `lss_permission`
--

CREATE TABLE IF NOT EXISTS `lss_permission` (
  `perm_id` int(11) NOT NULL AUTO_INCREMENT,
  `org_id` int(11) DEFAULT NULL,
  `controller_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`perm_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

--
-- Dumping data for table `lss_permission`
--

INSERT INTO `lss_permission` (`perm_id`, `org_id`, `controller_id`) VALUES
(46, 1, 47);

-- --------------------------------------------------------

--
-- Table structure for table `lss_privileges`
--

CREATE TABLE IF NOT EXISTS `lss_privileges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controller_id` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '0',
  `action_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `controller_id` (`controller_id`),
  KEY `role_id` (`role_id`),
  KEY `action_id` (`action_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=138 ;

--
-- Dumping data for table `lss_privileges`
--

INSERT INTO `lss_privileges` (`id`, `controller_id`, `role_id`, `action_id`) VALUES
(1, 41, 1, 206),
(2, 42, 1, 207),
(3, 42, 1, 208),
(4, 42, 1, 209),
(5, 42, 1, 210),
(6, 42, 1, 211),
(7, 47, 1, 233),
(8, 47, 1, 234),
(9, 47, 1, 235),
(10, 47, 1, 236),
(11, 47, 1, 237),
(12, 50, 1, 242),
(13, 50, 1, 243),
(14, 50, 1, 244),
(15, 50, 1, 246),
(16, 50, 1, 320),
(17, 50, 1, 354),
(18, 51, 1, 247),
(19, 51, 1, 248),
(20, 51, 1, 249),
(21, 51, 1, 321),
(22, 51, 1, 322),
(23, 51, 1, 355),
(24, 52, 1, 253),
(25, 52, 1, 254),
(26, 52, 1, 255),
(27, 52, 1, 323),
(28, 52, 1, 324),
(29, 52, 1, 356),
(30, 54, 1, 256),
(31, 54, 1, 257),
(32, 54, 1, 325),
(33, 54, 1, 326),
(34, 54, 1, 327),
(35, 65, 1, 282),
(36, 67, 1, 288),
(37, 67, 1, 289),
(38, 67, 1, 290),
(39, 67, 1, 342),
(41, 69, 1, 293),
(42, 69, 1, 294),
(43, 69, 1, 297),
(44, 69, 1, 344),
(45, 69, 1, 345),
(46, 75, 1, 308),
(47, 75, 1, 309),
(48, 75, 1, 310),
(49, 75, 1, 311),
(50, 75, 1, 312),
(51, 75, 1, 313),
(52, 76, 1, 363),
(53, 76, 1, 364),
(54, 77, 1, 359),
(55, 77, 1, 360),
(56, 78, 1, 361),
(57, 78, 1, 362),
(58, 79, 1, 357),
(59, 79, 1, 358),
(114, 79, 1, 365),
(115, 79, 1, 366),
(116, 79, 1, 367),
(117, 76, 1, 368),
(118, 76, 1, 369),
(119, 76, 1, 370),
(120, 77, 1, 371),
(121, 77, 1, 372),
(122, 77, 1, 373),
(124, 81, 1, 374),
(125, 81, 1, 375),
(126, 81, 1, 376),
(127, 81, 1, 377),
(128, 82, 1, 378),
(129, 82, 1, 379),
(130, 82, 1, 380),
(131, 82, 1, 381),
(132, 83, 1, 384),
(133, 80, 1, 385);

-- --------------------------------------------------------

--
-- Table structure for table `lss_product`
--

CREATE TABLE IF NOT EXISTS `lss_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `org_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT '0',
  `product_title` text NOT NULL,
  `product_regular_price` float(10,2) NOT NULL,
  `product_sale_price` float(10,2) NOT NULL,
  `product_SKU` varchar(255) NOT NULL,
  `product_stock_quantity` int(11) NOT NULL,
  `product_description` text NOT NULL,
  `product_image_name` varchar(255) NOT NULL,
  `product_is_active` enum('0','1') NOT NULL DEFAULT '1',
  `product_is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `product_created_time` datetime NOT NULL,
  `product_updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`),
  KEY `org_id` (`org_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `lss_product`
--

INSERT INTO `lss_product` (`product_id`, `org_id`, `user_id`, `category_id`, `product_title`, `product_regular_price`, `product_sale_price`, `product_SKU`, `product_stock_quantity`, `product_description`, `product_image_name`, `product_is_active`, `product_is_deleted`, `product_created_time`, `product_updated_time`) VALUES
(2, 1, 0, 15, 'test', 23.00, 34.00, 'fgdf', 10, '<p>\r\n	test</p>\r\n', '1523542847Status Light_1.png', '1', '0', '2018-04-13 14:15:12', '2018-04-16 05:45:25'),
(3, 1, 0, 15, '<script>alert ''abc'' </script>', 23.00, 23.00, '32', 32, '<p>\n	3</p>\n', '', '1', '1', '2018-04-13 09:50:29', '2018-04-13 09:51:26'),
(4, 1, 0, 39, 'product 1', 3.00, 3.00, '33', 33, '<p>\n	333</p>\n', '152361318018 mb.jpg', '1', '1', '2018-04-13 12:01:11', '2018-04-13 12:32:47'),
(5, 1, 0, 39, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it?  It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it?  It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it?  It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it?  It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it?  It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it?  It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it?  It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it?  It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it?  It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it?  It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).sdfsdf', 100000000.00, 100000000.00, '65456466666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666', 2147483647, '<p>\n	<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<div>\n	<h2>\n		Why do we use it?</h2>\n	<p>\n		It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n	<p>\n		<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n	<div>\n		<h2>\n			Why do we use it?</h2>\n		<p>\n			It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n		<p>\n			<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n		<div>\n			<h2>\n				Why do we use it?</h2>\n			<p>\n				It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n			<p>\n				<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n			<div>\n				<h2>\n					Why do we use it?</h2>\n				<p>\n					It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n				<p>\n					<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n				<div>\n					<h2>\n						Why do we use it?</h2>\n					<p>\n						It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n					<p>\n						<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n					<div>\n						<h2>\n							Why do we use it?</h2>\n						<p>\n							It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n						<p>\n							<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n						<div>\n							<h2>\n								Why do we use it?</h2>\n							<p>\n								It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n							<p>\n								<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n							<div>\n								<h2>\n									Why do we use it?</h2>\n								<p>\n									It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n								<p>\n									<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n								<div>\n									<h2>\n										Why do we use it?</h2>\n									<p>\n										It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n									<p>\n										<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n									<div>\n										<h2>\n											Why do we use it?</h2>\n										<p>\n											It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n										<p>\n											<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n										<div>\n											<h2>\n												Why do we use it?</h2>\n											<p>\n												It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n											<p>\n												<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n											<div>\n												<h2>\n													Why do we use it?</h2>\n												<p>\n													It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n												<p>\n													<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n												<div>\n													<h2>\n														Why do we use it?</h2>\n													<p>\n														It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n													<p>\n														<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n													<div>\n														<h2>\n															Why do we use it?</h2>\n														<p>\n															It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n														<p>\n															<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n														<div>\n															<h2>\n																Why do we use it?</h2>\n															<p>\n																It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n															<p>\n																<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n															<div>\n																<h2>\n																	Why do we use it?</h2>\n																<p>\n																	It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n															</div>\n														</div>\n													</div>\n												</div>\n											</div>\n										</div>\n									</div>\n								</div>\n							</div>\n						</div>\n					</div>\n				</div>\n			</div>\n		</div>\n	</div>\n</div>\n<p>\n	&nbsp;</p>\n', '', '1', '1', '2018-04-13 12:32:24', '2018-04-16 05:44:51'),
(6, 1, 0, 39, 'test product 1', 100000000.00, 25000.00, '234', 234, '<p>\n	234234234</p>\n', '', '1', '0', '2018-04-16 05:46:04', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `lss_property`
--

CREATE TABLE IF NOT EXISTS `lss_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_name` varchar(255) NOT NULL,
  `property_type` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `contact_no` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `Area(sqft)` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `lss_property`
--


-- --------------------------------------------------------

--
-- Table structure for table `lss_property_features`
--

CREATE TABLE IF NOT EXISTS `lss_property_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL,
  `value` tinyint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `lss_property_features`
--


-- --------------------------------------------------------

--
-- Table structure for table `lss_questions`
--

CREATE TABLE IF NOT EXISTS `lss_questions` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `org_id` int(11) NOT NULL,
  `question_title` text NOT NULL,
  `question_category` int(11) NOT NULL,
  `question_description` text NOT NULL,
  `question_created_date` datetime NOT NULL,
  `question_updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `question_is_deleted` int(2) NOT NULL DEFAULT '0',
  `question_is_active` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`question_id`),
  KEY `question_id` (`question_id`),
  KEY `org_id` (`org_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `lss_questions`
--

INSERT INTO `lss_questions` (`question_id`, `org_id`, `question_title`, `question_category`, `question_description`, `question_created_date`, `question_updated_date`, `question_is_deleted`, `question_is_active`) VALUES
(1, 1, 'Test Question', 10, '<p>\n	Test Question</p>', '2018-04-13 10:42:58', '2018-04-13 10:42:58', 0, 1),
(2, 1, 'Question 1', 13, '<p>\n	What is Question ?</p>', '2018-04-13 12:55:21', '2018-04-16 06:24:14', 1, 1),
(3, 1, 'Question 2', 13, '<p>\n	What is a&nbsp;Question 2 ?</p>', '2018-04-13 12:56:24', '2018-04-16 06:24:11', 1, 1),
(4, 1, 'survey 1', 43, '<p>\n	survey desc</p>', '2018-04-16 06:17:38', '2018-04-16 06:17:57', 1, 1),
(5, 1, 'survey 11', 13, '<p>\n	asd1</p>', '2018-04-16 06:18:11', '2018-04-16 06:23:27', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `lss_question_options`
--

CREATE TABLE IF NOT EXISTS `lss_question_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `weightage` int(2) DEFAULT NULL,
  `is_correct` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `question_id` (`question_id`),
  KEY `question_id_2` (`question_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `lss_question_options`
--

INSERT INTO `lss_question_options` (`id`, `question_id`, `answer`, `weightage`, `is_correct`) VALUES
(1, 1, 'ans1', 2, 0),
(2, 1, 'ans2', 3, 1),
(3, 2, 'Question 1 is a Q', 5, 1),
(4, 2, 'Question 2 is a Q', 5, 0),
(5, 2, '', 0, 0),
(6, 3, 'Question 1 is a Q', 5, 0),
(7, 3, 'Question 2 is a Q', 5, 1),
(8, 4, '1', 2, 1),
(9, 4, '12', 12, 0),
(10, 4, '', 0, 0),
(13, 5, '11', 1, 1),
(14, 5, '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `lss_recipe`
--

CREATE TABLE IF NOT EXISTS `lss_recipe` (
  `recipe_id` int(11) NOT NULL AUTO_INCREMENT,
  `org_id` int(11) NOT NULL,
  `recipe_title` varchar(255) NOT NULL,
  `recipe_ingredients` text NOT NULL,
  `recipe_description` text NOT NULL,
  `recipe_image` varchar(255) NOT NULL,
  `recipe_prep_time` varchar(255) NOT NULL,
  `recipe_no_serving` int(11) NOT NULL,
  `recipe_is_active` tinyint(4) NOT NULL DEFAULT '1',
  `recipe_is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `recipe_created_date` datetime NOT NULL,
  `recipe_updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`recipe_id`),
  KEY `org_id` (`org_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `lss_recipe`
--

INSERT INTO `lss_recipe` (`recipe_id`, `org_id`, `recipe_title`, `recipe_ingredients`, `recipe_description`, `recipe_image`, `recipe_prep_time`, `recipe_no_serving`, `recipe_is_active`, `recipe_is_deleted`, `recipe_created_date`, `recipe_updated_date`) VALUES
(1, 1, 'test', '<p>\n	test</p>\n', '<p>\n	test</p>\n', '', 'test', 5, 1, 0, '2018-04-13 08:55:49', '2018-04-13 08:55:49'),
(2, 1, '2', '[{"title":"ee","quentity":"ee"},{"title":"ee","quentity":"ee"},{"title":"22","quentity":"22"}]', '<p>\n	eee</p>\n', '1523857687_logo-image-bank-dontblink.jpg', 'e', 12, 1, 0, '2018-04-16 06:36:02', '2018-04-16 06:36:02');

-- --------------------------------------------------------

--
-- Table structure for table `lss_roles`
--

CREATE TABLE IF NOT EXISTS `lss_roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_description` varchar(255) NOT NULL,
  `role_title` varchar(255) NOT NULL,
  `role_is_active` tinyint(4) NOT NULL DEFAULT '1',
  `role_is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `lss_roles`
--

INSERT INTO `lss_roles` (`role_id`, `role_description`, `role_title`, `role_is_active`, `role_is_deleted`) VALUES
(1, '', 'Super Administrator', 1, 0),
(2, '', 'Organisation', 1, 0),
(3, '', 'Individual', 1, 0),
(4, '', 'Staff', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `lss_site_config`
--

CREATE TABLE IF NOT EXISTS `lss_site_config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `config_key` varchar(255) NOT NULL,
  `config_value` varchar(255) NOT NULL,
  `config_label` varchar(255) NOT NULL,
  `config_status` tinyint(4) NOT NULL DEFAULT '1',
  `config_is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `lss_site_config`
--

INSERT INTO `lss_site_config` (`config_id`, `config_key`, `config_value`, `config_label`, `config_status`, `config_is_deleted`) VALUES
(6, 'config_website_down_message', '<p>Our website is currently undergoing some improvement work, weÂ´ll be back up and running again soon and even better than before!</p> <p>Thank you for your patience and sorry for any inconvenience this may have caused. Pop back in 30-minutes and we shou', 'Website Down Message', 1, 0),
(7, 'administrative_fee', '10', 'Administrative Fee', 1, 1),
(8, 'questions', '10', 'Total Number of Questions', 1, 1),
(9, 'videos', '15', 'Total Number of videos', 1, 1),
(10, 'role_title_array', 'Administrator ', 'role_title_array', 1, 1),
(11, 'questions_level', '5', 'questions_level', 1, 1),
(12, 'number_of_users', '20', 'Total Number Of users in Organisation', 1, 1),
(13, 'vat', '20', 'VAT TAX (Percentage)', 1, 1),
(14, 'referral_amount', '100', 'referral_amount', 1, 1),
(15, 'max_days_to_expire_course', '350', 'Max Days To Expire Course', 1, 1),
(16, 'partner_commission', '100', 'Partner Comission', 1, 1),
(17, 'test', 'test', 'tset', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `lss_survey`
--

CREATE TABLE IF NOT EXISTS `lss_survey` (
  `survey_id` int(11) NOT NULL AUTO_INCREMENT,
  `org_id` int(11) NOT NULL,
  `survey_title` varchar(255) CHARACTER SET latin1 NOT NULL,
  `survey_category` int(11) NOT NULL,
  `survey_description` text CHARACTER SET latin1 NOT NULL,
  `survey_is_active` tinyint(4) NOT NULL DEFAULT '1',
  `survey_created_date` datetime NOT NULL,
  `survey_updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `survey_is_deleted` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`survey_id`),
  KEY `org_id` (`org_id`),
  KEY `org_id_2` (`org_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `lss_survey`
--

INSERT INTO `lss_survey` (`survey_id`, `org_id`, `survey_title`, `survey_category`, `survey_description`, `survey_is_active`, `survey_created_date`, `survey_updated_date`, `survey_is_deleted`) VALUES
(2, 1, 'Test Survey', 10, '<p>\n	<span style="color: rgb(68, 68, 68); font-family: &quot;Source Sans Pro&quot;, sans-serif; font-size: 18px; text-transform: capitalize;">Survey</span></p>', 1, '2018-04-13 10:43:20', '2018-04-13 10:43:20', 0),
(3, 1, 'Survey 2', 13, '<p>\n	This is for testing.</p>', 1, '2018-04-13 12:57:10', '2018-04-13 12:57:10', 0),
(4, 1, 'survey 1', 13, '<p>\n	asd</p>', 1, '2018-04-16 06:23:00', '2018-08-20 06:37:31', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lss_survey_question`
--

CREATE TABLE IF NOT EXISTS `lss_survey_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `servey_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `servey_id` (`servey_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `lss_survey_question`
--

INSERT INTO `lss_survey_question` (`id`, `servey_id`, `question_id`) VALUES
(2, 2, 1),
(3, 3, 2),
(4, 3, 3),
(7, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `lss_survey_result`
--

CREATE TABLE IF NOT EXISTS `lss_survey_result` (
  `survey_result_id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `result` text NOT NULL,
  PRIMARY KEY (`survey_result_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `lss_survey_result`
--

INSERT INTO `lss_survey_result` (`survey_result_id`, `survey_id`, `user_id`, `result`) VALUES
(1, 1, 3, '[{"question_id":"2","option_id":"4","weightage":"2"},{"question_id":"3","option_id":"6","weightage":"4"}]'),
(2, 1, 3, '[{"question_id":"2","option_id":"4","weightage":"2"},{"question_id":"3","option_id":"6","weightage":"4"}]'),
(3, 1, 3, '[{"question_id":"2","option_id":"4","weightage":"2"},{"question_id":"3","option_id":"6","weightage":"4"}]');

-- --------------------------------------------------------

--
-- Table structure for table `lss_users`
--

CREATE TABLE IF NOT EXISTS `lss_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `org_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `register_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 ="Registered User", 2 = "Guest User"',
  `user_username` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `user_profile_image` varchar(255) NOT NULL,
  `key_code` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `user_last_login` datetime DEFAULT NULL,
  `user_login_count` int(11) NOT NULL DEFAULT '0',
  `user_user_type` int(10) NOT NULL,
  `google_auth_code` varchar(255) NOT NULL,
  `last_login_ip` varchar(255) NOT NULL,
  `is_term_accepted` tinyint(4) NOT NULL DEFAULT '1',
  `is_auth_requied` tinyint(4) NOT NULL DEFAULT '0',
  `user_is_active` enum('1','0') NOT NULL DEFAULT '1',
  `user_is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL,
  `updated_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  KEY `user_id` (`user_id`),
  KEY `role_id` (`role_id`),
  KEY `user_id_2` (`user_id`),
  KEY `role_id_2` (`role_id`),
  KEY `user_id_3` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `lss_users`
--

INSERT INTO `lss_users` (`user_id`, `org_id`, `role_id`, `register_type`, `user_username`, `user_password`, `user_email`, `first_name`, `last_name`, `user_profile_image`, `key_code`, `auth_key`, `user_last_login`, `user_login_count`, `user_user_type`, `google_auth_code`, `last_login_ip`, `is_term_accepted`, `is_auth_requied`, `user_is_active`, `user_is_deleted`, `created_time`, `updated_time`) VALUES
(1, 0, 1, 1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'support@yopmail.com', 'Admin User', '', '', '', '', '0000-00-00 00:00:00', 544, 0, '', '', 0, 0, '1', '0', '0000-00-00 00:00:00', '2017-04-25 03:10:36'),
(2, 1, 4, 1, 'TestUser', '47c13b5809f31711e5b6ef1d96c6bf2efc5530272592841223585c38e52f46da8', 'testuser@gmail.om', 'Test', 'User', 'user.png', '', '', NULL, 0, 0, '', '', 1, 0, '1', '0', '0000-00-00 00:00:00', '2018-04-17 12:50:10');

-- --------------------------------------------------------

--
-- Table structure for table `lss_user_info`
--

CREATE TABLE IF NOT EXISTS `lss_user_info` (
  `user_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `vat_number` varchar(255) DEFAULT NULL,
  `direct_allow_final_quiz` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0="Not Allow", 1="Allow"',
  `user_mobile_number` varchar(255) NOT NULL,
  `address_line1` varchar(255) NOT NULL,
  `address_line2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `postcode` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(11) NOT NULL,
  `billing_state` varchar(255) NOT NULL,
  `billing_country` varchar(11) NOT NULL,
  `billing_zipcode` varchar(255) NOT NULL,
  `user_profile_image` varchar(255) NOT NULL,
  `user_referral_code` varchar(255) NOT NULL,
  PRIMARY KEY (`user_info_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `lss_user_info`
--

INSERT INTO `lss_user_info` (`user_info_id`, `user_id`, `vat_number`, `direct_allow_final_quiz`, `user_mobile_number`, `address_line1`, `address_line2`, `city`, `region`, `postcode`, `state`, `country`, `billing_state`, `billing_country`, `billing_zipcode`, `user_profile_image`, `user_referral_code`) VALUES
(1, 2, '', 0, '1234567', 'add1', 'add2', 'jpr', 'raj', '453535', '', '', '', '99', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_activity`
--

CREATE TABLE IF NOT EXISTS `tbl_user_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_event_log_id` int(11) DEFAULT NULL,
  `user_last_login` datetime DEFAULT NULL,
  `user_login_count` int(11) DEFAULT NULL,
  `login_ip` varchar(150) COLLATE latin1_bin DEFAULT NULL,
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_bin COMMENT='Add account and subadmin activity' AUTO_INCREMENT=12644 ;

--
-- Dumping data for table `tbl_user_activity`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_assignment`
--

CREATE TABLE IF NOT EXISTS `tbl_user_assignment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `assign_action` varchar(150) COLLATE latin1_bin DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_user_assignment`
--


--
-- Constraints for dumped tables
--

--
-- Constraints for table `lss_actions`
--
ALTER TABLE `lss_actions`
  ADD CONSTRAINT `lss_actions_ibfk_1` FOREIGN KEY (`controller_id`) REFERENCES `lss_controllers` (`controller_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `lss_blog`
--
ALTER TABLE `lss_blog`
  ADD CONSTRAINT `lss_blog_lss_org` FOREIGN KEY (`org_id`) REFERENCES `lss_organisation` (`org_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lss_business_directories`
--
ALTER TABLE `lss_business_directories`
  ADD CONSTRAINT `lss_directory_lss_org` FOREIGN KEY (`org_id`) REFERENCES `lss_organisation` (`org_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lss_career`
--
ALTER TABLE `lss_career`
  ADD CONSTRAINT `lss_job_lss_org` FOREIGN KEY (`org_id`) REFERENCES `lss_organisation` (`org_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lss_event`
--
ALTER TABLE `lss_event`
  ADD CONSTRAINT `lss_event_lss_org` FOREIGN KEY (`org_id`) REFERENCES `lss_organisation` (`org_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lss_faq`
--
ALTER TABLE `lss_faq`
  ADD CONSTRAINT `lss_faq_lss_org` FOREIGN KEY (`org_id`) REFERENCES `lss_organisation` (`org_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lss_news`
--
ALTER TABLE `lss_news`
  ADD CONSTRAINT `lss_new_lss_org` FOREIGN KEY (`org_id`) REFERENCES `lss_organisation` (`org_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lss_organisation_info`
--
ALTER TABLE `lss_organisation_info`
  ADD CONSTRAINT `lss_organisation_info_id` FOREIGN KEY (`org_id`) REFERENCES `lss_organisation` (`org_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lss_privileges`
--
ALTER TABLE `lss_privileges`
  ADD CONSTRAINT `lss_privileges_ibfk_1` FOREIGN KEY (`controller_id`) REFERENCES `lss_controllers` (`controller_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `lss_privileges_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `lss_roles` (`role_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `lss_privileges_ibfk_3` FOREIGN KEY (`action_id`) REFERENCES `lss_actions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `lss_product`
--
ALTER TABLE `lss_product`
  ADD CONSTRAINT `lss_product_lss_org` FOREIGN KEY (`org_id`) REFERENCES `lss_organisation` (`org_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lss_questions`
--
ALTER TABLE `lss_questions`
  ADD CONSTRAINT `lss_que_lss_org` FOREIGN KEY (`org_id`) REFERENCES `lss_organisation` (`org_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lss_question_options`
--
ALTER TABLE `lss_question_options`
  ADD CONSTRAINT `lss_question_table_que_anser` FOREIGN KEY (`question_id`) REFERENCES `lss_questions` (`question_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lss_recipe`
--
ALTER TABLE `lss_recipe`
  ADD CONSTRAINT `lss_recipe_lss_org` FOREIGN KEY (`org_id`) REFERENCES `lss_organisation` (`org_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lss_survey`
--
ALTER TABLE `lss_survey`
  ADD CONSTRAINT `lss_survey_info_id` FOREIGN KEY (`org_id`) REFERENCES `lss_organisation` (`org_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lss_survey_question`
--
ALTER TABLE `lss_survey_question`
  ADD CONSTRAINT `lss_surveyquestion_lss_serve_id` FOREIGN KEY (`servey_id`) REFERENCES `lss_survey` (`survey_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lss_user_info`
--
ALTER TABLE `lss_user_info`
  ADD CONSTRAINT `lss_user_info_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `lss_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;
