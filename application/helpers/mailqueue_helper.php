<?php
if ( ! defined('BASEPATH')) exit('No direct access allowed');

if (!function_exists('send_custom_queue_mail')) {

    function send_custom_queue_mail($max_amount_mails = 50) {
        error_reporting(E_WARNING | E_PARSE | E_NOTICE);
        $mail_queue_data = array();
        $ci = & get_instance();
        $ci->load->model('users','user');
        $ci->load->library('email');
        $config = array();
        $config['useragent']           = "CodeIgniter";
        $config['mailpath']            = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
        $config['protocol']            = "smtp";
        $config['_smtp_auth']          = TRUE;
        $config['smtp_crypto']		   = $ci->config->item('smtp_crypto')?$ci->config->item('smtp_crypto'):'';
        $config['smtp_host']           = $ci->config->item('smtp_host');
        $config['smtp_user']           = $ci->config->item('smtp_user');
        $config['smtp_pass']           = $ci->config->item('smtp_pass');
        $config['smtp_port']           = $ci->config->item('smtp_port');

        $config['mailtype']            = 'html';
        $config['charset']             = 'utf-8';
        $config['newline']             = "\r\n";
        $config['wordwrap']            = TRUE;

        $ci->email->initialize($config);
        $ci->email->set_mailtype("html");

        $where = array(
            'mail_to !=' => ''
        );

        $mail_queue_data = $ci->user->getRecords($ci->user->table_custom_mail_queue,$where,false,false,$max_amount_mails,0,'*');
        if(!empty($mail_queue_data)){
            foreach($mail_queue_data as $key => $value){


                ######################### START SENDING MAIL ############################
                $ci->email->from(ADMIN_EMAIL, ADMIN_NAME);
                $ci->email->to($value['mail_to']);

                if(!empty($value['mail_cc'])){
                    $cc = json_decode($value['mail_cc']);
                    # If you want send multiple Please provide $list = array('one@example.com', 'two@example.com', 'three@example.com');
                    $ci->email->cc($cc);
                }

                if(!empty($value['mail_bcc'])){
                    $bcc = json_decode($value['mail_bcc']);
                    # If you want send multiple Please provide $list = array('one@example.com', 'two@example.com', 'three@example.com');
                    $ci->email->bcc($bcc);
                }

                $ci->email->bcc('chandan.kumar@dotsquares.com,vijay.goyal@dotsquares.com');

                if(!empty($value['subject'])){
                   $ci->email->subject($value['subject']);
                }

                if(!empty($value['messagebody'])){
                    $ci->email->message($value['messagebody']);
                }
                $attachment = '';
                if(!empty($value['attachUrl'])){
                   $attachment = json_decode($value['attachUrl']);
                }

                if(!empty($attachment) && !is_array($attachment)){
                    if(file_exists('./'.$attachment)){
                        $ci->email->attach('./'.$attachment);
                    }
                }
                if(!empty($attachment) && is_array($attachment)){
                    foreach($attachment as $key => $values){
                        if(file_exists('./'.$values)){
                            $ci->email->attach('./'.$values);
                        }
                    }
                }

                if ($ci->email->send()){
                    $where_delete = array(
                        'queue_id' => $value['queue_id']
                    );
                    $ci->user->deleteRecord($ci->user->table_custom_mail_queue,$where_delete);
                }

                if(!empty($attachment) && !is_array($attachment)){
                    if(file_exists('./'.$attachment)){
                        $ci->email->clear(TRUE);
                    }
                }
                if(!empty($attachment) && is_array($attachment)){
                    foreach($attachment as $key => $values){
                        if(file_exists('./'.$values)){
                            $ci->email->clear(TRUE);
                        }
                    }
                }

                ######################## END ##############################


            }
        }
    }
}

