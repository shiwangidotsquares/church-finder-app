<?php  if ( ! defined('BASEPATH')) exit('No direct access allowed');

/**
 * CodeIgniter Custom Helpers
 *
 */

 if( ! function_exists('get_current_controller_and_function')){
    function get_current_controller_and_function(){
        $CI =& get_instance();
        $array = array();
        $array['current_controller'] = $CI->router->class;
        $array['current_function'] = $CI->router->method;
        return $array;
    }
}

if(!function_exists('array_equal')){
	function array_equal($a, $b) {
		return (
			 is_array($a) && is_array($b) && count($a) == count($b) && array_diff($a, $b) === array_diff($b, $a)
		);
	}
}

if( !function_exists('random_string') )
{
    function random_string($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        return $key;
    }
}
if(!function_exists('get_email_body')){
    function get_email_body($template_id,$data_array){
        $CI =& get_instance();
        $emaildata=get_email_message_data($template_id);

        $subject = $emaildata['subject'];
        $data_array['base_url']=base_url();

        $data_array['site_title']=$CI->config->item('site_title');
        $messagebody = $CI->parser->parse_string($emaildata['body'], $data_array, true);


        $mailbody=array('EMAIL_CONTENT' => $messagebody);
        $tpl_name="dynamic_template";

        $messagebody = $CI->parser->parse('emailer/'.$tpl_name, $mailbody, true);


        $data=array("subject"=>$subject,"body"=>$messagebody);
        return $data;

    }
}
if(!function_exists('get_email_body_user')){
    function get_email_body_user($template_id,$data_array){
        $CI =& get_instance();
        $emaildata=get_email_message_data($template_id);

        $subject = $emaildata['subject'];
        $data_array['base_url']=base_url();

        $data_array['site_title']=$CI->config->item('site_title');
        $messagebody = $CI->parser->parse_string($emaildata['body'], $data_array, true);


        $mailbody=array('EMAIL_CONTENT' => $messagebody);
        $tpl_name="dynamic_template_user";

        $messagebody = $CI->parser->parse('emailer/'.$tpl_name, $mailbody, true);


        $data=array("subject"=>$subject,"body"=>$messagebody);
        return $data;

    }
}
 if(!function_exists('get_email_message_data')){
    function get_email_message_data($id){
        $CI =& get_instance();
        $CI->load->model('emails');

        $res=array();
        $data = $CI->emails->get_emailer($id);
        return $data;
    }
}
if ( ! function_exists('resize_image')) {
    function resize_image($file_path = '',$thumb_path = '', $width = 100, $height = 100) {
            $CI =& get_instance();
            $CI->load->library('image_lib');
            $img_cfg['image_library'] = 'gd2';
            $img_cfg['source_image'] = $file_path;
            $img_cfg['maintain_ratio'] = FALSE;
            $img_cfg['create_thumb'] = FALSE;
            $img_cfg['new_image'] = $thumb_path;
            $img_cfg['thumb_marker'] = '';
            $img_cfg['width'] = $width;
            $img_cfg['quality'] = 100;
            $img_cfg['height'] = $height;

            $CI->image_lib->initialize($img_cfg);
            $CI->image_lib->resize();
            $CI->image_lib->clear();
    }
}
if ( ! function_exists('create_unique_slug')){
	function create_unique_slug($string, $table,$field='slug',$key=NULL,$value=NULL){
		$t =& get_instance();
		$slug = url_title($string);
		$slug = strtolower($slug);
		$i = 0;
		$params = array ();
		$params[$field] = $slug;

		if($key)$params["$key !="] = $value;

		while ($t->db->where($params)->get($table)->num_rows()) {
			if (!preg_match ('/-{1}[0-9]+$/', $slug )) {
				$slug .= '-' . ++$i;
			} else {
				$slug = preg_replace ('/[0-9]+$/', ++$i, $slug );
			}
			$params [$field] = $slug;
			}
		return $slug;
	}
}


if ( ! function_exists('get_parent_category')){
    function get_parent_category($string){
            $ci=& get_instance();
        $ci->load->database();

        $sql = "SELECT category_name
        FROM  lss_categories where category_id=".$string;
        $query = $ci->db->query($sql);
        $row = $query->result();
        return $row;
      }
}


if ( ! function_exists('generateHash'))
{
	function generateHash($plainText, $salt = null)
        {
            if ($salt === null)
            {
                $salt = substr(md5(uniqid(rand(), true)), 0, 25);
            }
            else
            {
                $salt = substr($salt, 0, 25);
            }

            return $salt . sha1($salt . $plainText);
        }
}

// ------------------------------------------------------------------------

if ( ! function_exists('setCookies'))
{
	function setCookies($uname = '', $pass = '')
        {
            if(isset($uname) && isset($pass)) {
                //set the cookies for 1 day, ie, 1*24*60*60 secs
                //change it to something like 30*24*60*60 to remember user for 30 days
                $this->input->set_cookie('login_username', $uname, time() + 1*24*60*60);
                $this->input->set_cookie('login_password', $pass, time() + 1*24*60*60);
            }

        }
}
//pr()
// ------------------------------------------------------------------------


if ( ! function_exists('pr'))
{
	function pr($array)
        {
           echo "<pre>";
		   print_r($array);
	       echo "</pre>";
        }
}
if ( ! function_exists('prd'))
{
	function prd($array)
        {
           echo "<pre>";
		   print_r($array);
	       echo "</pre>";
		   die;
        }
}
//---------------------------------------------------------------------------

if (!function_exists('commaArray')) {

 function commaArray($string) {
  if (!is_string($string)) {
   return FALSE;
  }

  $a = explode(',', $string);
  if (count($a) < 1) {
   return false;
  } else {
   $i = 0;
   $c = array();
   foreach ($a as $b) {
    if (!empty($b)) {
     $c[$i] = $b;
     $i++;
    }
   }
  }

  return $c;

 }

}



if ( ! function_exists('contextual_time')){
	function contextual_time($small_ts, $large_ts=false) {
	    if(!$large_ts) $large_ts = strtotime(date('Y-m-d H:i:s',time()));
	        $n = $large_ts - $small_ts;
	        //echo '<br>'.$small_ts.'<br>'.$large_ts.'<br>'.$n.'<br>';
	    if($n <= 1) return 'less than 1 second ago';

	    if($n < (60)) return $n . ' seconds ago';

	    if($n < (60*60)) { $minutes = round($n/60); return 'about ' . $minutes . ' minute' . ($minutes > 1 ? 's' : '') . ' ago'; }

	    if($n < (60*60*16)) { $hours = round($n/(60*60)); return 'about ' . $hours . ' hour' . ($hours > 1 ? 's' : '') . ' ago'; }

	    if($n < (time() - strtotime('yesterday'))) return 'yesterday';

	    //if($n < (60*60*24*2)) { $hours = round($n/(60*60)-24); return '1 day, ' . $hours . ' hour' . ($hours > 1 ? 's' : '') . ' ago'; }

	    if($n < (60*60*24)) { $hours = round($n/(60*60)); return 'about ' . $hours . ' hour' . ($hours > 1 ? 's' : '') . ' ago'; }

	    if($n < (60*60*24*6.5)) return 'about ' . round($n/(60*60*24)) . ' days ago';

	    if($n < (time() - strtotime('last week'))) return 'last week';

	    if(round($n/(60*60*24*7))  == 1) return 'about a week ago';

	    if($n < (60*60*24*7*3.5)) return 'about ' . round($n/(60*60*24*7)) . ' weeks ago';

	    if($n < (time() - strtotime('last month'))) return 'last month';

	    if(round($n/(60*60*24*7*4))  == 1) return 'about a month ago';

	    if($n < (60*60*24*7*4*11.5)) return 'about ' . round($n/(60*60*24*7*4)) . ' months ago';

	    if($n < (time() - strtotime('last year'))) return 'last year';

	    if(round($n/(60*60*24*7*52)) == 1) return 'about a year ago';

	    if($n >= (60*60*24*7*4*12)) return 'about ' . round($n/(60*60*24*7*52)) . ' years ago';

	    return false;
	}
}

if ( ! function_exists('show_datetime'))
{
	function show_datetime($data,$type='dd-mm-yyyy'){
			if(!empty($data)) {
				switch($type){
					case "dd/mm/yyyy" : return date('d/m/Y',strtotime($data));
					case "dd-mm-yyyy" : return date('d/m/Y',strtotime($data));
					case "yyyy-mm-dd" : return date('d/m/Y',strtotime($data));
					case "dd/mm/yyyy:H:i:s" : return date('d/m/Y H:i:s',strtotime($data));
					case "dd/mm/yyyy:H:i a" : return date('d/m/Y H:i a',strtotime($data));
				}
			}else{
				return '';
			}
	}
}

//---------------------------------------------------------------------------

if ( ! function_exists('objectToArray'))
{
 function objectToArray($d)
	{
		if (is_object($d))
		{
			$d = get_object_vars($d);
		}
		if (is_array($d))
		{
			return array_map(__FUNCTION__, $d);
		}
		else
		{
			return $d;
		}
	}
 }

 //----------------------------------------------------------------------------------------------------

 if ( ! function_exists('secondsToWords'))
    {
     function secondsToWords($seconds)
        {
            /*** return value ***/
            $ret = "";

            /*** get the hours ***/
            $hours = intval(intval($seconds) / 3600);
            if($hours > 0)
            {
                $ret .= "$hours:";
            }
            /*** get the minutes ***/
            $minutes = bcmod((intval($seconds) / 60),60);
            if($hours > 0 || $minutes > 0)
            {
                $ret .= "$minutes:";
            }

            /*** get the seconds ***/
            $seconds = bcmod(intval($seconds),60);
                if($seconds < 10){
                    $ret .= "0"."$seconds";
                }else{
                    $ret .= "$seconds";
                }


            return $ret;
        }
     }



//---------------------------------------------------------------------------------

if ( ! function_exists('clean')) {
    function clean($str) {
    	$str = @trim($str);
    	if(get_magic_quotes_gpc()) {
    		$str = stripslashes($str);
    	}
    	return $str;
    }
}


################################### Date months function ######################################

if ( ! function_exists('get_next_month')){
    function get_next_month(){
        $date = date('M');
        return $next_month = date('M',strtotime($date.' next month'));
    }
}

if ( ! function_exists('get_last_month')){
    function get_last_month(){
        $date = date('M');
        return $previous_month = date('M',strtotime($date.' previous month'));
    }
}

if(!function_exists('get_all_months')){
    function get_all_months(){
        $months_array = array();
        for ($m=1; $m<=12; $m++) {
           $months_array[] = $month = date('F', mktime(0,0,0,$m, 1, date('Y')));
        }
        return $months_array;
    }
}

if(!function_exists('get_next_fifteen_years')){
    function get_next_fifteen_years($n = 0){
        $years_array = array();
        $year = date('Y');
        $total = 15;
        if(!empty($n)){
           $total = $n;
        }
        for ($i=0; $i<=$total; $i++) {
            $year = $year+$i;
            $years_array[] = $year;
            $year = date('Y');
        }
        return $years_array;
    }
}

if(!function_exists('get_month_name_by_value')){
    function get_month_name_by_value($monthNum = 1){
        $dateObj   = DateTime::createFromFormat('!m', $monthNum);
        $monthName = $dateObj->format('F');
        return $monthName;
    }
}

if(!function_exists('base64_url')){
    function base64_url($file, $mime) {
        $contents = file_get_contents($file);
        $base64   = base64_encode($contents);
        return ('data:' . $mime . ';base64,' . $base64);
    }
}

/*--------------------encrypt  the item ID---------*/

 if ( ! function_exists('toPublicId')) {
  function toPublicId($id) {
    return $id * 14981488888 + 8259204988888;
  }
 }


/*--------------------decrypt the encrypted item ID---------*/

 if ( ! function_exists('toInternalId')) {
  function toInternalId($publicId) {
    return ($publicId - 8259204988888) / 14981488888;
  }
 }



 if ( !function_exists( 'get_date_format' ) ) {
  function get_date_format($date){
    // extract Y,M,D
    $explode = explode('-',$date);
    if($explode):
      $time = strtotime("{$explode[2]}-{$explode[1]}-{$explode[0]}");
      return date('D, F d, Y', $time);
    else :
      return $date;
    endif;
  }
}


if ( ! function_exists('getSidebar')){
    function getSidebar($role_id){
        $CI =& get_instance();
        $CI->load->model('privileges');
        $sidebar = $CI->privileges->get_controller_and_action_by_role_id($role_id );
        return $sidebar;
    }
}

if ( ! function_exists('get_partner_logo')){
    function get_partner_logo($user_id){
		$part_array = array();
		if($user_id){
			$CI =& get_instance();
			$CI->load->model('partners');
			$where_array = array(
				'user_id' => $user_id,
				'partner_is_active' => '1',
				'partner_is_deleted' => 0
			);
			$partners_array = $CI->partners->getRecords($CI->partners->table_partner,$where_array);
			if(!empty($partners_array) && isset($partners_array[0])){
				$part_array = $partners_array;
			}
		}
        return $part_array;
    }
}

if ( ! function_exists('get_company_user_id')){
    function get_company_user_id($user_id){
		$company_user_id = array();
        $CI =& get_instance();
        $CI->load->model('staff_model');
		$where_array = array(
            'user_id' => $user_id
        );
        $staff_array = $CI->staff_model->getRecords($CI->staff_model->table_staff,$where_array);
		if(!empty($staff_array) && isset($staff_array[0])){
			$company_user_id = $staff_array[0]['company_id'];
		}
        return $company_user_id;
    }
}

if ( ! function_exists('get_refferer_user_id')){
    function get_refferer_user_id($user_id){
		$refer_by_user_id = 0;
        $CI =& get_instance();
        $CI->load->model('referals');
		$where_array = array(
            'refer_to_user_id' => $user_id
        );
        $referral_array = $CI->referals->getRecords($CI->referals->table_referral,$where_array);
		if(!empty($referral_array) && isset($referral_array[0])){
			$refer_by_user_id = $referral_array[0]['refer_by_user_id'];
		}
        return $refer_by_user_id;
    }
}






function showSubCategories($cat_id,$count = '',$dashes = ''){
    $dashes .= '--';
    $countsub = $count+1;
   $ci=& get_instance();
        $ci->load->database();
    $sql = "SELECT * FROM  lss_categories where category_parent_id=".$cat_id." AND category_is_deleted=".'0'; ;
    $query = $ci->db->query($sql);
    $rows_sub = $query->result();
    if(!empty($rows_sub)){
         echo '<ul class="treeview-menu">';
    foreach ($rows_sub as $key => $value) { ?>

            <li class="treeview_category lavel<?php echo $count; ?>" id="<?php echo $value->category_id; ?>" class="<?php echo (countSubCategories($value->category_id)>0)?'treeview':'' ?>"><a href="javascript:void(0);"> <i></i><?php echo $value->category_name; ?></a> <?php showSubCategories($value->category_id,$countsub) ?></li>

   <?php  }

echo '</ul>';
    }

}
function countSubCategories($cat_id, $dashes = ''){
    $dashes .= '--';
   $ci=& get_instance();
        $ci->load->database();
    $sql = "SELECT * FROM  lss_categories where category_parent_id=".$cat_id." AND category_is_deleted=".'0';
    $query = $ci->db->query($sql);
    $rows_sub = $query->result();
   return count($rows_sub);

}



# Get parent (main) categories parent_id=0;
function getParentCategory(){
   $ci=& get_instance();
        $ci->load->database();
     $sql = "SELECT *
        FROM  lss_categories where category_parent_id=0 and category_is_deleted=0";
        $query = $ci->db->query($sql);
        $row = $query->result();
        echo '<ul class="treeview-menu"><li class="treeview_category parent_level lavel1" id="0"><a href="javascript:void(0)" class="active">Category Manager</a></li>';

       foreach ($row as $key => $value) { ?>

             <li class="treeview_category lavel1" id="<?php echo $value->category_id; ?>" class="<?php echo (countSubCategories($value->category_id)>0)?'treeview':'' ?>"><a href="javascript:void(0);"> <i></i><?php echo $value->category_name;  ?></a>

                <?php showSubCategories($value->category_id,2)?></li>

       <?php }
       echo '</ul>';
}
















if (!function_exists('last_time_play_final_quiz')){
    function last_time_play_final_quiz($user_id = 0, $quiz_id = 0){
		$return = false;
		$last_played_date_time = 0;
        $CI =& get_instance();
        $CI->load->model('users');
		$CI->session->set_userdata('final_quiz_id',$quiz_id);
		$where_array = array(
            'user_id' => $user_id,
			'quiz_id' => $quiz_id
        );
        $quiz_array = $CI->users->getRecords($CI->users->table_quiz_reports,$where_array);
		if(!empty($quiz_array) && isset($quiz_array[0])){
			$last_played_date_time = $quiz_array[0]['updated_time'];
			$last_played_date_time = date('Y-m-d H:i:s', strtotime($last_played_date_time . ' +1 day'));
		}
		if($last_played_date_time <= date('Y-m-d H:i:s')){
			$return = true;
		}
        return $return;
    }
}


    function getcategoryname($cat_id){
   $ci=& get_instance();
        $ci->load->database();
     $sql = "SELECT *
        FROM  lss_categories where category_id=".$cat_id;
        $query = $ci->db->query($sql);
        $row = $query->result_array();
        return $row;
    }
if ( ! function_exists('getfooterabout')){
    function getfooterabout(){
        $page_data_array = array();
        $footer_about = '';
        $CI =& get_instance();
        $CI->load->model('page_model','pages');
        $page_slug_array = array('footer-about');
        $where_array = array(
            'is_active' => '1',
            'is_delete' => '0'
        );
        $where_in_array = array(0 => 'page_slug', 1 => $page_slug_array);
        $page_data_array = $CI->pages->getRecords($CI->pages->table_pages,$where_array,'','','','',$where_in_array);
        if(!empty($page_data_array)){
            $footer_about = $page_data_array[0]['page_html'];
        }
        return $footer_about;
    }
}

if ( ! function_exists('getContries')){
    function getContries(){
        $CI =& get_instance();
        $CI->load->model('countries');
        $countries = $CI->countries->get_active_countries();
        return $countries;
    }
}

if( ! function_exists('getTitleById')){
    function getTitleById($id,$model){

        $CI =& get_instance();
        $CI->load->model($model,'obj_model');
        $title = $CI->obj_model->getTitle($id);
        return $title;
    }
}

if( ! function_exists('getRecords')){
    function getRecords($model,$select,$where){
        die('jdjj');
        $CI =& get_instance();
        $CI->load->model($model,'obj_model');
        $dbData = $CI->obj_model->getData($where,$select,$order_by= NULL, $offset=0, $limit=NULL,$where_in = false,$where_not_in = false);
        return $dbData['data'];
    }
}

if ( !function_exists( 'do_resize_thumb' ) ) {
    function do_resize_thumb($filename,$source_path){
        $CI =& get_instance();
        $source_path = $source_path.$filename;
        $target_path = $source_path."/thumb/".$filename;
        $config_manip = array(
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => TRUE,
            'create_thumb' => TRUE,
            'thumb_marker' => '',
            'width' => 150,
            'height' => 150
        );
        $CI->load->library('image_lib', $config_manip);
        if (!$CI->image_lib->resize()) {
            echo $CI->image_lib->display_errors();
        }
        // clear //
        $CI->image_lib->clear();
        return true;
    }
}

if (!function_exists('getRandomWord')){
    function getRandomWord($len = 6) {
        $word = array_merge(range('a', 'z'), range('A', 'Z'));
        shuffle($word);
        return substr(implode($word), 0, $len);
    }
}

if (!function_exists('getRandomNum')){
    function getRandomNum($len = 6) {
        return substr(number_format(time() * rand(),0,'',''),0,$len);
    }
}

if(!function_exists('get_config_data')){
    function get_config_data($config_key = ''){
        #echo $config_key; die;
        $config_value = '';
        $result = array();
        $CI =& get_instance();
        $CI->load->model('users','obj_users');
        $where = array('config_key' => $config_key);
        $result = $CI->obj_users->getRecords($CI->obj_users->table_site_config,$where);
        if(!empty($result[0]['config_value']) && isset($result[0])){
           $config_value = $result[0]['config_value'];
        }
        return $config_value;
    }
}

if(!function_exists('get_max_number_of_user_allow')){
    function get_max_number_of_user_allow($id = 0,$user_id = 0){
        #echo $config_key; die;
        $no_of_user_allowed = 0;
        $result = array();
        $CI =& get_instance();
        $CI->load->model('users','obj_users');
        $where = array('user_id' => $user_id, 'id' => $id, 'order_status' => '1');
        $no_of_user_allowed = $CI->obj_users->get_count_company_license($id,$user_id);
        return $no_of_user_allowed;
    }
}

if (!function_exists('get_embeded_video')){
    function get_embeded_video($video_url = '', $width = '640') {
		if(is_url_exist($video_url)){
			$oembed_endpoint = 'http://vimeo.com/api/oembed';
			#$json_url = $oembed_endpoint . '.json?url=' . rawurlencode($video_url) . '&width=640';
			$xml_url = $oembed_endpoint . '.xml?url=' . rawurlencode($video_url) . '&width='.$width;
			$oembed = simplexml_load_string(curl_get($xml_url));
			$return_data = array();
			$return_data['title'] = $oembed->title;
			$return_data['author_url'] = $oembed->author_url;
			$return_data['author_name'] = $oembed->author_name;
			$oembed_html = html_entity_decode($oembed->html);
			return $oembed_html;
		}else{
			return "<h2> The video URL provided by administrator does not exist!! </h2>";
		}
    }
}

if (!function_exists('curl_get')){
    function curl_get($url) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        $return = curl_exec($curl);
        curl_close($curl);
        return $return;
    }
}
if(!function_exists('is_url_exist')){
	function is_url_exist($url = NULL){
		if ( ! empty($url) && filter_var($url, FILTER_VALIDATE_URL)){
			if (function_exists('curl_version')){
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_NOBODY, TRUE);
				curl_exec($ch);
				$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					if ($code == 200){
						$status = TRUE;
					}else{
						$status = FALSE;
					}
				curl_close($ch);
				return $status;
			}else{
				$headers = @get_headers($url);
				return stripos($headers[0], '200 OK') ? TRUE : FALSE;
			}
		}else{
			return FALSE;
		}
	}
}

if(!function_exists('substract_date_with_current_date')){
    function substract_date_with_current_date($date = ''){
        if(!empty($date)){
            $date_array = explode('/',$date);
            #pr($date_array);
            $h = 18;
            $i = 11;
            $s = 00;
            if(!empty($date_array[0]) && !empty($date_array[1]) && !empty($date_array[2])){
                $d = $date_array[0];
                $m = $date_array[1];
                $y = $date_array[2];
                $date = date("Y-m-d",mktime($h,$i,$s,$m,$d,$y));
            }else{
                $date = date('Y-m-d',time());
            }

        }else{

        }

        $datetime1 = new DateTime($date);
        $current = new DateTime(date('Y-m-d',time()));
        $interval = $datetime1->diff($current); #'%y years %m months and %d days'
        $days = $interval->format('%d');
        $months = $interval->format('%m');
        $year = $interval->format('%y');
        $final_days = (int)$days+(int)(30*$months)+(int)($year*365);
        return $final_days;
    }
}

if(!function_exists('save_user_log')){
    function save_user_log($log_id = '', $user_id){
        $CI =& get_instance();
        $CI->load->model('users','obj_users');
        $where = array(
			'user_event_log_id' => $log_id,
			'user_id' => $user_id,
			'login_ip' => $CI->input->ip_address()
		);
        $result = $CI->obj_users->getRecords($CI->obj_users->table_user_logs,$where);
        if(!empty($result[0]) && isset($result[0])){
           $update_log = array(
				'user_login_count' => $result[0]['user_login_count']+1,
				'user_last_login' => $CI->obj_users->currentDateTime
		   );
		   $update_where = array(
				'user_event_log_id' => $log_id,
				'user_id' => $user_id,
				'login_ip' => $CI->input->ip_address()
		   );
		   $CI->obj_users->save($CI->obj_users->table_user_logs,$update_log,$update_where);
        }else{
			$save_log_data = array(
				'user_event_log_id' => $log_id,
				'user_id' => $user_id,
				'login_ip' => $CI->input->ip_address(),
				'user_login_count' => 1,
				'user_last_login' => $CI->obj_users->currentDateTime
			);
			$CI->obj_users->save($CI->obj_users->table_user_logs,$save_log_data);
		}
        return true;
    }
}

if(!function_exists('get_question_text_by_id')){
    function get_question_text_by_id($question_id = 0){
        $question_text = '';
        $CI =& get_instance();
        $CI->load->model('users','obj_users');
        $where = array('question_id' => $question_id, 'question_is_active' => 1, 'question_is_deleted' => 0);
        $question_data_array = $CI->obj_users->getRecords($CI->obj_users->table_questions,$where,'','','','','','','question_title');
        if(!empty($question_data_array)){
			$question_text = isset($question_data_array[0]['question_title'])?$question_data_array[0]['question_title']:'';
		}
		return $question_text;
    }
}
if(!function_exists('get_question_by_surveyid')){
    function get_question_by_surveyid($surveyid = ""){
        $question_text = '';
        $CI =& get_instance();
        $query = $CI->db->get_where('lss_survey_question', array('servey_id' => $surveyid));
        $questions = array();
        foreach ($query->result() as $key => $value) {
            $questions[] = $value->question_id;
        }
        return $questions;
    }
}

if(!function_exists('get_correct_option_text_by_id')){
    function get_correct_option_text_by_id($question_id = 0){
        $option_text = '';
		$option_array = array();
        $CI =& get_instance();
        $CI->load->model('users','obj_users');
        $where = array(
			'question_id' => $question_id,
			'option_correct_answer' => 1
		);
        $option_data_array = $CI->obj_users->getRecords($CI->obj_users->table_question_options,$where,'','','','','','','option_value');
		if(!empty($option_data_array)){
			foreach($option_data_array as $key => $value){
				$option_array[] = $value['option_value'];
			}
		}
		if(!empty($option_array)){
			$option_text = implode('|',$option_array);
		}
		return $option_text;
    }
}

if(!function_exists('get_user_option_text_by_id')){
    function get_user_option_text_by_id($option_id_array = array()){
        $option_text = '';
		$option_array = array();
        $CI =& get_instance();
        $CI->load->model('users','obj_users');
        if(!empty($option_id_array)){
			$where_in_array = array(
				0 => 'option_id',
				1 => $option_id_array
			);
			$option_data_array = $CI->obj_users->getRecords($CI->obj_users->table_question_options,'','','','','',$where_in_array,'','option_value');
			if(!empty($option_data_array)){
				foreach($option_data_array as $key => $value){
					$option_array[] = $value['option_value'];
				}
			}
			if(!empty($option_array)){
				$option_text = implode('|',$option_array);
			}
		}
		return $option_text;
    }
}

if(!function_exists('is_array_equal')){
    function is_array_equal($a, $b) {
        return (
            is_array($a) && is_array($b) && count($a) == count($b) && array_diff($a, $b) === array_diff($b, $a)
        );
    }
}

if(!function_exists('validate_vat_number')){
    function validate_vat_number($vat_number = '',$endpoint = ''){
        $validationResultArray = array();
        $api_endpoint_url = API_ENDPOINT_URL;
        $access_key = ACCESS_KEY;
        $ch = curl_init($api_endpoint_url.$endpoint.'?access_key='.$access_key.'&vat_number='.$vat_number.'');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($ch);
        curl_close($ch);
        $validationResult = json_decode($json, true);
        $validationResult = objectToArray($validationResult);
        if(!empty($validationResult)){
            $validationResultArray = $validationResult;
        }
        return $validationResultArray;
    }
}


/* End of file array_helper.php */
/* Location: ./system/helpers/custom_helper.php */