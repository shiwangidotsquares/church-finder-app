<?php  if ( ! defined('BASEPATH')) exit('No direct access allowed');

/**
 * CodeIgniter CSS JS Helpers
 *
 */
//Dynamically add Javascript files to header page
if(!function_exists('add_js')){
    function add_js($file='')
    {
        $str = '';
        $ci = &get_instance();
        $js_array  = $ci->config->item('js_array');

        if(empty($file)){
            return;
        }

        if(is_array($file)){
            if(!is_array($file) && count($file) <= 0){
                return;
            }
            foreach($file AS $item){
                $js_array[] = $item;
            }
            $ci->config->set_item('js_array',$js_array);
        }else{
            $str = $file;
            $js_array[] = $str;
            $ci->config->set_item('js_array',$js_array);
        }
    }
}

//Dynamically add CSS files to header page
if(!function_exists('add_css')){
    function add_css($file='')
    {
        $str = '';
        $ci = &get_instance();
        $css_array = $ci->config->item('css_array');

        if(empty($file)){
            return;
        }

        if(is_array($file)){
            if(!is_array($file) && count($file) <= 0){
                return;
            }
            foreach($file AS $item){
                $css_array[] = $item;
            }
            $ci->config->set_item('css_array',$css_array);
        }else{
            $str = $file;
            $css_array[] = $str;
            $ci->config->set_item('css_array',$css_array);
        }
    }
}

if(!function_exists('put_headers')){
    function put_headers()
    {
        $str = '';
        $ci = &get_instance();
        $css_array = $ci->config->item('css_array');
        foreach($css_array AS $item){
            if(is_remote($item)){
                $str .= '<link rel="stylesheet" href="'.$item.'" type="text/css" />'."\n";
            }else{
                $str .= '<link rel="stylesheet" href="'.base_url().'assets/'.$item.'" type="text/css" />'."\n";
            }

        }
		return $str;
    }
}

if(!function_exists('put_footer')){
    function put_footer()
		{
			$str = '';
			$ci = &get_instance();
			$js_array  = $ci->config->item('js_array');
			foreach($js_array AS $item){
                if(is_remote($item)){
                    $str .= '<script type="text/javascript" src="'.$item.'"></script>'."\n";
                }else{
                    $str .= '<script type="text/javascript" src="'.base_url().'assets/'.$item.'"></script>'."\n";
                }

			}
			return $str;
		}
}

if(!function_exists('is_remote')){
    function is_remote($url)
    {
        if(strpos($url, '//cdnjs') !== false || strpos($url, '//cdn') !== false){
            return true;
        }else{
            return false;
        }
    }
}

if(!function_exists('show_div')){
function show_div($data,$type='yyyy-mm-dd'){
        switch($type){
            case "dd/mm/yyyy" : return date('d/m/Y',strtotime($data));
            case "dd-mm-yyyy" : return date('d-m-Y',strtotime($data));
            case "yyyy-mm-dd" : return date('d/m/Y',strtotime($data));
            case "dd/mm/yyyy:H:i:s" : return date('d/m/Y H:i:s',strtotime($data));
            case "dd/mm/yyyy:H:i a" : return date('d/m/Y H:i a',strtotime($data));
         }
    }
}

if(!function_exists('show_date')){
function show_date($data,$type='yyyy-mm-dd'){
        switch($type){
            case "dd/mm/yyyy" : return date('d/m/Y',strtotime($data));
            case "dd-mm-yyyy" : return date('d-m-Y',strtotime($data));
            case "yyyy-mm-dd" : return date('d/m/Y',strtotime($data));
            case "dd/mm/yyyy:H:i:s" : return date('d/m/Y H:i:s',strtotime($data));
            case "dd/mm/yyyy:H:i a" : return date('d/m/Y H:i a',strtotime($data));
         }
    }
}
if(!function_exists('getallquestions')){
    function getallquestions($question_category = '')
    {
        $ci = &get_instance();
        $query = $this->db->get_where('lss_questions', array('question_is_deleted' => 0,'question_is_active'=>1,'question_category'=>$question_category));
        return $query->result();

    }
}