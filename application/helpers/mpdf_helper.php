<?php
if ( ! defined('BASEPATH')) exit('No direct access allowed');

require 'mpdf/mpdf.php';

if (!function_exists('create_pdf')) {

    function create_pdf($html_data, $file_name = "", $mode = '') {
        if ($file_name == "") {
            $file_name = 'report' . date('dMY');
        }

        $mypdf = new mPDF('c','A4','','',0,0,0,0,0,0);
        $mypdf->SetDisplayMode('fullpage');

        $stylesheet = @file_get_contents(base_url().'assets/pdf/mpdfstyleA4.css');
        #$stylesheet1 = file_get_contents(base_url().'assets/front/css/print.css');
        #$stylesheet2 = file_get_contents(base_url().'assets/front/css/pdf-main.css');
        $mypdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
        #$mypdf->WriteHTML($stylesheet1,1);
        #$mypdf->WriteHTML($stylesheet2,1);

        // Bullets in columns are probably best not indented
        $mypdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

        $mypdf->max_colH_correction = 1.1;

        $mypdf->WriteHTML($html_data);
        if(!empty($mode)){
            $mypdf->Output($file_name . '.pdf', $mode);
        }else{
            $mypdf->Output($file_name . '.pdf', 'D');
        }
    }

}