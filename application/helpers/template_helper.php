<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');
 /**
  * templates
  *
  * WordPress like templates for CodeIgniter
  *
  * @package        templates
  * @subpackage    Helpers
  * @version        0.1.0
  * @author        WebInOne
  * @link            http://www.webinone.net/codeigniter-templates-library/
  * @copyright        Copyright (c) 2011, WebInOne
  * @license        http://opensource.org/licenses/mit-license.php MIT Licensed
  *
  */

  if ( ! function_exists('get_top'))
  {
      function get_top($name=null)
      {
          $ci =& get_instance();
          return $ci->templates->get_top($name);
      }
  }

   if ( ! function_exists('get_header'))
  {
      function get_header($name=null)
      {
        if(!empty($name)) $name = 'header-'.$name;
          $ci =& get_instance();
          return $ci->templates->get_header($name);
      }
  }

  if ( ! function_exists('get_slideshow'))
  {
      function get_slideshow($name=null)
      {
        if(!empty($name)) $name = 'slideshow-'.$name;
          $ci =& get_instance();
          return $ci->templates->get_slideshow($name);
      }
  }

  if ( ! function_exists('get_left'))
  {
      function get_left($name=null)
      {
          $ci =& get_instance();
          return $ci->templates->get_left($name);
      }
  }

  if ( ! function_exists('get_content'))
  {
      function get_content($name=null)
      {
          $ci =& get_instance();
          return $ci->templates->get_content($name);
      }
  }

  if ( ! function_exists('get_right'))
  {
      function get_right($name=null)
      {
          $ci =& get_instance();
          return $ci->templates->get_right($name);
      }
  }

  if ( ! function_exists('get_breadcrumbs'))
  {
      function get_breadcrumbs($name=null)
      {
          $ci =& get_instance();
          return $ci->templates->get_breadcrumbs($name);
      }
  }

    if ( ! function_exists('get_banner'))
  {
      function get_banner($name=null)
      {
          $ci =& get_instance();
          return $ci->templates->get_banner($name);
      }
  }



  if ( ! function_exists('get_sidebar'))
  {
      function get_sidebar($name=null)
      {
          $ci =& get_instance();
          return $ci->templates->get_sidebar($name);
      }
  }

  if ( ! function_exists('get_footer'))
  {
      function get_footer($name=null)
      {
         //if(!empty($name)) $name = 'footer-'.$name;
          $ci =& get_instance();
          return $ci->templates->get_footer($name);
      }
  }

  if ( ! function_exists('get_template_part'))
  {
      function get_template_part($slug, $name=null)
      {
          $ci =& get_instance();
          return $ci->templates->get_template_part($slug, $name);
      }
  }
?>
