<!DOCTYPE html>
<!--[if lt IE 7]>   <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>     <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>     <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
  <!--<![endif]-->
  <!--[if IE]>
  <link rel="stylesheet"  href="<?php echo base_url(); ?>assets/front/css/ie8.css">
  <![endif]-->
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta name="format-detection" content="telephone=no">
    <title>:: LMS ::</title>
    <link href="<?php echo base_url(); ?>assets/front/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/front/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/front/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/front/css/owl.theme.default.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/front/css/stylesheet.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>assets/front/js/html5shiv.js"></script>
    <script src="<?php echo base_url(); ?>assets/front/js/html5shiv-printshiv.js"></script>
  </head>
  <body>
    <!--[if lt IE 9]>
    <script src="dist/html5shiv.js"></script>
    <![endif]-->
    <!-- Modal -->
    <div class="modal fade" id="lets-talk-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">GET IN TOUCH</h4>
          </div>
          <div class="modal-body">
            <form class="contact-form">
              <div class="form-group">
                <input class="form-control" type="text" name="q" value="" placeholder="First name">
              </div>
              <div class="form-group">
                <input class="form-control" type="text" name="q" value="" placeholder="Last name">
              </div>
              <div class="form-group">
                <input class="form-control" type="text" name="q" value="" placeholder="Company">
              </div>
              <div class="form-group">
                <input class="form-control" type="email" name="q" value="" placeholder="Email address">
              </div>
              <div class="form-group">
                <input class="form-control" type="tel" name="q" value="" placeholder="Telephone number">
              </div>
              <div class="form-group">
                <button class="button submit" type="submit">Submit</button>
              </div>
              <div class="form-group">
                <p>We'll get back to you as soon as we can.</p>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
    <header>
      <div class="head-mid">
        <div class="container">
          <div class="row">
            <div class="col-md-2 col-sm-3">
              <a href="#" class="logo"><img src="<?php echo base_url(); ?>assets/front/images/logo.png"></a>
            </div>
            <div class="col-md-10 col-sm-9">
              <div class="top-links">
                <ul>
                  <li class="register"><a href="#"><span class="icon"><i class="fa fa-sign-in" aria-hidden="true"></i></span>REGISTER </a></li>
                  <li class="login"><a href="#"><span class="icon"><i class="fa fa-user" aria-hidden="true"></i></span>LOGIN </a></li>
                </ul>
              </div>
              <div class="head-right">
                <div class="call"><span><i class="fa fa-phone" aria-hidden="true"></i>Call us now on </span>+111 555 666</div>
                <div class="search-from">
                  <form class="form-search clearfix">
                    <input id="search_query_top" class="form-control" type="search" name="q" value="" placeholder="Type your search here...">
                    <button id="search_button" class="btn btn-sm" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="head-main">
        <div class="container">
          <div class="sidebar-nav">
            <ul class="content clearfix">
              <li class="sidebardropdown">
                <a href="#" class="Currency">Products<i class="fa fa-bars"></i></a>
                <ul class="sub-menu" style="display: none;">
                  <li class="sidebardropdown">
                    <a href="#">Lorem ipsum dolor sit amet</a>
                    <ul class="sub-menu" style="display: none;">
                      <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                      <li class="sidebardropdown">
                        <a href="#">Lorem ipsum dolor sit amet</a>
                        <ul class="sub-menu">
                          <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                          <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                          <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                          <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                          <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                        </ul>
                      </li>
                      <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                      <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                      <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                    </ul>
                  </li>
                  <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                  <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                  <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                  <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                  <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                  <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                  <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                  <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                  <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                  <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                  <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                </ul>
              </li>
            </ul>
          </div>

          <div class="navigation">
            <nav class="navbar">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
              </div>
              <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                  <li class="active"><a href="#">Home</a></li>
                  <li><a href="#">About us</a></li>
                  <li><a href="#">Services</a></li>
                  <li><a href="#">physical tangible products</a></li>
                  <li><a href="#">CONTACT</a></li>
                </ul>
              </div>
            </nav>
          </div>
          <div class="mini-cart">
            <a href="#" class="cart-toggle"><i class="fa fa-shopping-cart" aria-hidden="true"></i>(My cart: 0 items)</a>
          </div>
        </div>
      </div>
    </header>
    <div class="slider-zone">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          <li data-target="#carousel-example-generic" data-slide-to="3"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <img src="<?php echo base_url(); ?>assets/front/images/slide-img-1.jpg" alt="...">
            <div class="carousel-caption">

              <div class="container">
                <div clas="row">
                  <div class="col-md-8 col-sm-8">
                    <h2>LMS</h2>
                    <h1>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</h1>
                    <a href="#" class="btn button">Download</a>
                  </div>
                  <div class="col-md-4 col-sm-4">
                    <div class="talk-banner">
                      <h2>Want to find out more?</h2>
                      <button class="lets-talk btn btn-primary btn-lg" type="button" data-toggle="modal" data-target="#lets-talk-model">Let's talk</button>
                      <span>We'll take your basic details and get in touch</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <img src="<?php echo base_url(); ?>assets/front/images/slide-img-2.jpg" alt="...">
            <div class="carousel-caption">
              <div class="container">
                <div clas="row">
                  <div class="col-md-8 col-sm-8">
                    <h2>LMS</h2>
                    <h1>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</h1>
                    <a href="#" class="btn button">Download</a>
                  </div>
                  <div class="col-md-4 col-sm-4">
                    <div class="talk-banner">
                      <h2>Want to find out more?</h2>
                      <button class="lets-talk btn btn-primary btn-lg" type="button" data-toggle="modal" data-target="#lets-talk-model">Let's talk</button>
                      <span>We'll take your basic details and get in touch</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <img src="<?php echo base_url(); ?>assets/front/images/slide-img-3.jpg" alt="...">
            <div class="carousel-caption">
              <div class="container">
                <div clas="row">
                  <div class="col-md-8 col-sm-8">
                    <h2>LMS</h2>
                    <h1>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</h1>
                    <a href="#" class="btn button">Download</a>
                  </div>
                  <div class="col-md-4 col-sm-4">
                    <div class="talk-banner">
                      <h2>Want to find out more?</h2>
                      <button class="lets-talk btn btn-primary btn-lg" type="button" data-toggle="modal" data-target="#lets-talk-model">Let's talk</button>
                      <span>We'll take your basic details and get in touch</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <img src="<?php echo base_url(); ?>assets/front/images/slide-img-4.jpg" alt="...">
            <div class="carousel-caption">
              <div class="container">
                <div clas="row">
                  <div class="col-md-8 col-sm-8">
                    <h2>LMS</h2>
                    <h1>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt. </h1>
                    <a href="#" class="btn button">Download</a>
                  </div>
                  <div class="col-md-4 col-sm-4">
                    <div class="talk-banner">
                      <h2>Want to find out more?</h2>
                      <button class="lets-talk btn btn-primary btn-lg" type="button" data-toggle="modal" data-target="#lets-talk-model">Let's talk</button>
                      <span>We'll take your basic details and get in touch</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="talk-banner in-mobile">
        <h2>Want to find out more?</h2>
        <button class="lets-talk btn btn-primary btn-lg" type="button" data-toggle="modal" data-target="#lets-talk-model">Let's talk</button>
        <span>We'll take your basic details and get in touch</span>
      </div>
    </div>
    <main>
    <section class="document-search">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-4">
            <div class="document-box">
              <div class="icon-img"><img src="<?php echo base_url(); ?>assets/front/images/pdf-icon.png"></div>
              <h2>Ready to use templates and forms</h2>
              <p>Lorem ipsum dolor sit amet, liquam erat volutpat.</p>
              <a href="#" class="btn button">Search</a>
            </div>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="document-box mid-box">
              <div class="icon-img"><img src="<?php echo base_url(); ?>assets/front/images/pdf-icon.png"></div>
              <h2>Bespoke Policy documents</h2>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit ut laoreet dolore magna aliquam erat volutpat.</p>
              <a href="#" class="btn button">Search</a>
            </div>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="document-box">
              <div class="icon-img"><img src="<?php echo base_url(); ?>assets/front/images/pdf-icon.png"></div>
              <h2>Other documents</h2>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit erat volutpat.</p>
              <a href="#" class="btn button">Search</a>
            </div>
          </div>
        </div>

      </div>
    </section>

    <section class="training-courses">
      <div class="container">
        <div class="title"><h2>Training Courses</h2><span class="bar-icon"></span></div>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris et tellus arcu.<br> Nam mi augue, congue et sem quis, gravida.
        </p>
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <div class="courses-wrap">
              <span class="icon"><img src="<?php echo base_url(); ?>assets/front/images/iosh-icon.png"></span>
              <h3>IOSH Managing Safely</h3>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
              <p>diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
              <a href="#" class="btn button">View courses</a>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="courses-wrap">
              <span class="icon"><img src="<?php echo base_url(); ?>assets/front/images/risk-assessment.png"></span>
              <h3>IOSH Managing Safely</h3>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
              <p>diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
              <a href="#" class="btn button">View courses</a>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="courses-wrap">
              <span class="icon"><img src="<?php echo base_url(); ?>assets/front/images/functional-safety.png"></span>
              <h3>IOSH Managing Safely</h3>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
              <p>diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
              <a href="#" class="btn button">View courses</a>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="courses-wrap">
              <span class="icon"><img src="<?php echo base_url(); ?>assets/front/images/emergency-icon.png"></span>
              <h3>IOSH Managing Safely</h3>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
              <p>diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
              <a href="#" class="btn button">View courses</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="our-services">
      <div class="container">
        <div class="title"><h2>OUR SERVICES</h2><span class="bar-icon"></span></div>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris et tellus arcu.<br> Nam mi augue, congue et sem quis, gravida.
        </p>
        <div id="tabs" class="services-section">
          <ul id="test" class="nav nav-tabs" role="tablist">
            <li class="list-1 active"><a href="#unlimited" role="tab" data-toggle="tab"><h3>Unlimited Downloads</h3><p>Lorem ipsum dolor sit amet, consectetuer .adipiscing elit</p></a>
            <div class="circle-box"><span class="icon"><img src="<?php echo base_url(); ?>assets/front/images/unlimited-downloads.png"></span><h4>Unlimited Downloads</h4></div>
          </li>
          <li class="list-2"><a href="#services1" role="tab" data-toggle="tab"><h3>Services1</h3><p>Lorem ipsum dolor sit amet, consectetuer .adipiscing elit</p></a>
          <div class="circle-box"><span class="icon"><img src="<?php echo base_url(); ?>assets/front/images/unlimited-downloads.png"></span><h4>Services1</h4></div>
        </li>
        <li class="list-3"><a href="#free-legal" role="tab" data-toggle="tab"><h3>Free Legal Consultation</h3><p>Lorem ipsum dolor sit amet, consectetuer .adipiscing elit</p></a>
        <div class="circle-box"><span class="icon"><img src="<?php echo base_url(); ?>assets/front/images/unlimited-downloads.png"></span><h4>Free Legal Consultation</h4></div>
      </li>
    </ul>
    <ul id="test1" class="nav nav-tabs" role="tablist">
      <li class="list-1"><a href="#regular-update" role="tab" data-toggle="tab"><h3>Regular Updates</h3><p>Lorem ipsum dolor sit amet, consectetuer .adipiscing elit</p></a>
      <div class="circle-box"><span class="icon"><img src="<?php echo base_url(); ?>assets/front/images/unlimited-downloads.png"></span><h4>Regular Updates</h4></div>
    </li>
    <li class="list-2"><a href="#update-alert" role="tab" data-toggle="tab"><h3>Update Alerts</h3><p>Lorem ipsum dolor sit amet, consectetuer .adipiscing elit</p></a>
    <div class="circle-box"><span class="icon"><img src="<?php echo base_url(); ?>assets/front/images/unlimited-downloads.png"></span><h4>Update Alerts</h4></div>
  </li>
  <li class="list-3"><a href="#newsletters" role="tab" data-toggle="tab"><h3>Newsletters</h3><p>Lorem ipsum dolor sit amet, consectetuer .adipiscing elit</p></a>
  <div class="circle-box"><span class="icon"><img src="<?php echo base_url(); ?>assets/front/images/unlimited-downloads.png"></span><h4>Newsletters</h4></div>
</li>
</ul>
<div class="bigcircle tab-content">
<div class="circle-content tab-pane fade in active" id="unlimited">
  <div class="title">
    <h3>Unlimited Downloads</h3>
  </div>
  <h4>Lorem ipsum consectetuer</h4>
  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat</p>
</div>
<div class="circle-content tab-pane fade" id="services1">
  <div class="title">
    <h3>Services1</h3>
  </div>
  <h4>Lorem ipsum consectetuer</h4>
  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat</p>
</div>
<div class="circle-content tab-pane fade" id="free-legal">
  <div class="title">
    <h3>Free Legal Consultation</h3>
  </div>
  <h4>Lorem ipsum consectetuer</h4>
  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat</p>
</div>
<div class="circle-content tab-pane fade" id="regular-update">
  <div class="title">
    <h3>Regular Updates</h3>
  </div>
  <h4>Lorem ipsum consectetuer</h4>
  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat</p>
</div>
<div class="circle-content tab-pane fade" id="update-alert">
  <div class="title">
    <h3>Update Alerts</h3>
  </div>
  <h4>Lorem ipsum consectetuer</h4>
  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat</p>
</div>
<div class="circle-content tab-pane fade" id="newsletters">
  <div class="title">
    <h3>Newsletters</h3>
  </div>
  <h4>Lorem ipsum consectetuer</h4>
  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat</p>
</div>
</div>
</div>
</div>
</section>
<section class="tangible-products">
<div class="container">
<div class="title"><h2>physical tangible products</h2><span class="bar-icon"></span></div>
<div class="row">
<div class="col-md-3 col-sm-6">
<div class="amazone-product">
  <div class="product-img"><img src="<?php echo base_url(); ?>assets/front/images/product-img-1.png"></div>
  <h3>Donec pellentesque</h3>
  <span class="price">£45.00</span>
  <a href="#" class="btn button"><i class="fa fa-amazon" aria-hidden="true"></i> Buy Now</a>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div class="amazone-product">
  <div class="product-img"><img src="<?php echo base_url(); ?>assets/front/images/product-img-2.png"></div>
  <h3>Donec pellentesque</h3>
  <span class="price">£45.00</span>
  <a href="#" class="btn button"><i class="fa fa-amazon" aria-hidden="true"></i> Buy Now</a>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div class="amazone-product">
  <div class="product-img"><img src="<?php echo base_url(); ?>assets/front/images/product-img-3.png"></div>
  <h3>Donec pellentesque</h3>
  <span class="price">£45.00</span>
  <a href="#" class="btn button"><i class="fa fa-amazon" aria-hidden="true"></i> Buy Now</a>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div class="amazone-product">
  <div class="product-img"><img src="<?php echo base_url(); ?>assets/front/images/product-img-4.png"></div>
  <h3>Donec pellentesque</h3>
  <span class="price">£45.00</span>
  <a href="#" class="btn button"><i class="fa fa-amazon" aria-hidden="true"></i> Buy Now</a>
</div>
</div>

</div>
</div>
</section>
<section class="pricing-section">
<div class="container">
<div class="title"><h2><span>Pricing</span> Plans</h2>
<span class="bar-icon"></span></div>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris et tellus arcu. Nam mi augue, congue et sem quis, gravida
.auctor augue. Aliquam elementum, risus non bibendum viverra</p>
<div class="row">
<div class="col-md-4 col-sm-4">
<div class="pricing-box">
  <h3 class="price-name">Gold</h3>
  <div class="pricing-panel">
    <span class="sign">£</span>
    <span class="price">35</span>
    <span class="year">/Per year</span>
  </div>
  <div class="pricing-disc">
    <p>Access all of the documents
    within any one Folder</p>
  </div>
  <div class="pricing-icon">
    <img src="<?php echo base_url(); ?>assets/front/images/folder-icon.png">

  </div>
  <ul class="pricing-Tble-DocNam">
    <li>Business</li>
    <li>Corporate</li>
    <li>Employment</li>
    <li>Health & Safety</li>
    <li>Property</li>
  </ul>
  <div class="pricing-More">
    <a href="#" class="more-btn btn">More<span class="icon"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span></a>
  </div>
</div>
</div>
<div class="col-md-4 col-sm-4">
<div class="pricing-box yellow-box">
  <h3 class="price-name">Silver</h3>
  <div class="pricing-panel">
    <span class="sign">£</span>
    <span class="price">35</span>
    <span class="year">/Per year</span>
  </div>
  <div class="pricing-disc">
    <p>Access all of the documents
    within any one Folder</p>
  </div>
  <div class="pricing-icon">
    <img src="<?php echo base_url(); ?>assets/front/images/folder-icon-yellow.png">

  </div>
  <ul class="pricing-Tble-DocNam">
    <li>Business</li>
    <li>Corporate</li>
    <li>Employment</li>
    <li>Health & Safety</li>
    <li>Property</li>
  </ul>
  <div class="pricing-More">
    <a href="#" class="more-btn btn">More<span class="icon"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span></a>
  </div>
</div>
</div>
<div class="col-md-4 col-sm-4">
<div class="pricing-box">
  <h3 class="price-name">Bronze</h3>
  <div class="pricing-panel">
    <span class="sign">£</span>
    <span class="price">35</span>
    <span class="year">/Per year</span>
  </div>
  <div class="pricing-disc">
    <p>Access all of the documents
    within any one Folder</p>
  </div>
  <div class="pricing-icon">
    <img src="<?php echo base_url(); ?>assets/front/images/folder-icon.png">

  </div>
  <ul class="pricing-Tble-DocNam">
    <li>Business</li>
    <li>Corporate</li>
    <li>Employment</li>
    <li>Health & Safety</li>
    <li>Property</li>
  </ul>
  <div class="pricing-More">
    <a href="#" class="more-btn btn">More<span class="icon"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span></a>
  </div>
</div>
</div>
</div>
</div>
</section>
</main>
<footer id="footer-sec">
<div class="footer-top">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-3">
  <div class="footer-links">
    <a href="#" class="footer-logo"><img src="<?php echo base_url(); ?>assets/front/images/logo.png"></a>
  </div>
</div>
<div class="col-md-3 col-sm-3">
  <div class="footer-links">
    <h4>INFORMATION</h4>
    <ul>
      <li><a href="#">About us</a></li>
      <li><a href="#">Privacy policy</a></li>
      <li><a href="#">Terms & condition</a></li>
    </ul>
  </div>
</div>
<div class="col-md-3 col-sm-3">
  <div class="footer-links">
    <h4>CUSTOMER SERVICE</h4>
    <ul>
      <li><a href="#">Contact Us</a></li>
      <li><a href="#">Help & FAQs</a></li>
    </ul>
  </div>
</div>
<div class="col-md-3 col-sm-3">
  <div class="footer-links">
    <h4>STAY CONNECTED</h4>
    <ul class="social">
      <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
      <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
      <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
      <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
    </ul>
        <span class="paypal">
      <img src="<?php echo base_url(); ?>assets/front/images/paypal-icon.png">
    </span>
  </div>
</div>
</div>
</div>
</div>
<div class="copyright">
<p>Copyright © 2017 LMS. All rights reserved.</p>
</div>
</footer>
<a href="#" class="user-conversation">
<img src="<?php echo base_url(); ?>assets/front/images/user-conversation-icon.png" width="100">
</a>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/front/js/bootstrap.min.js"></script>
<script>
$(document).ready(function() {
$( '.sidebardropdown' ).hover(
function(){
$(this).children('.sub-menu').slideDown(200);
},
function(){
$(this).children('.sub-menu').slideUp(200);
}
);
}); // end ready
</script>
</body>
</html>