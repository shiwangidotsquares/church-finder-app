<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: <?php echo $this->config->item('site_title'); ?> ::</title>
</head>
<style>
body {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 11px;
    margin: 0;
    padding: 0;
    line-height:24px;
}
</style>
<body>
<table width="600" align="center" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td><a href="#"><img src="<?php echo base_url() ?>assets/front/images/top-ban.jpg" alt="" /></a></td>
    </tr>

    <tr>
     <td height="10"></td>
    </tr>

    <tr>
     <td>
       <table width="100%" align="center"  style="font-family: Arial, sans-serif; color:#282828; font-size: 14px;" cellpadding="5" cellspacing="0">

          <tr>
               <td style="font-size: 14px;">{EMAIL_CONTENT}</td>
         </tr>
         <tr>
           <td>
             Thank you!,<br />

           </td>
         </tr>


       </table>
     </td>
    </tr>

    <tr>
     <td height="10"></td>
    </tr>

          <tr>
           <td>
             <table bgcolor="2348a3" width="100%" align="center"  style="font-family: Arial, sans-serif;color:#fff;" cellpadding="10" cellspacing="0">
                 <tr align="center">
                    <td>
                    <p style="font-size: 12px;font-family: Arial, sans-serif; text-align:justify;text-align: center; font-weight: bold;">CMS Consultancy</p>


                    </td>
                 </tr>


             </table>
              </td>
                 </tr>

    <tr>
                     <td>
              <table width="100%" >
                 <tr>
                     <td>

                           <p style="font-size: 11px !important;">CMS Consultancy Limited, CMS, Wickenby, LINCOLN, LN3 5AB, UK
                     CMS Consultancy is the trading name of CMS Consultancy Limited Registered in England No. 9028231 registered office address 4 Henley Way, LINCOLN, LN6 3QR, UK.</p>

                     </td>
                 </tr>
             </table>

             </td>
             </tr>
  </tbody>
</table>
</body>
</html>
