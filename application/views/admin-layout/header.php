<style type="text/css">

input.ImgPrePopupSelect:checked + label { opacity: 0.5; }
 + label { width: 100%; }
.SelectImgPopup { width: 800px; max-width: 96%; }
.SelectImgPopup .dropzone .dz-preview {
    position: relative;
    display: inline-block;
    vertical-align: top;
    margin: 4px 5px 20px 5px;
    min-height: 100px;
}
input.ImgPrePopupSelect {
    position: absolute;
    bottom: -15px;
    left: 50%;
    margin: 0 0 0 -7px;
}
.SelectImgPopup .modal-header, .SelectImgPopup .modal-footer {
    padding: 10px 15px;
    border-bottom: 1px solid #e5e5e5;
}
.SelectImgPopup .dropzone {
    padding: 10px 15px;
}
.SelectImgPopup .dropzone .dz-preview .dz-image {
    width: 105px;
    height: 105px;
}
.mar-b10 { margin-bottom: 10px; }
#div_profile_image { margin-top:10px; }
#div_profile_image img { display: inline-block; border-radius: 5px; margin-left: 5px; margin-bottom: 5px; }
</style>

<?php #$role_title_array = get_config_data('role_title_array'); prd($role_title_array); ?>
<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url('admin/dashboard') ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">
        CMS
        <!-- <img src="<?php echo base_url();?>assets/smalllogo.png"/> -->
    </span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>CMS </b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
     <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <!-- <a target="_blank" href="<?php echo base_url() ?>" class="logo">
      mini logo for sidebar mini 50x50 pixels
      <span class="logo-mini"><img src="<?php echo base_url();?>assets/view.png"/></span>
      logo for regular state and mobile devices
      <span class="logo-lg"><b>View </b>Site</span>
          </a> -->

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
         <?php $admin_session_data = $this->session->userdata("admin_session_data");	$role_id = $admin_session_data['role_id']; ?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url(); ?>assets/lte/dist/img/user2-160x160.jpg" class="user-image" alt="User Image" />
              <span class="hidden-xs"><?php echo $role_title_array = get_config_data('role_title_array'); #echo isset($role_title_array)?$role_title_array[$role_id]:''; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url(); ?>assets/lte/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />

                <p>
                  Administrator
                  <!-- <small>Member since April 2017</small> -->
                </p>
              </li>


              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url('admin/Change_password/');?>" class="btn btn-default btn-flat"> Change password </a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url('admin/logout');?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>

            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>
    </nav>
  </header>