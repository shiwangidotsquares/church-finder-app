<!DOCTYPE html>

<html>

<head>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>Activation</title>

	<!-- Tell the browser to be responsive to screen width -->

	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />

	<!-- Bootstrap 3.3.6 -->

	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/lte/bootstrap/css/bootstrap.min.css" />

	<!-- Font Awesome -->

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" />

	<!-- Ionicons -->

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" />

	<!-- Theme style -->

	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/lte/dist/css/AdminLTE.min.css" />

	<!-- iCheck -->

	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/lte/plugins/iCheck/square/blue.css" />

	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css" />

	<link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">

	<link rel="icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">

</head>

<body class="hold-transition login-page">

	<div class="login-box">

		<div class="login-logo">

			<a href="<?php echo base_url(); ?>">
				<div class="imgDiv">
					<img src="<?php echo base_url(); ?>assets/admin/img/logo1.png" width="127" >
				</div>
			</a>

		</div>

		<!-- /.login-logo -->

		<div class="login-box-body">

			<h3 class="login-box-msg"><?php echo $msg_title; ?></h3>

			<div class="<?php echo $msg_class; ?>"><?php echo $message; ?></div>

		</div>

		<!-- /.login-box-body -->

	</div>

	<!-- /.login-box -->

	

</body>

</html>





