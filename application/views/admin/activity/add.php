<style>
.form-error{
color: red;
}
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
            </div>
            <form role="form" method="post" id="frm-event">
                <div class="box-body">
                 <!--  <div class="form-group">
                    <label for="mass_activity_name" class="required"> Mass Activity Name <span style="color: red;"> &nbsp;*</span></label>
                    <input type="text" class="form-control" id="mass_activity_name" name="mass_activity_name" placeholder="Enter Mass Activity Name" value="<?php echo set_value('mass_activity_name'); ?>">
                    <span class="form-error"><?php echo form_error('mass_activity_name'); ?></span>
                  </div> -->

                   <div class="form-group">
                    <label for="mass_activity_name" class="required">Mass Activity Name <span style="color: red;"> &nbsp;*</span></label>
                    <select class="form-control"  name="mass_activity_name" id="mass_activity_name">
                        <option value="" selected="selected">--Select--</option>
                        <?php if (!empty($business_directories_category_array)) { ?>
                        <?php foreach($business_directories_category_array as $val){?>
                        <option value="<?php echo $val['parish_name'];?>"><?php echo $val['parish_name'];?></option>
                        <?php }} ?>
                    </select>
                    <span class="form-error"><?php echo form_error('parishes_location'); ?></span>
                </div>


        
                  <div class="form-group">
                    <label for="mass_activity_date" class="required">Mass Date <span style="color: red;"> &nbsp;*</span></label>
                    <input type="text" class="form-control"  onkeydown="return false;"  id="mass_activity_date" name="mass_activity_date" placeholder="" value="<?php echo set_value('mass_activity_date'); ?>">
                    <span class="form-error"><?php echo form_error('mass_activity_date'); ?></span>
                  </div>
                
                  <div class="form-group">
                    <label for="opening_time" class="required">Mass Time<span style="color: red;"> &nbsp;*</span></label>
                    <input type="text" class="form-control"  onkeydown="return false;"  id="opening_time" name="mass_activity_time" placeholder="" value="<?php echo set_value('opening_time'); ?>">
                    <span class="form-error"><?php echo form_error('opening_time'); ?></span>
                  </div> 


                  <div class="form-group">
                    <label for="mass_activity_description">Mass Activity Description<span style="color: red;"> &nbsp;*</span></label>
                    <?php  $this->ckeditor->editor('mass_activity_description');?>
                     <span class="form-error"><?php echo form_error('mass_activity_description'); ?></span>
                  </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="<?php echo base_url('admin/activity/index');?>"> <button type="button" class="btn btn-white">Cancel</button></a>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog SelectImgPopup">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Select Media</h4>
    </div>
    <div class="modal-body">

        <div class="dropzone dropzone-list-view">
            <?php

            $files1 = scandir('uploads/mediamanager/');

            unset($files1[0]);
            unset($files1[1]);

            ?>
            <?php
            $count= 1;
            foreach($files1 as $files){
              $file_array=explode('.',$files);
              $path= 'uploads/mediamanager/'.$files;
              $docpath= 'assets/img/doc-icon.png';
              $pdfpath= 'assets/img/pdficon.png';
              $videopath= 'assets/img/videoicon.png';
              $audiopath= 'assets/img/audioicon.jpg';
              $xlsxpath= 'assets/img/xlsxicon.png';
              if($file_array[1]=="jpg" || $file_array[1]=="jpeg" || $file_array[1]=="png" || $file_array[1]=="JPG" || $file_array[1]=="jpeg" || $file_array[1]=="png"){
                 ?>

                 <div class="dz-preview dz-image-preview dz-processing dz-success dz-complete">
                   <input type="checkbox" name="user_profile_image[]" value="<?php echo ($files)?$files:""; ?>" id="imgCheckbox<?php echo $count; ?>" class="ImgPrePopupSelect" >
                   <label for="imgCheckbox<?php echo $count; ?>" >
                      <div class="dz-image">
                        <a href="javascript:void(0)" target="_blank">  <img src="<?php echo base_url().$path; ?>" width="100%" height="100%">
                        </a>
                </div>
                    </label>
            </div>
            <?php
        }
        $count++; }
        ?>

    </div>


</div>
<div class="modal-footer">
  <form id="frm-multiimage" enctype="multipart/form-data">
    <input type="file" class="form-control mar-b10" id="user_profile_image" name="user_profile_image[]" multiple>
  </form>
    <a href="JavaScript:void(0);" class="btn btn-primary uploadMedia" >Done</a>
    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
</div>
</div>

</div>
</div>
<script type="text/javascript">
  $(document).on("change", "#page_type", function(){
    var val = $(this).val();
    jQuery('.cld').css('display','none');
    jQuery("#clDiv"+val).show('slow');
    jQuery('.ele').attr('disabled','true');
    jQuery('#pageLink'+val).removeAttr('disabled');
    if(val == 1){
      var menuTitle = jQuery('#menu_title').val();
      var trimMenu = menuTitle.replace(/[^A-Z0-9]+/ig, '-');
      $("#pageLink1").val(trimMenu).change();
    }
  });
  $(document).ready(function(){
    $(document).on("keyup",'#menu_title',function(){
      var menu_title = $("#menu_title").val();
            menu_title = menu_title.toLowerCase();
            //menu_title = menu_title.replace('/\s/g','-');
      var trimMenu = menu_title.replace(/[^A-Z0-9]+/ig, '-');
      $("#pageLink1").val(trimMenu);
    });

        $(document).on("change",'#pageLink1',function(){
      var menu_title = $(this).val();
                        menu_title = menu_title.toLowerCase();
                        var trimMenu = menu_title.replace(/[^A-Z0-9]+/ig, '-');
      $(this).val(trimMenu);
    });
  });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
  <script>

  $(function() {
    $( "#mass_activity_date" ).datepicker({
      format: 'yyyy-mm-dd'
    });

  });
  </script>
  <script>
 $('#user_profile_image').change(function(){
    var files = $(this)[0].files;
    if(files.length > 0){
        $("input[type='checkbox']").prop('checked', false);
    }
});
 $(document).on('click', '.uploadMedia', function(event) {
    var form = $('#frm-multiimage').get(0);
    var form_data = new FormData(form);
    var length = jQuery('#user_profile_image').prop('files').length;
    var file_data = jQuery('#user_profile_image').prop('files')[0];

    if(file_data){

        jQuery.ajax({
            url: '<?php echo base_url();?>admin/organisation/upload_multi_media',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (data) {
              $('#hidden_profile_image').val(data);
              var arr = data.split(',');
              var length = arr.length;
              var html = "";
              for (var i = 0; i < length; i++) {
                html+='<img src="<?php echo base_url()."uploads/mediamanager/"?>'+arr[i]+'" class="img-responsive" width="100px">';
              }
              $('#div_profile_image').html(html);
              $('#myModal').modal('hide');

            }
        });
    }else{
      var profile_image = [];
      $("input:checkbox:checked").each(function(){
          profile_image.push($(this).val());
      });

      profile_image = jQuery.grep(profile_image, function(value) {
        return value != "on";
    });


      profile_image.join(",");

      var html = "";
      $("input:checkbox:checked").each(function(){
        if($(this).val()!="on"){
          html+='<img src="<?php echo base_url()."uploads/mediamanager/"?>'+$(this).val()+'" class="img-responsive" width="100px">';
        }
      });

      $('#hidden_profile_image').val(profile_image);
      $('#div_profile_image').html(html);
      $('#myModal').modal('hide');

      }

    });
</script>
<script>

    $( document ).ready(function() {
        jQuery("#frm-event").validate({

            rules: {
                mass_activity_name:{
                    required: true,
                },
                mass_activity_date:{
                    required: true,
                },
                mass_activity_time: {
                    required: true
                },
                mass_activity_description: {
                    required: true
                }
            },
            messages: {

              mass_activity_name: {
      required: "The Mass Activity Name field is required.",

      },
      mass_activity_date: {
      required: "The Mass Activity Date field is required.",

  },
  mass_activity_time: {
      required: "The Mass Activity Time field is required.",

  },
  mass_activity_description: {
      required: "The Mass Activity Description field is required.",

  }
            }
        });

    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>
<!--JavaScript -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<style type="text/css">
label.error{
color:red;
font-weight: normal;
font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
}
</style>



<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#opening_time').timepicker({});
        $('#closing_time').timepicker({});
    });

</script>