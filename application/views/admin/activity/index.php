<div class="row">
  <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
            </div>
            <div class="row-fluid">
                <div class="col-lg-3">
                    <a href="<?php echo base_url('admin/activity/add');?>" class="btn btn-primary ">Add Mass Activity</a>
                </div>
                <div class="col-lg-3 ">
                </div>
                <div class="col-lg-6 ">
                    <div class="filter">
                        <label>Search Keyword:</label>
                        <input class="form-control" type="text" id="searchBox" name="searchBox" />
                        <input type="button" value="Search" id="searchButton" class="btn btn-default blue-bg" />
                        <input type="reset" value="Reset" id="reSet" class="btn btn-default orange-bg dis-inline width-auto" />
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

        <table class="table table-striped table-bordered table-hover jambo_table" id="editable" >
          <thead>
            <tr class="headings">
                <th>sr_no</th>
                <th> Mass Activity Name</th>
                <th> Mass Activity Date</th>
                <th> Mass Activity Time</th>
                <th> Mass Activity Description</th>
               <th> Mass Activity Created Time </th>
                <th> Mass Activity Updated Time </th>
                <th>Action</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


   <script type="text/javascript">
    var oTable;
    $(document).ready(function() {
        oTable= $('#editable').dataTable({
             "processing": true,
             "serverSide": true,
             "fnDrawCallback" : function(oSettings){$('#editable').tooltip();},
             "ajax": {
                 "url": "<?php echo base_url('admin/activity/get_activity'); ?>",
                 "type": "POST",
                 "data": function ( d ){
                    d.myKey = "myValue";
                    d.searchBox = $('#searchBox').val();
                 }
             },
             "bFilter": false,
             "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,7 ] }],
             "order": [[ 1, "desc" ]],
             "aoColumns": [
                    { "data": "sr_no", "sClass": "text-center"},
                    { "data": "mass_activity_name" },
  					        { "data": "mass_activity_date" },
                    { "data": "mass_activity_time" },
                    { "data": "mass_activity_description" },
                    { "data": "mass_activity_description" },
                    { "data": "mass_activity_description" },
                    { "data": "action" }
             ]
        });

        $('#searchButton').click( function(){
            oTable.fnDraw();
        });

        $('#searchBox').keydown(function (e){
            if(e.keyCode == 13){
                oTable.fnDraw();
            }
        });

        $('#facility_id').change( function(){
            oTable.fnDraw();
        });

        $('#coach_type').change( function(){
            oTable.fnDraw();
        });

        $('#pStatus').change( function(){
            oTable.fnDraw();
        });

        $('#reSet').click( function(){
            $('#company_id').val($('#company_id option[selected]').val());
            $('#facility_id').empty();
            $('#facility_id').append('<option value="0" selected="selected">-Select-</option>');
            $('#pStatus').val($('#pStatus option[selected]').val());
            $('#searchBox').val('');
            oTable.fnDraw();
        });
    });



    jQuery("#editable" ).on("click", ".activeRecord", function(){
        var sID= jQuery(this).attr('rel');
        var button=jQuery(this);
        $.ajax({
            url: '<?php echo site_url('admin/activity/status'); ?>',
            type: 'post',
            dataType: 'json',
            data: {sID:sID, sStatus:0},
            success: function(data){
                button.removeClass('activeRecord').addClass('deactiveRecord');
                button.removeClass('fa fa-check-square').addClass('fa fa-ban');
                button.attr('title','active');
                $('#editable').tooltip();
                swal("", "Mass Activity has deactivated successfully!", "success");
                oTable.fnDraw();
            },
            error: function(){
                swal("warning", "You Don't have permision to actived Mass activity", "error");
            }
        });
    });


    jQuery("#editable" ).on("click", ".deactiveRecord", function(){
        var sID= jQuery(this).attr('rel');
        var button=jQuery(this);
        $.ajax({
            url: '<?php echo site_url('admin/activity/status'); ?>',
            type: 'post',
            dataType: 'json',
            data: {sID:sID, sStatus:1},
            success: function(data){
                button.addClass('activeRecord').removeClass('deactiveRecord');
                button.removeClass('fa fa-ban').addClass('fa fa-check-square');
                button.attr('title','inactive');
                $('#editable').tooltip();
                swal("", "Mass Activity has activated successfully!", "success");
                oTable.fnDraw();
            },
            error: function(){
                swal("warning", "You Don't have permision to actived Mass Activity.", "error");
            }
        });
    });

    jQuery("#editable" ).on("click", ".deleteRecord", function(){
        var sID = jQuery(this).attr('rel');
        //alert(sID);
        swal({
          title: "Are you sure?",
          text: "You will not be able to recover this Mass Activity in future!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel plz!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {
                $.ajax({
                    url: '<?php echo site_url('admin/activity/delete'); ?>',
                    type: 'post',
                    dataType: 'json',
                    data: {sID:sID},
                    success: function(data){
                        swal("Deleted", "Mass activity has been deleted.", "success");
                        bResetDisplay = false;
                        /* override default behaviour */
                        oTable.fnDraw();
                        bResetDisplay = true;
                         /*restore default behaviour */
                    },
                    error: function(){
                        swal("warning", "You Don't have permision to actived Mass Activity.", "error");
                    }
                });
          } else {
                swal("Cancelled", "Mass Activity is safe now.", "error");
          }
        });
    });
    $('#searchBox').keydown(function (e){
        if(e.keyCode == 13){
            oTable.fnDraw();
        }
    });
</script>