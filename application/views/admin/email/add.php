<?php
    $router_class = $this->router->class;
    $router_function = $this->router->method;
?>
<style>
.form-error{
color: red;
}
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
            </div>
            <form role="form" method="post" enctype="multipart/form-data">
             	<div class="box-body">

				  <div class="form-group">
					<label for="company_user_id" class="required">Choose Company <span style="color: red;"> &nbsp;*</span></label>
					<select name="company_user_id" class="form-control">
						<option value="">---Please Select---</option>
						<option <?php if(isset($company_user_id) && $company_user_id == 1){ echo 'selected="true"';} ?> value="1">CMS Consultancy(Default)</option>
						<?php foreach($dataC as $val){ ?>
							<option <?php if(isset($company_user_id) && $company_user_id == $val['user_id']){ echo 'selected="true"';} ?> value="<?php echo $val['user_id']; ?>"><?php echo $val['company_name'].' ( '.$val['user_email'].' ) '; ?></option>
						   <?php } ?>
					</select>
					<span class="form-error"><?php echo form_error('company_user_id'); ?></span>
				  </div>

                  <div class="form-group">
                    <label for="tpl_name" class="required">Template Name<span style="color: red;"> &nbsp;*</span></label>
                    <input type="text" class="form-control" id="tpl_name" name="tpl_name" placeholder="Please Enter Template Name" value="<?php echo set_value('tpl_name',isset($tpl_name)?$tpl_name:''); ?>">
                    <span class="form-error"><?php echo form_error('tpl_name'); ?></span>
                  </div>

                  <?php
                   if($router_function == 'edit'){
                        echo form_hidden('id', $id?$id:0);
                   }
                  ?>
                  <div class="form-group">
                    <label for="subject" class="required">Subject<span style="color: red;"> &nbsp;*</span></label>
                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Please Enter Subject" value="<?php echo set_value('subject',isset($subject)?$subject:''); ?>">
                    <span class="form-error"><?php echo form_error('subject'); ?></span>
                  </div>


                  <div class="form-group">
                    <label for="body">Email Body<span style="color: red;"> &nbsp;*</span></label>
                    <?php  $this->ckeditor->editor('body',isset($body)?$body:'');?>
                  <span class="form-error"><?php echo form_error('body'); ?></span>
                  </div>
			  </div><!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="<?php echo base_url('admin/email/index');?>"> <button type="button" class="btn btn-white">Cancel</button></a>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

