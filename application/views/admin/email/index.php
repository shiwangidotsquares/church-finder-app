<div class="row">
  <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
            </div>
            <div class="row">
                <div class="col-lg-3">
					<label>User Type:</label>
					<select name="company_user_id" id="company_user_id" class="form-control">
						<option value="">---Please Select---</option>
						<option <?php if(isset($company_user_id) && $company_user_id == 1){ echo 'selected="true"';} ?> value="1">CMS Consultancy(Default)</option>
						<?php foreach($dataC as $val){ ?>
							<option <?php if(isset($company_user_id) && $company_user_id == $val['user_id']){ echo 'selected="true"';} ?> value="<?php echo $val['user_id']; ?>"><?php echo $val['company_name'].' ( '.$val['user_email'].' ) '; ?></option>
						   <?php } ?>
					</select>
                </div>
                <div class="col-lg-3 ">
                </div>
                <div class="col-lg-6 ">
                    <div class="filter">
                        <label>Search Keyword:</label>
                        <input class="form-control" type="text" id="searchBox" name="searchBox" />
                        <input type="button" value="Search" id="searchButton" class="btn btn-default blue-bg" />
                        <input type="reset" value="Reset" id="reSet" class="btn btn-default orange-bg dis-inline width-auto" />
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

        <table class="table table-striped table-bordered table-hover jambo_table" id="email_manager" >
          <thead>
            <tr class="headings">
                <th>#</th>
				<th>ID</th>
                <th>Name</th>
                <th>Subject</th>
                <th>Actions</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


   <script type="text/javascript">
    var oTable;
    $(document).ready(function() {
        oTable= $('#email_manager').dataTable({
             "processing": true,
             "serverSide": true,
             "fnDrawCallback" : function(oSettings){$('#email_manager').tooltip();},
             "ajax": {
                 "url": "<?php echo base_url('admin/email/get_all'); ?>",
                 "type": "POST",
                 "data": function ( d ){
                    d.myKey = "myValue";
                    d.searchBox = $('#searchBox').val();
					d.company_user_id = $('#company_user_id').val();
                 }
             },
             "bFilter": false,
             "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,4 ] }],
             "order": [[ 1, "desc" ]],
             "aoColumns": [
                    { "data": "sr_no", "sClass": "text-center"},
					{ "data": "tpl_id" },
                    { "data": "tpl_name" },
                    { "data": "subject" },
                    { "data": "action" }
             ]
        });

		$('#company_user_id').change( function(){
            oTable.fnDraw();
        });

        $('#searchButton').click( function(){
            oTable.fnDraw();
        });

        $('#searchBox').keydown(function (e){
            if(e.keyCode == 13){
                oTable.fnDraw();
            }
        });
        $('#reSet').click( function(){
            $('#company_id').val($('#company_id option[selected]').val());
            $('#facility_id').empty();
            $('#facility_id').append('<option value="0" selected="selected">-Select-</option>');
            $('#pStatus').val($('#pStatus option[selected]').val());
            $('#searchBox').val('');
            oTable.fnDraw();
        });

    });

</script>