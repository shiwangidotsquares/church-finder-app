<?php
$router_class = $this->router->class;
$router_function = $this->router->method;
?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $page_title; ?></h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active"><?php echo $form_title; ?></li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title"><?php if(!empty($form_title)){ echo $form_title;} ?></h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->

                        <div id="collapseOne" class="body">
                            <?php echo form_open_multipart('',array('name' => 'email_form', 'id' => 'email_form', 'class' => 'form-horizontal'));?>

                            <?php
                             if($router_function == 'edit'){
                                  echo form_hidden('id', $id?$id:0);
                             }
                            ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <?php
                                        $attributes = array();
                                        echo form_label('Template Name', 'tpl_name', $attributes);
                                        $title_array = array(
                                          'name'        => 'tpl_name',
                                          'id'          => 'tpl_name',
                                          'type'        => 'text',
                                          'value'       => set_value('tpl_name',$tpl_name),
                                          'maxlength'   => '80',
                                          'class'       => 'form-control'
                                        );
                                        echo form_input($title_array); ?>
                                        <label class="control-label has-error" for="fname"><?php echo form_error('tpl_name'); ?></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <?php
                                         echo form_label('Subject', 'subject', $attributes);
                                          $subject_array = array(
                                            'name'        => 'subject',
                                            'id'          => 'subject',
                                            'type'        => 'text',
                                            'value'       => set_value('subject',$subject),
                                            'maxlength'   => '80',
                                            'class'       => 'form-control'
                                          );
                                      echo form_input($subject_array); ?>
                                        <label class="control-label has-error" for="fname"><?php echo form_error('subject'); ?></label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-12">
                                       <?php
                                        $attributes = array();
                                        echo form_label('Email Body', 'body', $attributes);
                                        ?>
                                        <textarea id="editor1" name="body" rows="10" cols="80">
                                            <?php echo set_value('body',$body); ?>
                                        </textarea>
                                        <label class="control-label has-error" for="editor1"><?php echo form_error('body'); ?></label>
                                    </div>
                                </div>
                                <div class="form-actions no-margin-bottom">
                                    <?php
                                    $add_button_array = array(
                                        'name' => 'edit-page',
                                        'id' => 'edit-page',
                                        'value' => $button_text?$button_text:'Edit',
                                        'type' => 'submit',
                                        'content' => $button_text?$button_text:'Edit',
                                        'class' => 'btn btn-primary'
                                    );
                                    echo form_button($add_button_array);
                                    ?>
                                    <?php
                                    $cancel_url = 'admin/page';
                                    $add_button_array = array(
                                        'name' => 'cancel-edit-page',
                                        'id' => 'cancel-edit-page',
                                        'value' => 'Cancel',
                                        'type' => 'button',
                                        'content' => 'Cancel',
                                        'onclick' => "redirect('".$cancel_url."')",
                                        'class' => 'btn btn-primary'
                                    );
                                    echo form_button($add_button_array);
                                    ?>
                                </div></div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div><!-- /.col-lg-12 -->
                </div>
            </section>
        </aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<!-- jQuery 2.0.2 -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="<?php echo base_url();?>assets/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript">
    function redirect(url){

        window.location.href = '<?php echo base_url();?>'+url;

    }
</script>
<script type="text/javascript">
    $(function() {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1');
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();
    });
</script>