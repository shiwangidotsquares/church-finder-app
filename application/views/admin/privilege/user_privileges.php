<script type="text/javascript">
    $(document).ready(function(){
        $('a.module').trigger('click');
    });
    function show_actions(element)
    {
        var id = $(element).attr('id');
        var ul_id = 'ul-'+id;
        $('ul#'+ul_id).slideToggle('slow');
        /*$('ul.controller_actions').each(function(){
            if($(this).attr('id')!=ul_id)
                $(this).slideUp('slow');
        });*/
    }
    function check_action(element)
    {
        if( $(element).is(':checked'))
        {
            var data_value = $(element).attr('data-value');
            $(element).val(data_value);
        }
        else
        {
            $(element).val('');
        }
    }
</script>
<?php //prd($groups); ?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
            </div>
<div class="row-fluid">
<div class="box-body">
  <div class="col-lg-12">



        <div class="col-md-4">
            <div class="form-group">
          <label>Country:</label>
         <select class="form-control"  name="country_id" id="country_id">
           <option value="0" selected="selected">--Select--</option>
           <?php foreach($country_array as $key => $val){ ?>
           <option value="<?php echo $val['country_id']; ?>" ><?php echo ucfirst($val['country_name']); ?></option>
           <?php } ?>
         </select>
       </div>
         </div>

        <div class="col-md-4">
           <div class="form-group">
              <label>Roles:</label>
             <select class="form-control"  name="territory_roles" id="territory_roles">
               <option value="0" selected="selected">--Select--</option>
                    <?php foreach ($groups['data'] as $key => $value) {
                        if($value['role_id'] != 1 ){
                            echo "<option value=".$value['role_id']."> ".$value['role_title']." </  option>";
                        }

                    } ?>
             </select>
           </div>
        </div>

        <div class="col-md-4">
           <div class="form-group">
              <label>Users:</label>
             <select class="form-control"  name="territory_users" id="territory_users">
               <option value="0" selected="selected">--Select--</option>
             </select>
           </div>
        </div>


            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content form-class1 hide">



                    <form accept-charset="utf-8" method="post" enctype="multipart/form-data" id="privilege_form" name="privilege_form" action=""><div style="width:100%;">
                    <div class="show-Form">

                    </div>

                    </div>
                    <div style="margin-left:28px;clear:both;padding-top:4px;padding-bottom:4px;"></div>
                    <button class="btn btn-info" type="submit" style="cursor:pointer; margin:10px;" title="Add">Assign Privilege</button>

                    </form>


        </div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>



<script type="text/javascript">
  $('#country_id').change( function(){
        var country_id = parseInt(jQuery(this).val());
        $.ajax({
            url: '<?php echo site_url('admin/settings/country/get_territory_by_country_id'); ?>',
            type: 'post',
            dataType: 'json',
            data: {country_id:country_id},
            success: function(data){
                $('#territory_id').empty();
                $('#territory_id').append('<option value="0">-Select-</option>');
                for (var i = 0; i < data.length; i++){
                    $('#territory_id').append('<option value="' + data[i].territory_id + '">' + data[i].territory_name + '</option>');
                }
               // oTable.fnDraw();
            }
        });
    });

    $('#territory_roles').change( function(){
        var role_type = parseInt(jQuery(this).val());
        var country_id = parseInt(jQuery("#country_id").val());
        var territory_id = parseInt(jQuery("#territory_id").val());
        $.ajax({
            url: '<?php echo site_url('admin/settings/privileges/get_users_territory'); ?>',
            type: 'post',
            dataType: 'json',
            data: {role_type:role_type,country_id:country_id,territory_id:territory_id},
            success: function(data){
                $('#territory_users').empty();
                $('#territory_users').append('<option value="0">-Select-</option>');
                for (var i = 0; i < data.length; i++){
                    $('#territory_users').append('<option value="' + data[i].user_id + '" data-name="' + data[i].user_firstname + ' ' + data[i].user_lastname + '">' + data[i].user_firstname + ' ' + data[i].user_lastname + '</option>');
                }
               // oTable.fnDraw();
            }
        });

    });

    $('#territory_users').change( function(){
        var user_id = parseInt(jQuery(this).val());
        var user_name = jQuery(this).find(":selected").text();
        //alert(user_name);
        //alert(user_name);
        if(user_id != 0){
            $.ajax({
                url: '<?php echo site_url('admin/settings/privileges/get_users_privileges'); ?>',
                type: 'post',
                dataType: 'json',
                data: {user_id:user_id},

                success: function(data){
                    //console.log(data);
                    var innerHtml = '';
                    innerHtml += '<fieldset><div style="margin-left:12px;margin-top:12px; border:1px dashed #E3E3E3; background-color: #F3F3F3;"><legend style=" margin-left:12px;border-top:none; padding-top:17px;"><label><strong style="margin-top:5px;">'+user_name+'</strong></label></legend></div><div style="margin-left:12px;margin-top:12px; border:1px dashed #E3E3E3"><div class="form_sep"><label style="margin:7px"><b>Module Name</b></label>';
                    innerHtml += '<input type="hidden" name="user_id" value="'+user_id+'">';
                    for (var i = 0; i < data.length; i++){
                        //console.log(data[i].actions);
                        var actions =data[i].actions;
                        innerHtml +='<div style="margin-left:28px;clear:both;padding-top:4px;padding-bottom:4px;"><strong><a id="administration-'+data[i].controller_id+'" title="'+data[i].controller_name+'" onclick="show_actions(this);" href="javascript:void(0);" class="module">'+data[i].controller_name+'</a></strong></div>';

                        for(var a = 0; a < actions.length; a++){
                            innerHtml += '<ul id="ul-administration-'+data[i].controller_id+'" style="margin-left: 16px; clear: both; padding-top: 4px; padding-bottom: 4px; display: none;" class="controller_actions"> <li style="list-style:none;"><input type="checkbox" onchange="check_action(this);" data-value="'+data[i].controller_id+'_'+actions[a].id+'" value="'+data[i].controller_id+'_'+actions[a].id+'" name="privileges['+data[i].controller_id+']['+actions[a].id+']"';
                           // alert(actions[a].action_id);
                            if(actions[a].action_id  !== null){
                                innerHtml += 'checked="checked"';
                            }

                            innerHtml += '><label style="display:inline;"><strong>'+actions[a].action+'</strong></label></li></ul>';
                        }

                    }

                    innerHtml += '</div></div></div></fieldset>';

                    $(".form-class1").removeClass("hide");
                    $(".show-Form").html(innerHtml);

                }
            });
        }

    });
</script>