        
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
            </div>
                    <?php echo form_open_multipart('',array('name' => 'privilege_form', 'id' => 'block-validate', 'class' => 'form-horizontal'));?>
                      <div class="box-body">
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Select Controller</label>
                        <div class="col-sm-10">
                           <select name="controller_id" id="controller_id" class="form-control" >
                                <option>--Choose--</option>
                              <?php
								if(!empty($controllers)){
									foreach ($controllers as $controller){	?>
                              <option value="<?php echo $controller['controller_id'];?>" > <?php echo $controller['controller_alias'];?></option>
                              <?php } }?>
                           </select>
            			  <span class="required-server"><?php echo form_error('controller_id'); ?></span>                          
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Action Name</label>
                        <div class="col-sm-10">
                          	 <input type="text" id="action" name="action" value="<?php if(isset($action)){ echo $action; } ?>" class="form-control"  />
            			     <span class="required-server"><?php echo form_error('action'); ?> </span>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Alias Name</label>
                        <div class="col-sm-10">
                          <input type="text" id="alias" name="alias" value="<?php if(isset($alias)){ echo $alias; } ?>" class="form-control"/>
            			  <span class="required-server"><?php echo form_error('alias'); ?></span>
                        </div>
                      </div>
                      <div class="box-footer">
                        <input type="submit" value="<?php echo $buttonText?$buttonText:'Save'; ?>" class="btn btn-info pull-right" />
                      </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div><!-- /.col-lg-12 -->
</div>

 <script>
      $(function() {
        //Metis.formValidation();
        $('#block-validate').validate({
        rules: {
            controller_id: "required",
            action: {
                required: true,
            },
            alias: {
                required: true,
                
            }
        },
        errorClass: 'help-block',
        errorElement: 'span',
        highlight: function(element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass('has-error').addClass('has-success');
        }
    });
      });
 </script>


