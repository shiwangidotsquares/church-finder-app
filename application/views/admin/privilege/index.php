<script type="text/javascript">
    $(document).ready(function(){
		$('a.module').trigger('click');
    });
	function show_actions(element)
	{
		var id = $(element).attr('id');
		var ul_id = 'ul-'+id;
		$('ul#'+ul_id).slideToggle('slow');
		/*$('ul.controller_actions').each(function(){
			if($(this).attr('id')!=ul_id)
				$(this).slideUp('slow');
		});*/
	}
	function check_action(element)
	{
		if( $(element).is(':checked'))
		{
			var data_value = $(element).attr('data-value');
			$(element).val(data_value);
		}
		else
		{
			$(element).val('');
		}
	}
</script>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
            </div>
<div class="row-fluid">
<div class="box-body">
  <div class="col-lg-12">

    	<?php echo form_open_multipart('',array('name' => 'privilege_form', 'id' => 'privilege_form'));?>

					<?php
			  			foreach($groups as $group)
						{
					?>
							<fieldset>
								<div style="margin-left:12px;margin-top:12px; border:1px dashed #E3E3E3; background-color: #F3F3F3;">
									<legend style=" margin-left:12px;border-top:none; padding-top:17px;">
										<label><strong style="margin-top:5px;"><?php echo $group['role_title'];?></strong></label>
									</legend>
								</div>

								<div style="margin-left:12px;margin-top:12px; border:1px dashed #E3E3E3">
									<div class="form_sep">
                                        <label style="margin:7px"><b>Module Name</b></label>
								           <?php
									          foreach( $controllers as $controller_id => $controller){
								           ?>
										     <div  style="margin-left:28px;clear:both;padding-top:4px;padding-bottom:4px;">
											    <strong><a class="module" href="javascript:void(0);" onclick="show_actions(this);" title="<?php echo $controller['controller_alias'] ;?>"  id="<?php echo strtolower( preg_replace('/\s/','-',$group['role_title']));?>-<?php echo strtolower($controller['controller_id']);?>"><?php echo $controller['controller_alias'] ;?></a></strong>
										     </div>
										         <ul class="controller_actions" style="margin-left:16px;clear:both;padding-top:4px;padding-bottom:4px;" id="ul-<?php echo strtolower( preg_replace('/\s/','-',$group['role_title']));?>-<?php echo strtolower($controller['controller_id']);?>">
												<?php
													if( count($controller['actions'])>0)
													{
														foreach($controller['actions'] as $actions)
														{
															if( isset($assigned_privileges[$group['role_id']]) && is_array($assigned_privileges[$group['role_id']]) && count($assigned_privileges[$group['role_id']]) )
															{
																if( in_array($actions['id'],$assigned_privileges[$group['role_id']]) )
																{
																	$checked = "checked='checked'";
																}
																else
																{
																	$checked = "";
																}
															}
															else
															{
																$checked = "";
															}
												?>
															<li style="list-style:none;">
																<input  type="checkbox" <?php echo $checked; ?> name="privileges[<?php echo $group['role_id'];?>][]" value="<?php echo $controller['controller_id'];?>_<?php echo $actions['id'];?>" data-value="<?php echo $controller['controller_id'];?>_<?php echo $actions['id'];?>" onchange="check_action(this);"  />
																<label style="display:inline;">
																	<strong>
																		<?php echo $actions['alias'] ; ?>
																	</strong>
																</label>
															</li>
												<?php
														}
													}
													else
													{
														?>
															<li style="list-style:none;">
																<label style="display:inline; color:red;"><strong>No Action Found</strong></label>
															</li>
														<?php
													}
												?>
										</ul>
										<?php
									}
								?>
							</div>
								</div>
							</fieldset>



					<?php
						}
					?>
				<button title="Add" style="cursor:pointer; margin:10px;" type="submit" class="btn btn-info">
						Assign Privilege
				</button>
				</div>
				<?php echo form_close();?>

      </div>
    </div>
     </div>

</div>

