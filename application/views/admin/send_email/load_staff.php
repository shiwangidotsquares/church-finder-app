
  <label for="staff_list" class="required">Select Staff<span style="color: red;"> &nbsp;*</span></label>
  <select name="staff_list[]" id="staff_list_arr" multiple class="form-control" required>
    <?php foreach($staff_array as $val) { ?>
    <option value="<?php echo $val['user_id'] ?>"><?php echo $val['first_name'].' '.$val['last_name'] ?></option>
    <?php } ?>
</select>
<span class="form-error" id="langOpt14"><?php echo form_error('langOpt14'); ?></span>
<span class="form-error"><?php echo form_error('staff_list'); ?></span>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('#staff_list_arr').multiselect({
            columns: 1,
            placeholder: 'Select Users',
            search: true,
            selectAll: true
        });
    });
</script>