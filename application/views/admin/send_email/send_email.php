<link rel="stylesheet" href="<?php echo base_url();?>assets/css/chosen/chosen.min.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>assets/js/chosen/chosen.jquery.min.js"></script>
<style>
    .form-error{
        color: red;
    }
    label.error{
        color: red;
        font-weight: normal;
    }
</style>
<style>
    body { font-family:'Open Sans' Arial, Helvetica, sans-serif}
    ul,li { margin:2px; padding:0; list-style:none;}
    .label { color:#000; font-size:16px;}
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
          </div>
          <form id='email_form' role="form" method="post">
              <div class="box-body">
                <div class="form-group">
                    <label for="whom_user" class="required">Whom you want to send email?<span style="color: red;"> &nbsp;*</span></label>
                    <select name="whom_user" id="whom_user" class="form-control" required>
                      <option value="" selected>Please Select</option>
                      <option value="1">All Users</option>
                      <option value="2">Company Users</option>
                  </select>
                  <span class="form-error"><?php echo form_error('whom_user'); ?></span>
              </div>
              <div class="form-group" id="course_list">
                  <label for="course_user" class="required">Select Course<span style="color: red;"> &nbsp;*</span></label>
                  <select name="course_user" id="course_user" class="form-control" required>
                    <option value="" selected>Please Select</option>
                    <?php foreach($course_array as $val) { ?>
                    <option value="<?php echo $val['course_id'] ?>"><?php echo $val['course_name'] ?></option>
                    <?php } ?>
                </select>
                <span class="form-error"><?php echo form_error('course_user'); ?></span>
            </div>
            <div class="form-group" id="list_users_type">
              <div class="form-group">
                  <label for="list_users_type" class="required">Select User Type<span style="color: red;"> &nbsp;*</span></label>
                  <select name="list_users_type" id="users_type" class="form-control" required>
                    <option value="" selected>Please Select</option>
                    <option value="1">Organization</option>
                    <option value="2">Individual</option>
                </select>
                <span class="form-error"><?php echo form_error('list_users_type'); ?></span>
            </div>
        </div>
        <div class="form-group" id="user_list">
            <label for="langOpt3" class="required">Select Users<span style="color: red;"> &nbsp;*</span></label>
            <select name="userlist[]" class="form-control" multiple id="langOpt3"  required>
                <?php foreach($customers_array as $key=>$value) { ?>
                <option value="<?php echo $value['user_id'] ?>"><?php echo $value['first_name'].' '.$value['last_name'] ?></option>
                <?php } ?>
            </select>
            <span class="form-error" id="langOpt13"><?php echo form_error('langOpt3'); ?></span>
        </div>
        <div class="form-group" id="staff_list">

        </div>
        <div class="form-group" id="indvidual_list">
        </div>
        <div class="form-group">
            <label for="subject">Subject<span style="color: red;"> &nbsp;*</span></label>
            <input type="text" class="form-control" id="subject" name="subject" value="" required>
            <span class="form-error"><?php echo form_error('subject'); ?></span>
        </div>
        <div class="form-group">
            <label for="message_body">Message Body<span style="color: red;"> &nbsp;*</span></label>
            <?php  $this->ckeditor->editor('message_body');?>
            <span class="form-error"><?php echo form_error('message_body'); ?></span>
        </div>
    </div><!-- /.box-body -->

    <div class="box-footer">
        <button id="send_email" type="submit" class="btn btn-primary">Send</button>
        <!-- <div id="send_email" class="btn btn-primary">Send</div> -->
        <a href="<?php echo base_url('admin/newsletter_subscription/index');?>"> <button type="button" class="btn btn-white">Cancel</button></a>
    </div>
</form>
</div>
</div>
</div>
</div>
<link href="<?php echo base_url(); ?>assets/css/jquery.multiselect.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url(); ?>assets/js/jquery.multiselect.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        jQuery(".chosen").chosen({ width:"100%" });
    });
</script>
<script type="text/javascript">
    $(function () {
        $("#email_form").validate();

        $('#langOpt3').multiselect({
            columns: 1,
            placeholder: 'Select Users',
            search: true,
            selectAll: true
        });

        $('#userlist').multiselect({
            includeSelectAllOption: true,
            enablefiltering: true
        });
        $('#btnSelected').click(function () {
            var selected = $("#userlist option:selected");
            var message = "";
            selected.each(function () {
                message += $(this).text() + " " + $(this).val() + "\n";
            });
            alert(message);
        });
        $('#course_list').hide();
        $('#user_list').hide();
        $('#list_users_type').hide();
        $('#indvidual_list').hide();
        $(document).on('change', '#whom_user', function(event) {
            if($(this).val() == 2){
                $('#course_list').show();
                $('#user_list').hide();
                $('#list_users_type').hide();
            }else if($(this).val() == 1){
                $('#user_list').show();
                $('#course_list').hide();
                $('#list_users_type').hide();
                $('#staff_list').hide();
                $('#indvidual_list').hide();
            }else{
                $('#course_list').hide();
                $('#user_list').hide();
                $('#list_users_type').hide();
            }
        });

        $(document).on('change', '#course_user', function(event) {
            var course_user = $(this).val();
            if(course_user){
                $('#list_users_type').show();
            }else{
               $('#list_users_type').hide();
           }
       });
        $(document).on('change', '#users_type', function(event) {
            var list_users_type_id = $(this).val();
            if(list_users_type_id == 1){
                $.ajax({
                    url: '<?php echo site_url('admin/message/load_company_users'); ?>',
                    type: 'POST',
                    data: {company_id:list_users_type_id},
                    success: function(staff_list){
                        $('#indvidual_list').hide();
                        $('#staff_list').show();
                        $('#staff_list').html(staff_list);
                    }
                });
            }else{
                $.ajax({
                    url: '<?php echo site_url('admin/message/load_individual_type'); ?>',
                    type: 'POST',
                    success: function(indvidual_list){
                        $('#staff_list').hide();
                        $('#indvidual_list').show();
                        $('#indvidual_list').html(indvidual_list);
                    }
                });
            }
        });
        $(document).on('change', '#course_user1', function(event) {
         var course_id = $(this).val();
         $.ajax({
             url: '<?php echo site_url('admin/message/load_user_type'); ?>',
             type: 'POST',
             data: {course_id:course_id},
             success: function(list_users_type){
                 $('#list_users_type').html(list_users_type);
             }
             });

         });

        //click event of form submit type
        $(document).on('click', '#send_email', function(event) {

            var whom_user =$("#whom_user option:selected").val();

            if(whom_user == 1){
                if ($('#langOpt3 option:selected').length == 0){
                    $('#langOpt13').next("span").remove();
                    $('#langOpt13').after('<span class="form-error">The user list field is required !</span>');
                }else{
                    $('#langOpt13').next("span").remove();
                }
            }else if(whom_user == 2){
                var users_type =$('#users_type').find(":selected").val();
                if(users_type == 1){
                    if ($('#staff_list_arr option:selected').length == 0){
                        $('#langOpt14').next("span").remove();
                        $('#langOpt14').after('<span class="form-error">The Staff list field is required !</span>');
                    }else{
                        $('#langOpt14').next("span").remove();
                    }
                }else{

                    if ($('#individual_list_arr option:selected').length == 0){
                        $('#langOpt15').next("span").remove();
                        $('#langOpt15').after('<span class="form-error">The individual user list field is required !</span>');
                    }else{
                        $('#langOpt15').next("span").remove();
                    }
                }
            }else{
                $.ajax({
                    url: '<?php echo site_url('admin/message/send'); ?>',
                    type: 'POST',
                    data: form_data,
                    success: function(msg){
                     alert("Email Sent successfully!");
                        location.reload();
                     }
                });
            }

        });

    });
</script>