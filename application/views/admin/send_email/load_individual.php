
  <label for="individual_list" class="required">Select Individual User<span style="color: red;"> &nbsp;*</span></label>
  <select name="individual_list[]" id="individual_list_arr"  multiple class="form-control" required>
    <?php foreach($individual_array as $val) { ?>
    <option value="<?php echo $val['user_id'] ?>"><?php echo $val['first_name'].' '.$val['last_name'] ?></option>
    <?php } ?>
</select>
<span class="form-error" id="langOpt15"><?php echo form_error('langOpt15'); ?></span>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('#individual_list_arr').multiselect({
            columns: 1,
            placeholder: 'Select Users',
            search: true,
            selectAll: true
        });
    });
</script>