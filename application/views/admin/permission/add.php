<style>
	.form-error{
		color: red;
	}

</style>
<?php //print_r($categories);  die;?>
<div class="row">
	<div class="col-lg-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
			</div>
			<?php echo form_open_multipart('', array('name' => 'frm-subscribe', 'id' => 'frm-subscribe', 'autocomplete' => 'off')); ?>
			<div class="box-body">
				<div class="row" >
					<div class="col-lg-4">
						<!-- <div class="form-group">
							<label for="first_name" class="required">Organization Name <span style="color: red;"> &nbsp;*</span></label>
							<input type="text" maxlength="100" class="form-control" id="first_name" name="first_name" placeholder="Enter Name" value="<?php //echo set_value('first_name'); ?>">
							<span class="form-error"><?php //echo form_error('first_name'); ?></span>
						</div> -->


						<div class="form-group" id="divParkingOperative">
							<label for="search_user_data">Organization Name</label>
							<input type="text" autocomplete="off" id="search_user_data" value="" name="search_user_data" class="form-control magicsearch" placeholder="Search By organization" />
							<span class="form-error"><?php echo form_error('first_name'); ?></span>
						</div>

					</div>
					<div class="col-lg-4" id="showData"></div>
					<div class="col-lg-4" id="showresult"></div>
					<div class="col-lg-4" id="showcontrollerData"></div>
					<!-- <div class="col-lg-4">
						<div class="form-group">
							<label for="profile_image" class="required">User Assignment</label><br>
							<p><input type="checkbox" name="assignMethod[]" value="mastercategory" > <label>Category Manager</label></p>
							<p><input type="checkbox" name="assignMethod[]" value="payment" > <label>Payment Manager</label></p>
							<p><input type="checkbox" name="assignMethod[]" value="vendorpayment" ><label> Vendor Payment Manager</label></p>
						</div>
					</div> -->
				</div><!-- /.box-body -->

				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Save</button>
					<a href="<?php echo base_url('admin/subadmin/index');?>"> <button type="button" class="btn btn-white">Cancel</button></a>
				</div>
			</form>
		</div>
	</div>
</div>
</div>
<script>

	
$( document ).ready(function() {
		jQuery("#frm-subscribe").validate({

			rules: {
				search_user_data:{
					required: true,
				},
				'assignMethod[]': {
					required: true
					
				} 
			},

			messages: {
				search_user_data:{
					required: "The organization name is required.",
				},
				'assignMethod[]':{
					required: "The assign field is required.",
				}


			},


		});

	});


$(document).ready(function() {

	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>admin/permission/getOrganisationList",
		data:'',
		beforeSend: function(){											
		},
		success: function(data){
			var categ = [];
			var categ = jQuery.parseJSON(data);
										//console.log(categ);
					if(categ.length > 0){
						var dataSource1 = categ;
					}else{
						var dataSource1 ='';
					}
					$('#search_user_data').magicsearch(
					{
						dataSource: dataSource1,
						fields: ['user_username'],
						id: 'org_id',
						format: '%user_username%',
						focusShow: true,
						multiField: 'user_username',
						multiple: false,
						width:20,
						/*maxItem: 2,*/
						multiStyle: {
							space: 5,	
							width: 100
						},	

						success: function ($input, data) {
							 //console.log(data);

							
							var org_id = data.org_id;
							 //console.log(data);

							$.ajax({
								type: "POST",
								url: "<?php echo base_url(); ?>admin/permission/organizationSubscriptionData",
								data: {org_id:org_id},
								dataType: 'json',
								beforeSend: function(){											
								},
								success: function(data){
									  //console.log(POST[]);
									outputResult='';
									outputControllerResult='';
									if(data.Subscribe_Array){ 

									  organization_id='<input type="hidden" id="org_id" name="org_id" value="'+data.Subscribe_Array.org_id+'">'; 
									      
								      outputResult += '<label>Organization Name:-</label>'+data.Subscribe_Array.organisation_name+'</br><label>User Name:-</label>'+data.Subscribe_Array.user_username+'</br><label>Email:-</label>'+data.Subscribe_Array.user_email+'</br><label>Profile Image:-</label>'+data.Subscribe_Array.user_profile_image+''; 


								    }else{
								    	outputResult += '<div class="col-md-4"><p>NO RECORD FOUND !!</p></div>'; 
								    }
								    
									if(data.ControllerArray.length == 0){ 
										outputControllerResult += '<p>NO RECORD FOUND !!</p>'; 
									}else{
										  
										  console.log(data.ControllerArray_selected); 
										outputControllerResult +='<div class="form-group"><label for="profile_image" class="required">User Assignment</label><br>';
										
											$.each(data.ControllerArray, function(key,value) {
												var checked = "";
												if(jQuery.inArray(value.controller_id, data.ControllerArray_selected) !== -1){
													checked = "checked";
													
												}
												outputControllerResult +='<p><input type="checkbox" name="assignMethod[]" value="'+value.controller_id+'"'+checked +' > <label>'+value.controller_alias+'</label></p>';
											});	
											outputControllerResult +='</div>';
									}

									

									$("#showData").html(outputResult);
									$("#showresult").html(organization_id);
									$("#showcontrollerData").html(outputControllerResult);

								}
							});
							return true;
						},

						afterDelete: function($input, data) {
						},

					}
				);

			}
	});

});

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>
<!--JavaScript -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<style type="text/css">
	label.error{
		color:red;
		font-weight: normal;
		font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
	}
</style>