<style>
.form-error{
color: red;
}

</style>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
            </div>
                <?php echo form_open_multipart('', array('name' => 'frm-subscribe', 'id' => 'frm-subscribe', 'autocomplete' => 'off')); ?>
              <div class="box-body">
                 <div class="form-group">
                      <label for="user_profile_image" class="required">User Profile Image </label>
                      <input type="file" class="form-control" id="user_profile_image" name="user_profile_image" placeholder="User Profile Image" value="<?php echo set_value('user_profile_image',$user_profile_image); ?>">
                      <span class="form-error"><?php echo form_error('user_profile_image'); ?></span>
                    </div>

                <div class="form-group">
                  <label for="organisation_name" class="required">Organisation Name <span style="color: red;"> &nbsp;*</span></label>
                  <input type="text" maxlength="100" class="form-control" id="organisation_name" name="organisation_name" placeholder="Enter Organisation Name" value="<?php echo set_value('organisation_name',$organisation_name); ?>">
                  <span class="form-error"><?php echo form_error('organisation_name'); ?></span>
                </div>
                <input type="hidden" name="org_id" value="<?php echo set_value('org_id',$org_id); ?>">
                <!-- <div class="form-group">
                  <label for="last_name" class="required">Last Name <span style="color: red;"> &nbsp;*</span></label>
                  <input type="text" maxlength="100" class="form-control" id="last_name" name="last_name" placeholder="Enter Last Name" value="<?php echo set_value('last_name',$last_name); ?>">
                  <span class="form-error"><?php echo form_error('last_name'); ?></span>
                </div> -->

                <div class="form-group">
                  <label for="username">Username </label>
                  <input type="text" readonly maxlength="100" class="form-control" name="username" id="username" placeholder="Enter Username" value="<?php echo set_value('user_username',$user_username); ?>">
                    <span class="form-error"><?php echo form_error('username'); ?></span>
                </div>

                 <div class="form-group">
                  <label for="user_email">Email<span style="color: red;"> &nbsp;*</span></label>
                  <input type="text" maxlength="100" readonly class="form-control" name="user_email" id="user_email" placeholder="Enter Email" value="<?php echo set_value('user_email',$user_email); ?>">
                    <span class="form-error"><?php echo form_error('user_email'); ?></span>
                </div>

                </div><!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                <a href="<?php echo base_url('admin/organisation/index');?>"> <button type="button" class="btn btn-white">Cancel</button></a>
            </div>
        </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  /*$(document).on("change", "#page_type", function(){
    var val = $(this).val();
    jQuery('.cld').css('display','none');
    jQuery("#clDiv"+val).show('slow');
    jQuery('.ele').attr('disabled','true');
    jQuery('#pageLink'+val).removeAttr('disabled');
    if(val == 1){
      var menuTitle = jQuery('#menu_title').val();
      var trimMenu = menuTitle.replace(/[^A-Z0-9]+/ig, '-');
      $("#pageLink1").val(trimMenu).change();
    }
  });*/
  $(document).ready(function(){
    $(document).on("keyup",'#menu_title',function(){
      var menu_title = $("#menu_title").val();
            menu_title = menu_title.toLowerCase();
            //menu_title = menu_title.replace('/\s/g','-');
      var trimMenu = menu_title.replace(/[^A-Z0-9]+/ig, '-');
      $("#pageLink1").val(trimMenu);
    });

        $(document).on("change",'#pageLink1',function(){
      var menu_title = $(this).val();
                        menu_title = menu_title.toLowerCase();
                        var trimMenu = menu_title.replace(/[^A-Z0-9]+/ig, '-');
      $(this).val(trimMenu);
    });
  });
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>
<!--JavaScript -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<script>



// Login function

// forgot Password function

$( document ).ready(function() {
  jQuery("#frm-subscribe").validate({

    rules: {
      organisation_name:{
        required: true,
      },
     /* last_name:{
        required: true,
      },*/

      username:{
        required: true,
      },
      user_password:{
       minlength: 6,
      },
     confirm_password:{
        equalTo: "#user_password",
         minlength: 6,
      },
      phone_number:{
        required: true,
        number: true,
      maxlength: 10


      }
    },

    messages: {
      organisation_name:{
        required: "The Organisation field is required.",
      },
     /* last_name:{
        required: "The Last Name field is required.",
      },*/
      user_email: {
        required: "The Email field is required.",

      },
      username:{
        required: "The Username field is required.",
      },
   user_password:{
       required: "The Password field is required.",
      },
     confirm_password:{
        required: "The Confirm Password field is required.",
     },
      phone_number:{
        required: "The Phone Number field is required.",
        number: "The Phone Number should be a valid phone number.",

      }

    },


  });

  });
</script>

<style type="text/css">
label.error{
color:red;
font-weight: normal;
font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
}
</style>>