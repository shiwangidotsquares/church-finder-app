<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.min.css">
      <div class="row">
      	<div class="box-header">
      		<div class="col-lg-3">
      			<a href="<?php echo base_url('admin/user/index');?>" class="btn btn-primary ">Back</a>
      		</div>
      	</div>
      </div>
      <div class="row">
      	<?php  if(!empty($userData)){

      		extract($userData[0]);

      		?>
      		<div class="col-md-6">

      			<!-- Profile Image -->
      			<div class="box box-primary">
      				<div class="box-body box-profile">
      					<div class="box-header text-center">
      						<h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
      					</div>

      					<ul class="list-group list-group-unbordered">

      						<li class="list-group-item clearfix">
      							<b>Name</b> <span class="right-panel pull-right"><?php echo $first_name." ".$last_name;?></span>
      						</li>
      						
      						<li class="list-group-item clearfix">
      							<b>Email</b> <span class="right-panel pull-right"><?php echo $email;?></span>
      						</li>
      						<li class="list-group-item clearfix">
      							<b>Phone Number </b> <span class="right-panel pull-right"><?php echo $phone_number;?></span>
      						</li>
      						<li class="list-group-item clearfix">
      							<b>User Type</b> <span class="right-panel pull-right"><?php echo ($role_id == 2)?'Vender':'Reseller'?></span>
      						</li>
      						<li class="list-group-item clearfix">
      							<b>Last Login</b> <span class="right-panel pull-right"><?php echo $last_login;?></span>
      						</li>
      						<li class="list-group-item clearfix">
      							<b>Created</b> <span class="right-panel pull-right"><?php echo ($created_at)?date("d/m/Y", strtotime($created_at)):'';?></span>
      						</li>
      						<li class="list-group-item clearfix">
      							<b>Address</b> <span class="right-panel pull-right"><?php echo $address;?></span>
      						</li>
      					</ul>
      				</div>
      				<!-- /.box-body -->
      			</div>
      			<!-- /.col -->
      		</div>
      		<!-- /.row -->
      		<div class="col-md-6">
      			<!-- Profile Image -->
      			<div class="box box-primary">
      				<div class="box-body box-profile">
      					<div class="box-header text-center">
      						<h3 class="box-title">Company Details</h3>
      					</div>
      					<ul class="list-group list-group-unbordered">
      						<li class="list-group-item clearfix">
      							<b>Company Name </b> <span class="right-panel pull-right"><?php echo $company_name;?></span>
      						</li>
      						<li class="list-group-item clearfix">
      							<b>Account Number </b> <span class="right-panel pull-right"><?php echo $account_number;?></span>
      						</li>
      						<li class="list-group-item clearfix">
      							<b>Account Holder Name </b> <span class="right-panel pull-right"><?php echo $holder_name;?></span>
      						</li>
      						<li class="list-group-item clearfix">
      							<b>IFSC Code </b> <span class="right-panel pull-right"><?php echo $ifsc_code;?></span>
      						</li>

      					</ul>
      				</div>
      				<!-- /.box-body -->
      			</div>
      			<!-- /.box -->

      			<!-- /.col -->
      		</div>
      	</div>
      	<div class="row">
      	    <div class="col-lg-12">
      	        <div class="box box-primary">
      	            <!-- /.box-header -->
      	            <div class="box-body">
      	            	<div class="box-header">
      	            		<h3 class="box-title">User Profile Image</h3>
      	            	</div>
      	                <p> <?php if(isset($profile_image)){ ?>
      	                    <a class="example-image-link" href="<?php echo base_url();?>assets/admin/user_images/<?php echo $profile_image; ?>" data-lightbox="example-set"><img class="example-image" src="<?php echo base_url();?>assets/admin/user_images/<?php echo $profile_image; ?>" width='250' alt=""/></a>
      	                    <?php }else{  ?>
      	                    <p>No image found !</p>
      	                    <?php } ?>
      	                </p>
      	            </div>
      	        </div>
      	    </div>
      	</div>
      <?php  }else{ ?>
      	<div class="col-md-12">
      		<h3 class="profile-username text-center">No Records Found !!</h3>
      	</div>
      <?php  } ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.0/js/lightbox-plus-jquery.min.js"></script>
