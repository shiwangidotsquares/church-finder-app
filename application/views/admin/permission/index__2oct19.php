<div class="row">
	<div class="col-lg-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?>
				<span class="pull-right">
					<a href="<?php echo base_url('admin/subadmin/add');?>" class="btn btn-primary ">Add Sub Admin</a>
				</span>
			</h3>
		</div>
		<div class="row-fluid">

			<div class="pull-right">
				<div class="filter">
					<label>Search Keyword:</label>
					<input class="form-control" type="text" id="searchBox" name="searchBox" />
					<input type="button" value="Search" id="searchButton" class="btn btn-default blue-bg" />
					<input type="reset" value="Reset" id="reSet" class="btn btn-default orange-bg dis-inline width-auto" />
				</div>
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body">

			<table class="table table-striped table-bordered table-hover jambo_table" id="user_manager" >
				<thead>
					<tr class="headings">
						<th></th>
						<th>#</th>
						<th>Profile Image</th>
						<th>Name</th>
						<th>Email</th>
						<th>Mobile number</th>
						<th>Alternate number</th>
						<th>Location</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
	 <div class="col-lg-2 ">
        <button class="form-control" id="send_email">Send Bulk Email</button>
    </div>
    <div class="col-lg-2 ">
        <button class="form-control" id="send_sms">Send Bulk SMS</button>
    </div>
</div>
</div>
 <!-- Trigger/Open The Modal -->

<div class="modal fade" id="viewCatalog" role="dialog">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header" style="background:#0a4348;">
                <button type="button" id="closeAdd" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Message</h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <div class="admin-address-delivery">
                      
        <form action="" name="usersSendMail" method="post">
        <input type="hidden" id="usersEmail" name="usersEmail">
        <input type="hidden" id="sendType" name="sendType">
                <label>Message</label>
                <textarea class="form-control" name="message" id="message"></textarea>
                <input class="form-control" type="submit" value="Send" id="sendEmail" name="sendEmail">
      </form>
                  
        </div>
            </div>
            
          
        </div>
    </div>
</div>
<script type="text/javascript">
	var oTable;
	$(document).ready(function() {
		    $("#send_email").click(function(){
        		$('#viewCatalog').modal('show');
	      })
	      $("#send_sms").click(function(){
	        $('#viewCatalog').modal('show');
	      })
	        $("#send_email").click(function(){
	            var allEmail = '';
	            $(':checkbox:checked').each(function(i){
	              if(allEmail == ""){
	                allEmail = $(this).val()
	              } else {
	                        allEmail = allEmail+','+$(this).val();
	              }
	            });
	            var btn = document.getElementById("send_email");
	            $("#usersEmail").val(allEmail);
	            $("#sendType").val("email");
	    });

	    $("#send_sms").click(function(){
	            var allEmail = '';
	            $(':checkbox:checked').each(function(i){
	              if(allEmail == ""){
	                allEmail = $(this).attr('rel');
	              } else {
	                        allEmail = allEmail+','+$(this).attr('rel');
	              }
	            });
	            var btn = document.getElementById("send_sms");
	            $("#usersEmail").val(allEmail);
	            $("#sendType").val("sms");
	    });  

	    $( ".send_email" ).on( "click", function() {
	            var allEmail = $(this).attr('rel');
	            $("#usersEmail").val(allEmail);
	            $("#sendType").val("email");
	    });

	    $(".send_sms").on( "click", function() {
	            var allEmail = $(this).attr('rel');
	            $("#usersEmail").val(allEmail);
	            $("#sendType").val("sms");
	    }); 
		oTable= $('#user_manager').dataTable({
			"processing": true,
			"serverSide": true,
			"fnDrawCallback" : function(oSettings){$('#user_manager').tooltip();},
			"ajax": {
				"url": "<?php echo base_url('admin/subadmin/get_users'); ?>",
				"type": "POST",
				"data": function ( d ){
					d.myKey = "myValue";
					d.searchBox = $('#searchBox').val();
				}
			},
			"bFilter": false,
			"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 1,8 ] }],
			"order": [[ 3, "ASC" ]],
			"aoColumns": [
			{ "data": "chkbox"},
			{ "data": "sr_no", "sClass": "text-center"},
			{ "data": "profile_image" },
			{ "data": "first_name" },
			{ "data": "email" },
			{ "data": "phone_number" },
			{ "data": "alternate_number" },
			{ "data": "address" },
			{ "data": "action" }
			]
		});

		$('#searchButton').click( function(){
			oTable.fnDraw();
		});

		$('#searchBox').keydown(function (e){
			if(e.keyCode == 13){
				oTable.fnDraw();
			}
		});

		$('#facility_id').change( function(){
			oTable.fnDraw();
		});

		$('#coach_type').change( function(){
			oTable.fnDraw();
		});

		$('#pStatus').change( function(){
			oTable.fnDraw();
		});

		$('#reSet').click( function(){
			$('#company_id').val($('#company_id option[selected]').val());
			$('#facility_id').empty();
			$('#facility_id').append('<option value="0" selected="selected">-Select-</option>');
			$('#pStatus').val($('#pStatus option[selected]').val());
			$('#searchBox').val('');
			oTable.fnDraw();
		});
	});



	jQuery("#user_manager" ).on("click", ".activeRecord", function(){
		var sID= jQuery(this).attr('rel');
		var button=jQuery(this);
		$.ajax({
			url: '<?php echo site_url('admin/accounts/status'); ?>',
			type: 'post',
			dataType: 'json',
			data: {sID:sID, sStatus:0},
			success: function(data){
				button.removeClass('activeRecord').addClass('deactiveRecord');
				button.removeClass('fa fa-check-square').addClass('fa fa-ban');
				button.attr('title','active');
				$('#user_manager').tooltip();
				swal("", "User has been deactivated successfully!", "success");
				oTable.fnDraw();
			},
			error: function(){
				swal("warning", "Don't have permision to deactivate User", "error");
			}
		});
	});


	jQuery("#user_manager" ).on("click", ".deactiveRecord", function(){
		var sID= jQuery(this).attr('rel');
		var button=jQuery(this);
		$.ajax({
			url: '<?php echo site_url('admin/accounts/status'); ?>',
			type: 'post',
			dataType: 'json',
			data: {sID:sID, sStatus:1},
			success: function(data){
				button.addClass('activeRecord').removeClass('deactiveRecord');
				button.removeClass('fa fa-ban').addClass('fa fa-check-square');
				button.attr('title','inactive');
				$('#user_manager').tooltip();
				swal("", "User has been activated successfully!", "success");
				oTable.fnDraw();
			},
			error: function(){
				swal("warning", "Don't have permision to actived User.", "error");
			}
		});
	});

	jQuery("#user_manager" ).on("click", ".deleteRecord", function(){
		var sID = jQuery(this).attr('rel');
        //alert(sID);
        swal({
        	title: "Are you sure?",
        	text: "You will not be able to recover this user in future!",
        	type: "warning",
        	showCancelButton: true,
        	confirmButtonColor: "#DD6B55",
        	confirmButtonText: "Yes, delete it!",
        	cancelButtonText: "No, cancel plz!",
        	closeOnConfirm: false,
        	closeOnCancel: false
        },
        function(isConfirm){
        	if (isConfirm) {
        		$.ajax({
        			url: '<?php echo site_url('admin/accounts/delete'); ?>',
        			type: 'post',
        			dataType: 'json',
        			data: {sID:sID},
        			success: function(data){
        				swal("Deleted", "User has been deleted.", "success");
        				bResetDisplay = false;
        				/* override default behaviour */
        				oTable.fnDraw();
        				bResetDisplay = true;
        				/*restore default behaviour */
        			},
        			error: function(){
        				swal("warning", "Don't have permision to actived user.", "error");
        			}
        		});
        	} else {
        		swal("Cancelled", "User is safe now.", "error");
        	}
        });
    });
	$('#searchBox').keydown(function (e){
		if(e.keyCode == 13){
			oTable.fnDraw();
		}
	});
</script>