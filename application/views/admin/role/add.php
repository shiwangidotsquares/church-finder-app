<?php
$router_class = $this->router->class;
$router_function = $this->router->method;
?>
<style>
.form-error{
color: red;
}
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
            </div>
            <form role="form" method="post">
                <div class="box-body">
                    <div class="form-group">
                        <label for="role_title" class="required">Role title <span style="color: red;"> &nbsp;*</span></label>
                        <input type="text" class="form-control" id="role_title" name="role_title" placeholder="Enter title" value="<?php echo set_value('role_title',!empty($role_title)?$role_title:''); ?>">
                        <span class="form-error"><?php echo form_error('role_title'); ?></span>
                    </div>
                    <?php if($router_class == 'role' && $router_function == 'edit'){ ?>
                    <input type="hidden" name="edited_role_id" id="edited_role_id" value="<?php echo !empty($role_id)?$role_id:''; ?>"
                    <?php } ?>
                    <div class="form-group">
                        <label for="role_description">Meta keyword<span style="color: red;"> &nbsp;*</span></label>
                        <textarea name="role_description" id="role_description" class="form-control"><?php echo set_value('role_description', !empty($role_description)?$role_description:''); ?></textarea>
                        <span class="form-error"><?php echo form_error('role_description'); ?></span>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="<?php echo base_url('admin/role/index');?>"> <button type="button" class="btn btn-white">Cancel</button></a>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).on("change", "#page_type", function(){
		var val = $(this).val();
		jQuery('.cld').css('display','none');
		jQuery("#clDiv"+val).show('slow');
		jQuery('.ele').attr('disabled','true');
		jQuery('#pageLink'+val).removeAttr('disabled');
		if(val == 1){
			var menuTitle = jQuery('#menu_title').val();
			var trimMenu = menuTitle.replace(/[^A-Z0-9]+/ig, '-');
			$("#pageLink1").val(trimMenu).change();
		}
	});
	$(document).ready(function(){
		$(document).on("keyup",'#menu_title',function(){
			var menu_title = $("#menu_title").val();
            menu_title = menu_title.toLowerCase();
            //menu_title = menu_title.replace('/\s/g','-');
			var trimMenu = menu_title.replace(/[^A-Z0-9]+/ig, '-');
			$("#pageLink1").val(trimMenu);
		});

        $(document).on("change",'#pageLink1',function(){
			var menu_title = $(this).val();
                        menu_title = menu_title.toLowerCase();
                        var trimMenu = menu_title.replace(/[^A-Z0-9]+/ig, '-');
			$(this).val(trimMenu);
		});
	});
</script>