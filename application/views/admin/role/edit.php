<style>
.form-error{
color: red;
}
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
            </div>
            <form role="form" method="post">
             	  <div class="box-body">
					<div class="form-group">
					  <label for="page_title" class="required">Page title <span style="color: red;"> &nbsp;*</span></label>
					  <input type="text" class="form-control" id="page_title" name="page_title" placeholder="Enter title" value="<?php echo $page_title; ?>">
					  <span class="form-error"><?php echo form_error('page_title'); ?></span>
					</div>

					<div class="form-group">
					  <label for="page_position">Page Body<span style="color: red;"> &nbsp;*</span></label>
					  <?php //echo form_textarea(array('name' =>'page_desc','id'=>'page_desc','class'=>"ckeditor")); ?>
					  <?php  $this->ckeditor->editor('page_html',$page_html);?>
					<span class="form-error"><?php echo form_error('page_html'); ?></span>
					</div>


					<div class="form-group">
					  <label class="form_status" for="status">Include in menu<span style="color: red;"> &nbsp;*</span></label>
					 <label class="checkbox-inline" for="is_active_a">
					   <input type="radio" <?php if($is_include_menu==1){echo 'checked="checked"';} ?>name ="is_include" value="1">Yes
					 </label>
					 <label class="checkbox-inline" for="reg_chbox_b">
					   <input type="radio" <?php if($is_include_menu==0){echo 'checked="checked"';} ?> name = "is_include" value="0">No
					  </label>
					</div>

					<fieldset>
						<legend>SEO PURPOSE:</legend>
						<div class="form-group">
						  <label for="meta_title">Meta Title<span style="color: red;"> &nbsp;*</span></label>
						  <input type="text" class="form-control" name="meta_title" id="meta_title" placeholder="Enter menu title" value="<?php echo $meta_title; ?>">
							<span class="form-error"><?php echo form_error('meta_title'); ?></span>
						</div>

						<div class="form-group">
						  <label for="meta_key">Meta keyword<span style="color: red;"> &nbsp;*</span></label>
						  <textarea name="meta_key" id="meta_key" class="form-control"><?php echo $meta_keywords; ?></textarea>
						  <span class="form-error"><?php echo form_error('meta_key'); ?></span>
						</div>

						<div class="form-group">
						  <label for="meta_desc">Meta description<span style="color: red;"> &nbsp;*</span></label>
						  <textarea name="meta_desc" id="meta_desc" class="form-control"><?php echo $meta_description; ?></textarea>
						  <span class="form-error"><?php echo form_error('meta_desc'); ?></span>
						</div>
					</fieldset>

				  </div><!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="<?php echo base_url('admin/pages/index');?>"> <button type="button" class="btn btn-white">Cancel</button></a>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	/*$(document).on("change", "#page_type", function(){
		var val = $(this).val();
		jQuery('.cld').css('display','none');
		jQuery("#clDiv"+val).show('slow');
		jQuery('.ele').attr('disabled','true');
		jQuery('#pageLink'+val).removeAttr('disabled');
		if(val == 1){
			var menuTitle = jQuery('#menu_title').val();
			var trimMenu = menuTitle.replace(/[^A-Z0-9]+/ig, '-');
			$("#pageLink1").val(trimMenu).change();
		}
	});*/
	$(document).ready(function(){
		$(document).on("keyup",'#menu_title',function(){
			var menu_title = $("#menu_title").val();
            menu_title = menu_title.toLowerCase();
            //menu_title = menu_title.replace('/\s/g','-');
			var trimMenu = menu_title.replace(/[^A-Z0-9]+/ig, '-');
			$("#pageLink1").val(trimMenu);
		});

        $(document).on("change",'#pageLink1',function(){
			var menu_title = $(this).val();
                        menu_title = menu_title.toLowerCase();
                        var trimMenu = menu_title.replace(/[^A-Z0-9]+/ig, '-');
			$(this).val(trimMenu);
		});
	});
</script>