<?php
$router_class = $this->router->class;
$router_function = $this->router->method;
?>
<style>
.form-error{
    color: red;
}
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
          </div>
          <form role="form" method="post" id="frm-question" enctype="multipart/form-data">
            <div class="box-body">
              <div class="form-group">
                <label for="title" class="required">Title<span style="color: red;"> &nbsp;*</span></label>
                <input type="text" class="form-control" id="title" name="title" placeholder="Please Enter Title" value="<?php echo set_value('title',$title); ?>">
                <span class="form-error"><?php echo form_error('title'); ?></span>
            </div>

            <?php
            if($router_function == 'edit'){
                echo form_hidden('question_id', $question_id?$question_id:0);
            }
            ?>
            <div class="form-group">
              <label for="question_category" class="required">Category <span style="color: red;"> &nbsp;*</span></label>
              <select class="form-control"  name="question_category" id="question_category">
                  <option value="" selected="selected">--Select--</option>
                  <?php if (!empty($question_category_array)) { ?>
                  <?php foreach($question_category_array as $val){?>
                  <option value="<?php echo $val['category_id'];?>" <?php echo ($question_category==$val['category_id'])?"selected='true'":''?>><?php echo $val['category_name'];?></option>
                  <?php }} ?>
              </select>
              <span class="form-error"><?php echo form_error('question_category'); ?></span>
          </div>
          <div class="form-group">
            <label for="description">Description<span style="color: red;"> &nbsp;*</span></label>
            <?php  $this->ckeditor->editor('description',$description);?>
            <span class="form-error"><?php echo form_error('description'); ?></span>
        </div>
        <?php
        if($router_function == 'edit'){
        ?>
          <div class="form-group">
              <input type="hidden" id="ans-count" value="<?php echo count($data_answer); ?>">
              <div class="answer_block">
                <?php
                $count_label = 0;
                foreach ($data_answer as $key => $val) {
                  ?>
                    <div class="row">
                        <div class="col-lg-4">
                            <label for="answer"><?php echo ($count_label == 0)?'Answer':''; ?></label>
                            <input type="text" class="form-control" id="answer<?php echo $key; ?>" name="answer[<?php echo $key; ?>]" placeholder="Please Enter Answer" value="<?php echo $val['answer']  ?>">
                            <span class="form-error"><?php echo form_error('answer'); ?></span>
                        </div>

                        <div class="col-lg-4">
                          <label for="weightage"><?php echo ($count_label == 0)?'Weightage':''; ?></label>
                          <input type="text" class="form-control" id="weightage<?php echo $key; ?>" name="weightage[<?php echo $key; ?>]" placeholder="Please Enter Weightage" value="<?php echo $val['weightage']  ?>">
                          <span class="form-error"><?php echo form_error('weightage'); ?></span>
                      </div>
                      <div class="col-lg-4">
                          <label for="correct_answer"><?php echo ($count_label == 0)?'Correct Answer':''; ?></label><br />
                          <input type="radio" id="correct_answer" name="correct_answer" value="<?php echo $key; ?>" <?php echo (1==$val['is_correct'])?"checked='checked'":''?>>
                          <span class="form-error"><?php echo form_error('correct_answer'); ?></span>
                      </div>
                  </div>
                <?php $count_label++; } ?>

            </div>
            <a style="margin-top: 5px; float: right;" class="btn btn-primary add_more_radio" href="javascript:void(0)">Add More</a>
        </div>
        <?php }else{   ?>
        <div class="form-group">
            <input type="hidden" id="ans-count" value="1">
            <div class="answer_block">
                <div class="row">
                    <div class="col-lg-4">
                        <label for="answer">Answer<span style="color: red;"> &nbsp;*</span></label>
                        <input type="text" class="form-control" id="answer0" name="answer[]" placeholder="Please Enter Answer" value="<?php echo set_value('answer'); ?>">
                        <span class="form-error"><?php echo form_error('answer'); ?></span>
                    </div>

                    <div class="col-lg-4">
                      <label for="weightage">Weightage</label>
                      <input type="text" class="form-control" id="weightage0" name="weightage[]" placeholder="Please Enter Weightage" value="<?php echo set_value('weightage'); ?>">
                      <span class="form-error"><?php echo form_error('weightage'); ?></span>
                  </div>
                  <div class="col-lg-4">
                      <label for="correct_answer">Correct Answer</label><br />
                      <input type="radio" id="correct_answer" name="correct_answer" value="0">
                      <span class="form-error"><?php echo form_error('correct_answer'); ?></span>
                  </div>
              </div>
          </div>
          <a style="margin-top: 5px; float: right;" class="btn btn-primary add_more_radio" href="javascript:void(0)">Add More</a>
      </div>
      <?php }   ?>

  </div><!-- /.box-body -->

  <div class="box-footer">
    <button type="submit" class="btn btn-primary">Save</button>
    <a href="<?php echo base_url('admin/question/index');?>"> <button type="button" class="btn btn-white">Cancel</button></a>
</div>
</form>
</div>
</div>
</div>
</div>
<script>
    $(document).on('click', '.add_more_radio', function(event) {
        var count = $("#ans-count").val();
        var count1 = parseInt(count)+1;
        var htmlData = '<div class="row"><div class="col-lg-4"><label for="answer"></label><input type="text" class="form-control" id="answer'+count1+'" name="answer['+count+']" placeholder="Please Enter Answer" ></div><div class="col-lg-4"><label for="weightage"></label><input type="text" class="form-control" id="weightage'+count+'" name="weightage['+count+']" placeholder="Please Enter Weightage" ></div><div class="col-lg-4"><label for="correct_answer"></label><br /><input type="radio" id="correct_answer" name="correct_answer" value="'+count+'"></div></div>';
        $("#ans-count").val(count1);
        $(".answer_block").append(htmlData);
    });
</script>
<script>

    $( document ).ready(function() {
        jQuery("#frm-question").validate({
            ignore: [],
            rules: {
                title:{
                    required: true,
                },
                question_category:{
                    required: true,
                }
            },
            messages: {
                 title:{
                    required: "The Title field is required.",
                },
                question_category:{
                    required: "The Category field is required.",
                }
            }
        });

    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>
<!--JavaScript -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<style type="text/css">
label.error{
color:red;
font-weight: normal;
font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
}
</style>
