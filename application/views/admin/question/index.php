<div class="row">
  <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
            </div>
            <div class="row-fluid">
                <div class="col-lg-3">
                    <a href="<?php echo base_url('admin/question/add');?>" class="btn btn-primary ">Add Question</a>
                </div>
                <div class="col-lg-3 ">
                </div>
                <div class="col-lg-6 ">
                    <div class="filter">
                        <label>Search Keyword:</label>
                        <input class="form-control" type="text" id="searchBox" name="searchBox" />
                        <input type="button" value="Search" id="searchButton" class="btn btn-default blue-bg" />
                        <input type="reset" value="Reset" id="reSet" class="btn btn-default orange-bg dis-inline width-auto" />
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

        <table class="table table-striped table-bordered table-hover jambo_table" id="question_manager" >
          <thead>
            <tr class="headings">
                <th>#</th>
                <th>Title</th>
                <th>Description</th>
                <th>Created Date</th>
                <th>Actions</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


   <script type="text/javascript">
    var oTable;
    $(document).ready(function() {
        oTable= $('#question_manager').dataTable({
             "processing": true,
             "serverSide": true,
             "fnDrawCallback" : function(oSettings){$('#question_manager').tooltip();},
             "ajax": {
                 "url": "<?php echo base_url('admin/question/get_all'); ?>",
                 "type": "POST",
                 "data": function ( d ){
                    d.myKey = "myValue";
                    d.searchBox = $('#searchBox').val();
                 }
             },
             "bFilter": false,
             "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,4 ] }],
             "order": [[ 3, "desc" ]],
             "aoColumns": [
                    { "data": "sr_no", "sClass": "text-center"},
                    { "data": "question_title" },
                    { "data": "question_description" },
                    { "data": "question_created_date" },
                    { "data": "action" }
             ]
        });

        $('#searchButton').click( function(){
            oTable.fnDraw();
        });

        $('#searchBox').keydown(function (e){
            if(e.keyCode == 13){
                oTable.fnDraw();
            }
        });
        $('#reSet').click( function(){
            $('#company_id').val($('#company_id option[selected]').val());
            $('#facility_id').empty();
            $('#facility_id').append('<option value="0" selected="selected">-Select-</option>');
            $('#pStatus').val($('#pStatus option[selected]').val());
            $('#searchBox').val('');
            oTable.fnDraw();
        });
        jQuery("#question_manager" ).on("click", ".activeRecord", function(){
            var sID= jQuery(this).attr('rel');
            var button=jQuery(this);
            $.ajax({
                url: '<?php echo site_url('admin/question/status'); ?>',
                type: 'post',
                dataType: 'json',
                data: {sID:sID, sStatus:0},
                success: function(data){
                    button.removeClass('activeRecord').addClass('deactiveRecord');
                    button.removeClass('fa fa-check-square').addClass('fa fa-ban');
                    button.attr('title','active');
                    $('#question_manager').tooltip();
                    swal("", "Question has deactivated successfully!", "success");
                    oTable.fnDraw();
                },
                error: function(){
                    swal("warning", "Don't have permision to dactivate question", "error");
                }
            });
        });


        jQuery("#question_manager" ).on("click", ".deactiveRecord", function(){
            var sID= jQuery(this).attr('rel');
            var button=jQuery(this);
            $.ajax({
                url: '<?php echo site_url('admin/question/status'); ?>',
                type: 'post',
                dataType: 'json',
                data: {sID:sID, sStatus:1},
                success: function(data){
                    button.addClass('activeRecord').removeClass('deactiveRecord');
                    button.removeClass('fa fa-ban').addClass('fa fa-check-square');
                    button.attr('title','inactive');
                    $('#question_manager').tooltip();
                    swal("", "Question has activated successfully!", "success");
                    oTable.fnDraw();
                },
                error: function(){
                    swal("warning", "Don't have permision to actived question.", "error");
                }
            });
        });

        jQuery("#question_manager" ).on("click", ".deleteRecord", function(){
            var sID = jQuery(this).attr('rel');
            //alert(sID);
            swal({
              title: "Are you sure?",
              text: "You will not be able to recover this question in future!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel plz!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm){
              if (isConfirm) {
                    $.ajax({
                        url: '<?php echo site_url('admin/question/delete'); ?>',
                        type: 'post',
                        dataType: 'json',
                        data: {sID:sID},
                        success: function(data){
                            swal("Deleted", "question has been deleted.", "success");
                            bResetDisplay = false;
                            /* override default behaviour */
                            oTable.fnDraw();
                            bResetDisplay = true;
                             /*restore default behaviour */
                        },
                        error: function(){
                            swal("warning", "Don't have permision to deleted question.", "error");
                        }
                    });
              } else {
                    swal("Cancelled", "question is safe now.", "error");
              }
            });
        });

    });

</script>