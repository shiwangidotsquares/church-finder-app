<style>
.form-error{
  color: red;
}

</style>
<div class="row">
  <div class="col-lg-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
    </div>
        <?php echo form_open_multipart('', array('name' => 'frm-subscribe', 'id' => 'frm-subscribe', 'autocomplete' => 'off')); ?>
        <div class="box-body">
           
        <div class="form-group">
    <label for="user_password">Old Password<span style="color: red;"> &nbsp;*</span></label>
    <input type="password" class="form-control" name="old_password" id="old_password" placeholder="Enter old Password" value="<?php echo set_value('user_password'); ?>">
    <span class="form-error"><?php echo form_error('user_password'); ?></span>
</div>  
    
<div class="form-group">
    <label for="user_password"> New Password<span style="color: red;"> &nbsp;*</span></label>
    <input type="password" class="form-control" name="user_password" id="user_password" placeholder="Enter New Password" value="<?php echo set_value('user_password'); ?>">
    <span class="form-error"><?php echo form_error('user_password'); ?></span>
</div>

<div class="form-group">
    <label for="confirm_password">Confirm Password<span style="color: red;"> &nbsp;*</span></label>
    <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Enter Confirm Password" value="<?php echo set_value('confirm_password'); ?>">
    <span class="form-error"><?php echo form_error('confirm_password'); ?></span>
</div>


</div><!-- /.box-body -->

<div class="box-footer">
  <button type="submit" class="btn btn-primary">Save</button>
  <a href="<?php echo base_url('admin/user/index');?>"> <button type="button" class="btn btn-white">Cancel</button></a>
</div>
</form>
</div>
</div>
</div>
</div>


<script>

$( document ).ready(function() {
  jQuery("#frm-subscribe").validate({

    rules: {

      old_password:{
        required: true
        
    },
      
    user_password:{
        required: true
        
    },
    
    confirm_password:{
        required: true,
        equalTo: "#user_password"
    }
   
  
},

messages: {
   
 old_password:{
   required: "The Old Password field is required.",
},

  user_password:{
   required: "The New Password field is required.",
},
confirm_password:{
    required: "The Confirm Password field is required.",
},


},

});

});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>
<!--JavaScript -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<style type="text/css">

label.error{
color:red;
font-weight: normal;
font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
}
</style>