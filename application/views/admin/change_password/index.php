<div class="row">
  <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
            </div>
            <div class="row-fluid">
                <div class="col-lg-3">
                    <a href="<?php echo base_url('admin/user/add');?>" class="btn btn-primary ">Add Users</a>
                </div>
                <div class="col-lg-3 ">
                </div>
                <div class="col-lg-6 ">
                    <div class="filter">
                        <label>Search Keyword:</label>
                        <input class="form-control" type="text" id="searchBox" name="searchBox" />
                        <input type="button" value="Search" id="searchButton" class="btn btn-default blue-bg" />
                        <input type="reset" value="Reset" id="reSet" class="btn btn-default orange-bg dis-inline width-auto" />
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

        <table class="table table-striped table-bordered table-hover jambo_table" id="user_manager" >
          <thead>
            <tr class="headings">
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>Created</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


   <script type="text/javascript">
    var oTable;
    $(document).ready(function() {
        oTable= $('#user_manager').dataTable({
             "processing": true,
             "serverSide": true,
             "fnDrawCallback" : function(oSettings){$('#user_manager').tooltip();},
             "ajax": {
                 "url": "<?php echo base_url('admin/user/get_users'); ?>",
                 "type": "POST",
                 "data": function ( d ){
                    d.myKey = "myValue";
                    d.searchBox = $('#searchBox').val();
                 }
             },
             "bFilter": false,
             "aoColumnDefs": [{ "bSortable": false, "aTargets": [0,6] }],
             "order": [[ 1, "desc" ]],
             "aoColumns": [
                    { "data": "sr_no", "sClass": "text-center"},
                    { "data": "user_username" },
                    { "data": "user_email" },
                    { "data": "phone_number" },
  					        { "data": "created_time" },
  					        { "data": "user_is_active" },
                    { "data": "action" }
             ]
        });

        $('#searchButton').click( function(){
            oTable.fnDraw();
        });

        $('#searchBox').keydown(function (e){
            if(e.keyCode == 13){
                oTable.fnDraw();
            }
        });

        $('#facility_id').change( function(){
            oTable.fnDraw();
        });

        $('#coach_type').change( function(){
            oTable.fnDraw();
        });

        $('#pStatus').change( function(){
            oTable.fnDraw();
        });

        $('#reSet').click( function(){
            $('#company_id').val($('#company_id option[selected]').val());
            $('#facility_id').empty();
            $('#facility_id').append('<option value="0" selected="selected">-Select-</option>');
            $('#pStatus').val($('#pStatus option[selected]').val());
            $('#searchBox').val('');
            oTable.fnDraw();
        });
    });



    jQuery("#user_manager" ).on("click", ".activeRecord", function(){
        var sID= jQuery(this).attr('rel');
        var button=jQuery(this);
        $.ajax({
            url: '<?php echo site_url('admin/user/status'); ?>',
            type: 'post',
            dataType: 'json',
            data: {sID:sID, sStatus:0},
            success: function(data){
                button.removeClass('activeRecord').addClass('deactiveRecord');
                button.removeClass('fa fa-check-square').addClass('fa fa-ban');
                button.attr('title','active');
                $('#user_manager').tooltip();
                swal("", "User has Blocked successfully!", "success");
                oTable.fnDraw();
            },
            error: function(){
                swal("warning", "Don't have permision to blocked Page", "error");
            }
        });
    });


    jQuery("#user_manager" ).on("click", ".deactiveRecord", function(){
        var sID= jQuery(this).attr('rel');
        var button=jQuery(this);
        $.ajax({
            url: '<?php echo site_url('admin/user/status'); ?>',
            type: 'post',
            dataType: 'json',
            data: {sID:sID, sStatus:1},
            success: function(data){
                button.addClass('activeRecord').removeClass('deactiveRecord');
                button.removeClass('fa fa-ban').addClass('fa fa-check-square');
                button.attr('title','inactive');
                $('#user_manager').tooltip();
                swal("", "User has Unblocked successfully!", "success");
                oTable.fnDraw();
            },
            error: function(){
                swal("warning", "Don't have permision to unblocked Page.", "error");
            }
        });
    });

    jQuery("#user_manager" ).on("click", ".deleteRecord", function(){
        var sID = jQuery(this).attr('rel');
        //alert(sID);
        swal({
          title: "Are you sure?",
          text: "You will not be able to recover this user in future!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel plz!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {
                $.ajax({
                    url: '<?php echo site_url('admin/user/delete'); ?>',
                    type: 'post',
                    dataType: 'json',
                    data: {sID:sID},
                    success: function(data){
                        swal("Deleted", "User has been deleted.", "success");
                        bResetDisplay = false;
                        /* override default behaviour */
                        oTable.fnDraw();
                        bResetDisplay = true;
                         /*restore default behaviour */
                    },
                    error: function(){
                        swal("warning", "Don't have permision to actived user.", "error");
                    }
                });
          } else {
                swal("Cancelled", "User is safe now.", "error");
          }
        });
    });
    $('#searchBox').keydown(function (e){
        if(e.keyCode == 13){
            oTable.fnDraw();
        }
    });
</script>