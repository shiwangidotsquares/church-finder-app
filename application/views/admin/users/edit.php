<style>
.form-error{
  color: red;
}

</style>
<?php //print_r($categories);  die;?>
<div class="row">
  <div class="col-lg-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
    </div>
        <?php echo form_open_multipart('', array('name' => 'frm-subscribe', 'id' => 'frm-subscribe', 'autocomplete' => 'off')); ?>
        <div class="box-body">
           
           <div class="form-group">
    <label for="username">Username <span style="color: red;"> &nbsp;*</span></label>
    <input type="text" maxlength="100" class="form-control" name="username" id="username" placeholder="Enter Username" value="<?php echo set_value('username',$username); ?>">
    <span class="form-error"><?php echo form_error('username'); ?></span>
</div>

      <div class="form-group">
       <label for="user_email">Email<span style="color: red;"> &nbsp;*</span></label>
       <input type="text" maxlength="100" class="form-control" name="user_email" id="user_email" placeholder="Enter Email" value="<?php echo set_value('user_email',$user_email); ?>">
       <span class="form-error"><?php echo form_error('user_email'); ?></span>
   </div>

  
<div class="form-group">
    <label for="user_password">Password<span style="color: red;"> &nbsp;*</span></label>
    <input type="password" class="form-control" name="user_password" id="user_password" placeholder="Enter Password" value="">
    <span class="form-error"><?php echo form_error('user_password'); ?></span>
</div>

<div class="form-group">
    <label for="confirm_password">Confirm Password<span style="color: red;"> &nbsp;*</span></label>
    <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Enter Confirm Password" value="<?php echo set_value('confirm_password'); ?>">
    <span class="form-error"><?php echo form_error('confirm_password'); ?></span>
</div>



<div class="form-group">
    <label for="phone_number" class="required">Phone Number <span style="color: red;"> &nbsp;*</span></label>
    <input type="text" maxlength="100" class="form-control" id="phone_number" name="phone_number" placeholder="Enter Phone Number" value="<?php echo set_value('phone_number',$phone_number); ?>">
    <span class="form-error"><?php echo form_error('phone_number'); ?></span>
</div>

</div><!-- /.box-body -->

<div class="box-footer">
  <button type="submit" class="btn btn-primary">Save</button>
  <a href="<?php echo base_url('admin/user/index');?>"> <button type="button" class="btn btn-white">Cancel</button></a>
</div>
</form>
</div>
</div>
</div>
</div>


<script>



// Login function

// forgot Password function

$( document ).ready(function() {
  jQuery("#frm-subscribe").validate({

    rules: {
      organisation_name:{
        required: true,
    },
    /*last_name:{
        required: true,
    },*/
    user_email: {
        required: true,
        email: true
    } ,
    username:{
        required: true,
    },
    user_password:{
        required: true,
    },
    user_type:{
        required: true,
    },
    confirm_password:{
        required: true,
        equalTo: "#user_password"
    },
    phone_number:{
        required: true,
        number: true,
        maxlength: 10


    },
  
},

messages: {

  user_email: {
      required: "The Email field is required.",

  },
  username:{
      required: "The Username field is required.",
  },

  user_type:{
      required: "The User Type field is required.",
  },

  user_password:{
   required: "The Password field is required.",
},
confirm_password:{
    required: "The Confirm Password field is required.",
},
phone_number:{
    required: "The Phone Number field is required.",
    number: "The Phone Number should be a valid phone number.",

},



},


});

});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>
<!--JavaScript -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<style type="text/css">

label.error{
color:red;
font-weight: normal;
font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
}
</style>