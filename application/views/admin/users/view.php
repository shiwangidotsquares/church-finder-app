      <div class="row">
      <div class="box-header">
      <div class="col-lg-3">
          <a href="<?php echo base_url('admin/user/index');?>" class="btn btn-primary ">Back</a>
      </div>
      </div>
      </div>
      <div class="row">
      <?php  if(!empty($userData)){

      extract($userData[0]);

      ?>

        <div class="col-md-6">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
            <div class="box-header text-center">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
            </div>



              <ul class="list-group list-group-unbordered">

                <li class="list-group-item clearfix">
                  <b>Name</b> <span class="right-panel pull-right"><?php echo $first_name." ".$last_name;?></span>
                </li>
                 <!-- <li class="list-group-item clearfix">
                  <b>last Name</b> <span class="right-panel pull-right"><?php echo $last_name;?></span>
                                 </li> -->
                <li class="list-group-item clearfix">
                  <b>Username</b> <span class="right-panel pull-right"><?php echo $user_username;?></span>
                </li>


              </ul>


            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <div class="box box-primary">
            <div class="box-body box-profile">
            <div class="box-header text-center">
              <h3 class="box-title">Billing Details</h3>
            </div>



              <ul class="list-group list-group-unbordered">

                <li class="list-group-item clearfix">
                  <b>Billing State</b> <span class="right-panel pull-right"><?php echo $billing_state;?></span>
                </li>
                <li class="list-group-item clearfix">
                  <b>Billing Postcode</b> <span class="right-panel pull-right"><?php echo $billing_zipcode;?></span>
                </li>
                <li class="list-group-item clearfix">
                  <b>Billing Country</b> <span class="right-panel pull-right"><?php echo $country_name;?></span>
                </li>

              </ul>


            </div>
            <!-- /.box-body -->
          </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
        <div class="col-md-6">
            <!-- Profile Image -->
            <div class="box box-primary">
              <div class="box-body box-profile">
              <div class="box-header text-center">
                <h3 class="box-title">Additional Info</h3>
              </div>



                <ul class="list-group list-group-unbordered">
                <li class="list-group-item clearfix">
                  <b>Email</b> <span class="right-panel pull-right"><?php echo $user_email;?></span>
                </li>
                  <li class="list-group-item clearfix">
                    <b>Phone Number </b> <span class="right-panel pull-right"><?php echo $user_mobile_number;?></span>
                  </li>
                  <li class="list-group-item clearfix">
                    <b>Address Line 1</b> <span class="right-panel pull-right"><?php echo $address_line1;?></span>
                  </li>
                  <li class="list-group-item clearfix">
                    <b>Address Line 2</b> <span class="right-panel pull-right"><?php echo $address_line2;?></span>
                  </li>
                  <li class="list-group-item clearfix">
                    <b>User City</b> <span class="right-panel pull-right"><?php echo $city;?></span>
                </li>
                  <li class="list-group-item clearfix">
                      <b>Region</b> <span class="right-panel pull-right"><?php echo $region;?></span>
                  </li>
                  <li class="list-group-item clearfix">
                    <b>Postcode</b> <span class="right-panel pull-right"><?php echo $postcode;?></span>
                  </li>

                </ul>


              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->

          <!-- /.col -->
        </div>
      </div>
      <?php  }else{ ?>
      <div class="col-md-12">
            <h3 class="profile-username text-center">No Records Found !!</h3>
        </div>
    <?php  } ?>
