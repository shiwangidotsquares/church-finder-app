<style>
.form-error{
  color: red;
}
.hidecat{
  display:none;
}
</style>
<div class="mainDivOuter" >
	<div class="row" >
   <div class="col-md-4 col-sm-5" >
     <div class="side-menu side-menu-container">
       <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/vmenuModule.css" />
       <div id="content" style="margin:0px auto; max-width:640px;">
        <div class="u-vmenu">
          <?php echo getParentCategory(); ?>
        </div>
      </div>
      <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vmenuModule.js"></script>
      <script type="text/javascript">
        $(document).ready(function() {
          $(".u-vmenu").vmenuModule({
            Speed: 200,
            autostart: false,
            autohide: true
          });
        });
      </script>
    </div>
  </div>
  <div class="col-md-8 col-sm-7" >
    <div class="panel box box-primary" >
     <div class="panel-body" >
      <div class="main-body-right" >
        <form role="form" method="post" id="category-edit" action="<?php echo base_url().'admin/category/edit'?>">
          <div class="row catbox hidecat" >
            <div class="col-md-12" ><label for="parent_category_name" class="required">Category Name </label></div>
              <div class="col-md-9" >
                <div class="">
                  <div class="form-group">

                    <input type="text" class="form-control" id="category_nameedit" name="parent_category_name" placeholder="Enter Category Name" value="Category Manager" maxlength="40">
                    <span class="form-error"><?php echo form_error('parent_category_name'); ?></span>
                  </div>
                  <input type="hidden" id="category_id" name="category_id" value="0">
                </div><!-- /.box-body -->
              </div>
              <div class="col-md-3" >
                <div class="text-right">
                  <button type="submit" class="btn btn-primary">Update</button>
                  <a href="javascript:void();" id="category_manager" > <button type="button" class="btn btn-danger deleteCategory">Delete</button></a>
                </div>
              </div>
            </div>


        </form>
        <h3 id="prnt-cat-name">Create sub category in Category Manager</h3>
        <form role="form" method="post" action="<?php echo base_url().'admin/category/add'?>">
          <div class="box-body">
            <div class="form-group">
              <label for="category_name" class="required">Category Name <span style="color: red;"> &nbsp;*</span></label>
              <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Enter Category Name" value="" maxlength="40">
              <span class="form-error"><?php echo form_error('category_name'); ?></span>
            </div>

            <input type="hidden" id="parent_category_id" name="parent_category_id" value="0">
          </div>

          <div class="text-right">
            <button type="submit" class="btn btn-primary" value="add">Create</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<script>
  $(document).ready(function(){
    $(".treeview_category").click(function(e){
     var result = $(this).attr('class').split(' ');
     var rclass = "."+result[1];
     $(rclass).find("a").removeClass('active');
     $(this).find("a").first().addClass('active');
     e.stopPropagation();
     var current_id=this.id;
     if(current_id!=0){
      $(".catbox").removeClass("hidecat");
      $.ajax({
        url: '<?php echo site_url('admin/category/getcateogoryname'); ?>',
        type: 'post',
        dataType: 'json',
        data:{id:current_id},
        success: function(data){
          $('#category_nameedit').val(data.category_name);
          $('#category_id').val(current_id);
          $('#parent_category_id').val(current_id);
          $("#prnt-cat-name").text('Create new category in '+data.category_name);
          

        }
      });
    }else{
      $(".catbox").addClass("hidecat");
      $('#category_nameedit').val('Category Manager');
      $('#category_id').val(0);
      $('#parent_category_id').val(0);
      $("#prnt-cat-name").text('Create new category in Category Manager');
      $(".treeview-menu a").removeClass("active");
      
    }

  });
  $(".deleteCategory").click(function(e){
    e.preventDefault();
    var url = "<?php echo base_url().'admin/category/delete'?>";
    if(confirm("Are you sure to delete this category and its sub category?")){
      $("#category-edit").attr("action",url);
      $("#category-edit").submit();
    }

  });


  });
</script>