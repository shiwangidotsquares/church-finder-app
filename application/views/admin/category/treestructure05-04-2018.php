<div class="mainDivOuter" >

	<div class="row" >
	<div class="col-md-3 col-sm-4" >
	<div class="side-menu side-menu-container">
	    <ul class="nav navbar-nav">

	        <?php
	      //  $categoryname = getcategoryname($this->uri->segment(4));

	         $submenu=getParentCategory($this->uri->segment(4)); ?>
	        <li><a href="#"><span class="fa fa-tags"></span> <?php //echo $categoryname[0]['course_category_name']; ?></a></li>

	         <?php foreach($submenu as $key=>$value){ ?>

<li><a href="http://192.168.2.86/appcms/admin/category/treestructure/<?php echo $value['course_category_id']; ?>"><?php echo $value['course_category_name']; ?></a></li>


            <?php } ?>






	        </ul>

	</div>
	</div>
	<div class="col-md-9 col-sm-8" >
		<div class="panel box box-primary" >
			<div class="panel-body" >
				<div class="main-body-right" >

					<form role="form" method="post" action="<?php echo base_url().'admin/category/edit'?>">
                              <div class="box-body">
                                <div class="form-group">
                                  <label for="course_category_name" class="required">Category Name </label>
                                  <input type="text" class="form-control" id="course_category_name" name="course_category_name" placeholder="Enter Category Name" value="<?php echo $categoryname[0]['course_category_name']; ?>">
                                  <span class="form-error"><?php echo form_error('course_category_name'); ?></span>
                                </div>


                                <input type="hidden" name="course_category_id" value="<?php echo $this->uri->segment(4); ?>">


                                </div><!-- /.box-body -->

                            <div class="box-footer text-right">
                                <button type="submit" class="btn btn-primary">Update</button>
                                <a href="<?php echo base_url('admin/category/index');?>"> <button type="button" class="btn btn-white">Cancel</button></a>
                                  <a href="javascript:void();" id="category_manager" > <button type="button" class="btn btn-danger deleteRecord">Delete</button></a>
                            </div>
                        </form>
                          <h3>Create new category in <?php echo $categoryname[0]['course_category_name']; ?></h3>
                         <form role="form" method="post" action="http://192.168.2.86/appcms/admin/category/add">
                <div class="box-body">
          <div class="form-group">
            <label for="course_category_name" class="required">Category Name <span style="color: red;"> &nbsp;*</span></label>
            <input type="text" class="form-control" id="course_category_name" name="course_category_name" placeholder="Enter Category Name" value="">
            <span class="form-error"><?php echo form_error('course_category_name'); ?></span>
          </div>

            <input type="hidden" name="parent_course_category_id" value="<?php echo $this->uri->segment(4); ?>">



          </div><!-- /.box-body -->

                <div class="box-footer text-right">
                    <button type="submit" class="btn btn-primary" value="add">Save</button>
                    <a href="<?php echo base_url('admin/category/index');?>"> <button type="button" class="btn btn-white">Cancel</button></a>
                </div>
            </form>


				</div>
			</div>
		</div>
		</div>
	</div>
</div>
        <style type="text/css">
        .BackBtn { font-size: 20px; margin-bottom: 0px; display: inline-block; }
        	.mainDivOuter { width: 100%; display: inline-block; margin-top: 0px; }
        	.main-body-right h1 {
        		margin-top: 0px;
        		font-size: 30px;
        	 }
.main-body-right .box-body, .main-body-right .box-footer { padding-left: 0px; padding-right: 0px; }
.side-menu {
  position: relative;
  width: 100%;
  background-color: #f8f8f8;
  border-right: 1px solid #e7e7e7;
  display: inline-block;
}
.side-menu .navbar {
  border: none;
}
.side-menu .navbar-header {
  width: 100%;
  border-bottom: 1px solid #e7e7e7;
}
.side-menu .navbar-nav .active a {
  background-color: transparent;
  margin-right: -1px;
  border-right: 5px solid #e7e7e7;
}
.side-menu .navbar-nav li {
  display: block;
  width: 100%;
  border-bottom: 1px solid #e7e7e7;
}
.side-menu .navbar-nav li a {
  padding: 8px 15px 8px 25px;
  color: #707070;
}
.side-menu .navbar-nav li.active a, .side-menu .navbar-nav li a:hover {
  background: #f3f3f3;
  color: #3c8dbc;
}
.side-menu .navbar-nav li:first-child a {
  padding: 15px;
}
.side-menu .navbar-nav li a .glyphicon {
  padding-right: 10px;
}
.side-menu #dropdown {
  border: 0;
  margin-bottom: 0;
  border-radius: 0;
  background-color: transparent;
  box-shadow: none;
}
.side-menu #dropdown .caret {
  float: right;
  margin: 9px 5px 0;
}
.side-menu #dropdown .indicator {
  float: right;
}
.side-menu #dropdown > a {
  border-bottom: 1px solid #e7e7e7;
}
.side-menu #dropdown .panel-body {
  padding: 0;
  background-color: #f3f3f3;
}
.side-menu #dropdown .panel-body .navbar-nav {
  width: 100%;
}
.side-menu #dropdown .panel-body .navbar-nav li {
  padding-left: 15px;
  border-bottom: 1px solid #e7e7e7;
}
.side-menu #dropdown .panel-body .navbar-nav li:last-child {
  border-bottom: none;
}
.side-menu #dropdown .panel-body .panel > a {
  margin-left: -20px;
  padding-left: 35px;
}
.side-menu #dropdown .panel-body .panel-body {
  margin-left: -15px;
}
.side-menu #dropdown .panel-body .panel-body li {
  padding-left: 30px;
}
.side-menu #dropdown .panel-body .panel-body li:last-child {
  border-bottom: 1px solid #e7e7e7;
}
.side-menu #search-trigger {
  background-color: #f3f3f3;
  border: 0;
  border-radius: 0;
  position: absolute;
  top: 0;
  right: 0;
  padding: 15px 18px;
}
.side-menu .brand-name-wrapper {
  min-height: 50px;
}
.side-menu .brand-name-wrapper .navbar-brand {
  display: block;
}
.side-menu #search {
  position: relative;
  z-index: 1000;
}
.side-menu #search .panel-body {
  padding: 0;
}
.side-menu #search .panel-body .navbar-form {
  padding: 0;
  padding-right: 50px;
  width: 100%;
  margin: 0;
  position: relative;
  border-top: 1px solid #e7e7e7;
}
.side-menu #search .panel-body .navbar-form .form-group {
  width: 100%;
  position: relative;
}
.side-menu #search .panel-body .navbar-form input {
  border: 0;
  border-radius: 0;
  box-shadow: none;
  width: 100%;
  height: 50px;
}
.side-menu #search .panel-body .navbar-form .btn {
  position: absolute;
  right: 0;
  top: 0;
  border: 0;
  border-radius: 0;
  background-color: #f3f3f3;
  padding: 15px 18px;
}
        </style>


        <script type="text/javascript">
        	$(function () {
    $('.navbar-toggle').click(function () {
        $('.navbar-nav').toggleClass('slide-in');
        $('.side-body').toggleClass('body-slide-in');
        $('#search').removeClass('in').addClass('collapse').slideUp(200);

        /// uncomment code for absolute positioning tweek see top comment in css
        //$('.absolute-wrapper').toggleClass('slide-in');

    });

   // Remove menu for searching
   $('#search-trigger').click(function () {
        $('.navbar-nav').removeClass('slide-in');
        $('.side-body').removeClass('body-slide-in');

        /// uncomment code for absolute positioning tweek see top comment in css
        //$('.absolute-wrapper').removeClass('slide-in');

    });
});
        </script>