<?php //print_r($dataParent); die;?>

<style>
.form-error{
color: red;
}
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
            </div>
            <form role="form" method="post">
             	  <div class="box-body">
					<div class="form-group">
					  <label for="category_name" class="required">Category Name <span style="color: red;"> &nbsp;*</span></label>
					  <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Enter Category Name" value="<?php echo set_value('category_name'); ?>">
					  <span class="form-error"><?php echo form_error('category_name'); ?></span>
					</div>


						 <?php /* ?>
  					<div class="form-group">
                    <label for="parent_category_id" class="required">Parent Category   <span style="color: red;"> &nbsp;</span></label>
                    <select name="parent_category_id" class="form-control">
                        <option value="">---Please Select---</option>
                        <?php foreach($dataParent as $val){ ?>
                            <option value="<?php echo $val['category_id']; ?>"><?php echo $val['category_name']; ?></option>
                           <?php } ?>
                    </select>
                    <span class="form-error"><?php echo form_error('parent_category_id'); ?></span>
                 	 </div> <?php */ ?>



					<div class="form-group">
					  <label for="category_description">Category Description<span style="color: red;"> &nbsp;*</span></label>
					  <?php  $this->ckeditor->editor('category_description',set_value('category_description'));?>
					<span class="form-error"><?php echo form_error('category_description'); ?></span>
					</div>

				  </div><!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="<?php echo base_url('admin/category/index');?>"> <button type="button" class="btn btn-white">Cancel</button></a>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).on("change", "#page_type", function(){
		var val = $(this).val();
		jQuery('.cld').css('display','none');
		jQuery("#clDiv"+val).show('slow');
		jQuery('.ele').attr('disabled','true');
		jQuery('#pageLink'+val).removeAttr('disabled');
		if(val == 1){
			var menuTitle = jQuery('#menu_title').val();
			var trimMenu = menuTitle.replace(/[^A-Z0-9]+/ig, '-');
			$("#pageLink1").val(trimMenu).change();
		}
	});
	$(document).ready(function(){
		$(document).on("keyup",'#menu_title',function(){
			var menu_title = $("#menu_title").val();
            menu_title = menu_title.toLowerCase();
            //menu_title = menu_title.replace('/\s/g','-');
			var trimMenu = menu_title.replace(/[^A-Z0-9]+/ig, '-');
			$("#pageLink1").val(trimMenu);
		});

        $(document).on("change",'#pageLink1',function(){
			var menu_title = $(this).val();
                        menu_title = menu_title.toLowerCase();
                        var trimMenu = menu_title.replace(/[^A-Z0-9]+/ig, '-');
			$(this).val(trimMenu);
		});
	});
</script>