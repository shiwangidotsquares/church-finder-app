<style>
.form-error{
color: red;
}
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
            </div>
                        <form role="form" method="post">
                              <div class="box-body">
                                <div class="form-group">
                                  <label for="category_name" class="required">Category Name </label>
                                  <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Enter Category Name" value="<?php echo set_value('category_name',$category_name); ?>">
                                  <span class="form-error"><?php echo form_error('category_name'); ?></span>
                                </div>


                                    <?php /* ?>  <div class="form-group">
                    <label for="parent_category_id" class="required">Parent Category<span style="color: red;"> &nbsp;</span></label>
                    <select name="parent_category_id" class="form-control">
                        <option value="">---Please Select---</option>
                        <?php foreach($dataParent as $key=>$val){
                            if($category_parent_id==$val['category_id']){ ?>
                                 <option selected value="<?php echo $val['category_id']; ?>"><?php echo $val['category_name']; ?></option>
                      <?php    }else{?>
                            <option value="<?php echo $val['category_id']; ?>"><?php echo $val['category_name']; ?></option>
                           <?php  }
                          ?>

                           <?php } ?>
                    </select>
                    <span class="form-error"><?php echo form_error('parent_category_id'); ?></span>
                  </div>
          <?php  */ ?>




                                <input type="hidden" name="category_id" value="<?php echo$this->uri->segment(4); ?>">


                                </div><!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Update</button>
                                <a href="<?php echo base_url('admin/category/index');?>"> <button type="button" class="btn btn-white">Cancel</button></a>
                                  <a href="javascript:void();" id="category_manager" > <button type="button" class="btn btn-danger deleteRecord">Delete</button></a>
                            </div>
                        </form>
                          <h3>Create new category in <?php echo $category_name; ?></h3>
                         <form role="form" method="post" action="http://192.168.2.86/appcms/admin/category/add">
                <div class="box-body">
          <div class="form-group">
            <label for="category_name" class="required">Category Name <span style="color: red;"> &nbsp;*</span></label>
            <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Enter Category Name" value="<?php echo set_value('category_name'); ?>">
            <span class="form-error"><?php echo form_error('category_name'); ?></span>
          </div>

            <input type="hidden" name="parent_category_id" value="<?php echo$this->uri->segment(4); ?>">



          </div><!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" value="add">Save</button>
                    <a href="<?php echo base_url('admin/category/index');?>"> <button type="button" class="btn btn-white">Cancel</button></a>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
<script>
 $(document).ready(function() {
   jQuery("#category_manager" ).on("click", ".deleteRecord", function(){

        var sID = '<?php echo$this->uri->segment(4); ?>';
        alert(sID);
        swal({
          title: "Are you sure?",
          text: "You will not be able to recover this category in future!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel plz!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {
                $.ajax({
                    url: '<?php echo site_url('admin/category/delete'); ?>',
                    type: 'post',
                    dataType: 'json',
                    data: {sID:sID},
                    success: function(data){
                        swal("Deleted", "Category has been deleted.", "success");
                        bResetDisplay = false;
                        /* override default behaviour */
                        oTable.fnDraw();
                        bResetDisplay = true;
                         /*restore default behaviour */
                    },
                    error: function(){
                        swal("warning", "Don't have permision to deleted Category.", "error");
                    }
                });
          } else {
                swal("Cancelled", "Category are safe now.", "error");
          }
        });
    });
    });
  </script>