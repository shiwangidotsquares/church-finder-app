<div class="row">
    <div class="col-lg-12">
       <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
            </div>
                <form role="form" method="post" enctype="multipart/form-data" >
                    <div class="box-body">
                        <input type="hidden" name="config_id" value="<?php echo $olddata['config_id'];?>">
                        <div class="form-group">
                            <label class="control-label">Config Key <span style="color: red;"> &nbsp;*</span></label>
                            <input value="<?php echo $olddata['config_key'] ;?>" name="config_key"  type="text" class="form-control">
                             <span style="color: red;"><?php echo form_error('config_key'); ?></span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Config Value <span style="color: red;"> &nbsp;*</span></label>
                            <input value="<?php echo $olddata['config_value'] ;?>" name="config_value"  type="text" class="form-control">
                            <span style="color: red;"><?php echo form_error('config_value'); ?></span>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Config Label <span style="color: red;"> &nbsp;*</span></label>
                             <input value="<?php echo $olddata['config_label'] ;?>" name="config_label"  type="text" class="form-control">
                            <span style="color: red;"><?php echo form_error('config_label'); ?></span>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a href="<?php echo base_url('admin/website_config/index');?>">  <button type="button" class="btn btn-white">Cancel</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>