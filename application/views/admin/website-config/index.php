<div class="row">
  <div class="col-lg-12">
    <div class="box">
        <div class="box-header">
          <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
        </div>
        <div class="row-fluid">
            <div class="col-lg-3">
                <a  href="<?php echo base_url('admin/website_config/add');?>" class="btn btn-primary ">Add a Config</a>
            </div>
            <div class="col-lg-3 ">
            </div>
            <div class="col-lg-6 ">
                <div class="filter">
                    <label>Search Keyword:</label>
                    <input class="form-control" type="text" id="searchBox" name="searchBox" />
                    <input type="button" value="Search" id="searchButton" class="btn btn-default blue-bg" />
                    <input type="reset" value="Reset" id="reSet" class="btn btn-default orange-bg dis-inline width-auto" />
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

        <table class="table table-striped table-bordered table-hover jambo_table" id="editable" >
          <thead>
            <tr class="headings">
                <th>sr_no</th>
                <th>Config Key</th>
                <th>Config label</th>
                <th>Action</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
    var oTable;
    $(document).ready(function() {
        oTable= $('#editable').dataTable({
             "processing": true,
             "serverSide": true,
             "fnDrawCallback" : function(oSettings){$('#editable').tooltip();},
             "ajax": {
                 "url": "<?php echo base_url('admin/website_config/get_website_config'); ?>",
                 "type": "POST",
                 "data": function ( d ){
                    d.myKey = "myValue";
                    d.searchBox = $('#searchBox').val();
                 }
             },
             "bFilter": false,
             "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,3 ] }],
             "order": [[ 1, "desc" ]],
             "aoColumns": [
                    { "data": "sr_no", "sClass": "text-center"},
                    { "data": "config_key" },
                    { "data": "config_label" },
                    { "data": "action" }
             ]
        });

        $('#searchButton').click( function(){
            oTable.fnDraw();
        });

        $('#searchBox').keydown(function (e){
            if(e.keyCode == 13){
                oTable.fnDraw();
            }
        });



        $('#facility_id').change( function(){
            oTable.fnDraw();
        });

        $('#coach_type').change( function(){
            oTable.fnDraw();
        });

        $('#pStatus').change( function(){
            oTable.fnDraw();
        });

        $('#reSet').click( function(){
            $('#company_id').val($('#company_id option[selected]').val());
            $('#facility_id').empty();
            $('#facility_id').append('<option value="0" selected="selected">-Select-</option>');
            $('#pStatus').val($('#pStatus option[selected]').val());
            $('#searchBox').val('');
            oTable.fnDraw();
        });
    });



    jQuery("#editable" ).on("click", ".activeRecord", function(){
        var sID= jQuery(this).attr('rel');
        var button=jQuery(this);
        $.ajax({
            url: '<?php echo site_url('admin/website_config/status'); ?>',
            type: 'post',
            dataType: 'json',
            data: {sID:sID, sStatus:0},
            success: function(data){
                button.removeClass('activeRecord').addClass('deactiveRecord');
                button.removeClass('fa fa-check-square').addClass('fa fa-ban');
                button.attr('title','active');
                $('#editable').tooltip();
                swal("", "website-config has deactivated successfully!", "success");
                oTable.fnDraw();
            },
            error: function(){
                swal("warning", "dont have permision to dactivate website-config", "error");
            }
        });
    });


    jQuery("#editable" ).on("click", ".deactiveRecord", function(){
        var sID= jQuery(this).attr('rel');
        var button=jQuery(this);
        $.ajax({
            url: '<?php echo site_url('admin/website_config/status'); ?>',
            type: 'post',
            dataType: 'json',
            data: {sID:sID, sStatus:1},
            success: function(data){
                button.addClass('activeRecord').removeClass('deactiveRecord');
                button.removeClass('fa fa-ban').addClass('fa fa-check-square');
                button.attr('title','inactive');
                $('#editable').tooltip();
                swal("", "website-config has activated successfully!", "success");
                oTable.fnDraw();
            },
            error: function(){
                swal("warning", "dont have permision to actived website-config.", "error");
            }
        });
    });

    jQuery("#editable" ).on("click", ".deleteRecord", function(){
        var sID= jQuery(this).attr('rel');
        //alert(sID);
        swal({
          title: "Are you sure?",
          text: "You will not be able to recover this user in future!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel plz!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {
                $.ajax({
                    url: '<?php echo site_url('admin/website_config/delete'); ?>',
                    type: 'post',
                    dataType: 'json',
                    data: {sID:sID},
                    success: function(data){
                        swal("Deleted", "website-config has been deleted.", "success");
                        bResetDisplay = false;
                        /* override default behaviour */
                        oTable.fnDraw();
                        bResetDisplay = true;
                         /*restore default behaviour */
                    },
                    error: function(){
                        swal("warning", "dont have permision to deleted website-config.", "error");
                    }
                });
          } else {
                swal("Cancelled", "Record is safe now.", "error");
          }
        });
    });
    $('#searchBox').keydown(function (e){
        if(e.keyCode == 13){
            oTable.fnDraw();
        }
    });
</script>