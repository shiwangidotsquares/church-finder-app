<style>
.form-error{
color: red;
}
</style>
<link href="<?php echo base_url()?>assets/js/dropzone/style.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url()?>assets/js/dropzone/dropzone.css" rel="stylesheet" type="text/css">
<div class="row">
<div class="col-lg-12">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo !empty($form_title)?$form_title:''?></h3>
        </div>
        <form role="form" id="frm-subscribe" method="post" enctype="multipart/form-data">
            <div class="box-body">
                <div class="form-group">
                    <label for="parish_name" class="required">Parish Name <span style="color: red;"> &nbsp;*</span></label>
                    <input type="text" class="form-control" id="parish_name" name="parish_name" placeholder="Parish Name" value="<?php echo set_value('parish_name'); ?>">
                    <span class="form-error"><?php echo form_error('parish_name'); ?></span>
                </div>
               
              <div class="form-group">
                <label for="parishes_location" class="required"> Select Parishes Image <span style="color: red;"> &nbsp;*</span></label>
                <input type="file" class="form-control" name="user_profile_image" multiple/>
                <span class="form-error"><?php echo form_error('user_profile_image'); ?></span>
           </div>
               
                 <div class="form-group">
                    <label for="parishes_location" class="required">Location <span style="color: red;"> &nbsp;*</span></label>
                    <input type="text" class="form-control" id="prac_location" name="parishes_location" placeholder=" Enter Location" value="<?php echo set_value('parishes_location'); ?>">

                    <span class="form-error"><?php echo form_error('parishes_location'); ?></span>
                </div>

                <div class="form-group">
                    <label for="parishes_phone_number" class="required">Phone Number <span style="color: red;"> &nbsp;*</span></label>
                    <input type="text" class="form-control" id="parishes_phone_number" name="parishes_phone_number" placeholder="Phone Number" value="<?php echo set_value('parishes_phone_number'); ?>">
                    <span class="form-error"><?php echo form_error('parishes_phone_number'); ?></span>
                </div>

                <div class="form-group">
                    <label for="parishes_website_url" class="required">Website Url</label>
                    <input type="text" class="form-control" id="parishes_website_url" name="parishes_website_url" placeholder="Website Url" value="<?php echo set_value('parishes_website_url'); ?>">
                    <span class="form-error"><?php echo form_error('parishes_website_url'); ?></span>
                </div>

<div class="row" >
  <div class="col-md-4" >
    <div class="form-group">
                    <label for="event_date" class="required"> Opening Time<span style="color: red;"> &nbsp;*</span></label>
                    <input type="text" class="form-control"  onkeydown="return false;"  id="opening_time" name="opening_time" placeholder="" value="<?php echo set_value('opening_time'); ?>">
                    <span class="form-error"><?php echo form_error('opening_time'); ?></span>
                  </div>   
  </div>
  <div class="col-md-4" >
    
                 <div class="form-group">
                    <label for="closing_time" class="required"> Closing Time<span style="color: red;"> &nbsp;*</span></label>
                    <input type="text" class="form-control"  onkeydown="return false;"  id="closing_time" name="closing_time" placeholder="" value="<?php echo set_value('closing_time'); ?>">
                    <span class="form-error"><?php echo form_error('closing_time'); ?></span>
                  </div> 
  </div>
</div>
                 

</div><!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                <a href="<?php echo base_url('admin/parishes/index');?>"> <button type="button" class="btn btn-white">Cancel</button></a>
            </div>
        </form>
    </div>
</div>
</div>
</div>



<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQFgKnTgdiih5m2xA1QhmXo39R9qwcgW8&libraries=places"></script>
<script>

        var input = document.getElementById('prac_location');
        var autocomplete = new google.maps.places.Autocomplete(input,{types: ['(cities)']});
        google.maps.event.addListener(autocomplete, 'place_changed', function(){
             var place = autocomplete.getPlace();
             for (var i = 0; i < place.address_components.length; i++) {
                var cityName = place.address_components[0].long_name;
             }
             console.log(cityName);
             jQuery("#location_city").val(cityName);
              console.log(place);
        });


</script>


<script type="text/javascript">
    $(document).ready(function(){
        $('#opening_time').timepicker({});
        $('#closing_time').timepicker({});
    });

</script>
<script>

$( document ).ready(function() {
  jQuery("#frm-subscribe").validate({

    rules: {
      
    parish_name: {
        required: true,
       
    } ,
    parishes_location:{
        required: true,
    },
    parishes_website_url:{
        required: true,
    },
    opening_time:{
        required: true,
    },
    closing_time:{
        required: true,
        
    },
    
  
},

messages: {
   
 
  parish_name:{
            required: "The Parishes Name field is required.",
        },

  parishes_location:{
            required: "The Location field is required.",
        },

  parishes_website_url: {
            required: "The website url is required.",
 },


 opening_time: {
            required: "The Opening Time field is required.",
        },

 closing_time:{
            required: "The Closing Time field is required.",
        },


},

});

});

</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>
<!--JavaScript -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<style type="text/css">
label.error{
color:red;
font-weight: normal;
font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
}
</style>
</script>
