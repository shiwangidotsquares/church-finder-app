<!-- <!doctype html>
<html>
    <head>
        <link href="<?php echo base_url()?>assets/js/dropzone/style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url()?>assets/js/dropzone/dropzone.css" rel="stylesheet" type="text/css">
        <script src="<?php echo base_url()?>assets/js/dropzone/dropzone.js" type="text/javascript"></script>
    </head>
    <body > -->
        <div class="container dropzonePage" >
            <div class='content'>
                <form action="uploadify" class="dropzone" id="dropzonewidget">

                </form>
                <div class="text-right" >
                    <input type="submit" style="display: none;" class="btn btn-primary submitBtn" value="Submit" name="submit" id="submit-all">
                </div>
            </div>

             <?php
                $files1 = scandir('uploads/mediamanager/');
                unset($files1[0]);
                unset($files1[1]);
                ?>
            <div>
              <label > Total Files: <?php echo  count($files1); ?></label> 
            </div>
            <div class="dropzone dropzone-list-view">
                <?php foreach($getMedia_data as $files){
                    
                    $file_name= $files['filename'];
                    $file_id = $files['id'];
                    $file_array=explode('.',$file_name);

                    $path= 'uploads/mediamanager/'.$file_name;
                    $docpath= 'assets/img/doc-icon.png';
                    $pdfpath= 'assets/img/pdficon.png';
                    $videopath= 'assets/img/videoicon.png';
                    $audiopath= 'assets/img/audioicon.jpg';
                    $xlsxpath= 'assets/img/xlsxicon.png';
                    ?>




                    <div class="dz-preview dz-image-preview dz-processing dz-success dz-complete">
                        <div class="dz-image">

                            <?php
                            if($file_array[1]=='doc' || $file_array[1]=='docx'){?>
                            <a href="<?php echo base_url().'uploads/mediamanager/'.$file_name; ?>" target="_blank"> <img  src="<?php echo base_url().$docpath; ?>" width="70px;" height="70px;">
                            </a>
                            <?php }
                            if($file_array[1]=='pdf'){ ?>
                            <a href="<?php echo base_url().'uploads/mediamanager/'.$file_name; ?>" target="_blank">  <img  src="<?php echo base_url().$pdfpath; ?>" width="70px;" height="70px;">
                            </a>
                            <?php }else if($file_array[1]=='mp4'||$file_array[1]=='wmv'){ ?>
                            <a href="<?php echo base_url().'uploads/mediamanager/'.$file_name; ?>" target="_blank"><img  src="<?php echo base_url().$videopath; ?>" width="70px;" height="70px;"></a>
                        </a>
                        <?php }else if($file_array[1]=='mp3'){ ?>
                        <a href="<?php echo base_url().'uploads/mediamanager/'.$file_name; ?>" target="_blank"> <img  src="<?php echo base_url().$audiopath; ?>" width="70px;" height="70px;">
                        </a>
                        <?php }else if($file_array[1]=='xlsx'){ ?>
                        <a href="<?php echo base_url().'uploads/mediamanager/'.$file_name; ?>" target="_blank">  <img  src="<?php echo base_url().$xlsxpath; ?>" width="70px;" height="70px;">
                        </a>
                        <?php }else { ?>
                        <a href="<?php echo base_url().'uploads/mediamanager/'.$file_name; ?>" target="_blank">  <img  src="<?php echo base_url().$path; ?>" width="70px;" height="70px;">
                        </a>
                        <?php }?>

                    </div>
                    <a href="<?php echo base_url().'uploads/mediamanager/'.$file_name; ?>" target="_blank">
                       <div class="dz-details">
                        <div class="dz-filename"><span data-dz-name=""><?php echo $file_name; ?></span>
                        </div>
                    </div>
                </a>
                <a class="dz-remove remove_file" id="<?php echo $file_id; ?>"  href="javascript:void(0);" data-dz-remove=""><i class="fa fa-remove" aria-hidden="true"></i>
</a>

            </div>
            <?php
        } ?>

    </div>


</div>
    <!-- </body>
        </html> -->
        <link href="<?php echo base_url()?>assets/js/dropzone/style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url()?>assets/js/dropzone/dropzone.css" rel="stylesheet" type="text/css">
        <script src="<?php echo base_url()?>assets/js/dropzone/dropzone.js" type="text/javascript"></script>
        <script type="text/javascript">


            Dropzone.options.dropzonewidget = {
 maxFilesize: 50, //mb- Image files not above this size
 uploadMultiple: true, // set to true to allow multiple image uploads
 parallelUploads: 15, //all images should upload same time
 maxFiles: 50, //number of images a user should upload at an instance
acceptedFiles: ".png,.jpg,.jpeg,.pdf,.mp3,.mp4,.doc,.docx,.xls,.xlsx,.wmv", //allowed file types, .pdf or anyother would throw error
 addRemoveLinks: false, // add a remove link underneath each image to
 autoProcessQueue: true, // Prevents Dropzone from uploading dropped files immediately

 removedfile: function(file) {

    var name = file.name;
    if (name) {
       $.ajax({

        headers:{
           'X-CSRF-Token':$('input[name="_token"]').val()
 }, //passes the current token of the page to image url

 type: 'GET',
 url: "/products/remove/"+name, //passes the image name to the method handling this url to //remove file
 dataType: 'json'
});
   }
   var _ref;
   return (ref = file.previewElement) != null ? ref.parentNode.removeChild(file.previewElement) : void 0;

},

init: function() {
var  error=0;
   // $('#submit-all').css('display','block');
   var submitButton = document.querySelector("#submit-all")
 myDropzone = this; // closure

 submitButton.addEventListener("click", function() {
 myDropzone.processQueue(); // Tell Dropzone to process all queued files.

 /*location. reload(true);*/
});

// You might want to show the submit button only when
 // files are dropped here:
 this.on("addedfile", function() {


 // Show submit button here and/or inform user to click it.
});


 this.on("complete", function (file) {
  if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
    if(!error){
      location. reload(true);
    }
    error=0;
    }
});


this.on("error", function(file, message, xhr) {
       if (xhr == null) this.removeFile(file); // perhaps not remove on xhr errors
       alert(message);
       error=1;

});


}
};






$(document).ready(function(){
    /*$(".remove_file").click(function(){*/
        jQuery(document).on("click", ".remove_file", function(){

            var filename = $(this).attr("id");

            swal({
              title: "Are you sure?",
              text: "You will not be able to recover this file in future!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel plz!",
              closeOnConfirm: false,
              closeOnCancel: false
          },
          function(isConfirm){
              if (isConfirm) {
                $.ajax({
                    url: 'unlinkFile',
                    type: 'post',
                    dataType: 'json',
                    data: {id: filename},
                    success: function(data){
                        swal("Deleted", "File has been deleted.", "success");
                        location.reload();
                    },
                    error: function(){
                        swal("warning", "Don't have permision to delete file.", "error");
                    }
                });
            } else {
                swal("Cancelled", "File is safe.", "error");
            }
        });

     });

    });

</script>

