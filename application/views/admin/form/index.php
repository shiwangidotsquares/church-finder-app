<div class="row">
  <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
            </div>
            <div class="row-fluid">
                <div class="col-lg-3">
                    <a href="<?php echo base_url('admin/blog/add');?>" class="btn btn-primary ">Add Blog</a>
                </div>
                <div class="col-lg-3 ">
                </div>
                <div class="col-lg-6 ">
                    <div class="filter">
                        <label>Search Keyword:</label>
                        <input class="form-control" type="text" id="searchBox" name="searchBox" />
                        <input type="button" value="Search" id="searchButton" class="btn btn-default blue-bg" />
                        <input type="reset" value="Reset" id="reSet" class="btn btn-default orange-bg dis-inline width-auto" />
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

        <table class="table table-striped table-bordered table-hover jambo_table" id="blog_manager" >
          <thead>
            <tr class="headings">
                <th>#</th>
                <!-- <th>Image</th> -->
                <th>Title</th>
                <th>Category</th>
                <th>Description</th>
                <th>Created</th>
                <th>Actions</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


   <script type="text/javascript">
    var oTable;
    $(document).ready(function() {
        oTable= $('#blog_manager').dataTable({
             "processing": true,
             "serverSide": true,
             "fnDrawCallback" : function(oSettings){$('#blog_manager').tooltip();},
             "ajax": {
                 "url": "<?php echo base_url('admin/blog/get_all'); ?>",
                 "type": "POST",
                 "data": function ( d ){
                    d.myKey = "myValue";
                    d.searchBox = $('#searchBox').val();
                 }
             },
             "bFilter": false,
             "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,5 ] }],
             "order": [[ 4, "desc" ]],
             "aoColumns": [
                    { "data": "sr_no", "sClass": "text-center"},
                    /*{ "data": "blog_image" },*/
                    { "data": "blog_title" },
                    { "data": "category_name" },
                    { "data": "blog_description" },
                    { "data": "blog_created_date" },
                    { "data": "action" }
             ]
        });

        $('#searchButton').click( function(){
            oTable.fnDraw();
        });

        $('#searchBox').keydown(function (e){
            if(e.keyCode == 13){
                oTable.fnDraw();
            }
        });
        $('#reSet').click( function(){
            $('#company_id').val($('#company_id option[selected]').val());
            $('#facility_id').empty();
            $('#facility_id').append('<option value="0" selected="selected">-Select-</option>');
            $('#pStatus').val($('#pStatus option[selected]').val());
            $('#searchBox').val('');
            oTable.fnDraw();
        });
        jQuery("#blog_manager" ).on("click", ".activeRecord", function(){
            var sID= jQuery(this).attr('rel');
            var button=jQuery(this);
            $.ajax({
                url: '<?php echo site_url('admin/blog/status'); ?>',
                type: 'post',
                dataType: 'json',
                data: {sID:sID, sStatus:0},
                success: function(data){
                    button.removeClass('activeRecord').addClass('deactiveRecord');
                    button.removeClass('fa fa-check-square').addClass('fa fa-ban');
                    button.attr('title','active');
                    $('#blog_manager').tooltip();
                    swal("", "blog has been deactivated successfully!", "success");
                    oTable.fnDraw();
                },
                error: function(){
                    swal("warning", "Don't have permision to dactivate blog", "error");
                }
            });
        });


        jQuery("#blog_manager" ).on("click", ".deactiveRecord", function(){
            var sID= jQuery(this).attr('rel');
            var button=jQuery(this);
            $.ajax({
                url: '<?php echo site_url('admin/blog/status'); ?>',
                type: 'post',
                dataType: 'json',
                data: {sID:sID, sStatus:1},
                success: function(data){
                    button.addClass('activeRecord').removeClass('deactiveRecord');
                    button.removeClass('fa fa-ban').addClass('fa fa-check-square');
                    button.attr('title','inactive');
                    $('#blog_manager').tooltip();
                    swal("", "blog has activated successfully!", "success");
                    oTable.fnDraw();
                },
                error: function(){
                    swal("warning", "Don't have permision to actived blog.", "error");
                }
            });
        });

        jQuery("#blog_manager" ).on("click", ".deleteRecord", function(){
            var sID = jQuery(this).attr('rel');
            //alert(sID);
            swal({
              title: "Are you sure?",
              text: "You will not be able to recover this blog in future!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel plz!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm){
              if (isConfirm) {
                    $.ajax({
                        url: '<?php echo site_url('admin/blog/delete'); ?>',
                        type: 'post',
                        dataType: 'json',
                        data: {sID:sID},
                        success: function(data){
                            swal("Deleted", "blog has been deleted.", "success");
                            bResetDisplay = false;
                            /* override default behaviour */
                            oTable.fnDraw();
                            bResetDisplay = true;
                             /*restore default behaviour */
                        },
                        error: function(){
                            swal("warning", "Don't have permision to delete blog.", "error");
                        }
                    });
              } else {
                    swal("Cancelled", "blog is safe now.", "error");
              }
            });
        });

    });

</script>