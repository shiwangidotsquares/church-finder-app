<?php
$router_class = $this->router->class;
$router_function = $this->router->method;
?>
<style>
.form-error{
    color: red;
}
</style>
<link href="<?php echo base_url()?>assets/js/dropzone/dropzone.css" rel="stylesheet" type="text/css">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header with-border">

          </div>
          <form role="form" method="post" id="setup_one" enctype="multipart/form-data">
            <div class="box-body">
              <div class="form-group">
                <label for="title" class="required">Name of your form<span style="color: red;"> &nbsp;*</span></label>
                <input type="text" class="form-control" id="form_name" name="form_name" placeholder="Please enter name of your form" value="<?php echo set_value('form_name',$form_name); ?>">
                <span class="form-error"><?php echo form_error('form_name'); ?></span>
            </div>
    
    </div><!-- /.box-body -->

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Next</button>
        
    </div>
</form>
</div>
</div>
</div>
</div>





<script>

    $( document ).ready(function() {
        jQuery("#frm-subscribe").validate({

            rules: {
                title:{
                    required: true,
                },
                category_id:{
                    required: true,
                },
                description: {
                    required: true
                }
            },
            messages: {

            }
        });

    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>
<!--JavaScript -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<style type="text/css">
label.error{
color:red;
font-weight: normal;
font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
}
</style>
<script>

  $("#setup_one").submit(function(e) {
    e.preventDefault();
    var form_name   = $('#form_name').val();
    $.ajax({
          url: "<?php echo base_url('admin/form/saveform'); ?>",
          type: 'post',
          data: {form_name:form_name},
          dataType: 'json',
          async: false,
          success:function(response){
          }
        });
     });
</script>
