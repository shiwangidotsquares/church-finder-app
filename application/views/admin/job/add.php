<?php //print_r($employee_array); ?><style>
.form-error{
    color: red;
}

</style>
<link href="<?php echo base_url()?>assets/js/dropzone/dropzone.css" rel="stylesheet" type="text/css">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
          </div>
          <form role="form" id="frm-subscribe" method="post" enctype="multipart/form-data">
            <div class="box-body">
             <div class="form-group">
               <label for="job_title" class="required">Job Title<span style="color: red;"> &nbsp;*</span></label>
               <input type="text" class="form-control" id="job_title" name="job_title" placeholder="Enter title" value="<?php echo set_value('job_title',$job_title); ?>">
               <span class="form-error"><?php echo form_error('job_title'); ?></span>
           </div>
              <div class="form-group">
               <label for="job_description">Job Description<span style="color: red;"> &nbsp;*</span></label>
               <?php  $this->ckeditor->editor('job_description',$job_description);?>
               <span class="form-error"><?php echo form_error('job_description'); ?></span>
           </div>
           <div class="form-group">
              <label for="client_id" class="required">Assigned Client </label>
              <select class="form-control"  name="client_id" id="client_id">
                <option value="" selected="selected">--Select--</option>
                <?php if (!empty($customer_array)) { ?>
                <?php foreach($customer_array as $val){?>
                <option value="<?php echo $val['customer_id'];?>" <?php echo ($client_id == $val['customer_id'])?'selected="true"':'' ?>><?php echo $val['client_name'];?></option>
                <?php }} ?>
            </select>

        </div>

           <div class="form-group">
              <label for="category_id" class="required">Assigned Employee </label>
              <select class="form-control"  name="employee_id" id="employee_id">
                <option value="" selected="selected">--Select--</option>
                <?php if(!empty($employee_array)) {
                 foreach($employee_array as $ke1 => $val1){?>
                <option value="<?php echo $val1['id'];?>" <?php echo ($employee_id == $val1['id'])?'selected="true"':'' ?>><?php echo $val1['first_name'];?></option>
                <?php }} ?>
            </select>

        </div>

       <div class="form-group">
              <label for="role" class="required">Role </label>
              <select class="form-control"  name="role" id="role">
                <option value="" selected="selected">--Select--</option>
                <?php if(!empty($role_array)) {
                 foreach($role_array as $ke1 => $val2){?>
                <option value="<?php echo $val2['role_id'];?>" <?php echo ($role == $val2['role_id'])?'selected="true"':'' ?>><?php echo $val2['role_title'];?></option>
                <?php }} ?>
            </select>

        </div>



      <div class="form-group">
          <label for="product_SKU" class="required">Start Date<span style="color: red;"> &nbsp;*</span></label>
          <input type="text" class="form-control" id="start_date" name="start_date" placeholder="Enter start date" value="<?php echo set_value('start_date',$start_date); ?>">
      </div>

      <div class="form-group">
          <label for="end_date" class="required">End Date<span style="color: red;"> &nbsp;*</span></label>
          <input type="text" class="form-control" id="end_date" name="end_date" placeholder="Enter end date" value="<?php echo set_value('end_date',$end_date); ?>">
      </div>


     
</div><!-- /.box-body -->

<div class="box-footer">
    <button type="submit" class="btn btn-primary">Save</button>
    <a href="<?php echo base_url('admin/product/index');?>">
        <button type="button" class="btn btn-white">Cancel</button></a>
    </div>
</form>
</div>
</div>
</div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog SelectImgPopup">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Select Media</h4>
    </div>
    <div class="modal-body">

        <div class="dropzone dropzone-list-view">

    </div>


</div>
<div class="modal-footer">
  <form id="frm-multiimage" enctype="multipart/form-data">
    <input type="file" class="form-control mar-b10" id="user_profile_image" name="user_profile_image[]" multiple>
  </form>
    <a href="JavaScript:void(0);" class="btn btn-primary uploadMedia" >Done</a>
    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
</div>
</div>

</div>
</div>
<script type="text/javascript">
	$(document).on("change", "#page_type", function(){
		var val = $(this).val();
		jQuery('.cld').css('display','none');
		jQuery("#clDiv"+val).show('slow');
		jQuery('.ele').attr('disabled','true');
		jQuery('#pageLink'+val).removeAttr('disabled');
		if(val == 1){
			var menuTitle = jQuery('#menu_title').val();
			var trimMenu = menuTitle.replace(/[^A-Z0-9]+/ig, '-');
			$("#pageLink1").val(trimMenu).change();
		}
	});
	$(document).ready(function(){
		$(document).on("keyup",'#menu_title',function(){
			var menu_title = $("#menu_title").val();
            menu_title = menu_title.toLowerCase();
            //menu_title = menu_title.replace('/\s/g','-');
            var trimMenu = menu_title.replace(/[^A-Z0-9]+/ig, '-');
            $("#pageLink1").val(trimMenu);
        });

        $(document).on("change",'#pageLink1',function(){
           var menu_title = $(this).val();
           menu_title = menu_title.toLowerCase();
           var trimMenu = menu_title.replace(/[^A-Z0-9]+/ig, '-');
           $(this).val(trimMenu);
       });
    });
</script>
<script>

    $( document ).ready(function() {
        jQuery("#frm-subscribe").validate({

            rules: {
                product_title:{
                    required: true,
                },
                category_id:{
                    required: true,
                },
                product_regular_price: {
                    required: true,
                    number: true
                },
                product_sale_price: {
                    required: true,
                    number: true,
                },
                product_SKU: {
                    required: true,
                },
                product_stock_quantity:{
                    required: true,
                    number: true
                },
                product_description:{
                    required: true,
                },
                product_image:{
                    required: true
                }
            },
            messages: {

            }
        });

    });
</script>
<script>
 $('#user_profile_image').change(function(){
    var files = $(this)[0].files;
    if(files.length > 0){
        $("input[type='checkbox']").prop('checked', false);
    }
});
 $(document).on('click', '.uploadMedia', function(event) {
    var form = $('#frm-multiimage').get(0);
    var form_data = new FormData(form);
    var length = jQuery('#user_profile_image').prop('files').length;
    var file_data = jQuery('#user_profile_image').prop('files')[0];

    if(file_data){

        jQuery.ajax({
            url: '<?php echo base_url();?>admin/organisation/upload_multi_media',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (data) {
              $('#hidden_profile_image').val(data);
              var arr = data.split(',');
              var length = arr.length;
              var html = "";
              for (var i = 0; i < length; i++) {
                html+='<img src="<?php echo base_url()."uploads/mediamanager/"?>'+arr[i]+'" class="img-responsive" width="100px">';
              }
              $('#div_profile_image').html(html);
              $('#myModal').modal('hide');




            }
        });
    }else{
      var profile_image = [];
      $("input:checkbox:checked").each(function(){
          profile_image.push($(this).val());
      });

      profile_image = jQuery.grep(profile_image, function(value) {
        return value != "on";
    });


      profile_image.join(",");

      var html = "";
      $("input:checkbox:checked").each(function(){
        if($(this).val()!="on"){
          html+='<img src="<?php echo base_url()."uploads/mediamanager/"?>'+$(this).val()+'" class="img-responsive" width="100px">';
        }
      });

      $('#hidden_profile_image').val(profile_image);
      $('#div_profile_image').html(html);
      $('#myModal').modal('hide');

      }

    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>
<!--JavaScript -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<style type="text/css">
label.error{
    color:red;
    font-weight: normal;
    font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
}
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
  <script>

  $(function() {
    $( "#start_date" ).datepicker({
      format: 'yyyy-mm-dd'
    });

    $( "#end_date" ).datepicker({
      format: 'yyyy-mm-dd'
    });


  });
  </script>