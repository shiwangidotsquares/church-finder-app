    <?php
    $role_id = 0;
    $action_count = 0;
    $ui_icons_sidebar = array();
    $router_class = '';
    $active_class = '';
    $router_class = $this->router->class;
    $router_function = $this->router->method;
    $session_data = $this->session->userdata('organization_session_data');
    $ui_icons_sidebar = $this->config->item('ui_icons_sidebar');
    $activemenu = array('index','Index','add','Add','send_email','add_final','viewfinalconfiguration','import_questions','report','assign','user_terms_file');
    $mainMenu = array('ajax');
    $role_id = $session_data;
    $sidebar = getSidebar($role_id);
    $class_name = $this->router->fetch_class();
    $method_name = $this->router->fetch_method();
    
    ?>
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less ------------------------------------------------------>
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo base_url(); ?>assets/lte/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>Organization</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

    <?php /* ?>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
    <li class="header">MAIN NAVIGATION</li>
    <!--li>
    <a href="<?php #echo base_url('admin/dashboard'); ?>">
    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
    </li-->

    <?php

    //print_r($sidebar); die;
    if(!empty($sidebar)){ ?>
    <?php foreach($sidebar as $key => $value){?>
    <?php if(!in_array($value['controller_name'],$mainMenu) && !$value['controllers_is_active']){ ?>
    <?php
    $action_count = 0;
    $span_class = '';
    $span_class  = $ui_icons_sidebar[$key];

    if(!empty($value['action'])){
    $action_count = count($value['action']);
    }
    ?>
    <li class="treeview <?php if($value['controller_name'] == $router_class){ echo 'active';} ?>">
    <a href=""><i class="<?php echo $span_class; ?>"></i> <span><?php echo ucfirst($value['controller_alias']); ?></span><?php if($action_count){ ?><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span><?php } ?></a>
    <?php if($action_count){ ?>
    <ul class="treeview-menu" style="<?php echo ($class_name == 'reports')? 'block':'none'; ?>">
    <?php if(!empty($value['action'])){ ?>
    <?php foreach($value['action'] as $key => $val){ ?>
    <?php if(in_array($val['action'],$activemenu)){ ?>
    <li class="<?php if($val['action'] == $router_function){ echo 'active';} ?>">
    <a href="<?php echo site_url('admin/'.$value['controller_name'].'/'.$val['action']); ?>"> <i class="fa fa-circle-o"></i><?php echo ucfirst($val['alias']); ?></a>
    </li>
    <?php } ?>
    <?php } ?>
    <?php } ?>
    </ul>
    <?php } ?>
    </li>
    <?php } ?>
    <?php } ?>
    <?php } ?>

    </ul>


    <?php */ ?>





    <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
    <!--li>
    <a href="">
    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
</li-->

<li class="treeview <?php if($class_name == 'dashboard'){ echo 'active';} ?>">
    <a href=""><i class="fa fa-dashboard"></i> <span>Dashboard</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
    <ul class="treeview-menu" style="<?php echo ($class_name == 'dashboard')? 'block':'none'; ?>">
        <li class="active">
            <a href="<?php echo base_url();?>user/dashboard/index"> <i class="fa fa-circle-o"></i>View Dashboard</a>
        </li>
    </ul>
</li>
   <!--  -->
</ul>
</section>
<!-- /.sidebar -->
</aside>