<?php
//if(empty($this->session->userdata('organization_session_data'))){
   // redirect(base_url('user/login'));
//}
?>
<?php
    $router_class = $this->router->class;
    $router_function = $this->router->method;
    header("cache-Control: no-store, no-cache, must-revalidate");
    header("cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title><?php echo !empty($page_title)?$page_title:'LMS'; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/lte/bootstrap/css/bootstrap.min.css" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/lte/dist/css/AdminLTE.min.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/lte/dist/css/custom.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/lte/plugins/select2/select2.css" />
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/lte/dist/css/skins/_all-skins.min.css" />

  <?php echo put_headers(); ?>

  <!-- jQuery 2.2.0 -->
  <script src="<?php echo base_url(); ?>assets/lte/plugins/jQuery/jQuery-2.2.0.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script type="text/javascript">
    $.widget.bridge('uibutton', $.ui.button);
    var base_url = '<?php echo base_url(); ?>';
  </script>

</head>
<body class="hold-transition sidebar-mini skin-blue">
<div class="wrapper">

  <?php echo get_header(); ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php echo get_left(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php echo get_breadcrumbs(); ?>
    <div class="row row-fluid" style="margin-top: 10px;">
    <div class="col-md-12">
    <?php
     $error = $this->messages->get("error");
     if(isset($error) && !empty($error[0])){ ?>
     <div class="alert alert-danger nor-space"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong><?php echo $this->lang->line('warning'); ?></strong>&nbsp;<?php echo $error[0]; ?></div>
    <?php }
         $success = $this->messages->get("success");
         if(isset($success[0])){ ?>
         <div class="alert alert-success nor-space"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong><?php echo $this->lang->line('success'); ?></strong>&nbsp;<?php echo $success[0]; ?></div>
     <?php } ?>
     <?php
        $message = $this->session->flashdata('message');
        if(isset($message) && !empty($message)){ ?>
        <div class="alert alert-success nor-space"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong><?php echo $this->lang->line('success'); ?></strong>&nbsp;<?php echo $message; ?></div>
    <?php } ?>
      <?php
         $error_message = $this->session->flashdata('error_message');
         if(isset($error_message) && !empty($error_message)){ ?>
         <div class="alert alert-danger nor-space"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong><?php echo $this->lang->line('warning'); ?></strong>&nbsp;<?php echo $error_message; ?></div>
    <?php } ?>
    </div>
    </div>


    <!-- Main content -->
    <?php echo get_content(); ?>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

    <?php echo get_footer(); ?>
  <!-- Control Sidebar -->
  <?php echo get_right(); ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
</div>
<!-- ./wrapper -->


<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url(); ?>assets/lte/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

<!-- Sparkline -->
<script src="<?php echo base_url(); ?>assets/lte/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url(); ?>assets/lte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/lte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(); ?>assets/lte/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

<!-- Slimscroll -->
<script src="<?php echo base_url(); ?>assets/lte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/lte/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/lte/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--script src="<?php echo base_url(); ?>assets/lte/dist/js/pages/dashboard.js"></script-->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/lte/dist/js/demo.js"></script>
<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/lte/plugins/select2/select2.full.min.js"></script>
<script type="text/javascript">
    $(document).ready(function($) {
        $(".select2").select2();
    });
</script>
<?php echo put_footer(); ?>
</body>
</html>
