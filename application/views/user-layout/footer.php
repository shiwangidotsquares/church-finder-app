<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>CI Version</b>
    </div>
    <strong>Copyright &copy; <?php echo date('Y');?> <a href="<?php echo base_url('admin/dashboard') ?>">CMS</a>.</strong> All rights
    reserved.
  </footer>