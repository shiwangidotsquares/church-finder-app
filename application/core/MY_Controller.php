<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class MY_Controller extends CI_Controller
 {
  //set the class variable for layout
	var $data      = array();
	var $error     = array();

	var $template  = array();
	var $middle     = '';
	var $navigation  = '';
	var $breadcrumbs    = '';
    var $sidebar = '';
    var $sidebarLeft = '';
    var $sidebarRight = '';
	var $user_account_session_data = array();
	var $custom_config = array();
    var $session_array = array();

	public function __construct() {
        parent::__construct();
        $this->custom_config = $this->config->config;
        $this->load->model('roles');
        $groups = $this->roles->show_all_active_roles();
        if(!empty($groups)){
            foreach($groups as $key => $value){
                $role_title_array[$value['role_id']] = $value['role_title'];
            }
        }
        $this->config->set_item('role_title_array',$role_title_array);
		
        if($this->session->userdata('user_session_data')){
            $this->session_array = $this->session->userdata('user_session_data');
        }

        $this->load->model('terms','obj_terms');
	
    }

	//Load front end layout
	public function layout(){
      #$this->users_is_login();
	  if (!$this->middle){
		 $this->middle = 'layout/middle';
	  }
      $this->template['sidebar']     = $this->load->view('layout/sidebar', $this->data, true);
	  $this->template['header']     = $this->load->view('layout/header', $this->data, true);
      $this->template['middle']     = $this->load->view($this->middle, $this->data, true);
      $this->template['footer']     = $this->load->view('layout/footer', $this->data, true);
      $this->load->view('layout/index', $this->template);
	}

    //Load front end common layout
	public function adminLayout(){
	  $this->check_user_login();

	  $this->users_session_data = $this->session->userdata('users_session_data');

	  if (!$this->middle){
		 $this->middle = 'admin/common/middle';
	  }
        $this->template['header']         = $this->load->view('admin/common/header', $this->data, true);
        $this->template['navigation']     = $this->load->view('admin/common/navigation', $this->data, true);
        $this->template['breadcrumbs']    = $this->load->view('admin/common/breadcrumbs', $this->data, true);
        $this->template['middle']         = $this->load->view($this->middle, $this->data, true);
        $this->template['sidebar']        = $this->load->view('admin/common/sidebar', $this->data, true);
        $this->template['footer']         = $this->load->view('admin/common/footer', $this->data, true);
      $this->load->view('admin/common/index', $this->template);
	}

	//check user is login or not
	public function check_user_login(){
		if(!$this->session->userdata("admin_session_data")){
			$this->session->set_flashdata('message' ,'Your login session expired! Please login again');
			redirect('admin', 'refresh');
		}else{
			return true;
		}
	}


	//Ajax Layout
	public function ajax_layout(){
	   $this->load->view('layout/ajax', $this->data);
	}

     //load Ckeditor....................
     //$path is relative path of ckeditor
     //$width is width of ckeditor

	 function editor($path,$width){
        //Load Library For Ckeditor.........
        $this->load->library('ckeditor');
        $this->load->library('ckFinder');
        //configure base path of ckeditor folder
        $this->ckeditor->basePath = base_url().'assets/js/ckeditor/';
        $this->ckeditor-> config['toolbar'] = 'Full';
        $this->ckeditor->config['language'] = 'en';
        $this->ckeditor-> config['width'] = $width;
        //configure ckfinder with ckeditor config
        $this->ckfinder->SetupCKEditor($this->ckeditor,$path);
	 }

    function is_exist_check($value, $str){
        $parts = explode('.', $str);
        $this->db->from($parts[0]);
        $this->db->where($parts[1], $value);
        $this->db->where('user_is_deleted', '0');
        $result = $this->db->get();
        if($result->num_rows() > 0) {
            $this->form_validation->set_message('is_exist_check', '%s already exists, please try another one');
            return false;
        }else{
            return true;
        }
     }

    function handle_upload($a,$string){
        $param = preg_split('/,/', $string); #echo '<pre>'; print_r($param); exit;
        $element = $param[0];
        #$fileName = time().'-'.$param[1];
        $fileName = time();
        $folder = $param[2];
        if (isset($_FILES[$element]) && !empty($_FILES[$element])){
            $this->load->library('upload');
            $config['upload_path'] = './uploads/'.$folder.'/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|docs|doc|docx';
            $config['file_name'] = $fileName;
            $config['overwrite'] = TRUE;
            $this->upload->initialize($config);
            if ($this->upload->do_upload($element)){
                // set a $_POST value for 'image' that we can use later
                $upload_data    = $this->upload->data(); #echo '<pre>'; print_r($upload_data);die;
                $_SERVER["REQUEST_METHOD"] = "POST";
                $_POST[$element] = $upload_data['file_name'];
                if(isset($param[3]) && !empty($param[3]) && $param[3] == 'is_thumb'){
                    do_resize_thumb($upload_data['file_name'],$config['upload_path']);
                }
                return true;
            }else{
                // possibly do some clean up ... then throw an error
                $this->form_validation->set_message('handle_upload', $this->upload->display_errors());
                return false;
            }
        }else{
            // throw an error because nothing was uploaded
            $this->form_validation->set_message('handle_upload', "You must upload an image!");
            return false;
        }
	  }

    public function sendmail($to = '', $subject = '', $messagebody = '', $cc = array(), $bcc = array(), $from_email = '', $from_name = '',$attachment = ''){
        $this->load->library('email');
        $config = array();
        $config['useragent']           = "CodeIgniter";
        $config['mailpath']            = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
        $config['protocol']            = "smtp";
        $config['_smtp_auth']          = TRUE;
        $config['smtp_crypto']		   = $this->config->item('smtp_crypto')?$this->config->item('smtp_crypto'):'';
        $config['smtp_host']           = $this->config->item('smtp_host');
        $config['smtp_user']           = $this->config->item('smtp_user');
        $config['smtp_pass']           = $this->config->item('smtp_pass');
        $config['smtp_port']           = $this->config->item('smtp_port');

        $config['mailtype']            = 'html';
        $config['charset']             = 'utf-8';
        $config['newline']             = "\r\n";
        $config['wordwrap']            = TRUE;

        $controller_array = array('coach','sysdocument');

        $this->email->initialize($config);

        $this->email->set_mailtype("html");

        if(!empty($from_email) && !empty($from_name)){
            $this->email->from($from_email, $from_name);
        }else{
            $this->email->from(ADMIN_EMAIL, ADMIN_NAME);
        }


        if(!empty($to) && !is_array($to)){
           $this->email->to($to);
           #$this->email->to('chandan.kumar@dotsquares.com');
        }

        if(!empty($to) && is_array($to)){
            # If you want send multiple Please provide $list = array('one@example.com', 'two@example.com', 'three@example.com');
            $this->email->to($to);
            #$this->email->to('chandan.kumar@dotsquares.com');
        }

        if(!empty($cc)){
            # If you want send multiple Please provide $list = array('one@example.com', 'two@example.com', 'three@example.com');
            $this->email->cc($cc);
        }

		if(!empty($bcc) && !is_array($bcc)){
			$bcc1 = array($bcc);
			$bcc = NULL;
			$bcc = $bcc1;

		}
		#$bcc[] = "vijay.goyal@dotsquares.com";
        if(!empty($bcc)){
            # If you want send multiple Please provide $list = array('one@example.com', 'two@example.com', 'three@example.com');
            $this->email->bcc($bcc);
        }
        #$this->email->bcc('chandan.kumar@dotsquares.com,vijay.goyal@dotsquares.com');
        if(!empty($subject)){
           $this->email->subject($subject);
        }

        if(!empty($messagebody)){
            $this->email->message($messagebody);
        }

        if(!empty($attachment) && !is_array($attachment)){
            if(file_exists('./'.$attachment)){
                $this->email->attach('./'.$attachment);
            }
        }
        if(!empty($attachment) && is_array($attachment)){
            foreach($attachment as $key => $value){
                if(file_exists('./'.$value)){
                    $this->email->attach('./'.$value);
                }
            }
        }

        #$this->email->send();
        #echo $this->email->print_debugger();

        if (!$this->email->send()){
            return false;
        }else{
            return true;
        }
        if(!empty($attachment)){
            if(file_exists('./'.$attachment)){
                $this->email->clear(TRUE);
            }
        }
   }

   public function login_as_user($user_id = 0){
	    $this->load->model('users','obj_user');
		$user_data = $this->obj_user->get_user_info_by_user_id($user_id);
		if($this->session->userdata('user_session_data')){
			$this->session->unset_userdata('user_session_data');
		}
		$this->session->set_userdata('user_session_data',$user_data);
		if($user_data['role_id'] == 2){
			$redirect_url = base_url('account/report/courseprogress');
		}else{
			$redirect_url = base_url('account/mycourse');
		}
		redirect($redirect_url);
	}
}