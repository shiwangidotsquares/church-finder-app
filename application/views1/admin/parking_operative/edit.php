<div class="row">
	<div class="col-lg-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
			</div>
			<?php echo form_open_multipart('', array('name' => 'frm-parking-operative', 'id' => 'frm-parking-operative', 'autocomplete' => 'off')); ?>
			<div class="box-body">
				<input type="hidden" name="user_id" value="<?php echo set_value('user_id',$user_id); ?>">
				<div class="row" >
					<div class="col-md-6" >
						<div class="form-group">
							<label for="user_name">Name <span style="color: red;"> &nbsp;*</span></label>
							<input type="text" maxlength="50" class="form-control" name="user_name" id="user_name" placeholder="Enter Name" value="<?php echo set_value('user_name',$user_name); ?>">
							<span class="form-error"><?php echo form_error('user_name'); ?></span>
						</div>
					</div>
					<div class="col-md-6" >
						<div class="form-group">
							<label for="user_email">Email <span style="color: red;"> &nbsp;*</span></label>
							<input readonly type="text" maxlength="50" class="form-control" name="user_email" id="user_email" placeholder="Enter Email" value="<?php echo set_value('user_email',$user_email); ?>">
							<span class="form-error"><?php echo form_error('user_email'); ?></span>
						</div>
					</div>
				</div>
				<div class="row" >
					<div class="col-md-6" >
						<div class="form-group">
							<label for="user_password">Current Password</label>
							<input disabled type="text" maxlength="20" class="form-control" name="show_user_password" id="show_user_password" placeholder="" value="<?php echo set_value('show_user_password',$show_user_password); ?>">

						</div>
					</div>
					<div class="col-md-6" >
						<div class="form-group">
							<label for="user_password">New Password </label>
							<input type="password" maxlength="20" class="form-control" name="user_password" id="user_password" placeholder="Enter Password" value="">
							<span class="form-error"><?php echo form_error('user_password'); ?></span>
						</div>
					</div>
				</div>
				<div class="row" >
					<div class="col-md-6" >
						<div class="form-group">
							<label for="confirm_password">Confirm Password </label>
							<input type="password" maxlength="20" class="form-control" name="confirm_password" id="confirm_password" placeholder="Enter Confirm Password" value="">
							<span class="form-error"><?php echo form_error('confirm_password'); ?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Save</button>
				<a href="<?php echo base_url('admin/parking_operative/index');?>"> <button type="button" class="btn btn-white">Cancel</button></a>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
	$( document ).ready(function() {

		$.validator.addMethod("noSpace", function(value, element) {
		  return value == '' || value.trim().length != 0;
		}, "No space please and don't leave it empty");

		jQuery("#frm-parking-operative").validate({
			rules: {
				user_name:{
					required: true,
					noSpace:true,
				},
				user_email:{
					required: true,
					email: true,
					noSpace:true,
				},
				user_password:{
					minlength: 6,
					noSpace:true,
				},
				confirm_password:{
					equalTo: "#user_password",
					minlength: 6,
				}
			},
			messages: {
				user_name:{
					required: "The Name field is required.",
				},
				user_email: {
					required: "The Email field is required.",
				},
				user_password:{
					required: "The Password field is required.",
				},
				confirm_password:{
					required: "The Confirm Password field is required.",
				}
			},
		});
	});
</script>
