<div class="row">
	<div class="col-lg-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
			</div>
			<div class="row-fluid">
				<div class="col-lg-3">
					<a href="<?php echo base_url('admin/parking_operative/add');?>" class="btn btn-primary ">Add Parking Operative</a>
				</div>
				<div class="col-lg-3 ">
				</div>
				<div class="col-lg-6 ">
					<div class="filter">
						<label>Search Keyword:</label>
						<input class="form-control" type="text" id="searchBox" name="searchBox" />
						<input type="button" value="Search" id="searchButton" class="btn btn-default blue-bg" />
						<input type="reset" value="Reset" id="reSet" class="btn btn-default orange-bg dis-inline width-auto" />
					</div>
				</div>
			</div>
			<div class="box-body">
				<table class="table table-striped table-bordered table-hover jambo_table" id="user_manager" >
					<thead>
						<tr class="headings">
							<th>#</th>
							<th>Name</th>
							<th>Email</th>
							<th>Created</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var oTable;
	$(document).ready(function() {
		oTable= $('#user_manager').dataTable({
			"processing": true,
			"serverSide": true,
			"fnDrawCallback" : function(oSettings){$('#user_manager').tooltip();},
			"ajax": {
				"url": "<?php echo base_url('admin/parking_operative/get_parking_operative'); ?>",
				"type": "POST",
				"data": function ( d ){
					d.myKey = "myValue";
					d.searchBox = $('#searchBox').val();
				}
			},
			"bFilter": false,
			"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,4 ] }],
			"order": [[ 3, "desc" ]],
			"aoColumns": [
			{ "data": "sr_no", "sClass": "text-center"},
			{ "data": "user_name" },
			{ "data": "user_email" },
			{ "data": "created_time" },
			{ "data": "action" }
			]
		});

		$('#searchButton').click( function(){
			oTable.fnDraw();
		});
		$('#reSet').click( function(){
			$('#searchBox').val('');
			oTable.fnDraw();
		});
		$('#searchBox').keydown(function (e){
			if(e.keyCode == 13){
				oTable.fnDraw();
			}
		});
	});

	jQuery("#user_manager" ).on("click", ".activeRecord", function(){
		var sID= jQuery(this).attr('rel');
		var button=jQuery(this);
		$.ajax({
			url: '<?php echo site_url('admin/parking_operative/status'); ?>',
			type: 'post',
			/*dataType: 'json',*/
			data: {sID:sID, sStatus:0},
			success: function(data){
				var myObj = JSON.parse(data);

				if(myObj.status == 'pending'){
					swal("Warning", "This Parking Operative cannot be deactivate as currently assigned with some business.", "error");
				}else{
					swal("Deleted", "Parking Operative has been deactivated successfully.", "success");
				}

				button.removeClass('activeRecord').addClass('deactiveRecord');
				button.removeClass('fa fa-check-square').addClass('fa fa-ban');
				button.attr('title','active');
				$('#user_manager').tooltip();
				/*swal("", "Parking Operative has been deactivated successfully.", "success");*/
				oTable.fnDraw();
			},
			error: function(){
				swal("warning", "Don't have permision to dactivate Parking Operative", "error");
			}
		});
	});


	jQuery("#user_manager" ).on("click", ".deactiveRecord", function(){
		var sID= jQuery(this).attr('rel');
		var button=jQuery(this);
		$.ajax({
			url: '<?php echo site_url('admin/parking_operative/status'); ?>',
			type: 'post',
			dataType: 'json',
			data: {sID:sID, sStatus:1},
			success: function(data){
				button.addClass('activeRecord').removeClass('deactiveRecord');
				button.removeClass('fa fa-ban').addClass('fa fa-check-square');
				button.attr('title','inactive');
				$('#user_manager').tooltip();
				swal("", "Parking Operative has been activated successfully!", "success");
				oTable.fnDraw();
			},
			error: function(){
				swal("warning", "Don't have permision to actived Parking Operative.", "error");
			}
		});
	});

	jQuery("#user_manager" ).on("click", ".deleteRecord", function(){
		var sID = jQuery(this).attr('rel');
        //alert(sID);
        swal({
        	title: "Are you sure?",
        	text: "You will not be able to recover this Parking Operative in future!",
        	type: "warning",
        	showCancelButton: true,
        	confirmButtonColor: "#DD6B55",
        	confirmButtonText: "Yes, delete it!",
        	cancelButtonText: "No, cancel plz!",
        	closeOnConfirm: true,
        	closeOnCancel: true
        },
        function(isConfirm){
        	if (isConfirm) {
        		$.ajax({
        			url: '<?php echo site_url('admin/parking_operative/delete'); ?>',
        			type: 'post',
        			/*dataType: 'json',*/
        			data: {sID:sID},
        			success: function(data){
        				var myObj = JSON.parse(data);

        				if(myObj.status == 'pending'){
        					swal("warning", "This Parking Operative cannot be deleted as currently assigned with some business.", "error");
        				}else{
        					/*swal("Deleted", "Parking Operative has been deleted.", "success");*/
        				}
        				bResetDisplay = false;
        				/* override default behaviour */
        				oTable.fnDraw();
        				bResetDisplay = true;
        				/*restore default behaviour */
        			},
        			error: function(){
        				swal("warning", "Don't have permision to delete Parking Operative.", "error");
        			}
        		});
        	} else {
        		/*swal("Cancelled", "Parking Operative is safe now.", "error");*/
        	}
        });
    });
	$('#searchBox').keydown(function (e){
		if(e.keyCode == 13){
			oTable.fnDraw();
		}
	});
</script>