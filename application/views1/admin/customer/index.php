<div class="row">
	<div class="col-lg-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
			</div>
			<div class="row-fluid">
				<div class="col-md-12">
					<!-- <div class="col-lg-3">
						<label for="dr_parking_off_data">Search By Parking Operative</label>
						<select class="form-control" name="dr_parking_off_data" id="dr_parking_off_data">
							<option value="">--Please Select--</option>
							<?php if(!empty($parking_operative_data)){
								foreach ($parking_operative_data as $key => $value) {?>
									<option value="<?php echo $value['user_id']; ?>"><?php echo $value['user_name']; ?></option>
								<?php }
							}
							?>
						</select>
					</div> -->
					<div class="col-lg-3 ">
						<div id="divParkingOperative">
							<label for="dr_business_data">Search By Business</label>
							<select class="form-control" name="dr_business_data" id="dr_business_data">
								<option value="">--Please Select--</option>
								<?php if(!empty($business_data)){
									foreach ($business_data as $key => $val) {?>
										<option value="<?php echo $val['user_id']; ?>"><?php echo $val['business_name']."(".$val['user_email'].")"; ?></option>
									<?php }
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-lg-4">
					<!-- <div class="col-lg-4 col-md-offset-1 "> -->
						<div class="row" >
							<div class="col-lg-11">
							<!-- <div class="col-lg-11 col-md-offset-1 "> -->
								<label for="min-date">Search By Date</label>
							</div>
							<div class="col-md-5 datePicIcon" >
							<!-- <div class="col-md-5 col-md-offset-1 datePicIcon" > -->
								<input type="text" autocomplete="off" name="min-date" id="min-date" class="form-control date-range-filter" data-date-format="dd-mm-yyyy" placeholder="From:" value="<?php echo date("Y-m-d") ?>">
							</div>
							<div class="col-md-1 padd0 text-center" >
								<span class="dateRangeMidTO" >To</span>
							</div>
							<div class="col-md-5 datePicIcon" >
								<input type="text" autocomplete="off" name="max-date" id="max-date" class="form-control date-range-filter" data-date-format="dd-mm-yyyy" placeholder="To:">
							</div>
						</div>
					</div>
					<div class="col-lg-5 ">
						<div class="filter" style="margin-top: 0px!important">
							<label>Search Registration:</label><br />
							<input class="form-control" type="text" id="searchBox" name="searchBox" />
							<input type="button" value="Search" id="searchButton" class="btn btn-default blue-bg" />
							<input type="reset" value="Reset" id="reSet" class="btn btn-default orange-bg dis-inline width-auto" />
						</div>
					</div>
					</div>
			</div>
			<!-- <div class="row-fluid">
				<div class="col-lg-3 "></div>
				<div class="col-lg-3 "></div>
				<div class="col-lg-6 ">
					<div class="filter">
						<label>Search Registration:</label><br />
						<input class="form-control" type="text" id="searchBox" name="searchBox" />
						<input type="button" value="Search" id="searchButton" class="btn btn-default blue-bg" />
						<input type="reset" value="Reset" id="reSet" class="btn btn-default orange-bg dis-inline width-auto" />
					</div>
				</div>
			</div> -->
			<div class="box-body">
				<table class="table table-striped table-bordered table-hover jambo_table" id="customer_manager" >
					<thead>
						<tr class="headings">
							<th>#</th>
							<th>Registration Number</th>
							<th>In Time</th>
							<th>Out Time</th>
							<th>Business</th>
							<th>Parking Operative</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- The Modal -->
<div class="modal" id="myModal">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Assign Parking Operative</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body" id="parkingOperativeData">

			</div>

			<!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" id="assignButton">Assign</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
	var oTable;
	/*$('.dataTables_empty').text("No record found !");*/
	$(document).ready(function() {
		oTable= $('#customer_manager').dataTable({
			"processing": true,
			"serverSide": true,
			"language": {
			        emptyTable: '<span class="datatables-empty-message">No record found !</span>'
			},
			"fnDrawCallback" : function(oSettings){
				$('#customer_manager').tooltip();
				$('.datatables-empty-message').text("Vehicle "+$('#searchBox').val()+" has not registered for a virtual permit");
			},
			"ajax": {
				"url": "<?php echo base_url('admin/customer/get_customer_list'); ?>",
				"type": "POST",
				"data": function ( d ){
					d.myKey = "myValue";
					d.searchBox = $('#searchBox').val();
					d.dr_business_data    = $('#dr_business_data').val();
					d.dr_parking_off_data = $('#dr_parking_off_data').val();
					d.mindate = $('#min-date').val();
					d.maxdate = $('#max-date').val();
				}
			},
			"bFilter": false,
			"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 ] }],
			"order": [[ 3, "desc" ]],
			"aoColumns": [
			{ "data": "sr_no", "sClass": "text-center"},
			{ "data": "vehicle_registration_number" },
			{ "data": "in_time" },
			{ "data": "out_time" },
			{ "data": "business_id" },
			{ "data": "parking_officer_id" }
			]
		});
		$('#reSet').click( function(){
			$('#searchBox').val('');
			oTable.fnDraw();
		});
		$('#searchButton').click( function(){
			oTable.fnDraw();
		});

		$('#searchBox').keydown(function (e){
			if(e.keyCode == 13){
				oTable.fnDraw();
			}
		});

		$('.date-range-filter').change(function() {
			oTable.fnDraw();
		});
		$(document).on('change', '#dr_business_data', function(event) {
			oTable.fnDraw();
		});

		$('#dr_parking_off_data').change(function() {
			var parkingOperativeId = $(this).val();

			$.ajax({
				url: '<?php echo site_url('admin/customer/get_business_list'); ?>',
				type: 'post',
				dataType: 'json',
				data: {parkingOperativeId:parkingOperativeId},
				success: function(data){

		     		var parkingOperative = [];
					parkingOperative = data;
					var selected = '';

					var operativeData = '<label for="dr_business_data">Search By Business</label><select class="form-control" name="dr_business_data" class="dr_business_data" id="dr_business_data"><option value="">--Please Select--</option>';

					jQuery.each(parkingOperative, function(i, Qitem) {
						operativeData += '<option value="'+Qitem.user_id+'">'+Qitem.user_name+'</option>';
					});
					operativeData += '</select>';
					$('#divParkingOperative').html(operativeData);
					oTable.fnDraw();
				},
				error: function(){
					swal("warning", "Don't have permision to actived Parking Operative.", "error");
				}
			});
		});
		$('#dr_parking_off_data').change(function() {
			oTable.fnDraw();
		});
		$("#min-date").datepicker({
			autoclose: true,
			format: 'yyyy-mm-dd',
		}).on('changeDate', function (selected) {
			var minDate = new Date(selected.date.valueOf());
			$('#max-date').datepicker('setStartDate', minDate);
			$('#max-date').val("");
		});

		$("#max-date").datepicker({
			autoclose: true,
			format: 'yyyy-mm-dd',
		})
	});
</script>