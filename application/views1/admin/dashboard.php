<link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,700" rel="stylesheet">
<section class="content dashboardOuter">
	<!-- Info boxes -->
	<div class="row">
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="bgDiv-infoBox" ></div>
			<div class="info-box">
				<a href="<?php echo base_url('admin/parking_operative'); ?>">
					<div class="info-box-icon bg-aqua">
						<img src="<?php echo base_url();?>assets/admin/img/user-profile.png">
						<div class="info-box-content">
							<span class="info-box-text">Parking Operative</span>
							<span class="info-box-number"><?php echo ($operative_count)?$operative_count:0;?></span>
						</div>
					</div>
				</a>

				<!-- /.info-box-content -->
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.col -->
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="bgDiv-infoBox" ></div>
			<div class="info-box">
				<a href="<?php echo base_url('admin/business'); ?>">
				<div class="info-box-icon bg-aqua">
					<img src="<?php echo base_url();?>assets/admin/img/business.png">
					<div class="info-box-content">
						<span class="info-box-text">Businesses</span>
						<span class="info-box-number"><?php echo ($business_count)?$business_count:0;?></span>
					</div>
				</div>
				</a>
			</div>
		</div>

		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="bgDiv-infoBox" ></div>
			<div class="info-box">
			<a href="<?php echo base_url('admin/customer'); ?>">
				<div class="info-box-icon bg-aqua">
					<img src="<?php echo base_url();?>assets/admin/img/vertual-permit.png">
					<div class="info-box-content">
						<span class="info-box-text">Virtual Permit Management</span>
						<span class="info-box-number"><?php echo ($customer_count)?$customer_count:0;?></span>
					</div>
				</div>
			</a>
				<!-- /.info-box-content -->
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.col -->

		<!-- fix for small devices only -->
		<div class="clearfix visible-sm-block"></div>

		<div class="col-md-3 col-sm-6 col-xs-12">

			<div class="bgDiv-infoBox" ></div>
			<a href="<?php echo base_url('admin/change_password'); ?>">
				<div class="info-box">
					<div class="info-box-icon bg-aqua">
						<img src="<?php echo base_url();?>assets/admin/img/settings.png">
						<div class="info-box-content">
							<span class="info-box-text">Change Password</span>
							<span class="info-box-number"> &nbsp </span>
						</div>
					</div>
				</div>
			</a>
		</div>

		<!-- /.col -->
	</div>
	<!-- /.row -->
