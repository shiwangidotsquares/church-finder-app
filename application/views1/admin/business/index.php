<link rel="stylesheet" href="<?php echo base_url();?>assets/css/chosen/chosen.min.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>assets/js/chosen/chosen.jquery.min.js"></script>
<style type="text/css">
	.ms-options ul{
		list-style: none;
		padding: 0 5px;
	}
	.ms-selectall{
		text-transform: capitalize!important;
	}

</style>
<div class="row">
	<div class="col-lg-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
			</div>
			<div class="row-fluid">
				<div class="col-md-12">
					<div class="tableInnerFilter1" >
						<div class="col-lg-3">
							<div style="display: none">
								<label for="dr_parking_off_data">Search By Parking Operative</label>
								<select class="form-control" name="dr_parking_off_data" id="dr_parking_off_data">
									<option value="">--Please Select--</option>
									<?php if(!empty($parking_operatives_data)){
										foreach ($parking_operatives_data as $key => $value) {?>
											<option value="<?php echo $value['user_id']; ?>"><?php echo $value['user_name']; ?></option>
										<?php }
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-lg-3 ">
						</div>
						<div class="col-lg-6 ">
							<div class="filter">
								<label>Search Keyword:</label>
								<input class="form-control" type="text" id="searchBox" name="searchBox" />
								<input type="button" value="Search" id="searchButton" class="btn btn-default blue-bg" />
								<input type="reset" value="Reset" id="reSet" class="btn btn-default orange-bg dis-inline width-auto" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box-body">
				<table class="table table-striped table-bordered table-hover jambo_table" id="user_manager" >
					<thead>
						<tr class="headings">
							<th>#</th>
							<th>Business Name</th>
							<th>Email</th>
							<th>Contact Number</th>
							<th>Business Type</th>
							<th>Location</th>
							<th>Parking Operative</th>							
							<th>Created</th>
							<th>Extended</th>
							<th width="13%">Action</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- The Modal -->
<div class="modal" id="myModal">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Assign Parking Operative</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body" id="parkingOperativeData">

			</div>

			<!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="assignButton">Assign</button>
				<button type="button" class="btn btn-danger btn-rounded" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>
<link href="<?php echo base_url(); ?>assets/jquery.multiselect.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url(); ?>assets/jquery.multiselect.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        jQuery(".chosen").chosen({ width:"100%" });
    });
</script>
<script type="text/javascript">
	var oTable;
	$(document).ready(function() {

		oTable= $('#user_manager').dataTable({
			"processing": true,
			"serverSide": true,
			"fnDrawCallback" : function(oSettings){$('#user_manager').tooltip();},
			"ajax": {
				"url": "<?php echo base_url('admin/business/get_business_list'); ?>",
				"type": "POST",
				"data": function ( d ){
					d.myKey = "myValue";
					d.searchBox = $('#searchBox').val();
					d.dr_parking_off_data = $('#dr_parking_off_data').val();
				}
			},
			"bFilter": false,
			"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,9 ] }],
			"order": [[ 1, "ASC" ]],
			"aoColumns": [
			{ "data": "sr_no", "sClass": "text-center"},
			{ "data": "business_name" },
			{ "data": "user_email" },
			{ "data": "contact_number" },
			{ "data": "business_type" },
			{ "data": "location" },
			{ "data": "parking_operative_id" },
			{ "data": "created_time" },
			{ "data": "is_extend" },
			{ "data": "action" }
			]
		});

		$('#searchButton').click( function(){
			oTable.fnDraw();
		});
		$('#reSet').click( function(){
			$('#searchBox').val('');
			oTable.fnDraw();
		});
		$('#dr_parking_off_data').change(function() {
			oTable.fnDraw();
		});
		$('#searchBox').keydown(function (e){
			if(e.keyCode == 13){
				oTable.fnDraw();
			}
		});
	});

	jQuery("#user_manager" ).on("click", ".activeRecord", function(){
		var sID= jQuery(this).attr('rel');
		var button=jQuery(this);
		$.ajax({
			url: '<?php echo site_url('admin/business/status'); ?>',
			type: 'post',
			dataType: 'json',
			data: {sID:sID, sStatus:0},
			success: function(data){
				button.removeClass('activeRecord').addClass('deactiveRecord');
				button.removeClass('fa fa-check-square').addClass('fa fa-ban');
				button.attr('title','active');
				$('#user_manager').tooltip();
				swal("", "Business has been deactivated successfully!", "success");
				oTable.fnDraw();
			},
			error: function(){
				swal("warning", "Don't have permision to dactivate Business", "error");
			}
		});
	});


	jQuery("#user_manager" ).on("click", ".deactiveRecord", function(){
		var sID= jQuery(this).attr('rel');
		var button=jQuery(this);
		$.ajax({
			url: '<?php echo site_url('admin/business/status'); ?>',
			type: 'post',
			/*dataType: 'json',*/
			data: {sID:sID, sStatus:1},
			success: function(data){

				var myObj = JSON.parse(data);

				if(myObj.status == 'pending'){
					swal("Warning", "Please assign parking operative first before active this business.", "error");
				}else{
					swal("success", "Business has been activated successfully.", "success");
				}

				button.addClass('activeRecord').removeClass('deactiveRecord');
				button.removeClass('fa fa-ban').addClass('fa fa-check-square');
				button.attr('title','inactive');
				$('#user_manager').tooltip();

				oTable.fnDraw();
			},
			error: function(){
				swal("warning", "Don't have permision to actived Business.", "error");
			}
		});
	});
	jQuery(document).on("click", "#assignButton", function(){
		var selectedVal = $('#selParkingOperative').val();

		/*var selectedVal = $("#selParkingOperative option:selected").val();*/
		var business_id = $('#selParkingOperative').attr('rel');

		if (selectedVal != null) {
			$.ajax({
				url: '<?php echo site_url('admin/business/assign_parking_operative'); ?>',
				type: 'post',
				dataType: 'json',
				data: {selectedVal:selectedVal,business_id:business_id},
				success: function(data){
					$('#myModal').modal('hide')
					swal("Assigned", "Parking Operative has been assigned successfully.", "success");
					bResetDisplay = false;
					/* override default behaviour */
					oTable.fnDraw();
					bResetDisplay = true;
					/*restore default behaviour */
				},
				error: function(){
					swal("Warning", "Don't have permision to assign Parking Operative.", "error");
				}
			});
		} else {
			swal("Not Selected", "Please select a Parking Operative.", "error");
		}

	});
	jQuery("#user_manager" ).on("click", ".assignOperative", function(){
		var existing_id= jQuery(this).attr('rel');
		var business_id= jQuery(this).attr('business');
		$.ajax({
			url: '<?php echo site_url('admin/business/get_parking_operative'); ?>',
			type: 'post',
			dataType: 'json',
			success: function(data){

				parkingOperative = data;
				var selected = '';
				/*if(existing_id!=''){
					selected = '<?php echo 'selected="selected"' ?>';
				}*/
				var operativeData = '<select class="form-control" id="selParkingOperative" name="selParkingOperative[]" rel="'+business_id+'" multiple>';

				jQuery.each(parkingOperative, function(i, Qitem) {
					if(existing_id == Qitem.user_id){
						operativeData += '<option value="'+Qitem.user_id+'" '+selected+'>'+Qitem.user_name+'</option>';
					}else{
						operativeData += '<option value="'+Qitem.user_id+'">'+Qitem.user_name+'</option>';
					}

				});
				operativeData += '</select>';
				$('#parkingOperativeData').html(operativeData);
				$('#selParkingOperative').multiselect({
				    columns: 1,
				    placeholder: 'Select Parking Operative',
				    search: true,
				    selectAll: true
				});
			},
			error: function(){
				swal("warning", "Don't have permision to actived Business.", "error");
			}
		});
	});

	jQuery("#user_manager" ).on("click", ".deleteRecord", function(){
		var sID = jQuery(this).attr('rel');

		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this Business in future!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			cancelButtonText: "No, cancel plz!",
			closeOnConfirm: true,
			closeOnCancel: true
		},
		function(isConfirm){
			if (isConfirm) {
				$.ajax({
					url: '<?php echo site_url('admin/business/delete'); ?>',
					type: 'post',
					dataType: 'json',
					data: {sID:sID},
					success: function(data){
						/*swal("Deleted", "Business has been deleted successfully.", "success");*/
						bResetDisplay = false;
						/* override default behaviour */
						oTable.fnDraw();
						bResetDisplay = true;
						/*restore default behaviour */
					},
					error: function(){
						swal("warning", "Don't have permision to actived Business.", "error");
					}
				});
			} else {
				/*swal("Cancelled", "Business is safe now.", "error");*/
			}
		});
	});
	$('#searchBox').keydown(function (e){
		if(e.keyCode == 13){
			oTable.fnDraw();
		}
	});
	
	
	jQuery("#user_manager" ).on("click", ".extand", function(){
		var sID= jQuery(this).attr('rel');
		var button=jQuery(this);
		var is_extend = jQuery(this).data('status');
		
		$.ajax({
			url: '<?php echo site_url('admin/business/changeExtendStatus'); ?>',
			type: 'post',
			dataType: 'json',
			data: {sID:sID, is_extend:is_extend},
			success: function(data){ 
				swal("", "Extend status has been changed successfully!", "success");
				oTable.fnDraw();
				
				/*if(is_extend == 0){ 
					button.removeClass('btn btn-success').addClass('btn btn-default').html('Retract');
					button.attr('data-status', 1); 
				}else{
					button.removeClass('btn btn-default').addClass('btn btn-success').html('Extend');
					button.attr('data-status', 0);
				}*/
			},
			error: function(){
				swal("warning", "Don't have permision to do this event", "error");
			}
		});
	});
</script>