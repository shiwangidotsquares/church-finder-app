<div class="row">
	<div class="col-lg-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
			</div>
			<?php echo form_open_multipart('', array('name' => 'frm-parking-operative', 'id' => 'frm-parking-operative', 'autocomplete' => 'off')); ?>
			<div class="box-body">

				<div class="row" >
					<div class="col-md-12" >
						<div class="form-group">
							<label for="old_password">Old Password <span style="color: red;"> &nbsp;*</span></label>
							<input type="password" class="form-control" name="old_password" id="old_password" placeholder="Enter Old Password" value="">
							<span class="form-error"><?php echo form_error('old_password'); ?></span>
						</div>
					</div>
					<div class="col-md-12" >
						<div class="form-group">
							<label for="new_password">New Password <span style="color: red;"> &nbsp;*</span></label>
							<input type="password" maxlength="20" class="form-control" name="new_password" id="new_password" placeholder="Enter New Password" value="">
							<span class="form-error"><?php echo form_error('new_password'); ?></span>
						</div>
					</div>
					<div class="col-md-12" >
						<div class="form-group">
							<label for="confirm_password">Confirm Password <span style="color: red;"> &nbsp;*</span></label>
							<input type="password" maxlength="20" class="form-control" name="confirm_password" id="confirm_password" placeholder="Enter Confirm Password" value="">
							<span class="form-error"><?php echo form_error('confirm_password'); ?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Save</button>
				<a href="<?php echo base_url('admin/parking_operative/index');?>"> <button type="button" class="btn btn-white">Cancel</button></a>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
	$( document ).ready(function() {

		$.validator.addMethod("noSpace", function(value, element) {
		  return value == '' || value.trim().length != 0;
		}, "No space please and don't leave it empty");

		jQuery("#frm-parking-operative").validate({
			rules: {
				old_password:{
					required: true,
					minlength: 6,
					noSpace:true,
				},
				new_password:{
					required: true,
					minlength: 6,
					noSpace:true,
				},
				confirm_password:{
					equalTo: "#new_password",
					minlength: 6,
				}
			},
			messages: {
				old_password:{
					required: "The Old Password field is required.",
				},
				new_password:{
					required: "The New Password field is required.",
				},
				confirm_password:{
					required: "The Confirm Password field is required.",
				}
			},
		});
	});
</script>
