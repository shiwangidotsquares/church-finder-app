<div class="row">
	<div class="col-lg-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
			</div>
			<div class="row-fluid">
				<div class="box-body">
					<h3 class="font-bold">Don`t have permission to do this action </h3>
					<div class="error-desc">
						You can create here any grid layout you want. And any variation layout you imagine:) Check out
						main dashboard and other site. It use many different layout.
						<br/> <br/><br/>
						<div align="center">
							<a href="<?php echo base_url('admin/dashboard');?>" class="btn btn-primary m-t">Dashboard</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>