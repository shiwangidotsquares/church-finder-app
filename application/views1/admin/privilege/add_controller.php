<div class="row">
	<div class="col-lg-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo !empty($form_title)?$form_title:'' ?></h3>
			</div>

			<?php echo form_open_multipart('',array('name' => 'privilege_form', 'id' => 'block-validate', 'class' => 'form-horizontal'));?>
			<div class="box-body">
				<div class="form-group">
					<label class="col-sm-2 control-label">Controller Name</label>
					<div class="col-sm-10">
						<input type="text" id="controller_name" name="controller_name" value="<?php if(isset($controller_name)){ echo $controller_name; } ?>" class="form-control" data-required="true" />
						<span class="required-server"><?php echo form_error('controller_name'); ?> </span>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Alias Name</label>
					<div class="col-sm-10">
						<input type="text" id="alias" name="alias" value="<?php if(isset($alias)){ echo $alias; } ?>" class="form-control" data-required="true" />

						<span class="required-server"><?php echo form_error('alias'); ?> </span>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Ordering</label>
					<div class="col-sm-10">
						<input type="text" name="ordering" id="ordering" value="<?php set_value('ordering',0) ?>" class="form-control" />
						<span class="required-server"><?php echo form_error('ordering'); ?> </span>
					</div>
				</div>
				<div class="box-footer">
					<input type="submit" value="<?php if(!empty($buttonText)){echo $buttonText;}else{ echo 'Save';} ?>" class="btn btn-info pull-right" />
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div><!-- /.col-lg-12 -->
</div>

