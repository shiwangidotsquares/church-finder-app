<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>App PCM | Login</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/lte/bootstrap/css/bootstrap.min.css" />
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" />
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" />
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/lte/dist/css/AdminLTE.min.css" />
	<!-- iCheck -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/lte/plugins/iCheck/square/blue.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css" />
	<link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<a href="javascript:void(0);">
				<img src="<?php echo base_url(); ?>assets/admin/img/ps-logo.png" width="300" >
			</a>
		</div>
		<!-- /.login-logo -->
		<div class="login-box-body">
			<h3 class="login-box-msg">Reset Your Password</h3>
			<?php
			$error = $this->messages->get("error");
			if(isset($error[0])){ ?>
				<div class="alert alert-danger nor-space"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Warning</strong>&nbsp;<?php echo $error[0]; ?></div>
			<?php }
			$success = $this->messages->get("success");
			if(isset($success[0])){ ?>
				<div class="alert alert-success nor-space"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong>&nbsp;<?php echo $success[0]; ?></div>
			<?php } ?>
			<?php
			$message = $this->session->flashdata('message');
			if(isset($message) && !empty($message)){ ?>
				<div class="alert alert-success nor-space"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong><?php echo $this->lang->line('success'); ?></strong>&nbsp;<?php echo $message; ?></div>
			<?php } ?>
			<?php
			$error_message = $this->session->flashdata('error_message');
			if(isset($error_message) && !empty($error_message)){ ?>
				<div class="alert alert-danger nor-space"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong><?php echo $this->lang->line('warning'); ?></strong>&nbsp;<?php echo $error_message; ?></div>
			<?php } ?>

			<form class="m-t" role="form" method="post" action="" id="frmlogin">
				<div class="form-group has-feedback">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					<input type="password" class="form-control" placeholder="New Password" id="password" name="new_password"/>
					<span class="form-error"><?php echo form_error('password'); ?></span>
				</div>
				<div class="form-group has-feedback">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					<input type="password" class="form-control" placeholder="Confirm Password" id="confirm_password" name="confirm_password"/>
					<span class="form-error"><?php echo form_error('confirm_password'); ?></span>
				</div>
				<div class="row">
					<!-- /.col -->
					<div class="col-xs-5">
						<button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
					</div>
					<!-- /.col -->
				</div>
			</form>
		</div>
		<!-- /.login-box-body -->
	</div>
	<!-- /.login-box -->

	<!-- jQuery 2.2.0 -->
	<script src="<?php echo base_url(); ?>assets/lte/plugins/jQuery/jQuery-2.2.0.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="<?php echo base_url(); ?>assets/lte/bootstrap/js/bootstrap.min.js"></script>
	<!-- iCheck -->
	<script src="<?php echo base_url(); ?>assets/lte/plugins/iCheck/icheck.min.js"></script>
	<script>
		$(function () {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
      			increaseArea: '20%' // optional
      		});
		});
	</script>

	<script>
		jQuery(document).ready(function() {
			jQuery.validator.addMethod("noSpace", function(value, element) {
				return value == '' || value.trim().length != 0;
			}, "No space please and don't leave it empty");
    //login form
    jQuery("#frmlogin").validate({
    	rules: {
    		password:{
    			required: true,
    			minlength: 6,
    		},
    		confirm_password:{
    			equalTo: "#password",
    			minlength: 6,
    		}
    	},
    	messages: {
    		password: {
    			required: "Please provide a password",
    		}
    	}
    });
});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>
<!--JavaScript -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
</body>
</html>


