<?php
$role_id = 0;
$action_count = 0;
$ui_icons_sidebar = array();
$router_class = '';
$active_class = '';
$router_class = $this->router->class;
$router_function = $this->router->method;
$session_data = $this->session->userdata('admin_session_data');
$ui_icons_sidebar = $this->config->item('ui_icons_sidebar');
$activemenu = array('index','Index','add','Add','send_email','add_final','viewfinalconfiguration','import_questions','report','view');
$mainMenu = array('ajax');
if(!empty($session_data) && isset($session_data['role_id'])){
	$role_id = $session_data['role_id'];
}
$sidebar = getSidebar($role_id);

?>
<aside class="main-sidebar">
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
		</div>
		<ul class="sidebar-menu">
			<?php if(!empty($sidebar)){ ?>
				<?php foreach($sidebar as $key => $value){?>
					<?php if(!in_array($value['controller_name'],$mainMenu)){ ?>
						<?php
						$action_count = 0;
						$span_class = '';
						$span_class  = $ui_icons_sidebar[$key];
						if(!empty($value['action'])){
							$action_count = count($value['action']);
						}
						?>
						<li class="treeview <?php if($value['controller_name'] == $router_class){ echo 'active';} ?>">
							<a href=""><i class="<?php echo $span_class; ?>"></i> <span><?php echo ucfirst($value['controller_alias']); ?></span><?php if($action_count){ ?><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span><?php } ?></a>
							<?php if($action_count){ ?>
								<ul class="treeview-menu">
									<?php if(!empty($value['action'])){ ?>
										<?php foreach($value['action'] as $key => $val){ ?>
											<?php if(in_array($val['action'],$activemenu)){ ?>
												<li class="<?php if($val['action'] == $router_function){ echo 'active';} ?>">
													<a href="<?php echo site_url('admin/'.$value['controller_name'].'/'.$val['action']); ?>"> <i class="fa fa-circle-o"></i><?php echo ucfirst($val['alias']); ?></a>
												</li>
											<?php } ?>
										<?php } ?>
									<?php } ?>
								</ul>
							<?php } ?>
						</li>
					<?php } ?>
				<?php } ?>
			<?php } ?>
		</ul>
	</section>
</aside>