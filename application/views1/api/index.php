<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>:: API ::</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<style type="text/css">
	.bs-example{
		margin: 20px;
	}
</style>
<style type="text/css">

::selection{ background-color: #E13300; color: white; }
::moz-selection{ background-color: #E13300; color: white; }
::webkit-selection{ background-color: #E13300; color: white; }

body {
	background-color: #fff;
	margin: 40px;
	font: 13px/20px normal Helvetica, Arial, sans-serif;
	color: #4F5155;
}

a {
	color: #003399;
	background-color: transparent;
	font-weight: normal;
}

h1 {
	color: #444;
	background-color: transparent;
	border-bottom: 1px solid #D0D0D0;
	font-size: 19px;
	font-weight: normal;
	margin: 0 0 14px 0;
	padding: 14px 15px 10px 15px;
}

code {
	font-family: Consolas, Monaco, Courier New, Courier, monospace;
	font-size: 12px;
	background-color: #f9f9f9;
	border: 1px solid #D0D0D0;
	color: #002166;
	display: block;
	margin: 14px 0 14px 0;
	padding: 12px 10px 12px 10px;
}

#body{
	margin: 0 15px 0 15px;
}

p.footer{
	text-align: right;
	font-size: 11px;
	border-top: 1px solid #D0D0D0;
	line-height: 32px;
	padding: 0 10px 0 10px;
	margin: 20px 0 0 0;
}

#container{
	margin: 10px;
	border: 1px solid #D0D0D0;
	-webkit-box-shadow: 0 0 8px #D0D0D0;
}
</style>
</head>
<body>
	<div id="container">
		<h1 style="text-align: center !important;">Welcome to PCM  Virtual Permit Documentation</h1>
		<div id="body" style="text-align:left;">
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>SR. NO</th>
							<th>API NAME</th>
							<th>API URL</th>
							<th>METHOD</th>
							<th>PARAMETER</th>
							<th>OUTPUT</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>Parking Operative Login</td>
							<td>http://ds10.projectstatus.co.uk/parkingsystem/api/request/parking_operative_login</td>
							<td>POST</td>
							<td> {"email":"pawan@dotsquares.com","password":"admin@2017" }
							</td>
							<td>{ "status": true, "statusCode": "200", "message": "login successfully", "data": [ { "user_id": "2", "user_name": "DOtsquares", "user_email": "pawan@dotsquares.com", "contact_number": "12345678", "created_time": "0000-00-00 00:00:00", "user_is_active": "1" } ] }
							</td>
						</tr>
						<tr>
							<td>2</td>
							<td>Business List</td>
							<td>http://ds10.projectstatus.co.uk/parkingsystem/api/request/business_list</td>
							<td>POST</td>
							<td> {"parking_operative_id":"2" }
							</td>
							<td>{ "status": true, "statusCode": "200", "message": "Business List", "business_list": [ { "user_name": "Pawan Joshi", "user_email": "pawanJoshi@dotsquares.com", "contact_number": "", "business_type": "", "business_image": "", "location": "Jawahar Nagar" } ] }
							</td>
						</tr>
						<tr>
							<tr>
								<td>3</td>
								<td>Create Customer Parking Entries</td>
								<td>http://ds10.projectstatus.co.uk/parkingsystem/api/request/create_customer_parking_entries</td>
								<td>POST</td>
								<td> {"business_id":"4","parking_officer_id":"29","vehicle_registration_number":"rj06sp0632","in_time":"1528115520" }
								</td>
								<td>{ "status": true, "message": "Customer data added successfully", "statusCode": "200" }
								</td>
							</tr>
							<tr>
								<td>4</td>
								<td>View Customer Parking Entries</td>
								<td>http://ds10.projectstatus.co.uk/parkingsystem/api/request/view_customer_parking_entries_post</td>
								<td>POST</td>
								<td> {"business_id":"4" }
								</td>
								<td>{ "status": true, "statusCode": "200", "message": "Customer List", "customer_list": [ { "customer_name": "", "contact_number": "", "vehicle_registration_number": "RJ06ds5412", "in_time": "2018-05-23 00:00:00", "out_time": "2018-05-23 00:00:00", "parking_status": "1" }, { "customer_name": "", "contact_number": "992919", "vehicle_registration_number": "RJ06dtytyty", "in_time": "2018-05-23 00:00:00", "out_time": "2018-05-23 00:00:00", "parking_status": "1" } ] }
								</td>
							</tr>
							<tr>
								<td>5</td>
								<td>Customer Search</td>
								<td>http://ds10.projectstatus.co.uk/parkingsystem/api/request/customer_search</td>
								<td>POST</td>
								<td> {"business_id":"4","created_time":"1527249960" } <b>OR</b>{"business_id":"4","search_keyword":"keyword" }
								</td>
								<td>{ "status": true, "statusCode": "200", "message": "Customer List", "customer_list": [ { "customer_name": "", "contact_number": "992919", "vehicle_registration_number": "RJ06dtytyty", "in_time": "2018-05-23 05:07:10", "out_time": "2018-05-23 00:00:00", "parking_status": "1" } ] }
								</td>
							</tr>
							<tr>
								<td>6</td>
								<td>Business Login</td>
								<td>http://ds10.projectstatus.co.uk/parkingsystem/api/request/business_login</td>
								<td>POST</td>
								<td> {"email":"pawan@dotsquares.com","password":"admin@2017" }
								</td>
								<td>{ "status": true, "statusCode": "200", "message": "login successfully", "data": [ { "user_name": "DOtsquares", "user_email": "pawan@dotsquares.com", "contact_number": "12345678", "created_time": "0000-00-00 00:00:00", "user_is_active": "1" } ] }
								</td>
							</tr>
							<tr>
								<td>7</td>
								<td>Business Registration</td>
								<td>http://ds10.projectstatus.co.uk/parkingsystem/api/request/business_register</td>
								<td>POST</td>
								<td> {"email":"pawan@dotsquares.comd","name":"Pawan Joshi","password":"admin@2017d", "business_name":"Test Business"}
								</td>
								<td>{ "status": true, "message": "Business registered successfully", "statusCode": "200" }
								</td>
							</tr>
							<tr>
								<td>8</td>
								<td>Change Password</td>
								<td>http://ds10.projectstatus.co.uk/parkingsystem/api/request/change_password</td>
								<td>POST</td>
								<td> {"email":"pawan@dotsquares.comf","old_password":"admin@2017","new_password":"admin@2017", "confirm_password":"admin@2017"}
								</td>
								<td>{ "status": true, "message": "Password changed successfully.", "statusCode": "200" }
								</td>
							</tr>
							<tr>
								<td>9</td>
								<td>Forget Password</td>
								<td>http://ds10.projectstatus.co.uk/parkingsystem/api/request/forgot_password</td>
								<td>POST</td>
								<td> {"email":"pawankumar.joshi@dotsquares.com" }
								</td>
								<td>{
									"status": true,
									"message": "We have sent you a email to reset password, Please check your email.",
									"statusCode": "200"
								}
							</td>
						</tr>
						<tr>
							<td>10</td>
							<td>Create Business Profile</td>
							<td>http://ds10.projectstatus.co.uk/parkingsystem/api/request/create_business_profile</td>
							<td>POST</td>
							<td> {"business_id":"13","email":"pawan@dotsquares.comf","name":"Pawan Joshi","business_name":"business name","business_type":"hospital","contact_number":"123456789","location":"jawahar nagar"}
							</td>
							<td>{
								"status": true,
								"message": "Profile updated successfully",
								"statusCode": "200"
							}
						</td>
					</tr>
					<tr>
						<td>11</td>
						<td>View Business Profile</td>
						<td>http://ds10.projectstatus.co.uk/parkingsystem/api/request/view_business_profile</td>
						<td>POST</td>
						<td> {"business_id":"13" }
						</td>
						<td>{
							"status": true,
							"statusCode": "200",
							"message": "Business User Details",
							"business_user_details": [
							{
								"business_id": "13",
								"user_name": "Pawan Joshi",
								"user_email": "pawan@dotsquares.comf",
								"contact_number": "123456789",
								"business_type": "hospital"
							}
							]
						}
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<p style="text-align: center !important;" class="footer">Page loaded in <strong>{elapsed_time}</strong> seconds</p>
<p style="text-align: center !important;" class="footer"><strong>PCM&copy;<?php echo date('Y');?></strong></p>
</div>
</body>
</html>