<?php


 
class Api extends MY_Model {

	
	var $table_users = 'users';
    var $datestring = "%Y-%m-%d";
	var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
	var $currentDate = '';
	var $currentDateTime = '';

	function __construct() {
		parent::__construct();
		$this->load->database();

		//$this->currentDate = mdate($this->datestring, time());
		//$this->currentDateTime = mdate($this->dateStringWithTime, time());
		$this->currentTime = time();

	}

	/**
	* get_business_data.
	* @param where condition,select and order_by
	* @return Business list
	*/

	function get_user_data($where = NULL,$select =  '*',$order_by= NULL){

		$this->db->select($select, FALSE);
		$this->db->from($this->table_users.' AS U');


		if(!empty($where)){
			$this->db->where($where, FALSE);
		}
		if(!empty($order_by)){
			$this->db->order_by($order_by[0],$order_by[1]);
		}else{
			$this->db->order_by('U.id','DESC');
		}
		$result=array();
		$result =  $this->db->get()->result_array();
		#echo $this->db->last_query(); die;
		return $result;
	}
}
?>