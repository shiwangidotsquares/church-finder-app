<?php

class Event_model extends MY_Model {
    var $datestring = "%Y-%m-%d";
    var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
    var $currentDate = '';
    var $currentDateTime = '';
    var $table_pages = "event";
	var $table_roles = "roles";


    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->currentDate = mdate($this->datestring, time());
        $this->currentDateTime = mdate($this->dateStringWithTime, time());
        $this->currentTime = time();
    }

	function getData($table = NULL,$where = NULL,$select =  '',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

		$this->db->from($this->table_pages);
        if(!empty($where)){
            $this->db->where($where);
        }
        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
		}

        $result=array();
        if($limit){
            $tempdb 		 = clone $this->db;
			$totaldata 		 = $tempdb->get();
			$result['total']=$totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
	    #echo $this->db->last_query();die;
        return $result;

    }

    function getPages($where = NULL,$select =  '',$order_by= NULL){

        $this->db->from($this->table_pages);
        if(!empty($where)){
            $this->db->where($where);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }

        $result=array();

        $result['data'] =  $this->db->get()->result_array();
        #echo $this->db->last_query();die;
        return $result;

    }
    public function get_membership_for(){

		$result=array();
		$this->db->select('role_id,role_title');
		$this->db->where_in('role_id', array('3','4'));
		$this->db->from($this->table_roles);
		$result['data'] =  $this->db->get()->result_array();
		return $result;

    }

	public function get_features($where=''){

		$result=array();
		$this->db->select('*');
		$this->db->where($where);
		$this->db->from($this->table_feature);
		$result =  $this->db->get()->result_array();
		return $result;

    }
	public function get_detailed_users_list($where = NULL,$select =  '',$order_by= NULL, $offset=0, $limit=10,$where_in = false){

        $this->db->select($select);
        $this->db->from($this->table_users . ' AS U');
        $this->db->join($this->table_userinfo. ' AS UI','UI.user_id = U.user_id ','LEFT');
        $this->db->join($this->table_country. ' AS C','UI.country_id = C.country_id ','LEFT');
        $this->db->join($this->table_territory. ' AS T','T.territory_id = UI.territory_id ','LEFT');
        $this->db->join($this->table_continent. ' AS CO','CO.continent_id = UI.continent_id ','LEFT');
        $this->db->join($this->table_roles. ' AS R','R.role_id = U.role_id ','LEFT');
        $this->db->where(array('U.user_is_deleted' => '0'));


        if(!empty($where)){
            $this->db->where($where);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }

        $result=array();
        if($limit){
            $tempdb             =   clone $this->db;
            $totaldata  =   $tempdb->get();

            $result['total'] = $totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }

        $result['data'] =  $this->db->get()->result_array();
        return $result;

    }

    public function get_membershipinfo($where = NULL,$select =  '',$order_by= NULL, $offset=0, $limit=10,$where_in = false){

        $this->db->from($this->table_users . ' AS U');
        $this->db->join($this->table_userinfo. ' AS UI','UI.user_id = U.user_id ','LEFT');
        $this->db->where(array('U.user_is_deleted' => '0'));

        if(!empty($where)){
            $this->db->where($where);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }

        $result=array();
        if($limit){
            $tempdb             =   clone $this->db;
            $totaldata  =   $tempdb->get();
            $result['total'] = $totaldata->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
        return $result;

    }

}