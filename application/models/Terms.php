<?php

class Terms extends MY_Model {
    var $datestring = "%Y-%m-%d";
    var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
    var $currentDate = '';
    var $currentDateTime = '';
    var $table = "privacy_policy";
	var $table_roles = "roles";
    var $table_user_terms_pdf = "user_terms_pdf";
    var $table_users = "users";


    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->currentDate = date("Y-m-d", time());
        $this->currentDateTime = date("Y-m-d H:i:s", time());
        $this->currentTime = time();
    }

	function getData($table = NULL,$where = NULL,$select =  '',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){
		$this->db->select();
		$this->db->from($this->table);
        if(!empty($where)){
            $this->db->where($where);
        }
        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }
        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
		}
        $result=array();
        if($limit){
            $tempdb 		 = clone $this->db;
			$totaldata 		 = $tempdb->get();
			$result['total']=$totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
	    #echo $this->db->last_query();die;
        return $result;
    }
    
    function getPages($where = NULL,$select =  '',$order_by= NULL){
		$this->db->select();
        $this->db->from($this->table);
        if(!empty($where)){
            $this->db->where($where);
        }
        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }
        $result=array();
        $result['data'] =  $this->db->get()->result_array();
        #echo $this->db->last_query();die;
        return $result;

    }
    
    public function get_user_terms($where = NULL,$select =  '',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){
        $this->db->select('CONCAT_WS(" ",U.first_name,U.last_name) as `full_name`,UTP.accepted_date,UTP.pdf_name,UTP.user_terms_id',false);
		$this->db->from($this->table_user_terms_pdf.' AS UTP');
        $this->db->join($this->table_users.' AS U','U.user_id = UTP.user_id','LEFT');
        $this->db->where('U.user_is_deleted','0');
        $this->db->where('U.user_is_active','1');
        if(!empty($where)){
            $this->db->where($where,'',false);
        }
        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }
        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
		}
        $result=array();
        if($limit){
            $tempdb 		 = clone $this->db;
			$totaldata 		 = $tempdb->get();
			$result['total']=$totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
	    #echo $this->db->last_query();die;
        return $result;
    }
   
}