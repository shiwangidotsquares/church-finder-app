<?php

class Survey_model extends MY_Model {
    var $datestring = "%Y-%m-%d";
    var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
    var $currentDate = '';
    var $currentDateTime = '';
    var $table_survey = "survey";
    var $table_survey_result = "survey_result";
    var $table_users = "users";
    var $table_survey_question = "survey_question";
    var $table_questions = "questions";
	var $table_roles = "roles";


    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->currentDate = mdate($this->datestring, time());
        $this->currentDateTime = mdate($this->dateStringWithTime, time());
        $this->currentTime = time();
    }

	function getData($table = NULL,$where = NULL,$select =  '',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

		$this->db->from($this->table_survey);
        if(!empty($where)){
            $this->db->where($where);
        }
        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
		}

        $result=array();
        if($limit){
            $tempdb 		 = clone $this->db;
			$totaldata 		 = $tempdb->get();
			$result['total']=$totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
	    #echo $this->db->last_query();die;
        return $result;

    }
    function getDataResult($where = NULL,$select =  '',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

        $this->db->from($this->table_survey_result.' AS SR');
        $this->db->join($this->table_survey.' AS S', 'S.survey_id = SR.survey_id');
        $this->db->join($this->table_users.' AS U', 'U.user_id = SR.user_id');
        if(!empty($where)){
            $this->db->where($where);
        }
        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }

        $result=array();
        if($limit){
            $tempdb          = clone $this->db;
            $totaldata       = $tempdb->get();
            $result['total']=$totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
        return $result;

    }
}