<?php

class Questions_model extends MY_Model {
    var $datestring = "%Y-%m-%d";
    var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
    var $currentDate = '';
    var $currentDateTime = '';
    var $table_questions = "questions";
    var $table_question_types = "question_types";
    var $table_question_options = "question_options";
    var $table_course = "course";
    var $table_category = "categories";
	var $table_quiz = "quiz";
    var $table_quiz_configuration = "quiz_configuration";
    var $table_quiz_question_config = "quiz_question_config";
    var $table_final_quiz_configuration = "final_quiz_configuration";


    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->currentDate = mdate($this->datestring, time());
        $this->currentDateTime = mdate($this->dateStringWithTime, time());
        $this->currentTime = time();
    }

	function getQuestionsData($where = NULL,$select =  '*',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

        $this->db->select($select, FALSE);

        $this->db->from($this->table_questions.' AS Q');
        //$this->db->join($this->table_course.' AS C','C.course_id = Q.course_id','LEFT');
        $this->db->join($this->table_question_types.' AS T','T.question_type_id = Q.question_type_id','LEFT');

        if(!empty($where)){
            $this->db->where($where);
        }

        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('Q.question_id','DESC');
        }

        $result=array();

        if($limit){
            $tempdb 		 = clone $this->db;
			$totaldata 		 = $tempdb->get();
			$result['total'] = $totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
        #echo "test".$this->db->last_query();
        return $result;

	}
    function getAllQuestionsData($where = NULL,$select =  '*',$order_by= NULL){

        $this->db->select($select, FALSE);

        $this->db->from($this->table_questions.' AS Q');
        #$this->db->join($this->table_course.' AS C','C.course_id = Q.course_id','LEFT');
        $this->db->join($this->table_question_types.' AS T','T.question_type_id = Q.question_type_id','LEFT');

        if(!empty($where)){
            $this->db->where($where);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('Q.question_id','DESC');
        }

        $result=array();

        $result['data'] =  $this->db->get()->result_array();
        #echo "test".$this->db->last_query();
        return $result;

    }
    function getQuestionsEditData($where = NULL,$select =  '*',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

        $this->db->select($select, FALSE);

        $this->db->from($this->table_questions.' AS Q');
        #$this->db->join($this->table_course.' AS C','C.course_id = Q.course_id','LEFT');
        $this->db->join($this->table_question_options.' AS QO','QO.question_id = Q.question_id','LEFT');

        if(!empty($where)){
            $this->db->where($where);
        }

        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('Q.question_id','DESC');
        }

        $result=array();

        if($limit){
            $tempdb          = clone $this->db;
            $totaldata       = $tempdb->get();
            $result['total'] = $totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
        #echo "test".$this->db->last_query();
        return $result;

    }

    function getQuestionTypeData($where = NULL,$select =  '*',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

        $this->db->select($select, FALSE);

        $this->db->from($this->table_question_types);

        if(!empty($where)){
            $this->db->where($where);
        }

        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('question_type_id','ASE');
        }

        $result=array();

        if($limit){
            $tempdb          = clone $this->db;
            $totaldata       = $tempdb->get();
            $result['total'] = $totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
        #echo "test".$this->db->last_query();
        return $result;

    }
    function getCourseData($where = NULL,$select =  '*',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

        $this->db->select($select, FALSE);

        $this->db->from($this->table_course);

        if(!empty($where)){
            $this->db->where($where);
        }

        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('course_id','DESC');
        }

        $result=array();

        if($limit){
            $tempdb          = clone $this->db;
            $totaldata       = $tempdb->get();
            $result['total'] = $totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
        #echo "test".$this->db->last_query();
        return $result;

    }
    function getOptionsData($where = NULL,$select =  '*',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

        $this->db->select($select, FALSE);

        $this->db->from($this->table_question_options);

        if(!empty($where)){
            $this->db->where($where);
        }

        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('option_id','ASE');
        }

        $result=array();

        if($limit){
            $tempdb          = clone $this->db;
            $totaldata       = $tempdb->get();
            $result['total'] = $totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
        #echo "test".$this->db->last_query();
        return $result;

    }
    function upload($filename,$course_id = '') {
        $database = $this->load->database();
        $inputFileType = PHPExcel_IOFactory::identify($filename);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setReadDataOnly(true);
        $reader = $objReader->load($filename);
        #prd($reader);
        $ok = $this->uploadData( $reader, $database, $course_id);
        return $ok;
    }

    function uploadData( &$reader, &$database, $course_id){
        set_time_limit(0);
        $sheets = $reader->getSheetCount();
        $my_data = array();

        $data       = $reader->getSheet(0);
        $categories = array();
        $isFirstRow = TRUE;
        $i          = 0;
        $k          = $data->getHighestRow();

        for ($i=0; $i<$k; $i+=1){
            $j = 1;
            if ($isFirstRow)
            {
                $isFirstRow = FALSE;
                continue;
            }

            //initialize variable
            $question_type_id = '';
            $question_title = '';
            $difficulty_level = '';
            $right_answer_specification = '';
            $wrong_answer_specification = '';
            $option_value_a = '';
            $option_value_b = '';
            $option_value_c = '';
            $option_value_d = '';
            $option_correct_answer = '';

            $option_data = array();

            $question_type_id = $this->getCell($data,$i,$j++);
            $question_title = $this->getCell($data,$i,$j++);
            $difficulty_level = $this->getCell($data,$i,$j++);
            $option_correct_answer = $this->getCell($data,$i,$j++);
            $option_value_a = $this->getCell($data,$i,$j++);
            $option_value_b = $this->getCell($data,$i,$j++);
            $option_value_c = $this->getCell($data,$i,$j++);
            $option_value_d = $this->getCell($data,$i,$j++);

            $right_answer_specification = $this->getCell($data,$i,$j++);
            $wrong_answer_specification = $this->getCell($data,$i,$j++);

            /*$option_value_a = htmlentities( $option_value_a, ENT_QUOTES, $this->detect_encoding($option_value_a) );
            $option_value_b = htmlentities( $option_value_b, ENT_QUOTES, $this->detect_encoding($option_value_b) );
            $option_value_c = htmlentities( $option_value_c, ENT_QUOTES, $this->detect_encoding($option_value_c) );
            $option_value_d = htmlentities( $option_value_d, ENT_QUOTES, $this->detect_encoding($option_value_d) );*/

            $save_data = array(
                #'course_id' => $course_id,
                'question_type_id'=>$question_type_id,
                'question_title'=>$question_title,
                'questions_level'=> $difficulty_level,
                'right_answer_specification'=>$right_answer_specification,
                'wrong_answer_specification'=>$wrong_answer_specification,
                'question_created_time'=> $this->currentDateTime
                );

            if($question_type_id == 1 || $question_type_id == 2){

                $option_data[0]['option_value'] = $option_value_a;
                $option_data[1]['option_value'] = $option_value_b;
                $option_data[2]['option_value'] = $option_value_c;
                $option_data[3]['option_value'] = $option_value_d;

                for($p=0; $p<4; $p++){
                    $c = $p+1;
                    if($c == $option_correct_answer){
                        $option_data[$p]['option_correct_answer'] = $option_correct_answer;
                    }else{
                        $option_data[$p]['option_correct_answer'] = 0;
                    }
                }
            }elseif($question_type_id == 5){
                if($option_value_a){
                    $option_data[0]['option_value'] = 'True';
                }else{
                    $option_data[0]['option_value'] = 'False';
                }
                if($option_value_b){
                    $option_data[1]['option_value'] = 'True';
                }else{
                    $option_data[1]['option_value'] = 'False';
                }

                if($option_correct_answer == 1){
                    $option_data[0]['option_correct_answer'] = 1;
                    $option_data[1]['option_correct_answer'] = 0;
                }else{
                    $option_data[0]['option_correct_answer'] = 0;
                    $option_data[1]['option_correct_answer'] = 1;
                }

            }
            $my_data[$i]['question_data']  = $save_data;
            $my_data[$i]['option_data'] = $option_data;

        }

        if(!empty($my_data)){
            return $this->storeDataIntoDatabase( $database, $my_data);
        }else{
            $return_array = array(
                'return' => false,
                'message' => 'You are trying to upload a blank excel sheet.'
            );
            return $return_array;
        }
    }

    function storeDataIntoDatabase(&$database, $my_data){

        foreach($my_data as $key=>$value){

                 $question_data = $option_data = array();

                 $question_data = $value['question_data'];

                 $new_question_id = $this->save($this->table_questions,$question_data);

                 if(!empty($value['option_data']) && isset($new_question_id)){
                       $option_data = $value['option_data'];

                       foreach($value['option_data'] as $option_key=>$option_value){
                         $option_data[$option_key]['option_created_time'] = $this->currentDateTime;
                         $option_data[$option_key]['question_id'] = $new_question_id;
                       }
                        $option_data = $this->twodshuffle($option_data);

                    $last_query = $this->save_batch($this->table_question_options,$option_data);
                }
          }
          if($last_query){
                $return_array = array(
                    'return' => true,
                    'message' => 'Excel sheet imported successfully.'
                );
          }else{
               $return_array = array(
                   'return' => false,
                   'message' => 'Excel sheet not imported successfully.'
               );
          }
          return $return_array;

       }


    function checkusername($username){
        $where_array = array(
            'user_username' => $username,
            'user_is_deleted' => '0'
        );

        $result = $this->checkRecord($this->table_users,$where_array);
        if ($result){
            return false;
        }else{
            return true;
        }
    }

    function checkemail($user_email){
        $where_array = array(
            'user_email' => $user_email,
            'user_is_deleted' => '0'
        );

       $result = $this->checkRecord($this->table_users,$where_array);
        if ($result){
            return false;
        }else{
            return true;
        }
    }
    public function addOption($data=NULL) {
        $this->db->trans_start();
        $this->db->insert($this->table_option, $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
    }

    function getCell(&$worksheet,$row,$col,$default_val='') {
        $col -= 1; // we use 1-based, PHPExcel uses 0-based column index
        $row += 1; // we use 0-based, PHPExcel used 1-based row index
        return ($worksheet->cellExistsByColumnAndRow($col,$row)) ? $worksheet->getCellByColumnAndRow($col,$row)->getValue() : $default_val;
    }

    function validateHeading( &$data, &$expected ) {
        $heading = array();
        $k = PHPExcel_Cell::columnIndexFromString( $data->getHighestColumn() );
        if ($k != count($expected)) {
            return FALSE;
        }
        $i = 0;
        for ($j=1; $j <= $k; $j+=1) {
            $heading[] = $this->getCell($data,$i,$j);
        }
        $valid = TRUE;
        for ($i=0; $i < count($expected); $i+=1) {
            if (!isset($heading[$i])) {
                $valid = FALSE;
                break;
            }
            if (strtolower($heading[$i]) != strtolower($expected[$i])) {
                $valid = FALSE;
                break;
            }
        }
        return $valid;
    }

    protected function detect_encoding( $str ) {
        return mb_detect_encoding( $str, 'UTF-8,ISO-8859-15,ISO-8859-1,cp1251,KOI8-R' );
    }

    function twodshuffle($array){
        $count = count($array);
        $indi = range(0,$count-1);
        shuffle($indi);
        $newarray = array($count);
        $i = 0;
        foreach ($indi as $index)
            {
                $newarray[$i] = $array[$index];
                $i++;
            }
        return $newarray;
    }
}