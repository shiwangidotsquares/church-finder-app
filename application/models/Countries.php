<?php

class Countries extends MY_Model {
    var $datestring = "%Y-%m-%d";
    var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
    var $currentDate = '';
    var $currentDateTime = '';
	var $table_country = 'country';
    function __construct() {
        parent::__construct();
        $this->load->database();

        $this->currentTime = time();
    }

	public function get_active_countries($where = NULL,$select =  '',$order_by= NULL, $offset=NULL, $limit=NULL,$where_in = false){

		$this->db->from($this->table_country);
        if(!empty($where)){
            $this->db->where($where);
        }
        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
		}else{
            $this->db->order_by('name','ASE');
        }

        $result=array();
        if($limit){
            $tempdb 			= 	clone $this->db;
			$totaldata 	=  	$tempdb->get();
			$result['total'] = $totaldata->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
        return $result;

	}

	public function get_all_continet($where = NULL,$select =  '',$order_by= NULL, $offset=0, $limit=10,$where_in = false){

		$this->db->from($this->table_continent);
        if(!empty($where)){
            $this->db->where($where);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
		}

        $result=array();
        if($limit){
            $tempdb 			= 	clone $this->db;
			$totaldata 	=  	$tempdb->get();
			$result['total'] = $totaldata->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
        return $result;

	}


    public function get_country_details_by_id($country_id = ''){
        $country_name = '';
        $this->db->select('domain_country_slug_name,domain_country_location,country_payment_method');
        $this->db->from($this->table_active_countries);
        $this->db->where('country_id',$country_id);
        $this->db->where('domain_country_slug_status',1);
        $this->db->where('domain_country_slug_is_deleted',0);
        $query = $this->db->get();
        $result = $query->result_array();
        if(!empty($result)){
            $country_id = $result;
        }
        return $country_id;
    }

    public function get_country_code($country_id){
        $select = 'C.*,AC.*';
        $this->db->select($select);
        $this->db->from($this->table_country. ' AS C');
        $this->db->join($this->table_active_countries. ' AS AC','AC.country_id = C.country_id ','LEFT');
        $this->db->where('C.country_id',$country_id);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_country_code_by_country_id($country_id = 0){
        $country_code = '';
        $this->db->select('Iso-2');
        $this->db->from($this->table_country);
        $this->db->where('country_id',$country_id);
        $query = $this->db->get();
        $result = $query->result_array();
        if(!empty($result)){
            $country_code = $result[0]['Iso-2'];
        }
        return $country_code;
    }




}