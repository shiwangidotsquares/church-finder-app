<?php

class Customers extends MY_Model {
    var $datestring = "%Y-%m-%d";
    var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
    var $currentDate = '';
    var $currentDateTime = '';
    var $table_customer = "customer";
    var $table_categories = "categories";

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->currentDate = mdate($this->datestring, time());
        $this->currentDateTime = mdate($this->dateStringWithTime, time());
        $this->currentTime = time();
    }

	function getcustomerData($where = NULL,$select =  '*',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){
        $this->db->select($select, FALSE);
        $this->db->from($this->table_customer . ' AS A');
        if(!empty($where)){
            $this->db->where($where);
        }

        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('customer_id','DESC');
        }
        $result=array();
        if($limit){
            $tempdb 		 = clone $this->db;
			$totaldata 		 = $tempdb->get();
			$result['total'] = $totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
        return $result;

	}
    public function getAllBlogs(){
        $course_result_array = array();
        $this->db->select('blog_id,blog_title,blog_description,blog_image,blog_created_date');
        $this->db->from($this->table_blog);
        $this->db->where('blog_is_active',1);
        $this->db->where('blog_is_deleted',0);
        $this->db->order_by('blog_created_date','DESC');
        $query = $this->db->get();
        $result_array = $query->result_array();

       return $result_array;
    }
}