<?php

class User_model extends MY_Model {
    var $datestring = "%Y-%m-%d";
    var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
    var $currentDate = '';
    var $currentDateTime = '';
    var $table_users = "users";
    var $table_users_info = "user_info";
    var $table_country = "country";
	var $table_roles = "roles";


    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->currentDate = mdate($this->datestring, time());
        $this->currentDateTime = mdate($this->dateStringWithTime, time());
        $this->currentTime = time();
    }

	function getUserData($where = NULL,$select =  '',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

		$this->db->from($this->table_users);
        if(!empty($where)){
            $this->db->where($where);
        }
		#$this->db->where('role_id', INDIVIDUAL_USER_ROLE_ID);
        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
		}
        #echo $limit;


        $result=array();
        if($limit){
            $tempdb 		 = clone $this->db;
			$totaldata 		 = $tempdb->get();
			//$result['total'] = $tempdb->count_all_results($this->table_membership);
			$result['total']=$totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
	    #echo $this->db->last_query();die;
        return $result;

	}

        function getUserAllData($where = NULL,$select =  '*',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

            $this->db->select($select, FALSE);

            $this->db->from($this->table_users.' AS U');
            $this->db->join($this->table_users_info.' AS UF','U.user_id = UF.user_id','LEFT');

            if(!empty($where)){
                $this->db->where($where);
            }

            if($where_not_in){
                $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
            }

            if(!empty($order_by)){
               $this->db->order_by($order_by[0],$order_by[1]);
            }else{
              $this->db->order_by('U.user_id','DESC');
            }

            $result=array();

            if($limit){
                $tempdb          = clone $this->db;
                $totaldata       = $tempdb->get();
                $result['total'] = $totaldata->result_id->num_rows;
                $this->db->limit($limit, $offset);
            }
            $result['data'] =  $this->db->get()->result_array();
            #echo "test".$this->db->last_query();
            return $result;

        }


}