<?php

class Site_model extends MY_Model {
    
    var $table_site_config = 'site_config';
    var $table_venue       = 'venues';
    var $table_country     = 'country';
    var $table_users       = 'users';
    var $table_franchisee  = 'franchisee';
	var $table_course_license = "course_license";
	var $table_media = "lss_media";
    
    var $datestring = "%Y-%m-%d";
    var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
    var $currentDate = '';
    var $currentDateTime = '';        
    
	function __construct() {
        parent::__construct();
        $this->load->database();
        $this->currentDate = mdate($this->datestring, time());
        $this->currentDateTime = mdate($this->dateStringWithTime, time());
        $this->currentTime = time();
    }

	/**
	 * get data from the database
	 * @param 	String 		$table  		the table to select from
	 * @param 	String 		$where  		the conditions for the selection
	 * @param 	String 		$select 		the fields to be selected
	 * @param 	String 		$order  		the fields to order by
	 * @param 	Array  		$join   		the tables to join with
	 * @param 	Array  		$joinOn 		the fields to join on
	 * @param 	Integer		$limit  		the number of elements to be selected
	 * @param 	Integer		$offset 		the offset
	 * @param 	String 		$groupBy		the field to group by
	 * @return	Array  		        		the data from the database
	 */
	function getSiteData($table, $where = '', $select = '', $order = '', $join = array(), $joinOn = array(), $limit = 0, $offset = 0, $groupBy = '',$returnType='') {

		if ($select) {
			$this->db->select($select,false);
		}

		if ($where) {
			$this->db->where($where);
		}

		/*if ($order) {
			$this->db->order_by($order);
		}*/
		if(!empty($order)){
            if(is_array($order))
           $this->db->order_by($order[0],$order[1]);
           else
           $this->db->order_by($order);
           #$this->db->order_by($order[0],$order[1]);
        }


		if (!empty($join) && !empty($joinOn)) {
			foreach ($join as $key => $value) {
				$this->db->join($value, $joinOn[$key], 'left');
			}
		}

		if ($groupBy) {
			$this->db->group_by($groupBy);

		}

		$result=array();
		//echo $limit; die();
        if($limit){
            $tempdb 			= 	clone $this->db;
            $result['total'] 	=  	$tempdb->count_all_results($table);
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get($table)->result_array();

        return $result;

	}
    
    function getallfranchisee($select,$where = array(),$order,$limit = 10, $offset = 0) {
		if ($select) {
			$this->db->select($select,false);
		}
        $this->db->from($this->table_franchisee.' AS F');
        $this->db->join($this->table_country.' AS C', 'C.country_id = F.franchisee_country_id','LEFT');
        $this->db->join($this->table_users.' AS U', 'U.user_id = F.user_id','LEFT');
		if ($where) {
			$this->db->where($where);
		}
		if(!empty($order)){
            if(is_array($order))
           $this->db->order_by($order[0],$order[1]);
           else
           $this->db->order_by($order);
        }
		$result = array();
        if($limit){
            $tempdb 		 = 	clone $this->db;
            $result['total'] =  	$tempdb->count_all_results();
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();

        return $result;

	}

	/**
	 * get data from the database
	 * @param 	String 		$table 		the table to select from
	 * @param 	String 		$where 		the conditions for the selection
	 * @param 	String 		$select		the fields to be selected
	 * @param 	Array  		$join  		the tables to join with
	 * @param 	Array  		$joinOn		the fields to join on
	 * @param 	String 		$order 		the fields to order by
	 * @param 	Integer		$limit 		the limit
	 * @return	Array  		       		the row from the database
	 */
	function getDataRow($table, $where, $select = '', $join = array(), $joinOn = array(), $order = '', $limit = 0) {
		if ($select) {
			$this->db->select($select);
		}

		if (!empty($join) && !empty($joinOn)) {
			foreach ($join as $key => $value) {
				$this->db->join($value, $joinOn[$key], 'left');
			}
		}

		if ($order) {
			$this->db->order_by($order);
		}

		if ($limit) {
			$this->db->limit($limit);
		}

		return $this->db->where($where)->limit(1)->get($table)->row_array();
	}

	/**
	 * get value from the database
	 * @param 	String		$table		the table to select from
	 * @param 	String		$where		the conditions for the selection
	 * @param 	String		$field		the field to be selected
	 * @return	String		      		the value from the database
	 */
	function getDataValue($table, $where, $field) {
		$res = $this->db->select($field)->where($where)->get($table)->row_array();

		if (empty($res)) {
			return false;
		}

		return $res[$field];
	}

	/**
	 * get an arrray of database element (form: array[primary_key] = field)
	 * @param   String		$table       		the table to select from
	 * @param   String		$where       		the conditions for the selection
	 * @param   String		$index       		the primary key or other unique identifier (the index of the array)
	 * @param   String		$field       		the necesary field fro mthe database - if mare fields are required separate them by "," (the components of the array)
	 * @param   String		$order       		the fields to order by
	 * @param   String		$separator   		a character used to separate the selected field (it there are more than 1)
	 * @param   String		$initialValue		a initial value added to the array
	 * @return  Array 		             		the array of element or false if the query returned no results
	 */
	function getDataArray($table, $where = '', $index = 'id', $field = 'name', $order = 'id ASC', $separator = '', $initialValue = '', $initialValueIndex = '') {
		$res = $this->getData($table, $where, "$index,$field", $order);

		if (!empty($res)) {
			if ($initialValue) {
				$return[$initialValueIndex] = $initialValue;
			}

			foreach ($res as $key => $row) {
				$fields = explode(',', $field);

				$text = array();
				foreach ($fields as $k => $item) {
					$text[] = $row[$item];
                }

				$return[$row[$index]] = implode($separator, $text);
			}

			return $return;
		} else {
			return false;
		}
	}

	/**
	 * return the number of items
	 * @param 	String 		$table 		the table to select from
	 * @param 	String 		$where 		the conditions for the selection
	 * @param 	Array  		$join  		the tables to join with
	 * @param 	Array  		$joinOn		the fields to join on
	 * @return	Integer		       		the number of records
	 */
	function getDataCount($table, $where = '', $join = array(), $joinOn = array()) {
		if ($where) {
			$this->db->where($where);
		}

		if (!empty($join) && !empty($joinOn)) {
			foreach ($join as $key => $value) {
				$this->db->join($value, $joinOn[$key], 'left');
			}
		}

		return $this->db->count_all_results($table);
	}

	/**
	 * get the sum of a field
	 * @param   String      $table      the table to select from
	 * @param   String      $field      the field for the sum
	 * @param   String      $where      the conditions for the selection
	 * @return  Float                   the sum
	 */
	function getDataSum($table, $field, $where = '') {
		if ($where) {
			$this->db->where($where);
		}

		$return = $this->db->select_sum($field, 'sum')->get($table)->row_array();

		return $return['sum'];
	}

	/**
	 * return the average of a field
	 * @param 	String 		$table 		the table to select from
	 * @param 	String 		$fiel  		the field to get the avg
	 * @param 	String 		$where 		the conditions for the selection
	 * @param 	Array  		$join  		the tables to join with
	 * @param 	Array  		$joinOn		the fields to join on
	 * @return	Integer		       		the average
	 */
	function getDataAvg($table, $field, $where = '', $join = array(), $joinOn = array()) {
		if ($where) {
			$this->db->where($where);
		}

		if (!empty($join) && !empty($joinOn)) {
			foreach ($join as $key => $value) {
				$this->db->join($value, $joinOn[$key], 'left');
			}
		}

		$res = $this->db->select_avg($field, 'avg')->get($table)->row_array();

		return $res['avg'];
	}

	function update_user(){
		$this->load->helper('file');
		set_default_variables('application', true);
		set_default_variables('system', true);
		set_default_variables('css', true);
		set_default_variables('js', true);
		set_default_variables('assets', true);
		set_default_variables('img', true);
		//set_default_variables('test1', true);
	}

	/**
	 * insert data in the database
	 * @param 	String 		$table 		the table to insert into
	 * @param 	Array  		$insert		the data to be inserted
	 * @return	Integer		       		the id of the inserted data
	 */
	function insertData($table, $insert) {

		$this->db->insert($table, $insert);
		return $this->db->insert_id();
	}

	/**
	 * update data in the database
	 * @param 	String 		$table 		the table to update
	 * @param 	Array  		$update		the data to be updated
	 * @param 	String 		$where 		the conditions for the update
	 * @return	Boolean		       		true if the update was successfull
	 */
	function updateData($table, $update, $where = '') {
		if ($where) {
			$this->db->where($where);
		}

		return $this->db->update($table, $update);
	}

	/**
	 * delete data from the database
	 * @param 	String 		$table		the table to delete from
	 * @param 	String 		$where		the conditions for the delete
	 * @return	Boolean		      		true if the detele was successfull
	 */
	function deleteData($table, $where = '') {
		if ($where) {
			$this->db->where($where);
		}

		return $this->db->delete($table);
	}

    /**
	 * send an email
	 * @param 	Array  		$mailData		the information for the email sending
	 * @return	Boolean		         		true if the email was sent successfully
	 */
	/*function sendMail($mailData)
    {
      $this->email->clear();
      $config['mailtype'] = 'html';
      $this->email->initialize($config);
    //$this->email->from($mailData['from_email'], $mailData['from_name']);
      $this->email->from('manish.s@dotsquares.com', 'golearn');
      $this->email->to($mailData['to']);
      $this->email->subject($mailData['subject']);
      $this->email->message($mailData['message']);
      $status = $this->email->send();
        return $status;
    }*/

	/**
	 * get the ids for the data form a table
	 * @param 	String 		$table		the table to select from
	 * @param 	String 		$where		the conditions for the selection
	 * @param 	String 		$field		the field to make the array with (id it's different from id)
	 * @param 	Integer		$limit		the number of ids to get
	 * @param 	String 		$order		the order to sort by
	 * @return	Array  		      		the array of ids
	 */
	function getIdsArray($table, $where = '', $field = 'id', $limit = 0, $order = '') {
		if ($order == '') {
			$order = "$field ASC";
		}
		$res = $this->getData($table, $where, $field, $order, array(), array(), $limit);
		$return = array();
		if (!empty($res)) {
			foreach ($res as $key => $row) {
				$return[] = $row[$field];
			}
		}
		$return = array_unique($return);

		return $return;
	}



	/**
	 * set the last update date and last update action for the current user
	 * @param 	String 		$updateAction		the action that was performed (posibile actions: add_project, edit_project, archive_project, invite_person, share, add_comment,
	 *        	       		             		edit_profile, follow_project, follow_user)
	 * @param 	Array  		$extra       		extra fields to be updated
	 * @return	Boolean		             		true if the update was successful
	 */
	function setUserUpdate($updateAction, $extra = array()) {
		$update = array(
			'last_update_date' => date('Y-m-d H:i:s'),
			'last_update_action' => $updateAction
		);

		if (!empty($extra)) {
			$update = array_merge($update, $extra);
		}

		return $this->updateData("users", $update, "id = '{$this->data['user']->id}'");
	}

    function randomKey_genretor_for_passwordforgot() {
       $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $result = '';
        for ($i = 0; $i < 15; $i++)
        {
            $result .= $characters[mt_rand(0, 61)];
           }
      return $result;
    }
    //end search record

    public function return_session_list_for_activty()
    {
    $query = $this->db->get('ci_sessions');
    return $query->result();
    }

		/** pagination **/

    public function pagination($uri,$total_rows,$per_page,$segment = 4){
        $ci                          =& get_instance();
        $config['per_page']          = $per_page;
        $config['uri_segment']       = $segment;
        $config['base_url']          = base_url().$uri;
        $config['total_rows']        = $total_rows;
        // $config['use_page_numbers']  = TRUE;
        $ci->pagination->initialize($config);
        $return = $ci->pagination->create_links();
	  	return $return;
       //return $config;
    }
/* end pagination  */
	public function validation($fields,$class='') {
		//prd($fields);
        foreach ($fields as $fieldname => $fieldlable) {
        	$label = ucfirst($fieldname);
        	$label = str_replace("_", " ", $label);

			if($fieldname == "min_age" || $fieldname == "max_age" )
			{
				if($fieldname == "min_age")
				{
				$this->form_validation->set_rules($fieldname, $label, 'required|is_natural_no_zero');
				}
				if($fieldname == "max_age")
				{
					$this->form_validation->set_rules($fieldname, $label, 'required|is_natural|callback_check_equal_less['.$fields['min_age'].']');
				}
			}

			else
			{
			 	$this->form_validation->set_rules($fieldname, $label, 'required');
			}

			//$this->form_validation->set_error_delimiters('<div class="row"><div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div></div>');
			$this->form_validation->set_error_delimiters('<span class="help-block server-side-validation-error">', '</span>');
			//

		}
		//die();

	}

	public function validationOnAjax($fields,$class='') {

		$errorMsg = array();
        foreach ($fields as $fieldname => $fieldlable) {

        	$label = ucfirst($fieldname);

        	$label = str_replace("_", " ", $label);

			if($fieldname == "min_age" || $fieldname == "max_age" )
			{
				if($fieldname == "min_age")
				{
				$this->form_validation->set_rules($fieldname, $label, 'required|is_natural_no_zero');
				}
				if($fieldname == "max_age")
				{
					$this->form_validation->set_rules($fieldname, $label, 'required|is_natural|callback_check_equal_less['.$fields['min_age'].']');
				}
			}
			if($fieldname == "start_date_end_date"){
				$this->form_validation->set_rules($fieldname, "Start date", 'required');
			}

			elseif(is_array($fieldlable)){
				if(empty($fieldlable)){
					 $this->form_validation->set_rules($fieldname,$fieldname, "required");
				}
			}

			elseif($fieldname != "start_date_end_date")
			{
			 	 $this->form_validation->set_rules($fieldname, $label, 'required');
			}

		}

	}

	function check_equal_less($second_field,$first_field)
    {
        if ($second_field <= $first_field)
          {
            $this->form_validation->set_message('check_equal_less', 'The Maximum Age is greater than Minimum Age.');
            return false;
          }
          else
          {
            return true;
          }
    }

    function get_menu($menu_Id = 2){
        $parent_id = NULL;
		$this->load->model('teacher/Register_model','obj_register');
        $footer_menu_record = $this->obj_register->getHomeMenu($menu_Id, $parent_id=NULL);
        if(!empty($footer_menu_record)){
        	return  $footer_menu_record;
        }
    }

    public function getCoreQuery($query)
    {
    	$query = $this->db->query($query);
    	return $query->result_array();
    }

    public function upload_it($file = array(),$path, $size="", $width="", $height="")
    {
    	//print_r($file);
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        if($size == "")
        {
            $size = '100';
        }
        if($width == "")
        {
            $width = '1024';
        }
        if($height == "")
        {
            $height = '768';
        }
        $config['max_size'] = $size;
        $config['max_width']  = $width;
        $config['max_height']  = $height;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->set_allowed_types('*');
        $data['upload_data'] = '';
        $return = 0;
        //if not successful, set the error message
            if (!$this->upload->do_upload($file))
            {
                $return = 0;
            }
             else
            { //else, set the success message
                $return = 1;
            }

        return $return;
    }

    public function get_emailer($emailer_id){
        $result = array();
        $this->db->select('*');
        $this->db->from($this->table_emailtemaplate);
        $this->db->where('template_id',$emailer_id);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result[0];

     }

     public function genrate_report_data($territory_id,$report_from,$report_to){
     	
     	$sql = "SELECT A.order_detail_id,A.orders_id,A.item_id,A.item_stock_id,A.item_name,A.item_price,A.item_weight,SUM(A.item_quantity) as total_quantity,A.item_total,A.item_merchant_cost,A.item_size,A.item_color,A.orders_detail_created_date,B.orders_status,C.*,D.color_name,E.size_name,F.product_name,F.product_weight,F.product_production_cost,G.product_category_name,I.* 
     		FROM  `cgs_orders_detail` A
            INNER JOIN  `cgs_orders` B ON B.orders_id = A.orders_id
            INNER JOIN  `cgs_territory` C ON C.territory_id = B.territory_id
            INNER JOIN  `cgs_color` D ON D.color_id = A.item_color
            INNER JOIN  `cgs_size` E ON E.size_id = A.item_size
            INNER JOIN  `cgs_product` F ON F.product_id = A.item_id
            INNER JOIN  `cgs_product_category` G ON G.product_category_id = F.product_category_id
            INNER JOIN  `cgs_country` I ON I.country_id = C.country_id
            WHERE DATE_FORMAT(A.`orders_detail_created_date`, '%Y-%m-%d %H:%i:%s') >= '".date("Y-m-d h:i:s", strtotime($report_from))."' AND DATE_FORMAT(A.`orders_detail_created_date`, '%Y-%m-%d %H:%i:%s') <='".date("Y-m-d h:i:s", strtotime($report_to))."' AND C.territory_id = '".$territory_id."'"

             ;
             //AND B.orders_status = 'confirm' AND A.cart_type = 'Product' GROUP BY A.item_stock_id"
           
        return $this->getCoreQuery($sql)  ;  
        
     }




}
