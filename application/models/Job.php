<?php

class Customers extends MY_Model {
    var $datestring = "%Y-%m-%d";
    var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
    var $currentDate = '';
    var $currentDateTime = '';
    var $table_customer = "customer";
    var $table_categories = "categories";
    var $table_job  ="job"; 
    var $table_roles  ="roles"; 
    var $table_employee  ="employee"; 
    var $table_customer  ="customer"; 
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->currentDate = mdate($this->datestring, time());
        $this->currentDateTime = mdate($this->dateStringWithTime, time());
        $this->currentTime = time();
    }

	function getJobData($table = NULL,$where = NULL,$select =  '*',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){
        $this->db->select('J.*,R.role_title,E.first_name,E.last_name,C.client_name');
        $this->db->from($this->table_job.' AS J');
        $this->db->join($this->table_role.' AS R','R.role_id = J.role','LEFT');
        $this->db->join($this->table_employee.' AS E','E.id = J.employee_id','LEFT');
        $this->db->join($this->table_customer.' AS C','C.customer_id = J.client_id','LEFT');
        if(!empty($where)){
            $this->db->where($where);
        }

        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }
        $result=array();
        if($limit){
            $tempdb 		 = clone $this->db;
			$totaldata 		 = $tempdb->get();
			$result['total'] = $totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
       echo $this->db->last_query();die;
        return $result;


	}
    public function getAllBlogs(){
        $course_result_array = array();
        $this->db->select('blog_id,blog_title,blog_description,blog_image,blog_created_date');
        $this->db->from($this->table_blog);
        $this->db->where('blog_is_active',1);
        $this->db->where('blog_is_deleted',0);
        $this->db->order_by('blog_created_date','DESC');
        $query = $this->db->get();
        $result_array = $query->result_array();

       return $result_array;
    }
}