<?php

class Company_model extends MY_Model {
    var $datestring = "%Y-%m-%d";
    var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
    var $currentDate = '';
    var $currentDateTime = '';
    var $table_company = "company";
    var $table_country = "country";
    var $table_users = "users";
    var $table_users_info = "user_info";
	var $table_roles = "roles";


    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->currentDate = mdate($this->datestring, time());
        $this->currentDateTime = mdate($this->dateStringWithTime, time());
        $this->currentTime = time();
    }

    function getCompanyData($where = NULL,$select =  '*',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

        $this->db->select($select, FALSE);

        $this->db->from($this->table_company.' AS C');
        $this->db->join($this->table_users.' AS U','U.user_id = C.user_id','LEFT');
        $this->db->join($this->table_users_info.' AS UF','UF.user_id = U.user_id','LEFT');

        if(!empty($where)){
            $this->db->where($where);
        }

        $this->db->where('U.role_id', ORGANISATION_USER_ROLE_ID);

        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('C.company_id','DESC');
        }

        $result=array();

        if($limit){
            $tempdb          = clone $this->db;
            $totaldata       = $tempdb->get();
            $result['total'] = $totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
        #echo "test".$this->db->last_query();
        return $result;

    }
	function getCompanyDataForCourse($where = NULL,$select =  '*',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

        $this->db->select($select, FALSE);

        $this->db->from($this->table_company.' AS C');
        $this->db->join($this->table_users.' AS U','U.user_id = C.user_id','LEFT');
        $this->db->join($this->table_users_info.' AS UF','UF.user_id = U.user_id','LEFT');

        if(!empty($where)){
            $this->db->where($where);
        }

		$this->db->where('U.role_id', ORGANISATION_USER_ROLE_ID);

        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('C.company_id','DESC');
        }

        $result=array();

        $result['data'] =  $this->db->get()->result_array();
        #echo "test".$this->db->last_query();
        return $result;

	}


}