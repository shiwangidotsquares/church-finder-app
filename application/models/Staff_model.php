<?php

class Staff_model extends MY_Model {
    var $datestring = "%Y-%m-%d";
    var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
    var $currentDate = '';
    var $currentDateTime = '';
    var $table_staff = "staff";
    var $table_company = "company";
    var $table_users = "users";
    var $table_user_info = "user_info";
    var $table_roles = "roles";
    var $table_orders = "orders";
	var $table_user_courses = "user_courses";
	var $table_custom_mail_queue = 'custom_mail_queue';
    var $secret = '';

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->currentDate = mdate($this->datestring, time());
        $this->currentDateTime = mdate($this->dateStringWithTime, time());
        $this->currentTime = time();
        $this->load->library('googleauthenticator');
        $this->secret = $this->googleauthenticator->createSecret();
    }

	function getStaffData($where = NULL,$select =  '*',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

        $this->db->select($select, FALSE);

        $this->db->from($this->table_staff.' AS S');
        $this->db->join($this->table_company.' AS C','C.user_id = S.company_id','LEFT');
        $this->db->join($this->table_users.' AS U','U.user_id = S.user_id','LEFT');
		$this->db->join($this->table_user_info.' AS UI', 'UI.user_id = U.user_id','LEFT');

        if(!empty($where)){
            $this->db->where($where);
        }

        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('S.staff_id','DESC');
        }

        $result=array();

        if($limit){
            $tempdb 		 = clone $this->db;
			$totaldata 		 = $tempdb->get();
			$result['total'] = $totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
        #echo "test".$this->db->last_query();
        return $result;

	}

    function getallStaff($where = NULL,$select =  '*',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){
        $this->db->select($select, FALSE);
        $this->db->from($this->table_staff.' AS S');
        $this->db->join($this->table_company.' AS C','C.user_id = S.company_id','LEFT');
        $this->db->join($this->table_users.' AS U','U.user_id = S.user_id','LEFT');
        if(!empty($where)){
            $this->db->where($where);
        }
        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }
        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('S.staff_id','DESC');
        }

        $result=array();

        if($limit){
            $tempdb          = clone $this->db;
            $totaldata       = $tempdb->get();
            $result['total'] = $totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
        #echo "test".$this->db->last_query();
        return $result;

    }

    function get_staff($where = NULL,$select =  '*',$order_by= NULL){

        $this->db->select($select, FALSE);

        $this->db->from($this->table_staff.' AS S');
        $this->db->join($this->table_company.' AS C','C.user_id = S.company_id','LEFT');
        $this->db->join($this->table_users.' AS U','U.user_id = S.user_id','LEFT');
		$this->db->where('user_is_deleted','0');
		$this->db->where('user_is_active','1');
        if(!empty($where)){
            $this->db->where($where);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('S.staff_id','DESC');
        }

        $result=array();

        $result['data'] =  $this->db->get()->result_array();
        #echo "test".$this->db->last_query();
        return $result;
    }
	
	public function get_staff_details($user_id_array = array()){
		$result=array();
		if(!empty($user_id_array)){
			$this->db->select('U.user_id,C.company_id,S.staff_name,C.company_name,U.user_email,U.user_username');
			$this->db->from($this->table_staff.' AS S');
			$this->db->join($this->table_company.' AS C','C.user_id = S.company_id','LEFT');
			$this->db->join($this->table_users.' AS U','U.user_id = S.user_id','LEFT');
			$this->db->where('U.user_is_deleted','0');
			$this->db->where('U.user_is_active','1');
			$this->db->where_in('U.user_id',$user_id_array);
			if(!empty($order_by)){
			   $this->db->order_by($order_by[0],$order_by[1]);
			}else{
			  $this->db->order_by('S.staff_id','DESC');
			}
			$result =  $this->db->get()->result_array();
		}
        return $result;
	}

    function get_assigned_staff($where = NULL,$select =  '*',$order_by= NULL){

        $this->db->select($select, FALSE);

        $this->db->from($this->table_user_courses.' AS C');
        $this->db->join($this->table_users.' AS U','U.user_id = C.user_id','LEFT');

        if(!empty($where)){
            $this->db->where($where);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('C.user_course_assign_date','DESC');
        }

        $result=array();

        $result['data'] =  $this->db->get()->result_array();
        #echo "test".$this->db->last_query();
        return $result;

    }

    function upload($filename, $company_id) {
		$database = $this->load->database();
		$inputFileType = PHPExcel_IOFactory::identify($filename);
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setReadDataOnly(true);
		$reader = $objReader->load($filename);
        #prd($reader);
		$ok = $this->uploadData( $reader, $database,$company_id, $company_name);
        return $ok;
	}

	function uploadData( &$reader, &$database,$company_id, $company_name){
		set_time_limit(0);
		$sheets = $reader->getSheetCount();
		$my_data = array();

		$data 		= $reader->getSheet(0);
		$categories = array();
		$isFirstRow = TRUE;
		$i 			= 0;
		$k 			= $data->getHighestRow();

		for ($i=0; $i<$k; $i+=1){
			$j = 1;
			if ($isFirstRow) {
				$isFirstRow = FALSE;
				continue;
			}

			$staff_first_name = $this->getCell($data,$i,$j++);
			$staff_last_name = $this->getCell($data,$i,$j++);
			$staff_email = $this->getCell($data,$i,$j++);
			$staff_user_name = $this->getCell($data,$i,$j++);

            $save_data = array(
                'first_name' => $staff_first_name,
                'last_name' => $staff_last_name,
                'staff_email' => $staff_email,
                'user_name' => $staff_user_name
            );
			$staff_data[] = $save_data;
	    }

        if(!empty($staff_data)){
			return $this->storeDataIntoDatabase( $database, $staff_data, $company_id, $company_name );
		}else{
			$return_array = array(
                'return' => false,
                'message' => 'You are trying to upload a blank excel sheet.'
            );
            return $return_array;
		}
	}

	function storeDataIntoDatabase(&$database, $workbook,$company_id, $company_name){
	   $exist_array = array();
	   if(!empty($workbook)){
	       $last_incremented_id = $this->getNextAutoIncreament($this->table_users);
           $password = getRandomNum();
           $save_staff_data = array();
           $save_user_data = array();
           #prd($workbook);
	       foreach($workbook as $key => $value){
	           if($this->checkusername($value['user_name']) || $this->checkemail($value['staff_email'])){
	               $save_user_data[] = array(
                        'first_name' => $value['first_name'],
                        'last_name' => $value['last_name'],
        				'user_username' => $value['user_name'],
                        'role_id' => 4,
                        'user_email' => $value['staff_email'],
                        'user_password' => generateHash($password),
                        'google_auth_code' => $this->secret,
                        'created_time' => $this->currentDateTime
                    );

                    $save_staff_data[] = array(
                        'staff_name' => $value['first_name'].' '.$value['last_name'],
                        'company_id' => $company_id,
                        'user_id' => $last_incremented_id+$key,
                        'staff_created_by' => $company_id,
                        'staff_created_time' => $this->currentDateTime
                    );

                    $user_info_save_data[] = array(
                        'user_id' => $last_incremented_id+$key,
                        'user_mobile_number' => '',
                        'address_line1' => '',
                        'address_line2' => '',
                        'city' => '',
                        'postcode' => '',
                        'billing_state' => $this->session_array['billing_state'],
                        'billing_country' => $this->session_array['billing_country'],
                        'billing_zipcode' => $this->session_array['billing_zipcode']
                    );
                    #prd($user_info_save_data);
					$attachUrl = '';
					$sender_email = ADMIN_EMAIL;
					$sender_name  = ADMIN_NAME;
					$mail_cc = array();
					$mail_bcc = array();
					$body = array('staff_name' => $value['first_name'].' '.$value['last_name'], 'company_name'=> $company_name, 'user_email'=> $value['staff_email'], 'staff_username'=> $value['user_name'], 'password'=> $password, 'login_url' => base_url().'account/login');
					$message_data = get_email_body($template_id=1,$body);
					$messagebody=$message_data['body'];

                    $custom_mail_queue_array[] = array(
						'mail_to' => $value['staff_email'],
						'subject' => 'Welcome. You now have access to the GHC eLearning portal',
						'messagebody' => $messagebody,
						'mail_cc' => json_encode($mail_cc),
						'mail_bcc' => json_encode($mail_bcc),
						'sender_email' => $sender_email,
						'sender_name' => $sender_name,
						'attachUrl' => json_encode($attachUrl)
					);

	           }else{
	               $exist_array[$key] = array(
                        'user_email' => $value['staff_email'],
                        'user_username' => $value['user_name']
                   );
	           }

	       }
           #pr($save_user_data);
           #prd($save_staff_data);
			if(!empty($save_user_data)){
                $this->save_batch($this->table_users,$save_user_data);
                $this->save_batch($this->table_user_info,$user_info_save_data);
			}
			if(!empty($save_staff_data)){
                $this->save_batch($this->table_staff,$save_staff_data);
			}
			if(!empty($custom_mail_queue_array)){
				$this->save_batch($this->table_custom_mail_queue,$custom_mail_queue_array);
			}
	   }

       if(!empty($exist_array)){
            foreach($exist_array as $key => $val){
                $email_array[] = $val['user_email'];
            }
            $return_array = array(
                'return' => false,
                'message' => implode(',',$email_array).' ! These email address or its username already exist in our database.'
            );
       }else{
            $return_array = array(
                'return' => true,
                'message' => 'Excel sheet imported successfully.'
            );
       }
       return $return_array;
	}


    function checkusername($username){
        $where_array = array(
            'user_username' => $username,
            'user_is_deleted' => '0'
        );

        $result = $this->checkRecord($this->table_users,$where_array);
        if ($result){
            return false;
        }else{
            return true;
        }
    }

    function checkemail($user_email){
        $where_array = array(
            'user_email' => $user_email,
            'user_is_deleted' => '0'
        );

       $result = $this->checkRecord($this->table_users,$where_array);
        if ($result){
            return false;
        }else{
            return true;
        }
    }


	public function addQuestion($data=NULL) {
		$this->db->select('A.question_id');
		$this->db->from($this->table. ' AS A');
        $str = mb_convert_encoding(addslashes($data['question_title']),"UTF-8");

		$this->db->where('A.quize_id = '.$data['quize_id'].' AND A.question_title = "'.$str.'"');
		$result =  $this->db->get()->row();
		if($result){
			return 0;
		}else{
			$this->db->trans_start();
			$this->db->insert($this->table, $data);
			$insert_id = $this->db->insert_id();
			$this->db->trans_complete();
			return  $insert_id;
		}
	}

	public function addOption($data=NULL) {
		$this->db->trans_start();
		$this->db->insert($this->table_option, $data);
		$insert_id = $this->db->insert_id();
		$this->db->trans_complete();
		return  $insert_id;
	}

	function getCell(&$worksheet,$row,$col,$default_val='') {
		$col -= 1; // we use 1-based, PHPExcel uses 0-based column index
		$row += 1; // we use 0-based, PHPExcel used 1-based row index
		return ($worksheet->cellExistsByColumnAndRow($col,$row)) ? $worksheet->getCellByColumnAndRow($col,$row)->getValue() : $default_val;
	}

	function validateHeading( &$data, &$expected ) {
		$heading = array();
		$k = PHPExcel_Cell::columnIndexFromString( $data->getHighestColumn() );
		if ($k != count($expected)) {
			return FALSE;
		}
		$i = 0;
		for ($j=1; $j <= $k; $j+=1) {
			$heading[] = $this->getCell($data,$i,$j);
		}
		$valid = TRUE;
		for ($i=0; $i < count($expected); $i+=1) {
			if (!isset($heading[$i])) {
				$valid = FALSE;
				break;
			}
			if (strtolower($heading[$i]) != strtolower($expected[$i])) {
				$valid = FALSE;
				break;
			}
		}
		return $valid;
	}

	protected function detect_encoding( $str ) {
		return mb_detect_encoding( $str, 'UTF-8,ISO-8859-15,ISO-8859-1,cp1251,KOI8-R' );
	}

	function twodshuffle($array){
		$count = count($array);
		$indi = range(0,$count-1);
		shuffle($indi);
		$newarray = array($count);
		$i = 0;
		foreach ($indi as $index)
			{
				$newarray[$i] = $array[$index];
				$i++;
			}
		return $newarray;
	}

}