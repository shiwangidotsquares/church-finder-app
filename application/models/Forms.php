<?php

class Forms extends MY_Model {
    var $datestring = "%Y-%m-%d";
    var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
    var $currentDate = '';
    var $currentDateTime = '';
    var $table_form = "form";

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->currentDate = mdate($this->datestring, time());
        $this->currentDateTime = mdate($this->dateStringWithTime, time());
        $this->currentTime = time();
    }


}