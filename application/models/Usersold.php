<?php

class Users extends MY_Model {

    var $table = "users";
	var $table_users = "users";
    var $table_roles = "roles";
    var $table_user_info = "user_info";
    var $table_user_logs = "user_logs";
    var $table_site_config = 'site_config';
    var $table_invoice = 'invoice';
    var $table_orders = 'orders';
    var $table_raise_ticket = "raise_ticket";
    var $table_raise_ticket_reply = "raise_ticket_reply";
    var $table_custom_mail_queue = "custom_mail_queue";
    var $table_newsletter = 'newsletter';
    var $table_user_question = 'user_question';
    var $table_user_answers = "user_answers";
    var $table_questions = "questions";
    var $table_question_types = "question_types";
    var $table_question_options = "question_options";
    var $table_course = "course";
    var $table_company = "company";
	var $table_staff = "staff";
    var $table_category = "course_category";
    var $table_payment_history = "payment_history";
    var $table_user_courses = "user_courses";
    var $table_country = 'country';
    var $table_quiz_configuration = 'quiz_configuration';
    var $table_quiz_reports = "quiz_reports";
	var $table_miniquiz_report = "miniquiz_report";
	var $table_course_license = "course_license";
	var $table_user_terms_pdf = "user_terms_pdf";
	
	var $table_quiz_play_result = "quiz_play_result";
	var $table_user_quiz_play_history = "user_quiz_play_history";


    var $datestring = "%Y-%m-%d";
    var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
    var $currentDate = '';
    var $currentDateTime = '';

    function __construct() {
        parent::__construct();
        $this->load->database();

        $this->currentDate = mdate($this->datestring, time());
        $this->currentDateTime = mdate($this->dateStringWithTime, time());
        $this->currentTime = time();
    }

   function checkUser($users_uid = '', $users_password = '', $user_type = '') {
        $salt = $this->getSalt($users_uid);
        $users_password = generateHash($users_password, $salt);
        $users_uid = clean($users_uid);
        $users_password = clean($users_password);
        if($user_type){
            $sql = "SELECT user_id FROM `".$this->db->dbprefix.$this->table."` WHERE (`user_username` = '".$users_uid."' OR `user_email` = '".$users_uid."') AND `user_password` = '".$users_password."' AND `user_is_deleted` = '0' AND `user_is_active` = '1' AND `role_id` NOT IN('2','3','4')";
        }else{
            $sql = "SELECT user_id FROM `".$this->db->dbprefix.$this->table."` WHERE (`user_username` = '".$users_uid."' OR `user_email` = '".$users_uid."') AND `user_password` = '".$users_password."' AND `user_is_deleted` = '0' AND `user_is_active` = '1' AND `role_id` != '1' ";
        }


        $query = $this->db->query($sql);
        #echo $query->num_rows(); die;
        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
        $this->db->close();
    }

    function getSalt($user_username = '') {
        $user_password = '';
        $user_username = clean(trim($user_username));
        $sql = "SELECT user_password FROM `".$this->db->dbprefix.$this->table."` WHERE (`user_username` = '".$user_username."' OR `user_email` = '".$user_username."') AND `user_is_deleted` = '0' AND `user_is_active` = '1'";
        $query = $this->db->query($sql);
        $data = $query->result_array();
        $this->db->close();
        if (isset($data[0])) {
            $user_password = $data[0]['user_password'];
        }
        return $user_password;
    }

    function get_user_info($user_username = '',$users_password = '') {
        $result = array();
        $user_username = clean(trim($user_username));
        $sql = "SELECT U.*,UI.*,C.name AS country_name,C.country_id FROM `".$this->db->dbprefix.$this->table."` AS U INNER JOIN `".$this->db->dbprefix.$this->table_user_info."` AS UI ON U.user_id = UI.user_id INNER JOIN `".$this->db->dbprefix.$this->table_country."` AS C ON C.country_id = UI.billing_country WHERE (`user_username` = '".$user_username."' OR `user_email` = '".$user_username."') AND `user_password` = '".$users_password."' AND `user_is_deleted` = '0' AND `user_is_active` = '1'";
        $query = $this->db->query($sql);
        $data = $query->result_array();
        $this->db->close();
        if (isset($data[0])) {
            $result = $data[0];
        }
        return $result;
    }

    function getAllUserInfo($where = NULL,$select =  '*',$order_by= NULL){

        $this->db->select($select, FALSE);

        $this->db->from($this->table.' AS U');
        $this->db->join($this->table_user_info.' AS UI','U.user_id = UI.user_id','LEFT');

        if(!empty($where)){
            $this->db->where($where);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('U.user_id','DESC');
        }

        $result=array();
        $result['data'] =  $this->db->get()->result_array();
        #echo "test".$this->db->last_query();
        return $result;
    }

    function get_user_info_by_user_id($user_id = 0) {
        $result = array();
        if($user_id){
            $this->db->select('U.*,UI.*,C.name AS country_name,C.country_id');
            $this->db->from($this->table.' AS U');
            $this->db->join($this->table_user_info.' AS UI','UI.user_id = U.user_id','LEFT');
            $this->db->join($this->table_country.' AS C','C.country_id = UI.billing_country','LEFT');
            $this->db->where('U.user_id', $user_id);
            $query = $this->db->get();
            $data = $query->result_array();
            $this->db->close();
            if (isset($data[0])) {
                $result = $data[0];
            }
        }

        return $result;
    }

    function getSalt_reset($code = '') {
        $auth_key = '';
        $code = clean(trim($code));
        $this->db->select('user_auth_key');
        $this->db->from($this->table);
        $this->db->where('user_key_code', $code);
        $this->db->where('user_is_deleted', '0');
        #$this->db->where('user_is_active', 1);
        $query = $this->db->get();
        #echo $this->db->last_query();die;
        $data = $query->result_array();
        if (isset($data[0])) {
            $auth_key = $data[0]['user_auth_key'];
        }
        return $auth_key;
        $this->db->close();
    }

    public function checkcompany_name($where_array){
        $this->db->distinct();
        $this->db->select('company_name');
        $this->db->from($this->table_company.' AS C');
        $this->db->join($this->table.' AS U','U.user_id = C.user_id','LEFT');
        $this->db->where($where_array);
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
        return $result;
        $this->db->close();
    }

    public function getName($user_id) {
        $name = '';
        $user_id = clean(trim($user_id));
        $this->db->select('first_name,last_name');
        $this->db->from($this->table);
        $this->db->where('user_id', $user_id);
        $this->db->where('user_is_deleted', 0);
        $this->db->where('user_is_active', 1);
        $query = $this->db->get();
        $data = $query->result_array();
        if (isset($data[0])) {
            $name = ucfirst($data[0]['first_name']) . ' ' . ucfirst($data[0]['last_name']);
        }
        return $name;
        $this->db->close();
    }

    function getAllquestionwithanswerwhere($question_id,$course_id){
        $data = array();
        $this->db->select('Q.*,QT.question_type_title');
        $this->db->from($this->table_questions.' AS Q');
        $this->db->join($this->table_question_types.' AS QT', 'QT.question_type_id = Q.question_type_id','LEFT');
        $this->db->where(array('Q.question_id' => $question_id, 'Q.question_is_deleted' => 0,'Q.question_is_active' => 1));
        $query1 = $this->db->get();
        $result =  $query1->result_array();

        if(isset($result[0])){
            $data['question'] = $result[0];
            $this->db->select('option_id,answer_text');
            $this->db->from($this->table_user_answers);
            $this->db->where(array('question_id' => $result[0]['question_id'], 'user_id' => $this->session_array['user_id'], 'DATE_FORMAT(answer_created_time,"%Y-%m-%d") =' => date('Y-m-d')));
            $query2 = $this->db->get();
            $results =  $query2->result_array();
            if(isset($results[0]['option_id'])){
                $data['question']['answer_option_id'] = $results[0]['option_id'];
            }else{
                $data['question']['answer_option_id'] = '';
            }

            if(isset($results[0]['answer_text'])){
                $data['question']['answer_text'] = $results[0]['answer_text'];
            }else{
                $data['question']['answer_text'] = '';
            }

            $this->db->select('*');
            $this->db->from($this->table_question_options);
            $this->db->where(array('question_id' => $question_id));
            #$this->db->order_by('RAND()');
            $query3 = $this->db->get();
            $result_option =  $query3->result_array();
            #prd($result_option);
            if(in_array($result[0]['question_type_id'],array(1,2,5)) && !empty($result_option)){
                $data['question']['option'] = $this->twodshuffle($result_option);
            }else{
                $data['question']['option'] = $result_option;
            }

        }
        return $data;
        $this->db->close();
    }

    function getanswerdQuestion($where_array = array()){
        $this->db->distinct();
        $this->db->select('question_id,option_id');
        $this->db->from($this->table_user_answers);
        $this->db->where(array('user_id' => $this->session_array['user_id'], 'DATE_FORMAT(answer_created_time,"%Y-%m-%d") =' => date('Y-m-d')));
        if(!empty($where_array)){
            $this->db->where($where_array);
        }
		#$this->db->group_by('question_id');
        $query = $this->db->get();
        $result =  $query->result_array();
        return $result;
        $this->db->close();
    }

    function twodshuffle($array){
        #return $array;//As per client feedback no need suffle.
        // Get array length
        $count = count($array);
        // Create a range of indicies
        $indi = range(0,$count-1);
        // Randomize indicies array
        shuffle($indi);
        // Initialize new array
        $newarray = array($count);
        // Holds current index
        $i = 0;
        // Shuffle multidimensional array
        foreach ($indi as $index){
            $newarray[$i] = $array[$index];
            $i++;
        }
        return $newarray;
    }

    function getconfiguredQuestion($where_array,$limit){
        $this->db->distinct();
        $this->db->select('question_id');
        $this->db->from($this->table_questions);
        $this->db->where($where_array);
        $this->db->order_by('RAND()');
        $this->db->limit($limit);
        $query = $this->db->get();
        $result =  $query->result_array();
        return $result;
    }

    function get_user_logs($where = NULL,$select =  'UL.*,U.first_name,U.last_name',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

        $this->db->select($select, FALSE);

        $this->db->from($this->table_user_logs.' AS UL');
		$this->db->join($this->table.' AS U', 'U.user_id = UL.user_id','LEFT');
		$this->db->where('U.user_is_active','1');
		$this->db->where('U.user_is_deleted','0');
        if(!empty($where)){
            $this->db->where($where);
        }

        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('user_log_id','DESC');
        }

        $result=array();

        if($limit){
            $tempdb          = clone $this->db;
            $totaldata       = $tempdb->get();
            $result['total'] = $totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();

        return $result;
    }

    function check_is_uncompleted(){
        $res = '';
        $this->db->select('block_number');
        $this->db->from($this->table_quiz_configuration);
        $this->db->where('is_completed', 0);
        $this->db->limit(1, 0);
        $query = $this->db->get();
        $result =  $query->result_array();
        if(!empty($result)){
            $res = $result[0]['block_number'];
        }
        return $res;
    }

    function check_is_completed_count($quiz_id = 0,$user_id = 0,$course_id = 0){
        $res = '';
        $this->db->select('COUNT(block_number) AS total_completed');
        $this->db->from($this->table_miniquiz_report);
        $this->db->where('quiz_id', $quiz_id);
		$this->db->where('user_id', $user_id);
		$this->db->where('course_id', $course_id);
        $this->db->where('is_completed', 1);
        $query = $this->db->get();
        $result =  $query->result_array();
        if(!empty($result)){
            $res = $result[0]['total_completed'];
        }
        return $res;
    }

    function get_question_type($question_id = 0 ){
        $question_type_id = 0;
        if($question_id){
            $this->db->select('question_type_id');
            $this->db->from($this->table_questions);
            $this->db->where('question_id', $question_id);
            $query = $this->db->get();
            $res = $query->result_array();
            if(!empty($res) && isset($res[0])){
                $question_type_id = $res[0]['question_type_id'];
            }
            return $question_type_id;
        }else{
            return false;
        }
    }

	function get_count_company_license($course_id = 0, $user_id = 0){
		$res = 0;
		$this->db->select('SUM(no_of_user_allowed) AS total_license');
		$this->db->from($this->table_orders);
        $this->db->where('course_id', $course_id);
		$this->db->where('user_id', $user_id);
		$this->db->where('order_status', 1);
		$this->db->where('DATE_FORMAT(order_valid_till,"%Y-%m-%d") >=',date("Y-m-d"));
		$query = $this->db->get();
        $result =  $query->result_array();
        if(!empty($result)){
            $res = $result[0]['total_license'];
        }
        return $res;
    }
    
    function get_latest_invoice($user_id){
        $this->db->select();
        $this->db->from($this->table_invoice);
        $this->db->where('user_id',$user_id);
        $this->db->order_by('invoice_id','DESC');
        $this->db->limit(1,0);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    function getUsersinfo($where = NULL,$select =  '',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){
        $this->db->from($this->table.' AS U');
        $this->db->join($this->table_user_info.' AS UI','U.user_id = UI.user_id','LEFT');
        if(!empty($where)){
            $this->db->where($where);
        }
        if($where_in){
            $this->db->where_in($where_in[0], $where_in[1]);
        }
        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }
        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
		}
        $result=  array();
        if($limit){
            $tempdb 		 = clone $this->db;
			$totaldata 		 = $tempdb->get();
			$result['total'] = $totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
        return $result;
	}
    
    public function getcompamy_name($user_id){
        $company_name = '';
        $this->db->select('company_name');
        $this->db->from($this->table_company);
        $this->db->where('user_id',$user_id);
        $query = $this->db->get();
        $res = $query->result_array();
        if(!empty($res)){
            $company_name = isset($res[0]['company_name'])?$res[0]['company_name']:'';
        }
        return $company_name;
    }
    
    public function get_terms($user_id){
        $this->db->select();
        $this->db->from($this->table_user_terms_pdf);
        $this->db->where('user_id',$user_id);
        $this->db->order_by('user_terms_id','DESC');
        $this->db->limit(1, 0);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
}

