<?php
class Roles extends MY_Model {

	var $table = "roles";
	var $table_privileges = "privileges";
    var $datestring = "%Y-%m-%d";
	var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
	var $currentDate = '';
    var $currentDateTime = '';
	
	function __construct(){
        parent::__construct();
        $this->load->database();
    }
		
	public function show_all_roles(){
		$result = $this->db
				->select('*')
				->from($this->table)
				->get();
		return $result->result_array();
	}

	public function show_all_active_roles(){
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where('role_is_active',1);
		$this->db->where('role_is_deleted',0);
		#$this->db->where_not_in('role_id',array(2,3,4));
		$result = $this->db->get();	
		#echo $this->db->last_query();die;	
		return $result->result_array();
        $this->db->close();
	}

	public function show_perticular_active_roles(){
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where('role_is_active',1);
		$this->db->where('role_is_deleted',0);
		$this->db->where_not_in('role_id',array(1,2,3,4));
		$result = $this->db->get();	
		#echo $this->db->last_query();die;	
		return $result->result_array();
        $this->db->close();
	}
	
	public function edit_role($role_id){
		$result = $this->db->get_where($this->table,array('role_id' => $role_id));
		return $result->result_array();
		$this->db->close();
	}
    
    public function gettitleRole($role_id){
        $res = '';
		$result = $this->db->get_where($this->table,array('role_id' => $role_id));
		$data = $result->result_array();
        if(!empty($data[0])){
            $res = $data[0]['role_title'];
        }
	   return $res;
       $this->db->close();	
	}
	
	public function update_role($role_id,$data){
		$this->db->where('role_id', $role_id);
		$this->db->update($this->table, $data);
        $this->db->close();
	}

	public function delete_role($role_id,$data){
		$this->db->where('role_id', $role_id);
		$this->db->update($this->table,$data);
        $this->db->close(); 
	}

	function getData($table = NULL,$where = NULL,$select =  '',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){
		$this->db->from($this->table);
        if(!empty($where)){
            $this->db->where($where);
        }
        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }
        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
		}
        $result=array();
        if($limit){
            $tempdb 		 = clone $this->db;
			$totaldata 		 = $tempdb->get();
			$result['total']=$totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
	    #echo $this->db->last_query();die;
        return $result;

    }
    
}