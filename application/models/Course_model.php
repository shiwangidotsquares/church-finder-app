<?php

class Course_model extends MY_Model {
    var $datestring = "%Y-%m-%d";
    var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
    var $currentDate = '';
    var $currentDateTime = '';
    var $table_course = "course";
    var $table_users = "users";
    var $table_orders = "orders";
    var $table_category = "categories";
    var $table_user_courses = "user_courses";

    var $table_quiz = "quiz";
    var $table_quiz_configuration = "quiz_configuration";
    var $table_quiz_question_config = "quiz_question_config";
    var $table_final_quiz_configuration = "final_quiz_configuration";


    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->currentDate = mdate($this->datestring, time());
        $this->currentDateTime = mdate($this->dateStringWithTime, time());
        $this->currentTime = time();
    }

	function getCourseData($where = NULL,$select =  '*',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

        $this->db->select($select, FALSE);

        $this->db->from($this->table_course.' AS C');
        $this->db->join($this->table_category.' AS CC','CC.course_category_id = C.course_category_id','LEFT');

        if(!empty($where)){
            $this->db->where($where);
        }

        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('C.course_id','DESC');
        }

        $result=array();

        if($limit){
            $tempdb 		 = clone $this->db;
			$totaldata 		 = $tempdb->get();
			$result['total'] = $totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
        #echo "test".$this->db->last_query();
        return $result;

	}
        function getCourseids($where = NULL,$select =  '*',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

            $this->db->select($select, FALSE);

            $this->db->from($this->table_course.' AS C');
            $this->db->join($this->table_category.' AS CC','CC.course_category_id = C.course_category_id','LEFT');

            if(!empty($where)){
                $this->db->where($where);
            }

            if($where_not_in){
                $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
            }

            if(!empty($order_by)){
               $this->db->order_by($order_by[0],$order_by[1]);
            }else{
              $this->db->order_by('C.course_id','DESC');
            }

            $result=array();

            $result['data'] =  $this->db->get()->result_array();
            #echo "test".$this->db->last_query();
            return $result;

        }
    public function getallCourse(){
        $course_result_array = array();
        $this->db->select('course_category_name,course_category_id');
        $this->db->from($this->table_category.' AS CC');
        $this->db->where('course_category_is_active',1);
        $this->db->where('course_category_is_deleted',0);
        $query = $this->db->get();
        $result_array = $query->result_array();
        if(!empty($result_array)){
            foreach($result_array as $key => $value){
                $this->db->select();
                $this->db->from($this->table_course.' AS C');
                $this->db->where('course_category_id',$value['course_category_id']);
                $this->db->where('course_is_active',1);
                $this->db->where('course_is_deleted',0);
				$this->db->where('course_type',1);
                $query = $this->db->get();
                $course_result_array[$key]['category_name'] = $value['course_category_name'];
                $course_result_array[$key]['course_array'] = $query->result_array();
            }
        }
       return $course_result_array;
    }
    public function getDataProtectionCourse(){
        $course_result_array = array();
        $this->db->select();
        $this->db->from($this->table_course.' AS C');
        $this->db->where('course_category_id',1);
        $this->db->where('course_is_active',1);
        $this->db->where('course_is_deleted',0);
        $query = $this->db->get();
        $course_result_array['course_array'] = $query->result_array();

       return $course_result_array;
    }
    function get_company_users($where = NULL,$select =  '*',$order_by= NULL){

        $this->db->select($select, FALSE);

        $this->db->from($this->table_users);

        if(!empty($where)){
            $this->db->where($where);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('first_name','ASC');
        }

        $result=array();

        $result['data'] =  $this->db->get()->result_array();
        #echo "test".$this->db->last_query();
        return $result;

    }

    function get_assigned_user($where = NULL,$select =  '*',$order_by= NULL){

        $this->db->select($select, FALSE);

        $this->db->from($this->table_user_courses.' AS C');
        $this->db->join($this->table_users.' AS U','U.user_id = C.user_id','LEFT');

        if(!empty($where)){
            $this->db->where($where);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('C.user_course_assign_date','DESC');
        }

        $result=array();

        $result['data'] =  $this->db->get()->result_array();
        #echo "test".$this->db->last_query();
        return $result;

    }

    function get_user_course($where = NULL,$select =  '*',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

        $this->db->select($select, FALSE);
        $this->db->from($this->table_user_courses.' AS UC');
        $this->db->join($this->table_orders.' AS O','O.course_id = UC.course_id','LEFT');
        $this->db->join($this->table_course.' AS C','C.course_id = O.course_id','LEFT');
        $this->db->join($this->table_category.' AS CC','CC.course_category_id = C.course_category_id','LEFT');
        $this->db->where('UC.is_assigned',1);
        if(!empty($where)){
            $this->db->where($where);
        }
        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }
        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('O.order_created_time','DESC');
        }
        $this->db->group_by('UC.course_id');
        $result=array();

        if($limit){
            $tempdb 		 = clone $this->db;
			$totaldata 		 = $tempdb->get();
			$result['total'] = $totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
        #echo "test".$this->db->last_query();
        return $result;

	}

    function get_private_courses($where = NULL,$select =  '*',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

        $this->db->select($select, FALSE);
        $this->db->from($this->table_course.' AS C');
        $this->db->join($this->table_category.' AS CC','CC.course_category_id = C.course_category_id','LEFT');

        if(!empty($where)){
            $this->db->where($where);
        }
        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }
        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('C.course_created_time','DESC');
        }

        $result=array();

        if($limit){
            $tempdb          = clone $this->db;
            $totaldata       = $tempdb->get();
            $result['total'] = $totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
        #echo "test".$this->db->last_query();
        return $result;

    }



}