<?php

class emails extends MY_Model {
    var $datestring = "%Y-%m-%d";
    var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
    var $currentDate = '';
    var $currentDateTime = '';
    var $table_email = "email_template";

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->currentDate = mdate($this->datestring, time());
        $this->currentDateTime = mdate($this->dateStringWithTime, time());
        $this->currentTime = time();
    }

    public function get_emailer($emailer_id){
        $result = array();
        $this->db->select('*');
        $this->db->from($this->table_email);
        $this->db->where('id',$emailer_id);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result[0];
    }
    public function get_all_temp($where = NULL,$select =  '*',$order_by= NULL, $offset=0, $limit=10,$where_in = array()){

       $this->db->select($select);
        $this->db->from($this->table_email);

        $this->db->where(array("is_delete"=>0));


        if(!empty($where)){
            $this->db->where($where);
        }

        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }else{
          $this->db->order_by('id','ASC');
        }
        $result=array();

        if($limit)
            {
                $tempdb = clone $this->db;
                $result['total'] =  $tempdb->count_all_results();
                $this->db->limit($limit, $offset);
            }
        $result['data'] =  $this->db->get()->result_array();

        return $result;
    }
}

