<?php
defined('BASEPATH') OR exit('No direct access allowed');

class Careers extends MY_Controller {
 	var $userData   = array();
	public function __construct() {
        parent::__construct();
		$this->load->model('privileges');
		$adminlogin = $this->session->userdata('is_admin');
        $this->load->model('career_model','career');
        $this->load->model('category_model','category');
		$this->load->model('users');
    }

	public function index(){
		#$this->privileges->check_privileges();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$data = array();
		$layout = 'admin-layout';
		$view_file =  'admin/career/index';
		$index['page_title'] = ':: View Jobs ::';
		$content['form_title'] = 'View Jobs';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}


	public function get_job(){

        //$this->privilege->check_privileges();
		$order_by = array();
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        if(empty($length)){
            $length = 10;
            $start = 0;
        }
        $columnData = array(
            'sr_no',
            'jobs_title',
            'job_type',
            'desire_experience',
			'jobs_created_time',
			'jobs_is_active'
        );
        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
        $searchData = $this->input->post('searchBox');
        $where = '';
        $where .= 'jobs_is_deleted = "0"';
        $and=' and ';
        if($searchData){
            $where.= $and.'(jobs_title like "%'.$searchData.'%"   )';
        }
        $pageList_Array = $this->career->getData('',$where,$select =  '',$order_by, $start, $length,$where_in = false,$where_not_in = false);
        $pageList = $pageList_Array['data'];
        $totalData = $pageList_Array['total'];
        $jsonArray=array(
            'draw'=>$this->input->post('draw'),
            'recordsTotal'=>$totalData,
            'recordsFiltered'=>$totalData,
            'data'=>array(),
        );
        foreach($pageList as $key => $val){
           $active = $val['jobs_is_active']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['job_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['job_id'].'" title="Active"></i></a>';
            $edit = '<a href="'.site_url('admin/careers/edit/'.$val['job_id']).'" rel="'.$val['job_id'].'"><i class="fa fa-edit" title="Edit"></i></a>';
            $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['job_id'].'" title="Delete"></i></a>';
            switch ($val['job_type']) {
                case 1:
                    $job_type = 'Full Time';
                    break;
                case 2:
                    $job_type = 'Part Time';
                    break;
                case 3:
                    $job_type = 'Contarct';
                    break;
                case 4:
                    $job_type = 'Temporary';
                    break;
                default:
                    $job_type = '';
                    break;

            }
            $jsonArray['data'][] = array(
                'sr_no' => $start + $key + 1,
                'jobs_title' => $val['jobs_title']?ucfirst($val['jobs_title']):'-----',
                'job_type' => $job_type,
                'desire_experience' => $val['desire_experience']?$val['desire_experience']." Year":'-----',
				'jobs_created_time'=>$val['jobs_created_time'],
                'action' => $active.'&nbsp;'.$edit.'&nbsp;'.$delete
            );
        }
        echo json_encode($jsonArray); exit;
        echo $this->input->post('draw'); exit;
	}

	public function add(){
		#$this->privileges->check_privileges();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$data = array();

		$layout = 'admin-layout';
		$view_file =  'admin/career/add';
		$index['page_title'] = '::Add Job ::';
		$content['form_title'] = 'Add Job ';
		$this->templates->set($layout);
		$content['layout'] = $view_file;

        $cat_Array = $this->category->getCategoryData();

        $content['jobs_category_array'] = $cat_Array['data'];

        $path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);
		/*$membership_for = $this->job->get_membership_for();
		$content['membership_for_data'] = $membership_for['data'];*/
        $content['jobs_title']               = $this->input->post('jobs_title');
        #$content['department']               = $this->input->post('department');
        $content['desire_experience']        = $this->input->post('desire_experience');
        $content['desire_skills']            = $this->input->post('desire_skills');
        $content['job_type']                 = $this->input->post('job_type');
		$content['location']                 = $this->input->post('location');
		$content['jobs_description']         = $this->input->post('jobs_description');

		$config = array(
			array(
    			'field'   => 'jobs_title',
    			'label'   => 'Job Title',
    			'rules'   => 'required'
			)

		);

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE){
			$content['layout'] = $view_file;
		}else{

			$save_data = array(
                'jobs_title'        => $content['jobs_title'],
                'org_id'            => 1,
                #'department'        => $content['department'],
                'desire_experience' => $content['desire_experience'],
                'desire_skills'     => $content['desire_skills'],
                'job_type'          => $content['job_type'],
                'location'          => $content['location'],
                'jobs_description'  => $content['jobs_description'],
                'jobs_created_time' => $this->career->currentDateTime
            );

            $job_id = $this->career->save($this->career->table_jobs,$save_data);
			$this->messages->add('You have successfully added Job in your portal.','success');
            redirect('admin/careers/index');
		}
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}

    public function edit($job_id = ''){

        if($job_id == ""){
           $this->messages->add('No direct access allowed.','error');
           redirect('admin/careers/index');
        }
        //check record exist or not
        $record = $this->career->checkRecord($this->career->table_jobs, array('job_id'=>$job_id));
        if(empty($record)){
           $this->messages->add('No job found with given id.','error');
           redirect('admin/jobs/index');
        }
        $data = array();

		$layout = 'admin-layout';
		$view_file =  'admin/career/add';
		$index['page_title'] = '::Edit Job ::';
		$content['form_title'] = 'Edit Job ';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
		$path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);

		$where = array('job_id' => $job_id);
        $pageData = $this->career->getData($where,$select =  '',$order_by= NULL, $offset=0, $limit=NULL,$where_in = false,$where_not_in = false);
        $pages = $pageData['data'];
        $content['job_id']                   = $job_id;
        $content['jobs_title']               = $pages[0]['jobs_title'];
        #$content['department']               = $pages[0]['department'];
        $content['desire_experience']        = $pages[0]['desire_experience'];
        $content['desire_skills']            = $pages[0]['desire_skills'];
        $content['job_type']                 = $pages[0]['job_type'];
        $content['location']                 = $pages[0]['location'];
        $content['jobs_description']         = $pages[0]['jobs_description'];

        //prd($content);

		if($_POST){
               $content['jobs_title']               = $this->input->post('jobs_title');
               #$content['department']               = $this->input->post('department');
               $content['desire_experience']        = $this->input->post('desire_experience');
               $content['desire_skills']            = $this->input->post('desire_skills');
               $content['job_type']                 = $this->input->post('job_type');
               $content['location']                 = $this->input->post('location');
               $content['jobs_description']         = $this->input->post('jobs_description');
		}
       $config = array(
            array(
                'field'   => 'jobs_title',
                'label'   => 'Job Title',
                'rules'   => 'required'
            )

        );

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE){
			$content['layout'] = $view_file;
		}else{
			$where_update_array = array(
                'job_id' => $job_id
            );
            $update_data = array(
                 'jobs_title'        => $content['jobs_title'],
                 'org_id'            => 1,
                 #'department'        => $content['department'],
                 'desire_experience' => $content['desire_experience'],
                 'desire_skills'     => $content['desire_skills'],
                 'job_type'          => $content['job_type'],
                 'location'          => $content['location'],
                 'jobs_description'  => $content['jobs_description'],
                 'jobs_updated_time' => $this->career->currentDateTime
             );

            $result = $this->career->save($this->career->table_jobs,$update_data,$where_update_array);
            if($result){
    			$this->messages->add('Job updated successfully in your portal.','success');
                redirect('admin/careers/index');
            }else{
                $this->messages->add('Error Occurred.','error');
                redirect('admin/careers/index');
            }

		}
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
    }
    public function status(){
        #$this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray=array('flag'=>false);
        $where_array = array(
            'job_id' => $sID
        );
        $jobs_update_array = array(
            'jobs_is_active' => $this->input->post('sStatus')
        );
        if($this->career->save($this->career->table_jobs,$jobs_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }

    public function delete(){
        #$this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray = array('flag' => false);
        $where_array = array(
            'job_id' => $sID
        );
        $jobs_update_array = array(
            'jobs_is_deleted' => '1'
        );
        if($this->career->save($this->career->table_jobs,$jobs_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }
    public function image_upload(){
        if(isset($_FILES['jobs_image']) && $_FILES['jobs_image']['name'] != '') {
            $this->load->library('upload');
            $new_name = time().'_'.$_FILES["jobs_image"]['name'];
            $config['upload_path'] = './uploads/jobs_images/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['max_size']    = 1024*2; //2MB
            $config['file_name'] = $new_name;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('jobs_image')){
                $this->form_validation->set_message('jobs_images',str_replace(array('<p>', '</p>'),'', $this->upload->display_errors()));
                return false;
            } else {
                $this->upload_data = $this->upload->data();
                return true;
            }
        }
        return true;
    }
}
