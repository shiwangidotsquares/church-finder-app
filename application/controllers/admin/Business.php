<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Business extends MY_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('users');
		//$this->load->model('business');
		
	}

    /**
     * this is a generic function to show html for business reocrds in admin area
     * function having no parameter
     * function using template admin-login
     * @access public
    */
    public function index(){
    	add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
    	add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
    	$data = array();
    	$layout = 'admin-layout';
    	$view_file =  'admin/business/index';
    	$index['page_title'] = ':: View Business ::';
    	$content['form_title'] = 'View Business';
    	$where = array(
            'role_id' => '3',
    		'user_is_active' => '1',
            'user_is_deleted' => '0'
    	);
        // getting records of all parking operative users to display in assign list
    	$parking_operatives = $this->users->getRecords($this->users->table_users,$where,'user_id,user_name');
        $content['parking_operatives_data'] = $parking_operatives;
    	$content['parking_operatives'] = json_encode($parking_operatives);

    	$this->templates->set($layout);
    	$content['layout'] = $view_file;
    	$this->templates->set_data('index',$index);
    	$this->templates->set_data('content',$content);
    	$this->templates->load();
    }
    /**
     * this is a generic function to show business user list in datatable in admin section
     * function using template admin-login
     * @access public
    */
    public function get_business_list(){

    	$order_by = array();
    	$length = $this->input->post('length');
    	$start = $this->input->post('start');
    	if(empty($length)){
    		$length = 10;
    		$start = 0;
    	}
    	$columnData = array(
    		'sr_no',
    		'B.business_name',
            'U.user_email',
            'U.contact_number',
            'B.business_type',
    		'B.location',
    		'B.parking_operative_id',			
    		'U.created_time',
			'B.is_extend',
    		'action'
    	);
    	$sortData = $this->input->post('order');
    	$order_by[0] = $columnData[$sortData[0]['column']];
    	$order_by[1] = $sortData[0]['dir'];
    	$searchData = $this->input->post('searchBox');
        $dr_parking_off_data = $this->input->post('dr_parking_off_data');
    	$where = '';
    	$where .= 'U.user_is_deleted = "0" and U.role_id = 2';
    	$and=' and ';

    	if($searchData){
    		$searchData = trim($searchData);
    		$where.= $and.'(U.user_name like "%'.$searchData.'%" OR U.user_email like "%'.$searchData.'%" OR U.contact_number like "%'.$searchData.'%" OR B.business_type like "%'.$searchData.'%" OR B.location like "%'.$searchData.'%")';
    	}
        if($dr_parking_off_data){

            $where.= $and.'B.parking_operative_id =  '.$dr_parking_off_data;
        }
    	$select = "U.user_id,U.user_name,U.user_email,U.created_time,U.contact_number,U.user_is_active,B.parking_operative_id,B.business_type,B.location,B.business_name,B.user_id as business_id,B.is_extend as is_extend";
        // getting records of business users which have data in user and business table for datatable in admin section
    	$business_data = $this->users->getBusinessData($where, $select = $select,$order_by, $start, $length,$where_in = false,$where_not_in = false);
        #echo $this->db->last_query(); die;
    	$user_data = $business_data['data'];
    	$total_data = $business_data['total'];
    	$jsonArray=array(
    		'draw'=>$this->input->post('draw'),
    		'recordsTotal'=>$total_data,
    		'recordsFiltered'=>$total_data,
    		'data'=>array(),
    	);
        //creating view for business user in datatable
    	foreach($user_data as $key => $val){
    		/*$parking_operative_name = $this->users->getRecords($this->users->table_users,array('user_id'=>$val['parking_operative_id']),'user_name');*/

            $parking_operative_data = $this->users->getParkingOperativeData(array('O.business_id'=>$val['business_id']),'U.user_name');

            $parking_operative_name = '';
            if(!empty($parking_operative_data)){
                $i = 0;
                $numItems = count($parking_operative_data);
                foreach ($parking_operative_data as $key1 => $value) {
                    $parking_operative_name .= $value['user_name'];
                    if(++$i !== $numItems) {
                        $parking_operative_name .= ', ';
                    }
                }
            }
			 

    		$active = $val['user_is_active']?'<a href="javascript:void(0)" class="tableIcon"><i class="fa fa-check-square activeRecord" rel="'.$val['user_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)" class="tableIcon"><i class="fa fa-ban deactiveRecord" rel="'.$val['user_id'].'" title="Active"></i></a>';
    		$assign = '<a href="javascript:void(0)" class="tableIcon"><i class="fa fa-user assignOperative" rel="'.$val['parking_operative_id'].'" data-toggle="modal" data-target="#myModal" business="'.$val['user_id'].'" title="Assign Parking Operative"></i></a>';
            $delete = '<a href="javascript:void(0)" class="tableIcon"><i class="fa fa-trash-o deleteRecord" rel="'.$val['user_id'].'" title="Delete"></i></a>';
			$expand = $val['is_extend']?'<a href="javascript:void(0)" class="tableIcon extand" rel="'.$val['business_id'].'" title="Retract"  data-status=0><i class="fa fa-check-square" title="Retract" aria-hidden="true"></i></a>':'<a href="javascript:void(0)" class="extand tableIcon" title="Extend" rel="'.$val['business_id'].'" data-status=1><i class="fa fa-ban" aria-hidden="true" title="Extend" ></i></a>';

    		$jsonArray['data'][] = array(
    			'sr_no'           => $start + $key + 1,
    			'business_name'   => $val['business_name']?$val['business_name']:'---',
                'user_email'      => $val['user_email']?$val['user_email']:'---',
                'contact_number'  => $val['contact_number']?$val['contact_number']:'---',
                'business_type'   => $val['business_type']?$val['business_type']:'---',
    			'location'        => $val['location']?$val['location']:'---',
                /*'parking_operative_id' => $parking_operative_name?$parking_operative_name[0]['user_name']:'---',*/
    			'parking_operative_id' => $parking_operative_name?$parking_operative_name:'---',
    			'created_time'    =>$val['created_time'],
				'is_extend' => $expand,
                'action'          => $active.'&nbsp;'.$assign.'&nbsp;'.$delete,
    			#'action' 		  => $active
    		);
    	}
    	echo json_encode($jsonArray); exit;
    	echo $this->input->post('draw'); exit;
    }
    /**
     * this is a generic function to get parking oprtative user
     * function using template admin-login
     * @access public
    */
   public function get_parking_operative(){
        $where = array(
            'role_id' => '3',
            'user_is_active' => '1',
            'user_is_deleted' => '0'
        );
        // getting records of all parking operative users to display in assign list
        $parking_operatives = $this->users->getRecords($this->users->table_users,$where,'user_id,user_name');

        echo json_encode($parking_operatives);   exit;
   }

   /**
     * this is a generic function to assign parking oprtative to business user
     * function using template admin-login
     * @access public
    */
   /*public function assign_parking_operative(){
       $selectedVal = $this->input->post('selectedVal');
       $business_id = $this->input->post('business_id');
       $jsonArray=array('flag'=>false);
       $where_array = array(
           'user_id' => $business_id
       );
       $business_update_array = array(
           'parking_operative_id' => $selectedVal
       );
       if($this->users->save($this->users->table_business,$business_update_array, $where_array)){
           $jsonArray['flag'] = true;
       }
       echo json_encode($jsonArray); exit;
   }*/
    public function assign_parking_operative(){
	   $selectedVal = $this->input->post('selectedVal');
        $business_id = $this->input->post('business_id');
        $jsonArray=array('flag'=>false);
        $business_update_array = array();

        $business_array = array(
            'business_id' => $business_id
        );
        $this->users->deleteRecord($this->users->table_assign_operative,$business_array);

        if(!empty($selectedVal)){
            foreach($selectedVal as $key => $value) {
                $business_update_array[] = array(
                    'parking_operative_id' => $value,
                    'business_id' => $business_id,
                    'created_time' => $this->users->currentDateTime
                );
            }
        }
        //assigning parking oprtative to business user
        if($this->users->save_batch($this->users->table_assign_operative,$business_update_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }
    /**
     * this is a generic function to change status of business user from admin section
     * function using template admin-login
     * @access public
    */
    public function status(){
    	$sID = $this->input->post('sID');
    	$jsonArray=array('flag'=>false);
        if($this->input->post('sStatus') == '1'){
            $where_array = array(
                'business_id' => $sID
            );
             //checking email already exist or not in while edit records
            $result = $this->users->checkRecord($this->users->table_assign_operative,$where_array);

            if(empty($result)){
                $jsonArray['flag'] = false;
                $jsonArray['status'] = 'pending';
                echo json_encode($jsonArray); exit;
            }
        }
        $where_array = array(
            'user_id' => $sID
        );
    	$user_update_array = array(
    		'user_is_active' => $this->input->post('sStatus')
    	);
        //updating status of business user from admin section
    	if($this->users->save($this->users->table_users,$user_update_array, $where_array)){
    		$jsonArray['flag'] = true;
    	}
    	echo json_encode($jsonArray); exit;
    }

    /**
     * this is a generic function to add delete Parking operative
     * function using template admin-login
     * @access public
    */
    public function delete(){
        $sID = $this->input->post('sID');
        $jsonArray = array('flag' => false);

        $where_array = array(
            'user_id' => $sID
        );
        $user_update_array = array(
            'user_is_deleted' => '1'
        );
        if($this->users->save($this->users->table_users,$user_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }
	
	public function changeExtendStatus(){
    	$sID = $this->input->post('sID');
		$extend = $this->input->post('is_extend');
        $jsonArray = array('flag' => false);

        $where_array = array(
            'user_id' => $sID
        );
        $business_update_array = array(
            'is_extend' => $extend
        );
		
        if($this->users->save($this->users->table_business,$business_update_array, $where_array)){
			//echo $this->db->last_query(); die;
            $jsonArray['flag'] = true;
        }
		
        echo json_encode($jsonArray); exit;
    }

}
?>