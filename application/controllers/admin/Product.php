<?php
defined('BASEPATH') OR exit('No direct access allowed');

class Product extends MY_Controller {
 	var $userData   = array();
	public function __construct() {
        parent::__construct();
		$this->load->model('privileges');
		$adminlogin = $this->session->userdata('is_admin');
        $this->load->model('product_model','product');
        $this->load->model('category_model','category');
		$this->load->model('users');
    }

	public function index(){
		$this->privileges->check_privileges();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$data = array();
		$layout = 'admin-layout';
		$view_file =  'admin/product/index';
		$index['page_title'] = ':: View Products ::';
		$content['form_title'] = 'View Products';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}


	public function get_products(){
        //$this->privilege->check_privileges();
		$order_by = array();
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        if(empty($length)){
            $length = 10;
            $start = 0;
        }
        $columnData = array(
            'sr_no',
            'product_title',
            'category_name',
			'product_created_time',
			'product_is_active'
        );
        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
        $searchData = $this->input->post('searchBox');
        $where = '';
        $where .= 'P.product_is_deleted = "0"';
        $and=' and ';
        if($searchData){
            $where.= $and.'(P.product_title like "%'.$searchData.'%"   )';
        }
        $productList_Array = $this->product->getData('',$where,$select =  'P.*,C.category_name',$order_by, $start, $length,$where_in = false,$where_not_in = false);
        $productList = $productList_Array['data'];
        $totalData = $productList_Array['total'];
        #prd($productList);
        $jsonArray=array(
            'draw'=>$this->input->post('draw'),
            'recordsTotal'=>$totalData,
            'recordsFiltered'=>$totalData,
            'data'=>array(),
        );
        foreach($productList as $key => $val){
           $active = $val['product_is_active']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['product_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['product_id'].'" title="Active"></i></a>';
            $edit = '<a href="'.site_url('admin/product/edit/'.$val['product_id']).'" rel="'.$val['product_id'].'"><i class="fa fa-edit" title="Edit"></i></a>';
            $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['product_id'].'" title="Delete"></i></a>';
            $jsonArray['data'][] = array(
                'sr_no' => $start + $key + 1,
                'product_title' => $val['product_title']?ucfirst($val['product_title']):'-----',
                'category_name' => $val['category_name']?ucfirst($val['category_name']):'-----',
				'product_created_time'=>$val['product_created_time'],
                'action' => $active.'&nbsp;'.$edit.'&nbsp;'.$delete
            );
        }
        echo json_encode($jsonArray); exit;
        echo $this->input->post('draw'); exit;
	}

	public function add(){
		$this->privileges->check_privileges();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$data = array();

		$layout = 'admin-layout';
		$view_file =  'admin/product/add';
		$index['page_title'] = '::Add Product ::';
		$content['form_title'] = 'Add Product ';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
        $cat_Array = $this->category->getCategoryData(array('category_parent_id'=>14,'category_is_deleted'=>0));
        $content['product_category_array'] = $cat_Array['data'];

        $path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);
		/*$membership_for = $this->product->get_membership_for();
		$content['membership_for_data'] = $membership_for['data'];*/
        $content['product_title']               = $this->input->post('product_title');
		$content['category_id']            = $this->input->post('category_id');
		$content['product_description']         = $this->input->post('product_description');
        $content['product_regular_price']         = $this->input->post('product_regular_price');
        $content['product_sale_price']         = $this->input->post('product_sale_price');
        $content['product_SKU']         = $this->input->post('product_SKU');
        $content['product_stock_quantity']         = $this->input->post('product_stock_quantity');

		$config = array(
			array(
    			'field'   => 'product_title',
    			'label'   => 'Product Title',
    			'rules'   => 'required'
			),
			array(
    			'field'   => 'category_id',
    			'label'   => 'Product Category',
    			'rules'   => 'required'
			),
            array(
                'field'   => 'product_description',
                'label'   => 'Product Description',
                'rules'   => 'required'
            ),array(
                'field'   => 'product_regular_price',
                'label'   => 'product_regular_price',
                'rules'   => 'required'
            ),array(
                'field'   => 'product_sale_price',
                'label'   => 'product_sale_price'
            ),array(
                'field'   => 'product_SKU',
                'label'   => 'product_SKU'
            ),array(
                'field'   => 'product_stock_quantity',
                'label'   => 'product_stock_quantity'
            )

		);

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE){
			$content['layout'] = $view_file;
		}else{

			$save_data = array(
                'product_title'       => $content['product_title'],
                'org_id'       => 1,
                'category_id'        => $content['category_id'],
                'product_description' => $content['product_description'],
                 'product_regular_price' => $content['product_regular_price'],
                 'product_sale_price' => $content['product_sale_price'],
                 'product_SKU' => $content['product_SKU'],
                 'product_stock_quantity' => $content['product_stock_quantity'],

                /*'product_image_name' => $image_name,*/
                'product_is_active' => '1',
                'product_created_time' => $this->product->currentDateTime
            );
            if($this->input->post('hidden_profile_image')){
                $save_data['product_image_name'] = $this->input->post('hidden_profile_image');
           }
            /*if(!empty($this->upload_data)){
                if(!empty($this->upload_data['file_name'])){
                   $save_data['product_image_name'] = $this->upload_data['file_name'];
                }
            }*/

            $product_id = $this->product->save($this->product->table_product,$save_data);
			$this->messages->add('You have successfully added a Product in your portal.','success');
            redirect('admin/product/index');
		}
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}

    public function edit($product_id){

        $this->privileges->check_privileges();
        $data = array();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$layout = 'admin-layout';
		$view_file =  'admin/product/edit';
		$index['page_title'] = '::Edit Product ::';
		$content['form_title'] = 'Edit Product ';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
		$path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);

        $cat_Array = $this->category->getCategoryData(array('category_parent_id'=>14));
        $content['product_category_array'] = $cat_Array['data'];

		$where = array('product_id' => $product_id);
        $pageData = $this->product->getData('',$where,$select =  '',$order_by= NULL, $offset=0, $limit=NULL,$where_in = false,$where_not_in = false);
        $pages = $pageData['data'];
        $content['product_id']                = $product_id;
        $content['product_title']             = $pages[0]['product_title'];
        $content['category_id']          =  $pages[0]['category_id'];
        $content['product_description']       =  $pages[0]['product_description'];
        $content['product_regular_price']       =  $pages[0]['product_regular_price'];
        $content['product_sale_price']       =  $pages[0]['product_sale_price'];
        $content['product_SKU']       =          $pages[0]['product_SKU'];
        $content['product_stock_quantity']       =  $pages[0]['product_stock_quantity'];
         $content['product_image_name']       =  $pages[0]['product_image_name'];

		if($_POST){
	        $content['product_title']               = $this->input->post('product_title');
            $content['category_id']            = $this->input->post('category_id');
            $content['product_description']         = $this->input->post('product_description');
            $content['product_regular_price']       =  $this->input->post('product_regular_price');
            $content['product_sale_price']       =  $this->input->post('product_sale_price');
            $content['product_SKU']       =          $this->input->post('product_SKU');
            $content['product_stock_quantity']       =  $this->input->post('product_stock_quantity');

		}
       $config = array(
            array(
                'field'   => 'product_title',
                'label'   => 'Product Title',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'category_id',
                'label'   => 'Product Category',
                'rules'   => 'required'
            ),array(
                'field'   => 'product_regular_price',
                'label'   => 'product regular price'
            ),array(
                'field'   => 'product_sale_price',
                'label'   => 'product sale price'
            ),array(
                'field'   => 'product_SKU',
                'label'   => 'Product SKU'
            ),array(
                'field'   => 'product_stock_quantity',
                'label'   => 'product stock quantity',
            ),
        );

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE){
			$content['layout'] = $view_file;
		}else{
			$where_update_array = array(
                'product_id' => $product_id
            );
	         $update_data = array(
                    'product_title'       => $content['product_title'],
                    'category_id'    => $content['category_id'],
                    'product_description' => $content['product_description'],
                    'product_regular_price' => $content['product_regular_price'],
                    'product_sale_price' => $content['product_sale_price'],
                    'product_SKU' => $content['product_SKU'],
                    'product_stock_quantity' => $content['product_stock_quantity'],
                    /*'product_image_name' => $image_name,*/
                    'product_is_active' => '1',
                    'product_updated_time' => $this->product->currentDateTime
                );

                /*if(!empty($this->upload_data)){
                    if(!empty($this->upload_data['file_name'])){
                       $update_data['product_image_name'] = $this->upload_data['file_name'];
                    }
                }*/
                if($this->input->post('hidden_profile_image')){
                    $update_data['product_image_name'] = $this->input->post('hidden_profile_image');
               }
            $membership_id = $this->product->save($this->product->table_product,$update_data,$where_update_array);

			/*
            if($membership_id && $product_id == 16){
                $update_term_data = array(
                    'is_term_accepted' => '0'
                );
                $where_array = array('user_is_deleted' => '0');
                $this->users->save($this->users->table,$update_term_data,$where_array);
            }
			*/
			$this->messages->add('You have successfully added a Product in your portal.','success');
            redirect('admin/product/index');

		}
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
    }
    public function status(){
        $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray=array('flag'=>false);
        $where_array = array(
            'product_id' => (int)$sID
        );
        $product_update_array = array(
            'product_is_active' => (int)$this->input->post('sStatus')
        );
		
        if($this->product->save($this->product->table_product,$product_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }

    public function delete(){
        $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray = array('flag' => false);
        $where_array = array(
            'product_id' => $sID
        );
        $product_update_array = array(
            'product_is_deleted' => '1'
        );
        if($this->product->save($this->product->table_product,$product_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }
    public function image_upload(){
        if(isset($_FILES['product_image']) && $_FILES['product_image']['name'] != '') {
            $this->load->library('upload');
            $new_name = time().'_'.$_FILES["product_image"]['name'];
            $config['upload_path'] = './uploads/product_images/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['max_size']    = 1024*2; //2MB
            $config['file_name'] = $new_name;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('product_image')){
                $this->form_validation->set_message('product_images',str_replace(array('<p>', '</p>'),'', $this->upload->display_errors()));
                return false;
            } else {
                $this->upload_data = $this->upload->data();
                return true;
            }
        }
        return true;
    }
}
