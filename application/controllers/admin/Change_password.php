<?php
defined('BASEPATH') OR exit('No direct access allowed');

class Change_password extends My_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('User_model','users');
	}
	/**
	 * this is a generic function to show html to change password page
	 * @access public
	*/
	public function index(){

		$role_title_array = array();

		$data = array();

		$layout = 'admin-layout';

		$view_file =  'admin/change_password/add';

		$index['page_title'] = ':: Change Password ::';

		$content['form_title'] = 'Change Password';

		$old_password	 	= $this->input->post('old_password');
		$new_password 		= $this->input->post('user_password');
		$confirm_password   = $this->input->post('confirm_password');

		$this->templates->set($layout);
		$config = array(
			array(
				'field'   => 'old_password',
				'label'   => 'Old Password',
				'rules'   => 'trim|required|xss_clean'
			),
			array(
				'field'   => 'user_password',
				'label'   => 'Password',
				'rules'   => 'trim|required|xss_clean'
			),
			array(
				'field'   => 'confirm_password',
				'label'   => 'Confirm Password',
				'rules'   => 'trim|required|xss_clean|matches[user_password]|min_length[6]'
			)
		);

		$this->form_validation->set_rules($config);
		//searver side validation checking
		if ($this->form_validation->run() == false){

			$content['layout'] = $view_file;

		}else{
			$admin_session = $this->session->userdata('admin_session_data');

			if(empty($admin_session)){
				$this->messages->add("Session Expired!", "error");
				redirect(base_url('admin'));
			}
			$email = $admin_session['user_email'];
			$where_salt = array(
				'user_email'=>$email,
				'user_is_active'=>'1',
				'user_is_deleted'=>'0'
			);
	        //getting password salt using email id of active user
			$salt_data = $this->users->getRecords($this->users->table_users, $where_salt);
	        

			//encrypting password to get records
			$old_password = md5($old_password);
			$where_user = array(
				'user_email'=>$email,
				'user_password'=>$old_password
			);
			//checking old password for security
			$result = $this->users->getRecords($this->users->table_users,$where_user);
			if($result){

				$where_user = array(
					'user_email'=>$email
				);
				$save_data = array(
					'user_password'=>md5($new_password)
				);
			    //updating password
				$result = $this->users->save($this->users->table_users,$save_data,$where_user);
				$this->messages->add('Password changed successfully.','success');
				redirect(base_url('admin'));
			}else{
				$this->messages->add("Old password does not match.", "error");
				redirect(base_url('admin'));
			}
		}
		$this->templates->set_data('index',$index);

		$this->templates->set_data('content',$content);

		$this->templates->load();
	}

}