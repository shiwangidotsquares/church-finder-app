<?php  defined('BASEPATH') OR exit('No direct access allowed');

class Price extends MY_Controller {
    public function __construct() {
        parent ::__construct();
        $this->load->library('upload');
        $adminlogin = $this->session->userdata('is_admin');
       $this->load->model('controller_model');
        
    }

    public function index(){

        $session_array = $this->session->userdata('user_session_data');
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));

        $data = array();
        $index = array();
        $content = array();
        $layout = 'admin-layout';
        $view_file  = 'admin/price/index';
        $index['page_title'] = 'Price Manager';
        $index['form_title'] = 'View Price';
        $content['user_info']   = $session_array;
        $this->templates->set($layout);
        $content['layout']      = $view_file;
        $userData = $this->controller_model->getRecords($this->controller_model->table_controller);
        $content['userData']     = $userData;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }


public function get_all(){
    $length     = $this->input->post('length');
    $start      = $this->input->post('start');
    $columnData = array(
        'sr_no',
        'controller_name',
        'controller_price',
        'controller_description',
    );
    $sortData       = $this->input->post('order');
    $order_by[0]    = $columnData[$sortData[0]['column']];
    $order_by[1]    = $sortData[0]['dir'];
    $searchData     = $this->input->post('searchBox');
    $where  = '';
    $where .= 'controller_is_deleted = "0"';
    $and    = ' AND';
    if($searchData){
        $where.= $and.'(controller_name like "%'.$searchData.'%" OR controller_description like "%'.$searchData.'%")';
    }
    $select="";
    $list = $this->controller_model->getallcontrollers($where, $select, $order_by=NULL, $start, $length);
     
    $list_array= $list['data'];
    $totalData= $list['total'];
    $jsonArray = array(
        'draw'              => $this->input->post('draw'),
        'recordsTotal'      => $totalData,
        'recordsFiltered'   => $totalData,
        'data'              => array(),
    );
    

    foreach($list_array as $key => $val){
        $active = $val['controllers_is_active']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['controller_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['controller_id'].'" title="Active"></i></a>';
         $edit = '<a href="' . base_url('admin/price/edit/' . $val['controller_id']) . '" rel="' . $val['controller_id'] . '"><span class="fa fa-edit"></span></a>';
         $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['controller_id'].'" title="Delete"></i></a>';

        $edit = '<a href="' . base_url('admin/price/edit/' . $val['controller_id']) . '" rel="' . $val['controller_id'] . '"><span class="fa fa-edit"></span></a>';
        $jsonArray['data'][] = array(
           'sr_no' => $start + $key + 1,
           'controller_name' => $val['controller_name'],
           'controller_price' => $val['controller_price'],
           'controller_description' => $val['controller_description'],
           'action' =>  $active.'&nbsp;'.$edit.'&nbsp;'.$delete
       );
    }
    echo json_encode($jsonArray); exit;
}
public function Add(){
    $path = '../../../assets/js/ckfinder';
    $width = '1300px';
    parent::editor($path,$width);
    $index = array();
    $content = array();
    $email_array = array();
    $index['page_title'] = 'Price Manager';
    $content['form_title'] = 'Edit price';
    $content['button_text'] = 'Update price';
    $this->templates->set('admin-layout');

    $controller_Array = $this->controller_model->getRecords($this->controller_model->table_controller);
    $content['blog_category_array'] = $controller_Array;
     //Retrive form data
    $content['price_model']  = $this->input->post('price_model');
    $content['price']     = $this->input->post('price');
    $content['description']     = $this->input->post('description');
     $config = array(
      
        array(
            'field' => 'price',
            'label' => 'Enter price',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required'
        )
    );
    $this->form_validation->set_rules($config);
    if ($this->form_validation->run() == FALSE){
        $content['layout'] = 'admin/price/add';
    }else{

        $controller_id =$this->input->post('price_model');
        $where= array('controller_id' => $controller_id);
        $saveData = array(
            'controller_price' => $content['price'],
             'controller_description' => stripslashes($content['description']),
         );
        
      $this->controller_model->save($this->controller_model->table_controller,$saveData,$where);
        $this->messages->add('You have successfully added price.','success');
        redirect('admin/price');
    }
    $this->templates->set_data('index',$index);
    $this->templates->set_data('content',$content);
    $this->templates->load();
}

                                                /* Update record*/
    public function edit($id = ''){
   if($id == ""){
       $this->messages->add('No direct access allowed.','error');
       redirect('admin/price/index');
    }
    //check record exist or not
    $record = $this->controller_model->checkRecord($this->controller_model->table_controller, array('controller_id'=>$id));
   if(empty($record)){
       $this->messages->add('No blog found with given id.','error');
       redirect('admin/price/index');
    }
    $index = array();
    $content = array();

    //$this->privileges->check_privileges();
    $path = '../../../assets/js/ckfinder';
    $width = '1300px';
    parent::editor($path,$width);

    
    $controller_Array = $this->controller_model->getRecords($this->controller_model->table_controller);
    $content['blog_category_array'] = $controller_Array;
    $where_array = array(
        'controller_id' => $id
    );
    
    $switch = $this->controller_model->checkRecord($this->controller_model->table_controller,$where_array);

    if((empty($id) && !is_numeric($id)) && ($switch == false)){
        $this->messages->add('Selected record does not exist in our database!','error');
        redirect(base_url('admin/price/index'));
    }

    $email_array = array();
    $index['page_title'] = 'Price Manager';
    $content['form_title'] = 'Edit Price';
    $content['button_text'] = 'Update Price';
    $this->templates->set('admin-layout');
    $where_array = array(
        'controller_id' => $id
    );
    $email_array = $this->controller_model->getallcontrollers($where_array, $select=NULL, $order_by=NULL);
    $email_array = $email_array['data'];
    $content['controller_id']= $id;

    if(!empty($email_array)){

        $content['price_model']= !empty($email_array[0]['controller_name'])?$email_array[0]['controller_name']:'';
        $content['price']= !empty($email_array[0]['controller_price'])?$email_array[0]['controller_price']:'';
        $content['description']= !empty($email_array[0]['controller_description'])?$email_array[0]['controller_description']:'';
       
    }

    if($_POST){
        $content['price_model']= $this->input->post('price_model');
        $content['price']= $this->input->post('price');
        $content['description']= $this->input->post('description');

    }

    $config = array(
        array(
            'field' => 'price_model',
            'label' => 'Select model',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required'
        )
    );
    $this->form_validation->set_rules($config);
    if ($this->form_validation->run() == FALSE){
        $content['layout'] = 'admin/price/add';
    }else{
        $controller_id =$this->input->post('price_model');
        $where= array('controller_id' => $controller_id);
        $saveData = array(
            'controller_price' => $content['price'],
             'controller_description' => stripslashes($content['description']),
            
        );
        $this->controller_model->save($this->controller_model->table_controller,$saveData,$where);
        $this->messages->add('You have successfully updated selected price.','success');
        redirect('admin/price');
    }
    $this->templates->set_data('index',$index);
    $this->templates->set_data('content',$content);
    $this->templates->load();
    }
    public function status(){
        
        $sID = $this->input->post('sID');
        $jsonArray=array('flag'=>false);
        $where_array = array(
            'controller_id' => $sID
        );
        $page_update_array = array(
            'controllers_is_active' => $this->input->post('sStatus')
        );
        if($this->controller_model->save($this->controller_model->table_controller,$page_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }

    public function delete(){
        
        $sID = $this->input->post('sID');

       $jsonArray = array('flag' => false);
        $where_array = array(
           'controller_id' => $sID
       );
        $page_update_array = array(
            'controller_is_deleted' => '1'
        );
        if($this->controller_model->save($this->controller_model->table_controller,$page_update_array, $where_array)){
             
            $jsonArray['flag'] = true;
        }

        echo json_encode($jsonArray); exit;
    }


    
}
