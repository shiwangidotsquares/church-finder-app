<?php
defined('BASEPATH') OR exit('No direct access allowed');

class Activity extends MY_Controller {
 	var $userData   = array();
	public function __construct() {
        parent::__construct();
		$adminlogin = $this->session->userdata('is_admin');
        $this->load->model('Activity_model','activity');
        $this->load->model('parishes_model','parishes');
		
    }

	public function index(){
		
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$data = array();
		$layout = 'admin-layout';
		$view_file =  'admin/activity/index';
		$index['page_title'] = ':: View Mass Activity ::';
		$content['form_title'] = 'View Mass Activity';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}


	public function get_activity(){
        

		$order_by = array();
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        if(empty($length)){
            $length = 10;
            $start = 0;
        }
        $columnData = array(
            'sr_no',
            'mass_activity_name',
            'mass_activity_date',
			'mass_activity_time',
            'mass_activity_description',
			'mass_activity_is_active'
        );



        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
        $searchData = $this->input->post('searchBox');
        $where = '';
        $where .= 'mass_activity_is_deleted = "0"';
        $and=' and ';
        if($searchData){
            $where.= $and.'(mass_activity_name like "%'.$searchData.'%"   )';
        }
       $pageList_Array = $this->activity->getData('',$where,$select =  '',$order_by, $start, $length,$where_in = false,$where_not_in = false);
          $pageList = $pageList_Array['data'];
        $totalData = $pageList_Array['total'];
        $jsonArray=array(
            'draw'=>$this->input->post('draw'),
            'recordsTotal'=>$totalData,
            'recordsFiltered'=>$totalData,
            'data'=>array(),
        );
        foreach($pageList as $key => $val){
           $active = $val['mass_activity_is_active']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['mass_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['mass_id'].'" title="Active"></i></a>';
            $edit = '<a href="'.site_url('admin/activity/edit/'.$val['mass_id']).'" rel="'.$val['mass_id'].'"><i class="fa fa-edit" title="Edit"></i></a>';
            $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['mass_id'].'" title="Delete"></i></a>';
            $jsonArray['data'][] = array(
                'sr_no' => $start + $key + 1,
                'mass_activity_name' => $val['mass_activity_name']?ucfirst($val['mass_activity_name']):'-----',
                'mass_activity_date'=>$val['mass_activity_date'],
                'mass_activity_time'=>$val['mass_activity_time'],
                'mass_activity_description'=>$val['mass_activity_description'],
                'mass_activity_created_time'=>$val['mass_activity_created_time'],
                'mass_activity_updated_time'=>$val['mass_activity_updated_time'],
                'action' => $active.'&nbsp;'.$edit.'&nbsp;'.$delete
            );
        }
        echo json_encode($jsonArray); exit;
        echo $this->input->post('draw'); exit;
	}

	public function add(){
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$data = array();

		$layout = 'admin-layout';
		$view_file =  'admin/activity/add';
		$index['page_title'] = '::Add Mass Activity ::';
		$content['form_title'] = 'Add Mass Activity ';
		$this->templates->set($layout);
		$content['layout'] = $view_file;

          $cat_Array = $this->parishes->getRecords($this->parishes->table_parishes);
          $content['business_directories_category_array'] = $cat_Array;
         
        $path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);
		if($_POST){
            //prd($_POST);
        $content['mass_activity_name']= $this->input->post('mass_activity_name');
        $content['mass_activity_date']= $this->input->post('mass_activity_date');
		$content['mass_activity_time'] = $this->input->post('mass_activity_time');
		$content['mass_activity_description']= $this->input->post('mass_activity_description');

			$save_data = array(
                'mass_activity_name'=> $content['mass_activity_name'],
                'mass_activity_date' => $content['mass_activity_date'],
                'mass_activity_time' => $content['mass_activity_time'],
                'mass_activity_description' => $content['mass_activity_description'],
                'mass_activity_is_active' => '1',
                'mass_activity_created_time' => $this->activity->currentDateTime
            );
           

           $activity_id = $this->activity->save($this->activity->table_activity,$save_data);
 
            if($activity_id){
			$this->messages->add('You have successfully added a Mass activity in your portal.','success');
            redirect('admin/activity/index');
		 }

         else{
                $this->messages->add('Error occured.','sucess');
            redirect('admin/activity/add');
 }

   }
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
}

    
        public function edit($parishes_id =''){
        

        if(empty($parishes_id)){
           $this->messages->add('No direct access allowed.','error');
           redirect('admin/parishes/index');
        }


    //check record exist or not
    $record = $this->activity->getRecords($this->activity->table_activity, array('mass_id'=>$parishes_id));

           if(empty($record)){
               $this->messages->add('No directory found with given id.','error');
               redirect('admin/activity/index');
            }
    
                $data = array();
                add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
                add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
                $layout = 'admin-layout';
                $view_file =  'admin/activity/edit';
                $index['page_title'] = '::Edit mass activity  ::';
                $content['form_title'] = 'Edit mass activity ';
                $this->templates->set($layout);
                $content['layout'] = $view_file;
                $path = '../../../assets/js/ckfinder';
                $width = '1300px';
                parent::editor($path,$width);
               $where = array('mass_id' => $parishes_id);
                $cat_Array = $this->parishes->getRecords($this->parishes->table_parishes);
                $content['business_directories_category_array'] = $cat_Array;

               $userData = $this->activity->getRecords($this->activity->table_activity,$where);
                $content['mass_activity_name']= $userData[0]['mass_activity_name'];
                $content['mass_activity_date'] = $userData[0]['mass_activity_date'];
                $content['mass_activity_time'] = $userData[0]['mass_activity_time'];
                $content['mass_activity_description']= $userData[0]['mass_activity_description'];

                
         if($_POST){
           
        $content['mass_activity_name']= $this->input->post('mass_activity_name');
        $content['mass_activity_date']= $this->input->post('mass_activity_date');
        $content['mass_activity_time'] = $this->input->post('mass_activity_time');
        $content['mass_activity_description']= $this->input->post('mass_activity_description');

            $save_data = array(
                'mass_activity_name'=> $content['mass_activity_name'],
                'mass_activity_date' => $content['mass_activity_date'],
                'mass_activity_time' => $content['mass_activity_time'],
                'mass_activity_description' => $content['mass_activity_description'],
                'mass_activity_is_active' => '1',
                'mass_activity_created_time' => $this->activity->currentDateTime
            );
           

           $activity_id = $this->activity->save($this->activity->table_activity,$save_data,$where);
              

            if($activity_id){
            $this->messages->add('You have successfully updated a Mass activity in your portal.','success');
            redirect('admin/activity/index');
         }

         else{
                $this->messages->add('Error occured.','sucess');
            redirect('admin/activity/add');

        }

}
             $this->templates->set_data('index',$index);
             $this->templates->set_data('content',$content);
             $this->templates->load();

}

    public function status()
    {
        $sID = $this->input->post('sID');
        $jsonArray=array('flag'=>false);
        $where_array = array(
            'mass_id' => $sID
        );
        $activity_update_array = array(
            'mass_activity_is_active' => $this->input->post('sStatus')
        );


        if ($this->activity->save($this->activity->table_activity,$activity_update_array, $where_array)) {
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }



    public function delete()
       {
        $sID = $this->input->post('sID');
        $jsonArray = array('flag' => false);
        $where_array = array(
            'mass_id' => $sID
        );
        $activity_update_array = array(
            'mass_activity_is_deleted' => '1'
        );
        if($this->activity->save($this->activity->table_activity,$activity_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }
  
   
}




