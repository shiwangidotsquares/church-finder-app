<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Upload extends CI_Controller {

	public function __construct()
	{
	   parent::__construct();
	   $this->load->helper(array('url', 'form'));
	   $this->load->model('site_model');
	}

	public function index()
	{
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));

        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$layout = 'admin-layout';
		$view_file =  'admin/upload/upload_form';
		$index['page_title'] = ':: Media Manager ::';
		$content['form_title'] = 'Media Manager';
		$this->templates->set($layout);

		$where_array = array(
            'status' => 0
        );
        $content['getMedia_data'] = $this->site_model->getRecords($this->site_model->table_media,$where_array);

        $content['layout'] = $view_file;
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
		//$this->load->view('upload_form');
	}

	public function uploadify()
	{//print_r($_FILES);
		$target_dir = "uploads/mediamanager/";
		foreach ($_FILES['file']['name'] as $key => $value) {
			$target_file = $target_dir.time().basename($value);
			$save_file_name = time().basename($value);
			$file_name=$_FILES["file"]["name"][$key];
			$ext=pathinfo($file_name,PATHINFO_EXTENSION);
			if (move_uploaded_file($_FILES['file']["tmp_name"][$key],$target_file)) {
			    $status = 1;
			    $save_img_data = array(
			        /*'org_id' => 1,*/
			        /*'filename' => $_FILES["file"]["name"][$key],*/
			        'filename' => $save_file_name,
			        'type' => $ext
			        
			    );
			    $img_insrt_id = $this->site_model->save('lss_media', $save_img_data);
			}
		}


	}
	public function unlinkFile(){
			//echo getcwd();
			// $path=getcwd().'/uploads/mediamanager/'.$_POST["id"];
			// unlink($path);
		


		$jsonArray = array('flag' => false);
        $where_array = array(
            'id' => $_POST["id"]
        );
        $update_array = array(
            'status' => '1'
        );
        if($this->site_model->save($this->site_model->table_media,$update_array,$where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode(array('message' =>"file deleted successfully","status"=>"true"));
		die;







	}
}
/* End of file uploadify.php */
/* Location: ./application/controllers/uploadify.php */