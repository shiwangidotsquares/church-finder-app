<?php
defined('BASEPATH') OR exit('No direct access allowed');

class Company extends MY_Controller {
 	var $userData   = array();
    var $secret = '';
	public function __construct() {
        parent::__construct();

		$adminlogin = $this->session->userdata('is_admin');
        $this->load->model('company_model','company');
        $this->load->model('user_model','users');
        $this->load->model('Site_model');
        $this->load->library('googleauthenticator');
        $this->secret = $this->googleauthenticator->createSecret();
        $this->load->model('privileges','privilege');
    }

	public function index(){
        $this->privileges->check_privileges();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));

		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));

        //$this->privilege->check_privileges();
		$data = array();
		$layout = 'admin-layout';
		$view_file =  'admin/company/index';
		$index['page_title'] = ':: View Company ::';
		$content['form_title'] = 'View Company';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}

    public function view($company_id){
        $this->privileges->check_privileges();
        $data = array();
        $index['page_title'] = ':: View Company Details ::';
        $content['form_title'] = 'View Company Details';
        $layout = 'admin-layout';
        $view_file =  'admin/company/view';
        $where = array('company_id' => $company_id);

        $data = $this->company->getCompanyData($where,$select = '*');

        $userData = $data['data'];

        $where = array('country_id' => $userData[0]['billing_country']);
        $dataC = $this->company->getRecords($this->company->table_country,$where);

        $content['country_name'] = $dataC[0]['name'];
        $content['userData'] = $userData;

        $this->templates->set($layout);
        $content['layout'] = $view_file;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }
	public function get_company(){
		$order_by = array();
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        if(empty($length)){
            $length = 10;
            $start = 0;
        }
        $columnData = array(
            'sr_no',
			'full_name',
            'company_name',
            'user_username',
			'user_email',
            'company_created_time',
			'is_active'
        );
        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
        $searchData = $this->input->post('searchBox');
        $where = '';
        $where .= 'U.user_is_deleted = "0"';
        $and=' and ';

        if($searchData){
            $where.= $and.'(U.first_name like "%'.$searchData.'%" OR C.company_name like "%'.$searchData.'%" OR U.user_email like "%'.$searchData.'%" OR U.user_username like "%'.$searchData.'%")';
        }

        $List_Array = $this->company->getCompanyData($where,$select = 'C.company_name,C.company_created_time,C.company_id,U.first_name,U.last_name,U.user_username,U.user_email,U.user_is_active,U.user_id',$order_by, $start, $length,$where_in = false,$where_not_in = false);

        $companyList = $List_Array['data'];

        $totalData = $List_Array['total'];
        $jsonArray=array(
            'draw'=>$this->input->post('draw'),
            'recordsTotal'=>$totalData,
            'recordsFiltered'=>$totalData,
            'data'=>array(),
        );
        foreach($companyList as $key => $val){

			$active = $val['user_is_active']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['user_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['user_id'].'" title="Active"></i></a>';
			$view = '<a href="'.site_url('admin/company/view/'.$val['company_id']).'"><i class="fa fa-info-circle viewRecord" rel="'.$val['company_id'].'" title="View Details"></i></a>';
            $edit = '<a href="'.site_url('admin/company/edit/'.$val['company_id']).'" rel="'.$val['company_id'].'"><i class="fa fa-edit" title="Edit"></i></a>';
            $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['user_id'].'" title="Delete"></i></a>';
			$login_as_user = '<a target="_blank" href="'.site_url('admin/company/login_as_user/'.$val['user_id']).'" rel="'.$val['user_id'].'"><i class="fa fa-user" title="Login as user"></i></a>';

            $jsonArray['data'][] = array(
                'sr_no' => $start + $key + 1,
				'full_name' => $val['first_name'].' '.$val['last_name'],
                'company_name' => $val['company_name']?ucfirst($val['company_name']):'---',
                'user_username' => $val['user_username']?$val['user_username']:'---',
                'user_email' => $val['user_email']?$val['user_email']:'---',
				'company_created_time'=>$val['company_created_time'],
                #'action' => $active.'&nbsp;'.$delete
                'action' => $active.'&nbsp;'.$view.'&nbsp;'.$edit.'&nbsp;'.$delete.'&nbsp;'.$login_as_user
            );
        }

        echo json_encode($jsonArray); exit;
        echo $this->input->post('draw'); exit;

	}

	public function add(){
		$this->privileges->check_privileges();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));

		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$data = array();
        $country_data = getContries();
        $content['country_data'] = $country_data['data'];
		$layout = 'admin-layout';
		$view_file =  'admin/company/add';
		$index['page_title'] = '::Add Company ::';
		$content['form_title'] = 'Add Company ';
		$this->templates->set($layout);
		$content['layout'] = $view_file;

        $path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);
		/*$membership_for = $this->users->get_membership_for();
		$content['membership_for_data'] = $membership_for['data'];*/

       $content['first_name']              = $this->input->post('first_name');
       $content['last_name']               = $this->input->post('last_name');
       $content['user_email']              = $this->input->post('user_email');
       $content['company_name']              = $this->input->post('company_name');
       $content['username']                = $this->input->post('username');
       $content['phone_number']            = $this->input->post('phone_number');
       $content['address_line1']           = $this->input->post('address_line1');
       $content['address_line2']           = $this->input->post('address_line2');
       $content['user_city']               = $this->input->post('user_city');
       $content['region']                  = $this->input->post('region');
       $content['postcode']                = $this->input->post('postcode');
       $content['billing_state']           = $this->input->post('billing_state');
       $content['billing_postcode']        = $this->input->post('billing_postcode');
       $content['billing_country']        = $this->input->post('billing_country');


		$config = array(
            array(
                'field'   => 'first_name',
                'label'   => 'First Name',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'last_name',
                'label'   => 'Last Name',
                'rules'   => 'trim|required'
            ),
            array(
                    'field'   => 'company_name',
                    'label'   => 'Company Name',
                    'rules'   => 'trim|required'
                ),
            array(
                'field'   => 'user_email',
                'label'   => 'Company Email',
                'rules'   => 'trim|required|callback_check_email|valid_email'
            ),
            array(
                'field'   => 'username',
                'label'   => 'Company Username',
                'rules'   => 'trim|required|callback_check_username'
            ),
            array(
                'field'   => 'user_password',
                'label'   => 'Company Password',
                'rules'   => 'trim|required|matches[confirm_password]|min_length[6]'
            ),
             array(
                'field'   => 'confirm_password',
                'label'   => 'Confirm Password',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'phone_number',
                'label'   => 'Phone Number',
                'rules'   => 'trim|required|numeric'
            ),
            array(
                'field'   => 'address_line1',
                'label'   => 'Address Line 1',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'user_city',
                'label'   => 'Town/City',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'region',
                'label'   => 'Region',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'postcode',
                'label'   => 'Postcode',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'billing_state',
                'label'   => 'Billing State / Province',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'billing_postcode',
                'label'   => 'Billing Postal / Zip Code',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'billing_country',
                'label'   => 'Billing Country',
                'rules'   => 'trim|required'
            )

        );

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE){
			$content['layout'] = $view_file;

		}else{

            $save_data = array(
                'first_name' => $content['first_name'],
                'last_name' => $content['last_name'],
                'role_id' => 2,
                'user_email' => $content['user_email'],
                'user_username' => $content['username'],
                'user_password' => generateHash($this->input->post('user_password')),
                'google_auth_code' => $this->secret,
                'created_time' => $this->users->currentDateTime
            );
            $last_id = $this->users->save($this->users->table_users,$save_data);
            $chars = time() .'_'. "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $res = "";
            for ($i = 0; $i < 6; $i++) {
               $res .= $chars[mt_rand(0, strlen($chars)-1)];
            }
            $names = substr($content['first_name'], 0, 4);
            $res = $names.$res;
            if($last_id){
                $save_data = array(
                    'user_id' => $last_id,
                    'user_mobile_number' => $content['phone_number'],
                    'address_line1' => $content['address_line1'],
                    'address_line2' => $content['address_line2'],
                    'city' => $content['user_city'],
                    'region' => $content['region'],
                    'postcode' => $content['postcode'],
                    'billing_state' => $content['billing_state'],
                    'billing_zipcode' => $content['billing_postcode'],
                    'billing_country' => $content['billing_country'],
                  'user_referral_code' => $res
                );
                $this->users->save($this->users->table_users_info,$save_data);
                $address = $content['address_line1'].' '.$content['address_line2'].' '.$content['user_city'].' '.$content['billing_country'];
                $save_data = array(
                    'user_id' => $last_id,
                    'company_name' => $content['company_name'],
                    'company_logo' => '',
                    'company_slug' => create_unique_slug($content['first_name'],$this->company->table_company,'company_slug'),
                    'company_address' => $address,
                    'company_created_by' => 1,
                    'company_created_time' => $this->company->currentDateTime
                );

                $last_id = $this->company->save($this->company->table_company,$save_data);
				if($last_id){
					$mail_to      = $content['user_email'];
					$subject      = 'Welcome !! You have added as a company administrator to GHC eLearning';
					$mail_extra   = '';
					$mail_cc      = '';
					$mail_bcc     = '';
					if (!empty($mail_extra)) {
						$mail_bcc = $mail_extra;
					}

					$sender_email = ADMIN_EMAIL;
					$sender_name  = ADMIN_NAME;

					$body = array('first_name' => $content['first_name'], 'user_email'=> $content['user_email'], 'username'=> $content['user_username'], 'password'=> $this->input->post('user_password'), 'login_url' => base_url().'account/login');
					/* Send Inquiry in mail */
					$message_data = get_email_body($template_id=3,$body);
					$messagebody=$message_data['body'];

					$this->sendmail($mail_to, $subject, $messagebody, $mail_cc, $mail_bcc, $sender_email, $sender_name, false);
				}

                $this->messages->add('You have successfully added a company in your portal.','success');
                redirect('admin/company/index');

            }else{
                $this->messages->add('error occurred.','error');
                redirect('admin/company/index');
            }

        }
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}

    public function edit($company_id){
        $this->privileges->check_privileges();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
        $country_data = getContries();
        $content['country_data'] = $country_data['data'];
		$layout = 'admin-layout';
		$view_file =  'admin/company/edit';
		$index['page_title'] = '::Edit Company::';
		$content['form_title'] = 'Edit Company ';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
		$path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);
		$where = array('company_id' => $company_id);

        #$data = $this->Site_model->getData($this->Site_model->table_users,$where);
        $data = $this->company->getCompanyData($where,$select = 'C.*,U.*,UF.*');

        $userData = $data['data'];
        #prd($userData);
        $content['company_id']              = $company_id;
        $content['user_id']                 = $userData[0]['user_id'];
        $content['first_name']              = $userData[0]['first_name'];
        $content['last_name']              = $userData[0]['last_name'];
        $content['company_name']           = $userData[0]['company_name'];
        $content['user_email']              = $userData[0]['user_email'];
        $content['username']                = $userData[0]['user_username'];
        $content['phone_number']            = $userData[0]['user_mobile_number'];
        $content['address_line1']           = $userData[0]['address_line1'];
        $content['address_line2']           = $userData[0]['address_line2'];
        $content['user_city']               = $userData[0]['city'];
        $content['region']                  = $userData[0]['region'];
        $content['postcode']                = $userData[0]['postcode'];
        $content['billing_state']           = $userData[0]['billing_state'];
        $content['billing_postcode']        = $userData[0]['billing_zipcode'];
        $content['billing_country']        = $userData[0]['billing_country'];



		if($_POST){
            $content['first_name']              = $this->input->post('first_name');
            $content['last_name']               = $this->input->post('last_name');
            $content['company_name']               = $this->input->post('company_name');
            $content['user_email']              = $this->input->post('user_email');
            $content['username']                = $this->input->post('username');
            $content['phone_number']            = $this->input->post('phone_number');
            $content['address_line1']           = $this->input->post('address_line1');
            $content['address_line2']           = $this->input->post('address_line2');
            $content['user_city']               = $this->input->post('user_city');
            $content['region']                  = $this->input->post('region');
            $content['postcode']                = $this->input->post('postcode');
            $content['billing_state']           = $this->input->post('billing_state');
            $content['billing_postcode']        = $this->input->post('billing_postcode');
            $content['billing_country']        = $this->input->post('billing_country');
		}

		$config = array(
                array(
                    'field'   => 'first_name',
                    'label'   => 'First Name',
                    'rules'   => 'trim|required'
                ),
                 array(
                    'field'   => 'last_name',
                    'label'   => 'Last Name',
                    'rules'   => 'trim|required'
                ),
                array(
                    'field'   => 'username',
                    'label'   => 'Company Username',
                    'rules'   => 'trim|required|callback_check_username_edit'
                ),
                array(
                    'field'   => 'company_name',
                    'label'   => 'Company Name',
                    'rules'   => 'trim|required'
                ),
                array(
                    'field'   => 'user_email',
                    'label'   => 'Company Email',
                    'rules'   => 'trim|required|callback_check_email_edit|valid_email'
                ),

                array(
                    'field'   => 'user_password',
                    'label'   => 'Company Password',
                    'rules'   => 'trim|matches[confirm_password]|min_length[6]'
                ),
                 array(
                    'field'   => 'confirm_password',
                    'label'   => 'Confirm Password',
                    'rules'   => 'trim'
                ),
                array(
                    'field'   => 'phone_number',
                    'label'   => 'Phone Number',
                    'rules'   => 'trim|required|numeric'
                ),
                array(
                    'field'   => 'address_line1',
                    'label'   => 'Address Line 1',
                    'rules'   => 'trim|required'
                ),
                array(
                    'field'   => 'user_city',
                    'label'   => 'Town/City',
                    'rules'   => 'trim|required'
                ),
                array(
                    'field'   => 'region',
                    'label'   => 'Region',
                    'rules'   => 'trim|required'
                ),
                array(
                    'field'   => 'postcode',
                    'label'   => 'Postcode',
                    'rules'   => 'trim|required'
                ),
                array(
                    'field'   => 'billing_state',
                    'label'   => 'Billing State / Province',
                    'rules'   => 'trim|required'
                ),
                array(
                    'field'   => 'billing_postcode',
                    'label'   => 'Billing Postal / Zip Code',
                    'rules'   => 'trim|required'
                ),
                array(
                    'field'   => 'billing_country',
                    'label'   => 'Billing Country',
                    'rules'   => 'trim|required'
                )

            );


		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE){

			$content['layout'] = $view_file;

		}else{

			$where_update_array = array(
                'user_id' => $content['user_id']
            );
            $save_data = array(
                'first_name' => $content['first_name'],
                'last_name' => $content['last_name'],
                'user_username' => $content['username'],
                'role_id' => 2,
                'user_email' => $content['user_email'],
            );
            if($this->input->post('user_password')){
                $save_data['user_password'] = generateHash($this->input->post('user_password'));
            }
            $last_id = $this->company->save($this->company->table_users,$save_data,$where_update_array);


            $where_update_array = array(
                'company_id' => $company_id
            );
            if($last_id){
               $save_data = array(
                  'company_name' => $content['company_name']
              );

              $last_id = $this->company->save($this->company->table_company,$save_data,$where_update_array);
              $where_update_user = array(
                  'user_id' => $content['user_id']
              );

              $save_data = array(
                  'user_id' => $content['user_id'],
                  'user_mobile_number' => $content['phone_number'],
                  'address_line1' => $content['address_line1'],
                  'address_line2' => $content['address_line2'],
                  'city' => $content['user_city'],
                  'region' => $content['region'],
                  'postcode' => $content['postcode'],
                  'billing_state' => $content['billing_state'],
                  'billing_zipcode' => $content['billing_postcode'],
                  'billing_country' => $content['billing_country']
              );
              $this->users->save($this->users->table_users_info,$save_data,$where_update_user);

              $this->messages->add('You have successfully updated Company.','success');
                  redirect('admin/company/index');
              }else{
                  $this->messages->add('error occurred.','error');
                  redirect('admin/company/index');
              }

		}

		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
    }
    public function status(){
        $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray=array('flag'=>false);
        $where_array = array(
            'user_id' => $sID
        );
        $page_update_array = array(
            'user_is_active' => $this->input->post('sStatus')
        );
        if($this->company->save($this->company->table_users,$page_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }

    public function delete(){
        $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray = array('flag' => false);
        $where_array = array(
           'user_id' => $sID
       );
        $page_update_array = array(
            'user_is_deleted' => '1'
        );
        if($this->company->save($this->company->table_users,$page_update_array, $where_array)){
            #echo $this->db->last_query();die;
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }

	function check_username($username){
            $where_array = array(
                'user_username' => $username,
                'user_is_deleted' => '0'
            );

             $result = $this->users->checkRecord($this->users->table_users,$where_array);
            if ($result){
                $this->form_validation->set_message('check_username', 'The %s field value already exist, please try another.');
                return false;
            }else{
                return true;
            }
        }

        function check_email($user_email){
            $where_array = array(
                'user_email' => $user_email,
                'user_is_deleted' => '0'
            );

           $result = $this->users->checkRecord($this->users->table_users,$where_array);
            if ($result){
                $this->form_validation->set_message('check_email', 'The %s field value already exist, please try another.');
                return false;
            }else{
                return true;
            }
        }

	function check_username_edit($username){
        $user_id = $this->input->post('user_id');
        $where_array = array(
            'user_username' => $username,
            'user_is_deleted' => '0',
            'user_id !=' => $user_id
        );

        $result = $this->Site_model->getData($this->Site_model->table_users,$where_array);
       if (count($result['data'])){
 			$this->form_validation->set_message('check_username_edit', 'The %s field value already exist, please try another.');
 			return false;
  		}else{
 			return true;
  		}
    }

    function check_email_edit($user_email){
	    $user_id = $this->input->post('user_id');
        $where_array = array(
            'user_email' => $user_email,
            'user_is_deleted' => '0',
            'user_id !=' => $user_id
        );

       $result = $this->Site_model->getData($this->Site_model->table_users,$where_array);
       if (count($result['data'])){
 			$this->form_validation->set_message('check_email_edit', 'The %s field value already exist, please try another');
 			return false;
  		}else{
 			return true;
  		}
    }


	  public function check_unique_user_username($username=''){
        $user_username = $this->input->post('user_username');
        $page_type = $this->input->post('page_type');
        $company_id = $this->input->post('company_id');

         if($page_type == 'edit'){
            $where_array = array(
                'user_username' => $user_username,
                'company_id !=' => $company_id
            );
         }else{
           $where_array = array(
                'user_username' => $user_username,
            );
         }

	   $result = $this->Site_model->getData($this->table_users,$where_array);

       	if (count($result['data'])){
 			$jsonArray = array('result' => 'failure','message' => 'Username already exit! Please choose another');
  		}else{
 			$jsonArray = array('result' => 'success','message' => '');
  		}

		echo json_encode($jsonArray);exit;
    }



	public function check_unique_user_email($useremail=''){
        $user_email = $this->input->post('user_email');
        $page_type = $this->input->post('page_type');
        $company_id = $this->input->post('company_id');

         if($page_type == 'edit'){
            $where_array = array(
                'user_email' => trim($user_email),
                'company_id !=' => $company_id
            );
         }else{
            $where_array = array(
                'user_email' => trim($user_email),
            );
         }

	   $result = $this->Site_model->getData($this->table_users,$where_array);

       	if (count($result['data'])){
 			$jsonArray = array('result' => 'failure','message' => 'Email already exit! Please choose another');
  		}else{
 			$jsonArray = array('result' => 'success','message' => '');
  		}

		echo json_encode($jsonArray);exit;
    }



}
