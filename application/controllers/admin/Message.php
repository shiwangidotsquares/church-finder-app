<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Message extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('users');
        $this->load->model('course_model','course');
        $this->load->model('staff_model','staff');
        $this->load->model('privileges','privilege');
    }


    public function send_email(){
        $this->privileges->check_privileges();
        $data = array();
        $customers_array = array();
        $layout = 'admin-layout';
        $view_file =  'admin/send_email/send_email';
        $index['page_title'] = ':: Send Email ::';
        $content['form_title'] = 'Send Email';
        $this->templates->set($layout);
        $content['layout'] = $view_file;

        $path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);

        $where_arr = array('course_is_active' => '1','course_is_deleted' => '0');
        $course_array = $this->course->getRecords($this->course->table_course,$where_arr);


        $where_arr = array('role_id !=' => '1','user_is_active'=>'1','user_is_deleted'=>'0');
        $customers_array = $this->users->getRecords($this->users->table,$where_arr);

        $content['customers_array']        = $customers_array;
        $content['course_array']           = $course_array;
        $content['userlist']               = $this->input->post('userlist');
        $content['subject']                = $this->input->post('subject');
        $content['message_body']           = $this->input->post('message_body');

        $config = array(

            array(
                'field'   => 'userlist',
                'label'   => 'User',
                'rules'   => 'required'
            ),array(
                'field'   => 'subject',
                'label'   => 'Subject',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'message_body',
                'label'   => 'Message Body',
                'rules'   => 'required'
            )

        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE){
            $content['layout'] = $view_file;

        }else{

            foreach ($content['userlist'] as $key => $val) {
                $where_arr = array('user_id' => $val);
                $message_result = $this->users->getRecords($this->users->table,$where_arr);

                if($message_result){
                    $message = $content['message_body'];
                    $mail_to = $message_result[0]['user_email'];
                    $subject = $content['subject'];
                    $mail_cc = '';
                    $mail_bcc = '';
                    $sender_email = ADMIN_EMAIL;
                    $sender_name = ADMIN_NAME;
                    $attachUrl = '';

                    $body = array('message' => $message,'name' => $message_result[0]['first_name']);
                    /* Send Inquiry in mail */
                    $messagebody = $this->parser->parse('emailer/newslatter-email', $body, true);

                    $custom_mail_queue_array[$key] = array(
                        'mail_to' => $mail_to,
                        'subject' => $subject,
                        'messagebody' => $messagebody,
                        'mail_cc' => json_encode($mail_cc),
                        'mail_bcc' => json_encode($mail_bcc),
                        'sender_email' => $sender_email,
                        'sender_name' => $sender_name,
                        'attachUrl' => json_encode($attachUrl)
                    );
                }
            }

            if(!empty($custom_mail_queue_array)){
               $switch = $this->users->save_batch($this->users->table_custom_mail_queue,$custom_mail_queue_array);
            }

            if($switch){
                $this->messages->add('Email sent to selected users successfully.','success');
                redirect('admin/message/send_email');
            }else{
                $this->messages->add('error occered.','error');
                redirect('admin/message/send_email');
            }
        }

        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }

    public function send(){
        $this->privileges->check_privileges();
        if(true){
            if($_POST){
                $switch = '';
                $final_userlist = array();
                $whom_user              = $this->input->post('whom_user');
                $staff_list        = $this->input->post('staff_list');
                $list_users_type        = $this->input->post('list_users_type');
                $userlist               = $this->input->post('userlist');
                $individual_list               = $this->input->post('individual_list');
                $subject                = $this->input->post('subject');
                $message_body           = $this->input->post('message_body');

                if($whom_user == '1'){
                    $final_userlist = $userlist;
                }elseif($list_users_type=='1'){
                    $final_userlist = $staff_list;
                }elseif($list_users_type=='2'){
                    $final_userlist = $individual_list;
                }
                if(!empty($final_userlist)){

                    foreach ($final_userlist as $key => $val) {
                        $where_arr = array('user_id' => $val);
                        $message_result = $this->users->getRecords($this->users->table,$where_arr);

                        if($message_result){
                            $message = $message_body;
                            $mail_to = $message_result[0]['user_email'];
                            $subject = $subject;
                            $mail_cc = '';
                            $mail_bcc = '';
                            $sender_email = ADMIN_EMAIL;
                            $sender_name = ADMIN_NAME;
                            $attachUrl = '';

                            $body = array('message' => $message,'name' => $message_result[0]['first_name']);
                            /* Send Inquiry in mail */
                            $messagebody = $this->parser->parse('emailer/newslatter-email', $body, true);

                            $custom_mail_queue_array[$key] = array(
                                'mail_to' => $mail_to,
                                'subject' => $subject,
                                'messagebody' => $messagebody,
                                'mail_cc' => json_encode($mail_cc),
                                'mail_bcc' => json_encode($mail_bcc),
                                'sender_email' => $sender_email,
                                'sender_name' => $sender_name,
                                'attachUrl' => json_encode($attachUrl)
                            );
                        }
                    }

                    if(!empty($custom_mail_queue_array)){
                       $switch = $this->users->save_batch($this->users->table_custom_mail_queue,$custom_mail_queue_array);
                    }
                }
                if($switch){
                    $this->messages->add('Email sent to selected users successfully.','success');
                    redirect('admin/message/send_email');
                }else{
                    $this->messages->add('error occered.','error');
                    redirect('admin/message/send_email');
                }
            }
        }
    }
    public function load_individual_type(){
        $data= array();
        $data['individual_array'] = array();
        $where_arr = array('role_id' => 3,'user_is_active' => '1','user_is_deleted' => '0');
        $individual_array = $this->users->getRecords($this->users->table,$where_arr);
        if($individual_array){
            $data['individual_array']        = $individual_array;
        }
        $this->load->view('admin/send_email/load_individual', $data);
    }
    public function load_company_users(){
        $data= array();
        $data['staff_array'] = array();;
        $company_id = $this->input->post('company_id');
        $where_arr = array('C.company_id' => $company_id,'U.user_is_active' => '1','U.user_is_deleted' => '0');
        $staff_array = $this->staff->get_staff($where_arr);
        if($staff_array['data']){
            $data['staff_array']        = $staff_array['data'];
        }
        $this->load->view('admin/send_email/load_staff', $data);
    }

}

/* End of file newsletter.php */
/* Location: ./application/controllers/newsletter.php */