<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Slider extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('sliders','obj_slider');
        $this->admin_session_data = $this->session->userdata("admin_session_data");
        $this->load->model('privileges','privilege');
    }

    /**
       * this is a generic function to show all Sliders
       * function having no parameter
       * function using Sliders module to show all Sliders list
       * @access public
    */

	public function index(){
        $this->privileges->check_privileges();
	    add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));

        $data = array();
		$layout = 'admin-layout';
		$view_file =  'admin/slider/index';
		$index['page_title'] = ':: View Slider ::';
		$content['form_title'] = 'View Slider';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}


    public function get_slider() {
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        $columnData = array('sr_no', 'slider_title', 'slider_created_time', 'action');
        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
        $searchData = $this->input->post('search');
        $where = NULL;
        if ($searchData['value']) {
            $where = '(slider_title like "%' . $searchData['value'].'%")';
        }
        $sliderList = $this->obj_slider->get_all_sliders($where, $select = '*', $order_by, '', $start, $length);

        $sliderData = $sliderList['data'];
        $totalData = $sliderList['total'];
        $jsonArray = array
            (
                'draw' => $this->input->post('draw'),
                'recordsTotal' => $totalData,
                'recordsFiltered' => $totalData,
                'data' => array(),
            );
        foreach ($sliderData as $key => $val) {
            $active = $val['slider_is_active'] ? '<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['slider_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['slider_id'].'" title="Active"></i></a>';
            $edit = '<a href="' . site_url('admin/slider/edit/'. $val['slider_id']) . '" rel="' . $val['slider_id'] . '"><i class="fa fa-edit" title="Edit"></i></a>';
            $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['slider_id'].'" title="Delete"></i></a>';

            $jsonArray['data'][] = array
                (
                    'sr_no' => $start + $key + 1,
                    'slider_image' => '<img src="'.base_url().'timthumb.php?src='.base_url().'uploads/slider/'.$val['slider_image'].'&h=100&w=100" />',
                    'slider_title' => $val['slider_title'],
                    'slider_created_time' => show_datetime($val['slider_created_time']),
                    'action' => $active . '&nbsp;' . $edit . '&nbsp;' . $delete
                );
        }
        echo json_encode($jsonArray);
        exit;
    }

	/**
	 * this is a generic function to save data about Slider in db
	 * function having no parameter
	 * function using view to add Slider at location admin/Slider/add.php
 	 * @access public
	 * function will fill all field values for Slider and upload image for Slider after successful field validation
	*/

    public function add(){
        $this->privileges->check_privileges();
        $layout = 'admin-layout';
        $index = array();
        $this->templates->set($layout);
        $content['formTitle']    		   = 'Add Slider';
        $content['buttonText']   		   = 'Add';
        $content['slider_title']           = $this->input->post('slider_title');
        $content['slider_url']             = $this->input->post('slider_url');
        $content['slider_is_active']       = $this->input->post('slider_is_active');

        $config = array(
               array(
                     'field'   => 'slider_title',
                     'label'   => 'Slider Text',
                     'rules'   => 'trim|required|xss_clean'
                  ),
                  array(
                     'field'   => 'slider_url',
                     'label'   => 'Slider URL',
                     'rules'   => 'trim|xss_clean'
                  ),
			     array(
                     'field'   => 'slider_image',
                     'label'   => 'slider_image',
                     'rules'   => 'trim|callback_slider_upload|xss_clean'
                  )
            );

		$this->form_validation->set_rules($config);

        if($this->form_validation->run() == FALSE){
			$view_file = 'admin/slider/add';
		}else{

			$slider_title      = $this->input->post('slider_title');
			$slider_url        = $this->input->post('slider_url');
			$slider_is_active  = $this->input->post('slider_is_active');

		    $save_data = array(
				'slider_title' => $slider_title,
				'slider_url' => $slider_url,
				'slider_is_active' => $slider_is_active,
				'slider_created_time'=> $this->obj_slider->currentDateTime
			);

			//Upload data

			if(!empty($this->upload_data)){
				if(!empty($this->upload_data['file_name'])){
					$new_slider_name = rand().time().$this->upload_data['file_ext'];
					rename('./uploads/slider/'.$this->upload_data['file_name'], './uploads/slider/'.$new_slider_name);
					$save_data['slider_image'] = $new_slider_name;
                    resize_image('./uploads/slider/'.$new_slider_name,'./uploads/slider/thumbs/'.$new_slider_name,'1920','804');
				}
			}

			$this->obj_slider->save($this->obj_slider->table,$save_data);
	       	$this->session->set_flashdata('message','Slider added successfully.');
			redirect('admin/slider/index/');
		}

		$content['layout'] = $view_file;
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}

    /**
	 * this is a generic function to update data about Slider in db
	 * function having a single parameter having id of the Slider which have to update
	 * function using view to update Slider at location admin/Slider/edit.php
 	 * @access public
 	 * @param  integer
	 * function will fill all field values for Slider and image for Slider,after successful field validation
	*/

    public function edit($id){
        $this->privileges->check_privileges();
		$layout = 'admin-layout';
        $index = array();
        $this->templates->set($layout);
		if(empty($id)){
			$this->session->set_flashdata('error_message','Record not found.');
			redirect('admin/slider/index/');
		}

		$content['result'] = $this->obj_slider->show_all_slider(array('slider_id' => $id));
		if(empty($content['result'])){
			$this->session->set_flashdata('error_message','Record not found.');
			redirect('admin/slider/index/');
		}
        $content['formTitle']   = 'Edit Slider';
        $content['buttonText']  = 'Update';
        $config = array(
               array(
                     'field'   => 'slider_title',
                     'label'   => 'Slider Text',
                     'rules'   => 'trim|required|xss_clean'
                  ),
               array(
                     'field'   => 'slider_url',
                     'label'   => 'Slider URL',
                     'rules'   => 'trim|xss_clean'
                  ),
                array(
                     'field'   => 'slider_image',
                     'label'   => 'slider_image',
                     'rules'   => 'trim|callback_slider_upload|xss_clean'
                  )
            );

		$this->form_validation->set_rules($config);

        if($this->form_validation->run() == FALSE){
			$view_file = 'admin/slider/edit';
		}else{

			$slider_title      = $this->input->post('slider_title');
			$slider_url        = $this->input->post('slider_url');
			$slider_is_active  = $this->input->post('slider_is_active');

		    $save_data = array(
				'slider_title' => $slider_title,
                'slider_url' => $slider_url,
				'slider_is_active' => $slider_is_active,
				'slider_created_time' => $this->obj_slider->currentDateTime
			);

			//Upload data

			if(!empty($this->upload_data)){
				if(!empty($this->upload_data['file_name'])){
					$new_slider_name = rand().time().$this->upload_data['file_ext'];
					rename('./uploads/slider/'.$this->upload_data['file_name'], './uploads/slider/'.$new_slider_name);
                    resize_image('./uploads/slider/'.$new_slider_name,'./uploads/slider/thumbs/'.$new_slider_name,'1920','804');
					$save_data['slider_image'] = $new_slider_name;

					//Unlink Old image
					$slider_image_old = $content['result'][0]['slider_image'];
					if(!empty($slider_image_old)){
						@unlink('./uploads/slider/'.$slider_image_old);
					}
				}
			}

			$this->obj_slider->save($this->obj_slider->table,$save_data,array('slider_id'=>$id));
	       	$this->session->set_flashdata('message','Slider updated successfully.');
			redirect('admin/slider/index/');
		}

        $content['layout'] = $view_file;
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}

	/**
     * this is a generic function to upload image for Slider
     * function having no parameter
     * @access public
     * @return boolean
     * function will upload image for a Slider with specific file extensions
    */

	public function slider_upload(){
		if(isset($_FILES['slider_image']) && $_FILES['slider_image']['name'] != '') {
			$this->load->library('upload');
			$config['upload_path'] = './uploads/slider/';
			$config['allowed_types'] = 'jpg|jpeg|gif|png';
			$config['max_size']    = 1024*2; //2MB
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('slider_image')){
				$this->form_validation->set_message('slider_upload',str_replace(array('<p>', '</p>'),'', $this->upload->display_errors()));
				return false;
			} else {
				$this->upload_data = $this->upload->data();
				return true;
			}
		}
		return true;
	}

    public function delete() {
        $this->privileges->check_privileges();
        $cID = $this->input->post('cID');
        $whereArray = array(
            'slider_id' => $cID
        );
        $update_data = array('slider_is_deleted' => 1);
        $jsonArray = array('flag' => false);
        if ($this->obj_slider->save($this->obj_slider->table,$update_data,$whereArray))
            $jsonArray['flag'] = true;
        echo json_encode($jsonArray);
        exit;
    }

    public function status() {
        $this->privileges->check_privileges();
        $cID = $this->input->post('cID');
        $whereArray = array(
            'slider_id' => $cID
        );
        $jsonArray = array('flag' => false);
        if ($this->obj_slider->getRecords($this->obj_slider->table,$whereArray)) {
            $jsonArray['flag'] = true;
            $saveData = array
                (
                'slider_is_active' => $this->input->post('cStatus'),
            );
            $this->obj_slider->save($this->obj_slider->table, $saveData, $whereArray);
        }
        echo json_encode($jsonArray);
        exit;
    }

}

/* End of file slider.php */
/* Location: ./application/controllers/admin/slider.php */