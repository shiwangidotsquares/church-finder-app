<?php
defined('BASEPATH') OR exit('No direct access allowed');

class Category extends MY_Controller {
 	var $data   = array();
	public function __construct() {
        parent::__construct();
		$adminlogin = $this->session->userdata('is_admin');
        $this->load->model('category_model','category');
    }

	public function index(){
        #$this->privileges->check_privileges();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$data = array();
		$layout = 'admin-layout';
		$view_file =  'admin/category/index';
		$index['page_title'] = ':: View Categories ::';
		$content['form_title'] = 'View Categories';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}
    function getcateogoryname(){
        $category_name= getcategoryname($_POST['id']);

        echo json_encode(array('category_name'=>$category_name[0]['category_name']));
      die;
    }
    public function treestructure(){
        $data = array();
        $layout = 'admin-layout';
        $view_file =  'admin/category/treestructure';
        $index['page_title'] = ':: View Categories ::';
        $content['form_title'] = 'View Categories';
        $this->templates->set($layout);
        $content['layout'] = $view_file;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }
	public function get_category(){
		$order_by = array();
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        if(empty($length)){
            $length = 10;
            $start = 0;
        }
        $columnData = array(
            'sr_no',
            'category_name',
            'category_description',
            'category_created_time',
			'action'
        );
        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
        $searchData = $this->input->post('searchBox');
        $where = '';
        $where .= 'category_is_deleted = "0"';
        $and=' and ';

        if($searchData){
            $where.= $and.'(category_name like "%'.$searchData.'%" OR category_description like "%'.$searchData.'%")';
        }

        $List_Array = $this->category->getCategoryData($where,$select = '*',$order_by, $start, $length,$where_in = false,$where_not_in = false);

        $companyList = $List_Array['data'];

        $totalData = $List_Array['total'];
        $jsonArray=array(
            'draw'=>$this->input->post('draw'),
            'recordsTotal'=>$totalData,
            'recordsFiltered'=>$totalData,
            'data'=>array(),
        );
        foreach($companyList as $key => $val){
            $category=get_parent_category($val['category_parent_id']);
//print_r($category);
           $active = $val['category_is_active']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['category_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['category_id'].'" title="Active"></i></a>';

            $edit = '<a href="'.site_url('admin/category/edit/'.$val['category_id']).'" rel="'.$val['category_id'].'"><i class="fa fa-edit" title="Edit"></i></a>';

            $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['category_id'].'" title="Delete"></i></a>';


            if(isset($category[0]->category_name)){
               $jsonArray['data'][] = array(
                'sr_no' => $start + $key + 1,
                'category_name' => $val['category_name']?ucfirst($val['category_name']):'---',
                'category_parent_id' =>$category[0]->category_name,
                'category_created_time'=>$val['category_created_time'],
                'action' => $active.'&nbsp;'.$edit.'&nbsp;'.$delete
            );
           }else{

              $jsonArray['data'][] = array(
                'sr_no' => $start + $key + 1,
                'category_name' => $val['category_name']?ucfirst($val['category_name']):'---',
                'category_parent_id' =>'',
                'category_created_time'=>$val['category_created_time'],
                'action' => $active.'&nbsp;'.$edit.'&nbsp;'.$delete
            );
           }


        }

        echo json_encode($jsonArray); exit;
        echo $this->input->post('draw'); exit;

	}

	public function add(){
        #$this->privileges->check_privileges();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$data = array();

		$layout = 'admin-layout';
		$view_file =  'admin/category/treestructure';
		$index['page_title'] = '::Add Category ::';
		$content['form_title'] = 'Add Category ';
		$this->templates->set($layout);
		$content['layout'] = $view_file;

        $path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);

        $content['category_name']                = $this->input->post('category_name');
        $content['parent_category_id']                = $this->input->post('parent_category_id');
        $config = array(
			array(
                'field'   => 'category_name',
                'label'   => 'Category Name',
                'rules'   => 'trim|required|callback_check_category'
            ),

		);
        $this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE){
			$content['layout'] = $view_file;

		}else{
            $save_data = array(
                'category_name' => $content['category_name'],
                'category_parent_id' => $content['parent_category_id'],
				'category_created_time' => $this->category->currentDateTime
            );

            $last_id = $this->category->save($this->category->table_category,$save_data);
            if($last_id){
    			$this->messages->add('You have successfully added a Category in your portal.','success');
                 redirect('admin/category/treestructure/'.$content['parent_category_id']);
            }else{
                $this->messages->add('error occurred.','error');
                 redirect('admin/category/treestructure/'.$content['parent_category_id']);
            }


		}
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}

    public function edit(){

         $category_id = $this->input->post('category_id');

         if($category_id == 0 || $category_id == ''){
            $this->messages->add("Don't have permision to delete this Category",'error');
            redirect('admin/category/treestructure/');
         }

        #$this->privileges->check_privileges();
        $data = array();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$layout = 'admin-layout';
		$view_file =  'admin/category/treestructure/';
		$index['page_title'] = '::Edit Category::';
		$content['form_title'] = 'Edit Category ';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
		$path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);


        $where = array('category_is_deleted' => '0', 'category_is_active' => '1','category_parent_id'=>'0' );
        $dataC = $this->category->getRecords($this->category->table_category,$where);

        $content['dataParent']               = $dataC;


		$where = array('category_id' => $category_id);
        $data = $this->category->getCategoryData($where,$select = '*');
        $data = $data['data'];
        $content['category_parent_id']                =$data[0]['category_parent_id'];
        $content['category_id']                         = $category_id;
        $content['parent_category_name']                = $data[0]['category_name'];
        $content['category_description']                = $data[0]['category_description'];
		if($_POST){
          //  $content['category_parent_id']                = $this->input->post('parent_category_id');
            $content['category_id']                         = $this->input->post('category_id');
            $content['parent_category_name']                = $this->input->post('parent_category_name');
            $content['category_description']         = $this->input->post('category_description');
		}

		$config = array(
            array(
                'field'   => 'parent_category_name',
                'label'   => 'Category Name',
                'rules'   => 'trim|callback_check_category_edit'
            ),
           /* array(
                'field'   => 'category_description',
                'label'   => 'Category Description',
                'rules'   => 'trim'
            ) */
        );
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE){
			$content['layout'] = $view_file;
		}else{
			$where_update_array = array(
                'category_id' => $content['category_id']
            );
            $save_data = array(
               // 'category_parent_id' => $content['category_parent_id'],
                'category_name' => $content['parent_category_name'],
                'category_description' => $content['category_description']
            );
            $last_id = $this->category->save($this->category->table_category,$save_data,$where_update_array);
            #echo $this->db->last_query(); die;
            if($last_id){
              $this->messages->add('You have successfully updated Category.','success');
                  redirect('admin/category/treestructure/'.$category_id);
              }else{
                  $this->messages->add('error occurred.','error');
                  redirect('admin/category/treestructure/'.$category_id);
              }

		}
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
    }
    public function status(){
        #$this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray=array('flag'=>false);
        $where_array = array(
            'category_id' => $sID
        );
        $page_update_array = array(
            'category_is_active' => $this->input->post('sStatus')
        );
        if($this->category->save($this->category->table_category,$page_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }

    public function delete(){

        #prd($this->input->post());
        #$this->privileges->check_privileges();
        $sID = $this->input->post('category_id');
        $jsonArray = array('flag' => false);
        $where_array = array(
           'category_id' => $sID,
           'category_parent_id !=' => 0
        );
        $where_array1 = array(
           'category_parent_id' => $sID
        );
        $page_update_array = array(
            'category_is_deleted' => '1'
        );

        $result = $this->category->checkRecord($this->category->table_category,$where_array);

        if(empty($result)){
           $this->messages->add("Don't have permision to delete this Category",'error');
           redirect('admin/category/treestructure/'.$sID);
        }

        if($this->category->save($this->category->table_category,$page_update_array, $where_array)){
           $this->category->save($this->category->table_category,$page_update_array, $where_array1);
           $this->messages->add('Category Deleted Successfully','success');
           redirect('admin/category/treestructure/'.$sID);
        }else{
            $this->messages->add('error occurred.','error');
            redirect('admin/category/treestructure/'.$sID);
        }

    }

	function check_category($category_name){
        $where_array = array(
            'category_name' => $category_name,
            'category_is_deleted' => '0'
        );
          $result = $this->category->checkRecord($this->category->table_category,$where_array);
       	if (count($result['data'])){
 			$this->form_validation->set_message('check_category', 'The %s field value already exist, please try another.');
 			return false;
  		}else{
 			return true;
  		}
    }

	function check_category_edit($category_name = ''){

        $category_id = $this->input->post('category_id');
        $where_array = array(
            'category_name' => $category_name,
            'category_is_deleted' => '0',
            'category_id !=' => $category_id
        );
        $result = $this->category->checkRecord($this->category->table_category,$where_array);
       if (count($result['data'])){
 			$this->form_validation->set_message('check_category_edit', 'The %s field value already exist, please try another.');
 			return false;
  		}else{
 			return true;
  		}
    }

}
