<?php  defined('BASEPATH') OR exit('No direct access allowed');

class Question extends MY_Controller {
    public function __construct() {
        parent ::__construct();
        $this->load->library('upload');
        $adminlogin = $this->session->userdata('is_admin');
        $this->load->model('question_model','questions');
        $this->load->model('category_model','category');
        $this->load->model('privileges');
    }

public function index(){
    //$this->privileges->check_privileges();
    $session_array = $this->session->userdata('user_session_data');
    add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
    add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
    $data = array();
    $index = array();
    $content = array();
    $layout     = 'admin-layout';
    $view_file  = 'admin/question/index';
    $index['page_title'] = 'Question Manager';
    $index['form_title'] = 'View Question';
    $content['user_info']   = $session_array;
    $this->templates->set($layout);
    $content['layout']      = $view_file;
    $userData                = $this->questions->getRecords($this->questions->table_question);
    $content['userData']     = $userData;
    $this->templates->set_data('index',$index);
    $this->templates->set_data('content',$content);
    $this->templates->load();
}
public function get_all(){
    $length     = $this->input->post('length');
    $start      = $this->input->post('start');
    $columnData = array(
        'sr_no',
        'question_title',
        'question_description',
        'question_created_date',
        'action'

    );
    $sortData       = $this->input->post('order');
    $order_by[0]    = $columnData[$sortData[0]['column']];
    $order_by[1]    = $sortData[0]['dir'];
    $searchData     = $this->input->post('searchBox');
    $where  = '';
    $where  = 'question_is_deleted = 0';

    if($searchData){
        $where.= 'and (question_title like "%'.$searchData.'%" OR question_description like "%'.$searchData.'%")';
    }
    $list = $this->questions->getData('',$where, $select = '*', $order_by, $start, $length);
    #echo $this->db->last_query(); die;
    $list_array    = $list['data'];
    $totalData          = $list['total'];
    $jsonArray          = array(
        'draw'              => $this->input->post('draw'),
        'recordsTotal'      => $totalData,
        'recordsFiltered'   => $totalData,
        'data'              => array(),
    );
    foreach($list_array as $key => $val){
        $active = $val['question_is_active']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['question_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['question_id'].'" title="Active"></i></a>';
         $edit = '<a href="' . base_url('admin/question/edit/' . $val['question_id']) . '" rel="' . $val['question_id'] . '"><span class="fa fa-edit"></span></a>';
         $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['question_id'].'" title="Delete"></i></a>';
        $edit = '<a href="' . base_url('admin/question/edit/' . $val['question_id']) . '" rel="' . $val['question_id'] . '"><span class="fa fa-edit"></span></a>';
        $originalDate = $val['question_created_date'];
        $question_created_date = date("d/m/Y", strtotime($originalDate));
        //$question_logo = '<img src=../uploads/Question/'. $val["question_image"] .' width="80">';
        $jsonArray['data'][] = array(
           'sr_no' => $start + $key + 1,
           'question_title' => $val['question_title'],
           'question_description' => $val['question_description'],
           'question_created_date' => $question_created_date,
           'action' =>  $active.'&nbsp;'.$edit.'&nbsp;'.$delete
       );
    }
    echo json_encode($jsonArray); exit;
}
public function Add(){

    $path = '../../../assets/js/ckfinder';
    $width = '1300px';
    parent::editor($path,$width);
    $index = array();
    $content = array();
    $email_array = array();
    $index['page_title'] = 'Question Manager';
    $content['form_title'] = 'Add Question';
    $content['button_text'] = 'Update Question';
    $this->templates->set('admin-layout');

    $cat_Array = $this->category->getCategoryData(array('category_parent_id'=>9));
    $content['question_category_array'] = $cat_Array['data'];

    $content['question_id']           = '';
    $content['title']                 = $this->input->post('title');
    $content['question_category']     = $this->input->post('question_category');
    $content['description']           = $this->input->post('description');
    $config = array(
       array(
            'field' => 'title',
            'label' => 'Title',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'question_category',
            'label' => 'Question Category',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required'
        )
    );
    $this->form_validation->set_rules($config);
    if ($this->form_validation->run() == FALSE){
        $content['layout'] = 'admin/question/add';
    }else{
        $title = $this->input->post('title');
        $description = $this->input->post('description');
        $saveData = array(
            'question_title' => $title,
            'org_id'       => 1,
            'question_category' => $content['question_category'],
            'question_description' => stripslashes($description),
            'question_created_date'  => $this->questions->currentDateTime
        );
        $last_id = $this->questions->save($this->questions->table_question,$saveData);
        if($last_id){
            foreach ($this->input->post('answer') as $key => $value) {
                if($key==$this->input->post('correct_answer')){
                    $is_correct = 1;
                }else{
                    $is_correct = 0;
                }
               $saveAnswerData = array(
                   'question_id' => $last_id,
                   'answer' => $value,
                   'weightage' => $this->input->post('weightage')[$key],
                   'is_correct'  => $is_correct
               );
            $this->questions->save($this->questions->table_question_options,$saveAnswerData);
            }

        }

        $this->messages->add('You have successfully added question.','success');
        redirect('admin/question');
    }
    $this->templates->set_data('index',$index);
    $this->templates->set_data('content',$content);
    $this->templates->load();
}

public function edit($id = ''){
    if($id == ""){
       $this->messages->add('No direct access allowed.','error');
       redirect('admin/question/index');
    }
    //check record exist or not
    $record = $this->questions->checkRecord($this->questions->table_question, array('question_id'=>$id));
	
    if(empty($record)){
       $this->messages->add('No question found with given id.','error');
       redirect('admin/question/index');
    }
    $index = array();
    $content = array();
    $email_array = array();

    $this->privileges->check_privileges();
    $path = '../../../assets/js/ckfinder';
    $width = '1300px';
    parent::editor($path,$width);

    $cat_Array = $this->category->getCategoryData(array('category_parent_id'=>9));

    $content['question_category_array'] = $cat_Array['data'];

    $where_array = array(
        'question_id' => $id
    );
    #prd($where_array);
    $switch = $this->questions->checkRecord($this->questions->table_question,$where_array);

    $data_answer = $this->questions->getRecords($this->questions->table_question_options,$where_array);
	
    $content['data_answer'] = $data_answer;
    if((empty($id) && !is_numeric($id)) && ($switch == false)){
        $this->messages->add('Selected record does not exist in our database!','error');
        redirect(base_url('admin/question/index'));
    }

    $index['page_title'] = 'Question Manager';
    $content['form_title'] = 'Edit Question';
    $content['button_text'] = 'Update Question';
    $this->templates->set('admin-layout');
    $where_array = array(
        'question_id' => $id
    );
    $email_array = $this->questions->getData('',$where_array,$select = '*');

    $email_array = $email_array['data'];
    $content['question_id']           = $id;

    if(!empty($email_array)){
        $content['title']               = !empty($email_array[0]['question_title'])?$email_array[0]['question_title']:'';
        $content['question_category']   = !empty($email_array[0]['question_category'])?$email_array[0]['question_category']:'';
        $content['description']         = !empty($email_array[0]['question_description'])?$email_array[0]['question_description']:'';
    }

    if($_POST){
        $content['title']                       = $this->input->post('title');
        $content['question_category']           = $this->input->post('question_category');
        $content['description']                 = $this->input->post('description');
    }

    $config = array(
        array(
            'field' => 'title',
            'label' => 'Title',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'question_category',
            'label' => 'Question Category',
            'rules' => 'trim|required'
        )
    );
    $this->form_validation->set_rules($config);
    if ($this->form_validation->run() == FALSE){
        $content['layout'] = 'admin/question/add';
    }else{
        if($_FILES['question_image']['name']){
            $new_image_name = time() .'_'. str_replace(str_split(' ()\\/,:*?"<>|'), '',$_FILES['question_image']['name']);
            $config['upload_path'] = 'uploads/Question/';
            $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
            $config['file_name'] = $new_image_name;
            $config['max_size']  = '0';
            $config['max_width']  = '0';
            $config['max_height']  = '0';
            $config['$min_width'] = '0';
            $config['min_height'] = '0';
            #$this->load->library('upload', $config);
            $this->upload->initialize($config);
            if(!$this->upload->do_upload('question_image')) {
                $this->data['error'] = $this->upload->display_errors();
                $this->messages->add($this->data['error']);
                redirect('admin/question/add');
            }
        }
        $title = $this->input->post('title');
        $description = $this->input->post('description');
        $saveData = array(
            'question_title' => $title,
            'question_category' => $content['question_category'],
            'question_description' => stripslashes($description),
        );
        if($_FILES['question_image']['name']){
                $saveData['question_image'] = $new_image_name;
            }
        $last_id = $this->questions->save($this->questions->table_question,$saveData,array('question_id'=>$id));
        if($last_id){
            $this->db->where('question_id', $id);
            $this->db->delete('lss_question_options');
            foreach ($this->input->post('answer') as $key => $value) {
                if($key==$this->input->post('correct_answer')){
                    $is_correct = 1;
                }else{
                    $is_correct = 0;
                }
               $saveAnswerData = array(
                   'question_id' => $id,
                   'answer' => $value,
                   'weightage' => $this->input->post('weightage')[$key],
                   'is_correct'  => $is_correct
               );
            $this->questions->save($this->questions->table_question_options,$saveAnswerData);
            }

        }
        $this->messages->add('You have successfully updated selected Question.','success');
        redirect('admin/question');
    }
    $this->templates->set_data('index',$index);
    $this->templates->set_data('content',$content);
    $this->templates->load();
}

public function status(){
    $this->privileges->check_privileges();
    $sID = $this->input->post('sID');
    $jsonArray=array('flag'=>false);
    $where_array = array(
        'question_id' => $sID
    );
    $page_update_array = array(
        'question_is_active' => $this->input->post('sStatus')
    );
    if($this->questions->save($this->questions->table_question,$page_update_array, $where_array)){
        $jsonArray['flag'] = true;
    }
    echo json_encode($jsonArray); exit;
}
public function delete(){

    $sID = $this->input->post('sID');
    $jsonArray = array('flag' => false);
    $where_array = array(
       'question_id' => $sID
   );
    $page_update_array = array(
        'question_is_deleted' => '1'
    );

    if($this->questions->save($this->questions->table_question,$page_update_array, $where_array)){
        #echo $this->db->last_query();die;
        $jsonArray['flag'] = true;
    }
    echo json_encode($jsonArray); exit;
}
}
