<?php
defined('BASEPATH') OR exit('No direct access allowed');

class News extends MY_Controller {
 	var $userData   = array();
	public function __construct() {
        parent::__construct();
		$this->load->model('privileges');
		$adminlogin = $this->session->userdata('is_admin');
        $this->load->model('news_model','news');
        //$this->load->model('category_model','category');
		$this->load->model('users');
    }

	public function index(){

		$this->privileges->check_privileges();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$data = array();
		$layout = 'admin-layout';
		$view_file =  'admin/news/index';
		$index['page_title'] = ':: View News ::';
		$content['form_title'] = 'View News';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}


	public function get_news(){

        //$this->privilege->check_privileges();
		$order_by = array();
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        if(empty($length)){
            $length = 10;
            $start = 0;
        }
        $columnData = array(
            'sr_no',
            'news_title',
			'news_created_time',
			'news_is_active'
        );
        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
        $searchData = $this->input->post('searchBox');
        $where = '';
        $where .= 'news_is_deleted = "0"';
        $and=' and ';
        if($searchData){
            $where.= $and.'(news_title like "%'.$searchData.'%"   )';
        }
        $pageList_Array = $this->news->getData('',$where,$select =  '',$order_by, $start, $length,$where_in = false,$where_not_in = false);
        $pageList = $pageList_Array['data'];
        $totalData = $pageList_Array['total'];
        $jsonArray=array(
            'draw'=>$this->input->post('draw'),
            'recordsTotal'=>$totalData,
            'recordsFiltered'=>$totalData,
            'data'=>array(),
        );
        foreach($pageList as $key => $val){
           $active = $val['news_is_active']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['news_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['news_id'].'" title="Active"></i></a>';
            $edit = '<a href="'.site_url('admin/news/edit/'.$val['news_id']).'" rel="'.$val['news_id'].'"><i class="fa fa-edit" title="Edit"></i></a>';
            $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['news_id'].'" title="Delete"></i></a>';
            $jsonArray['data'][] = array(
                'sr_no' => $start + $key + 1,
                'news_title' => $val['news_title']?ucfirst($val['news_title']):'-----',
				'news_created_time'=>$val['news_created_time'],
                'action' => $active.'&nbsp;'.$edit.'&nbsp;'.$delete
            );
        }
        echo json_encode($jsonArray); exit;
        echo $this->input->post('draw'); exit;
	}

	public function add(){
		$this->privileges->check_privileges();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$data = array();

		$layout = 'admin-layout';
		$view_file =  'admin/news/add';
		$index['page_title'] = '::Add News ::';
		$content['form_title'] = 'Add News ';
		$this->templates->set($layout);
		$content['layout'] = $view_file;

        $path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);

        $content['news_title']               = $this->input->post('news_title');
		$content['news_description']         = $this->input->post('news_description');
        $content['hidden_image_name']         = $this->input->post('hidden_image_name');

		$config = array(
			array(
    			'field'   => 'news_title',
    			'label'   => 'News Title',
    			'rules'   => 'required'
			),
		  array(
                'field'   => 'news_description',
                'label'   => 'News Description',
                'rules'   => 'required'
            )/*,
			array(
    			'field'   => 'news_image_name',
    			'label'   => 'News Image Name',
    			'rules'   => 'callback_image_upload|xss_clean'
			)*/
		);

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE){
			$content['layout'] = $view_file;
		}else{



			$save_data = array(
                'news_title'       => $content['news_title'],
                'news_description' => $content['news_description'],
                'org_id' => 1,
                /*'news_image_name' => $image_name,*/
                'news_is_active' => '1',
                'news_created_time' => $this->news->currentDateTime
            );

            if($this->input->post('hidden_image_name')){
                 $save_data['news_image_name'] = $this->input->post('hidden_image_name');
            }

           /* if(!empty($this->upload_data)){
                if(!empty($this->upload_data['file_name'])){
                   $save_data['news_image_name'] = $this->upload_data['file_name'];
                }
            }*/

            $news_id = $this->news->save($this->news->table_pages,$save_data);
			$this->messages->add('You have successfully added a News in your portal.','success');
            redirect('admin/news/index');
		}
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}

    public function edit($news_id){
        $this->privileges->check_privileges();
        $data = array();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$layout = 'admin-layout';
		$view_file =  'admin/news/edit';
		$index['page_title'] = '::Edit News ::';
		$content['form_title'] = 'Edit News ';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
		$path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);


		$where = array('news_id' => $news_id);
        $pageData = $this->news->getData($where,$select =  '',$order_by= NULL, $offset=0, $limit=NULL,$where_in = false,$where_not_in = false);
        $pages = $pageData['data'];

        $content['news_id']                = $news_id;
        $content['news_title']             = $pages[0]['news_title'];
        $content['news_description']       =  $pages[0]['news_description'];
        $content['hidden_image_name']       =  $pages[0]['news_image_name'];
		if($_POST){
	        $content['news_title']               = $this->input->post('news_title');
            $content['news_description']         = $this->input->post('news_description');
            $content['hidden_image_name']         = $this->input->post('hidden_image_name');
		}
       $config = array(
            array(
                'field'   => 'news_title',
                'label'   => 'News Title',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'news_description',
                'label'   => 'News Description',
                'rules'   => 'required'
            ),

        );

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE){
			$content['layout'] = $view_file;
		}else{
			$where_update_array = array(
                'news_id' => $news_id
            );
	         $update_data = array(
                    'news_title'       => $content['news_title'],

                    'news_description' => $content['news_description'],
                    /*'news_image_name' => $image_name,*/
                    'news_is_active' => '1',
                    'news_updated_time' => $this->news->currentDateTime
                );
               if($this->input->post('hidden_image_name')){
                    $update_data['news_image_name'] = $this->input->post('hidden_image_name');
               }

            $membership_id = $this->news->save($this->news->table_pages,$update_data,$where_update_array);
			/*
            if($membership_id && $news_id == 16){
                $update_term_data = array(
                    'is_term_accepted' => '0'
                );
                $where_array = array('user_is_deleted' => '0');
                $this->users->save($this->users->table,$update_term_data,$where_array);
            }
			*/
			$this->messages->add('You have successfully added a News in your portal.','success');
            redirect('admin/news/index');

		}
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
    }
    public function status(){
        $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray=array('flag'=>false);
        $where_array = array(
            'news_id' => $sID
        );
        $news_update_array = array(
            'news_is_active' => $this->input->post('sStatus')
        );
        if($this->news->save($this->news->table_pages,$news_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }

    public function delete(){
        $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray = array('flag' => false);
        $where_array = array(
            'news_id' => $sID
        );
        $news_update_array = array(
            'news_is_deleted' => '1'
        );
        if($this->news->save($this->news->table_pages,$news_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }
    public function image_upload(){
        if(isset($_FILES['news_image_name']) && $_FILES['news_image_name']['name'] != '') {
            $this->load->library('upload');
            $new_name = time().'_'.$_FILES["news_image_name"]['name'];
            $config['upload_path'] = './uploads/news_images/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['max_size']    = 1024*2; //2MB
            $config['file_name'] = $new_name;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('news_image_name')){
                $this->form_validation->set_message('news_image_name',str_replace(array('<p>', '</p>'),'', $this->upload->display_errors()));
                return false;
            } else {
                $this->upload_data = $this->upload->data();
                return true;
            }
        }
        return true;
    }
}
