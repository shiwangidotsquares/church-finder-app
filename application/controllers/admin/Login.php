<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Login extends MY_Controller {

	function __construct(){
		parent::__construct();

		// if($this->session->userdata('admin_session_data')){

		// 	redirect(base_url('admin/dashboard'));
		// }

		$this->load->model('users','obj_users');
	}

    /**
     * this is a generic function to show html for login to admin area
     * function using template admin-login-layout
     * @access public
    */

   public function index(){

        $this->templates->set('admin-login-layout');
        $this->form_validation->set_rules('email', 'Username', 'required|trim|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'required|trim|xss_clean');
        if ($this->form_validation->run() == false)
        {
            $this->templates->load();
        }
        else{
            $result = false;
            $login_remember = '';
            $email   =   $this->input->post('email');
            $password   =   $this->input->post('password');
            
            $users_failed_login_count = 0;
            $result = $this->obj_users->checkUser($email,$password,1);
              

            if($result){
                $this->_authenticate($email,$password,$login_remember);
            }else{

                $this->messages->add("Provided credentials does not match! Please try again!", "error");
                redirect(base_url('admin'));
            }
        }
    }

    /**
     * this is a generic function to check for aunthentication to login
     * first parameter having information about user email
     * second parameter having information about password
     * third parameter having information if login information already remembered
     * @access public
     * @return boolean
    */

    public function _authenticate($users_uid = '',$users_password = '',$login_remember = ''){
        $res = false;
        if(isset($login_remember) && $login_remember != ''){
            setCookies($users_uid,$users_password);
        }
        if(isset($users_uid) && $users_uid != '' && $users_password != '' && isset($users_password)){
            
            $users_password = md5($users_password);
            $whereOR = '';
            $data = $this->obj_users->getRecords($this->obj_users->table, array('user_password' => $users_password),array('user_username' => $users_uid,'user_email' => $users_uid), '','','','',array(0 => 'role_id',1 => array(2,3,4)));
            
            if(!empty($data)){
                $this->session->set_userdata('admin_session_data',$data[0]);
                $this->session->set_userdata('last_login_time',strtotime(date('Y-m-d H:i:s',time())));
                $users_last_login = strtotime(date('Y-m-d H:i:s',time()));
                $login_count = $data[0]['user_login_count'];
                $login_count = $login_count + 1;
                $redirect_url = base_url('admin/dashboard');

                 $logindata = array(
                    'user_last_login' => $users_last_login,
                    'user_login_count' => $login_count
                 );
                 $this->obj_users->save($this->obj_users->table,$logindata,array('user_email' => $this->session->userdata['admin_session_data']['user_email'],'user_password' => $users_password));
                 

                 redirect($redirect_url);

                


            }else{

                $this->messages->add("You do not have permission to access.", "error");
                redirect(base_url('admin'));
            }

        }else{

            $this->messages->add("You do not have permission to access.", "error");
            redirect(base_url('admin'));
        }

    }
}

?>