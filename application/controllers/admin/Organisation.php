<?php
defined('BASEPATH') OR exit('No direct access allowed');

class Organisation extends MY_Controller {
    var $userData   = array();
    public function __construct() {
        parent::__construct();

        $adminlogin = $this->session->userdata('is_admin');
        $this->load->model('organisation_model','organisation');
        $this->load->model('category_model','category');
        $this->load->model('Site_model');
    }

    public function index(){
        #$this->privileges->check_privileges();
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
        //$this->privilege->check_privileges();
        $data = array();
        $layout = 'admin-layout';
        $view_file =  'admin/organisation/index';
        $index['page_title'] = ':: View Organisations ::';
        $content['form_title'] = 'View Organisations';
        $this->templates->set($layout);
        $content['layout'] = $view_file;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }

    public function view($org_id){
        #$this->privileges->check_privileges();
        $data = array();
        $index['page_title'] = ':: View organisation Details ::';
        $content['form_title'] = 'View organisation Details';
        $layout = 'admin-layout';
        $view_file =  'admin/organisation/view';
        $where = array('U.org_id' => $org_id);
        $data = $this->organisation->getUserAllData($where,$select = '*');

        $userData = $data['data'];

        $where = array('country_id' => $userData[0]['billing_country']);
        $dataC = $this->organisation->getRecords($this->organisation->table_country,$where);

        $content['country_name'] = $dataC[0]['name'];

        $content['userData'] = $userData;

        $this->templates->set($layout);
        $content['layout'] = $view_file;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }
    public function get_users(){
        //$this->privilege->check_privileges();
        $order_by = array();
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        if(empty($length)){
            $length = 10;
            $start = 0;
        }
        $columnData = array(
            'sr_no',
            'username',
            'user_email',
            'created_time',
            'is_active'
        );
        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
        $searchData = $this->input->post('searchBox');
        $where = '';
        $where .= 'user_is_deleted = "0" and role_id != "1"';
        $and=' and ';

        if($searchData){
            $searchData = trim($searchData);
            $where.= $and.'(username like "%'.$searchData.'%" OR user_email like "%'.$searchData.'%" OR username LIKE "'.$searchData.'")';
        }
        #prd($where);
        $pageList_Array = $this->organisation->getUserData($where,$select =  '',$order_by, $start, $length,$where_in = false,$where_not_in = false);

        $pageList = $pageList_Array['data'];

        $totalData = $pageList_Array['total'];
        $jsonArray=array(
            'draw'=>$this->input->post('draw'),
            'recordsTotal'=>$totalData,
            'recordsFiltered'=>$totalData,
            'data'=>array(),
        );

        foreach($pageList as $key => $val){

            $active = $val['user_is_active']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['org_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['org_id'].'" title="Active"></i></a>';
            $view = '<a href="'.site_url('admin/users/view/'.$val['org_id']).'"><i class="fa fa-info-circle viewRecord" rel="'.$val['org_id'].'" title="View Details"></i></a>';
            $login_as_user = '<a target="_blank" href="'.site_url('admin/organisation/login_as_user/'.$val['org_id']).'" rel="'.$val['org_id'].'"><i class="fa fa-user" title="Login as user"></i></a>';
            $edit = '<a href="'.site_url('admin/users/edit/'.$val['org_id']).'" rel="'.$val['org_id'].'"><i class="fa fa-edit" title="Edit"></i></a>';

            $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['org_id'].'" title="Delete"></i></a>';
            $full_name = ($val['username'])?ucfirst($val['username']):'---';
            $jsonArray['data'][] = array(
                'sr_no' => $start + $key + 1,
                'username' => $full_name,
                'user_email' => $val['user_email']?$val['user_email']:'---',
                'created_time'=>$val['created_time'],
                'user_is_active'=>($val['user_is_active'])?'Active':'Inactive',
                'action' => $active.'&nbsp;'.$edit.'&nbsp;'.$delete
            );
        }

        echo json_encode($jsonArray); exit;
        echo $this->input->post('draw'); exit;

    }

    public function add(){
        #$this->privileges->check_privileges();
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));

        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
        $data = array();
        $country_data = getContries();
        $content['country_data'] = $country_data['data'];
        $layout = 'admin-layout';
        $view_file =  'admin/organisation/add';
        $index['page_title'] = '::Add organisation ::';
        $content['form_title'] = 'Add organisation ';
        $this->templates->set($layout);
        $content['layout'] = $view_file;


        $where = array('category_is_deleted' => '0', 'category_is_active' => '1','category_parent_id'=>'4' );
        $datacategory = $this->category->getRecords('categories',$where);

        $content['categories']               = $datacategory;
        $path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);

        $content['organisation_name']              = $this->input->post('organisation_name');
        $content['user_email']              = $this->input->post('user_email');
        $content['username']                = $this->input->post('username');
        $content['phone_number']            = $this->input->post('phone_number');
        $content['address_line1']           = $this->input->post('address_line1');
        $content['address_line2']           = $this->input->post('address_line2');
        $content['user_city']               = $this->input->post('user_city');
        $content['region']                  = $this->input->post('region');
        $content['postcode']                = $this->input->post('postcode');
        $content['billing_country']        = $this->input->post('billing_country');
        $content['hidden_profile_image']   = $this->input->post('hidden_profile_image');
        $content['user_user_type']        = $this->input->post('user_type');


        $config = array(
            array(
                'field'   => 'organisation_name',
                'label'   => 'First Name',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'user_email',
                'label'   => 'Email',
                'rules'   => 'trim|required|callback_check_email|valid_email'
            ),
            array(
                'field'   => 'username',
                'label'   => 'Username',
                'rules'   => 'trim|required|callback_check_username'
            ),
            array(
                'field'   => 'user_password',
                'label'   => 'Password',
                'rules'   => 'trim|required|matches[confirm_password]|min_length[6]'
            ),
            array(
                'field'   => 'confirm_password',
                'label'   => 'Confirm Password',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'phone_number',
                'label'   => 'Phone Number',
                'rules'   => 'trim|required|numeric'
            ),
            /*array(
                'field'   => 'user_profile_image',
                'label'   => 'Event Image',
                'rules'   => 'callback_image_upload|xss_clean'
            )*/

        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE){
            $content['layout'] = $view_file;

        }else{

            $save_data = array(
                'organisation_name' => $content['organisation_name'],
                'user_email' => $content['user_email'],
                'user_user_type' => $content['user_user_type'],
                'user_username' => $content['username'],
                'user_password' => generateHash($this->input->post('user_password')),
                'created_time' => $this->organisation->currentDateTime
            );
            /*if(!empty($this->upload_data)){
                   if(!empty($this->upload_data['file_name'])){
                    $save_data['user_profile_image'] = $this->upload_data['file_name'];
                }
            } */
        if($this->input->post('hidden_profile_image')){
            $save_data['user_profile_image'] = $this->input->post('hidden_profile_image');
       }

        $last_id = $this->organisation->save($this->organisation->table_organisation,$save_data);

        $chars = time() .'_'. "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res = "";
        for ($i = 0; $i < 6; $i++) {
         $res .= $chars[mt_rand(0, strlen($chars)-1)];
     }
     $names = substr($content['organisation_name'], 0, 4);
     $res = $names.$res;
     if($last_id){
        $save_data = array(
            'org_id' => $last_id,
            'user_mobile_number' => $content['phone_number'],
            'address_line1'      => $content['address_line1'],
            'address_line2'      => $content['address_line2'],
            'city'               => $content['user_city'],
            'region'             => $content['region'],
            'postcode'           => $content['postcode'],
            'billing_country'    => $content['billing_country'],
            'user_referral_code' => $res
        );
            $last_id = $this->organisation->save($this->organisation->table_organisation_info,$save_data);

            $this->messages->add('You have successfully added a organisation in your portal.','success');
            redirect('admin/organisation/index');

            }else{

                $this->messages->add('error occurred.','error');
                redirect('admin/organisation/index');
            }

        }
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }

    public function edit($org_id){
        #$this->privileges->check_privileges();
        $data = array();

       //$this->privilege->check_privileges();
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));

        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
        $country_data = getContries();
        $content['country_data'] = $country_data['data'];
        $layout = 'admin-layout';
        $view_file =  'admin/organisation/edit';
        $index['page_title'] = '::Edit organisation ::';
        $content['form_title'] = 'Edit organisation ';
        $this->templates->set($layout);
        $content['layout'] = $view_file;
        $path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);
        $where = array('category_is_deleted' => '0', 'category_is_active' => '1','category_parent_id'=>'4' );
        $datacategory = $this->category->getRecords('categories',$where);

        $where = array('U.org_id' => $org_id);
        $data = $this->organisation->getUserAllData($where,$select = '*');

        #prd($data);
        $content['categories']               = $datacategory;


        $userData = $data['data'];
        $content['org_id']                = $org_id;
        $content['organisation_name']              = $userData[0]['organisation_name'];
        $content['user_email']              = $userData[0]['user_email'];
        $content['username']                = $userData[0]['user_username'];
        $content['phone_number']            = $userData[0]['user_mobile_number'];
        $content['address_line1']           = $userData[0]['address_line1'];
        $content['user_user_type']          = $userData[0]['user_user_type'];
        $content['address_line2']           = $userData[0]['address_line2'];
        $content['user_city']               = $userData[0]['city'];
        $content['region']                  = $userData[0]['region'];
        $content['postcode']                = $userData[0]['postcode'];
        $content['billing_state']           = $userData[0]['billing_state'];
        $content['billing_postcode']        = $userData[0]['billing_zipcode'];
        $content['billing_country']         = $userData[0]['billing_country'];
        $content['user_profile_image']      = $userData[0]['user_profile_image'];
        //$content['user_password']         = $userData[0]['user_password'];

        if($_POST){
            $content['org_id']                = $org_id;
            $content['organisation_name']              = $this->input->post('organisation_name');
            $content['user_email']              = $this->input->post('user_email');
            $content['username']                = $this->input->post('username');
            $content['phone_number']            = $this->input->post('phone_number');
            $content['address_line1']           = $this->input->post('address_line1');
            $content['user_user_type']           = $this->input->post('user_type');
            $content['address_line2']           = $this->input->post('address_line2');
            $content['user_city']               = $this->input->post('user_city');
            $content['region']                  = $this->input->post('region');
            $content['postcode']                = $this->input->post('postcode');
            $content['billing_state']           = $this->input->post('billing_state');
            $content['billing_postcode']        = $this->input->post('billing_postcode');
            $content['billing_country']        = $this->input->post('billing_country');
        }

        $config = array(
            array(
                'field'   => 'organisation_name',
                'label'   => 'First Name',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'username',
                'label'   => 'Username',
                'rules'   => 'trim|required|callback_check_username_edit'
            ),
            array(
                'field'   => 'user_email',
                'label'   => 'Email',
                'rules'   => 'trim|callback_check_email_edit|valid_email'
            ),
            array(
                'field'   => 'user_password',
                'label'   => 'Password',
                'rules'   => 'trim|matches[confirm_password]|min_length[6]'
            ),
            array(
                'field'   => 'confirm_password',
                'label'   => 'Confirm Password',
                'rules'   => 'trim'
            ),
            array(
                'field'   => 'phone_number',
                'label'   => 'Phone Number',
                'rules'   => 'trim|required|numeric'
            ),
            array(
                'field'   => 'user_profile_image',
                'label'   => 'Event Image',
                'rules'   => 'callback_image_upload|xss_clean'
            )
           

        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE){

            $content['layout'] = $view_file;

        }else{


            $where_update_array = array(
                'org_id' => $org_id
            );
            $update_data = array(
                'organisation_name' => $content['organisation_name'],
                'user_username' => $content['username'],
                'role_id' => INDIVIDUAL_USER_ROLE_ID,
                'user_email' => $content['user_email'],
                'user_user_type' => $content['user_user_type'],
            );

            if(!empty($this->upload_data)){
               if(!empty($this->upload_data['file_name'])){
                $update_data['user_profile_image'] = $this->upload_data['file_name'];
            }
        }

        if($this->input->post('user_password')){
            $update_data['user_password'] = generateHash($this->input->post('user_password'));
        }
        $last_id = $this->organisation->save($this->organisation->table_organisation,$update_data,$where_update_array);

        if($last_id){
            $save_data = array(
                'org_id' => $org_id,
                'user_mobile_number' => $content['phone_number'],
                'address_line1' => $content['address_line1'],
                'address_line2' => $content['address_line2'],
                'city' => $content['user_city'],
                'region' => $content['region'],
                'postcode' => $content['postcode'],
                'billing_state' => $content['billing_state'],
                'billing_zipcode' => $content['billing_postcode'],
                'billing_country' => $content['billing_country']
            );
            $this->organisation->save($this->organisation->table_organisation_info,$save_data,$where_update_array);

            $this->messages->add('You have successfully Updated a organisation in your portal.','success');
            redirect('admin/organisation/index');

        }else{

            $this->messages->add('error occurred.','error');
            redirect('admin/organisation/index');
        }

    }
    $this->templates->set_data('index',$index);
    $this->templates->set_data('content',$content);
    $this->templates->load();
}
public function upload_multi_media(){
    #prd($_FILES);
    if(!empty($_FILES['user_profile_image'])){
        $return = array();
       # prd($_FILES['user_profile_image']);
        $file= array();
        foreach ($_FILES['user_profile_image']['name'] as $key => $value) {
           $this->load->library('upload');
           $new_name = time().'_'.$value;
           $config['upload_path'] = './uploads/mediamanager/';
           $config['allowed_types'] = 'jpg|jpeg|gif|png';
           $config['file_name'] = $new_name;
           $target_file = $config['upload_path'].$new_name;

           if (move_uploaded_file($_FILES['user_profile_image']["tmp_name"][$key], $target_file)) {
                 $return[] = $new_name;
           }
        }
        echo implode(",",$return);
        
    }
}
public function upload_media(){


   if(isset($_FILES['file']) && $_FILES['file']['name'] != '') {

           $this->load->library('upload');
           $new_name = time().'_'.$_FILES["file"]['name'];
           $config['upload_path'] = './uploads/mediamanager/';
           $config['allowed_types'] = 'jpg|jpeg|gif|png';
               $config['file_name'] = $new_name;
               $this->upload->initialize($config);
               if (!$this->upload->do_upload('file')){
                   echo 'false';
               } else {
                   $this->upload_data = $this->upload->data();
                   if($this->upload_data){
                    echo $this->upload_data['file_name'];
                    }

               }
           }
}
public function status(){
    #$this->privileges->check_privileges();
    $sID = $this->input->post('sID');
    $jsonArray=array('flag'=>false);
    $where_array = array(
        'org_id' => $sID
    );
    $page_update_array = array(
        'user_is_active' => $this->input->post('sStatus')
    );
    if($this->organisation->save($this->organisation->table_organisation,$page_update_array, $where_array)){
        $jsonArray['flag'] = true;
    }
    echo json_encode($jsonArray); exit;
}

public function delete(){
    #$this->privileges->check_privileges();
    $sID = $this->input->post('sID');
    $jsonArray = array('flag' => false);
    $where_array = array(
     'org_id' => $sID
 );
    $page_update_array = array(
        'user_is_deleted' => '1'
    );
    if($this->organisation->save($this->organisation->table_organisation,$page_update_array, $where_array)){
            #echo $this->db->last_query();die;
        $jsonArray['flag'] = true;
    }
    echo json_encode($jsonArray); exit;
}

function check_username($username){
    $where_array = array(
        'user_username' => $username,
        'user_is_deleted' => '0'
    );

    $result = $this->organisation->checkRecord($this->organisation->table_organisation,$where_array);
    if ($result){
        $this->form_validation->set_message('check_username', 'The %s field value already exist, please try another.');
        return false;
    }else{
        return true;
    }
}

function check_email($user_email){
    $where_array = array(
        'user_email' => $user_email,
        'user_is_deleted' => '0'
    );

    $result = $this->organisation->checkRecord($this->organisation->table_organisation,$where_array);
    if ($result){
        $this->form_validation->set_message('check_email', 'The %s field value already exist, please try another.');
        return false;
    }else{
        return true;
    }
}



function check_username_edit($username){
    $org_id = $this->input->post('org_id');
    $where_array = array(
        'user_username' => $username,
        'user_is_deleted' => '0',
        'org_id !=' => $org_id
    );


    $result = $this->Site_model->checkRecord($this->organisation->table_organisation,$where_array);

    if (count($result['data'])){
        $this->form_validation->set_message('check_username_edit', 'The %s field value already exist, please try another.');
        return false;
    }else{
        return true;
    }
}

function check_email_edit($user_email){
 $org_id = $this->input->post('org_id');
 $where_array = array(
    'user_email' => $user_email,
    'user_is_deleted' => '0',
    'org_id !=' => $org_id
);

 $result = $this->organisation->checkRecord($this->organisation->table_organisation,$where_array);

 if (count($result['data'])){
    $this->form_validation->set_message('check_email_edit', 'The %s field value already exist, please try another');
    return false;
}else{
    return true;
}
}


public function check_unique_user_username($username=''){
    $user_username = $this->input->post('user_username');
    $page_type = $this->input->post('page_type');
    $org_id = $this->input->post('org_id');

    if($page_type == 'edit'){
        $where_array = array(
            'user_username' => $user_username,
            'org_id !=' => $org_id
        );
    }else{
     $where_array = array(
        'user_username' => $user_username,
    );
 }

 $result = $this->Site_model->getData($this->organisation->table_organisation,$where_array);

 if (count($result['data'])){
    $jsonArray = array('result' => 'failure','message' => 'Username already exit! Please choose another');
}else{
    $jsonArray = array('result' => 'success','message' => '');
}

echo json_encode($jsonArray);exit;
}



public function check_unique_user_email($useremail=''){
    $user_email = $this->input->post('user_email');
    $page_type = $this->input->post('page_type');
    $org_id = $this->input->post('org_id');

    if($page_type == 'edit'){
        $where_array = array(
            'user_email' => trim($user_email),
            'org_id !=' => $org_id
        );
    }else{
        $where_array = array(
            'user_email' => trim($user_email),
        );
    }

    $result = $this->Site_model->getData($this->organisation->table_organisation,$where_array);

    if (count($result['data'])){
        $jsonArray = array('result' => 'failure','message' => 'Email already exit! Please choose another');
    }else{
        $jsonArray = array('result' => 'success','message' => '');
    }

    echo json_encode($jsonArray);exit;
}

public function image_upload(){

    if(isset($_FILES['user_profile_image']) && $_FILES['user_profile_image']['name'] != '') {
        $this->load->library('upload');
        $new_name = time().'_'.$_FILES["user_profile_image"]['name'];
        $config['upload_path'] = './uploads/profile-pic/';
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['max_size']    = 1024*2; //2MB
            $config['file_name'] = $new_name;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('user_profile_image')){
                $this->form_validation->set_message('user_profile_image',str_replace(array('<p>', '</p>'),'', $this->upload->display_errors()));
                return false;
            } else {
                $this->upload_data = $this->upload->data();
                return true;
            }
        }
        return true;
    }
}
