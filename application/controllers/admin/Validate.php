<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Validate extends MY_Controller {

	var $data      = array();
	var $error     = array();
	var $templates  = array();
	var $content     = '';
	var $navigation  = '';
	var $breadcrumbs    = '';
    var $sidebar = '';
    var $upload_data = array();

    public function __construct() {
        parent::__construct();

    }

    /**
       * this is a generic function to give information if a user can access a particular section of admin or not
       * function having no parameter
       * function using view at location crm/validate/index to show information if user haven't permission for a specific section in admin
       * @access public
    */

    public function index(){
        $session_array = array();
        if($this->session->userdata('admin_session_data')){
            $session_array = $this->session->userdata('admin_session_data');
        }else{
            redirect(base_url('admin/logout'));
        }

        $layout = 'admin-layout';
        $view_file = 'admin/validate/index';
        $this->templates->set($layout);
        $content['layout'] = $view_file;
		$content['formTitle'] = 'You do not have permission for this section.';
        $this->templates->set_data('content',$content);
        $this->templates->set_data('header',$session_array);
        $this->templates->load();
    }

    public function profile_not_updated(){
        $this->templates->set('admin-layout');
        $content['layout'] = 'validate/profile-not-updated';
		$content['formTitle'] = 'Your profile is not updated! Please update your profile.';
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }
  }


/* End of file validate.php */
/* Location: ./application/controllers/validate.php */