<?php  defined('BASEPATH') OR exit('No direct access allowed');

class Property extends MY_Controller {
    public function __construct() {
        parent ::__construct();
        $this->load->library('upload');
        $adminlogin = $this->session->userdata('is_admin');
        $this->load->model('blogs');
        $this->load->model('category_model','category');
        $this->load->model('privileges','privilege');
    }

    public function index(){
        $session_array = $this->session->userdata('user_session_data');
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
        $data = array();
        $index = array();
        $content = array();
        $layout     = 'admin-layout';
        $view_file  = 'admin/blog/index';
        $index['page_title'] = 'Blog Manager';
        $index['form_title'] = 'View Blog';
        $content['user_info']   = $session_array;
        $this->templates->set($layout);
        $content['layout']      = $view_file;
        $userData                = $this->blogs->getRecords($this->blogs->table_blog);
        $content['userData']     = $userData;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }


public function get_all(){
    $length     = $this->input->post('length');
    $start      = $this->input->post('start');
    $columnData = array(
        'sr_no',
        /*'blog_image',*/
        'blog_title',
        'category_name',
        'blog_description',
        'blog_created_date',
        'action'

    );
    $sortData       = $this->input->post('order');
    $order_by[0]    = $columnData[$sortData[0]['column']];
    $order_by[1]    = $sortData[0]['dir'];
    $searchData     = $this->input->post('searchBox');
    $where  = '';
    $where .= 'blog_is_deleted = "0"';
    $and    = ' AND';
    if($searchData){
        $where.= $and.'(blog_title like "%'.$searchData.'%" OR blog_description like "%'.$searchData.'%")';
    }
    $list = $this->blogs->getBlogData($where, $select = 'B.*,C.category_name', $order_by, $start, $length);
    #echo $this->db->last_query(); die;
    $list_array    = $list['data'];
    $totalData          = $list['total'];
    $jsonArray          = array(
        'draw'              => $this->input->post('draw'),
        'recordsTotal'      => $totalData,
        'recordsFiltered'   => $totalData,
        'data'              => array(),
    );
    foreach($list_array as $key => $val){
        $active = $val['blog_is_active']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['blog_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['blog_id'].'" title="Active"></i></a>';
         $edit = '<a href="' . base_url('admin/blog/edit/' . $val['blog_id']) . '" rel="' . $val['blog_id'] . '"><span class="fa fa-edit"></span></a>';
         $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['blog_id'].'" title="Delete"></i></a>';
        $edit = '<a href="' . base_url('admin/blog/edit/' . $val['blog_id']) . '" rel="' . $val['blog_id'] . '"><span class="fa fa-edit"></span></a>';
        $originalDate = $val['blog_created_date'];
        $blog_created_date = date("d/m/Y", strtotime($originalDate));
        $blog_logo = '<img src='.base_url().'uploads/blog/'. $val["blog_image"] .' width="80">';
        $jsonArray['data'][] = array(
           'sr_no' => $start + $key + 1,
           /*'blog_image' => $blog_logo,*/
           'blog_title' => $val['blog_title'],
           'category_name' => $val['category_name'],
           'blog_description' => $val['blog_description'],
           'blog_created_date' => $blog_created_date,
           'action' =>  $active.'&nbsp;'.$edit.'&nbsp;'.$delete
       );
    }
    echo json_encode($jsonArray); exit;
}
public function Add(){
    $path = '../../../assets/js/ckfinder';
    $width = '1300px';
    parent::editor($path,$width);
    $index = array();
    $content = array();
    $email_array = array();
    $index['page_title'] = 'Property Manager';
    $content['form_title'] = 'Edit property';
    $content['button_text'] = 'Update property';
    $this->templates->set('admin-layout');

    

    $content['blog_id']         = '';
    $content['title']           = $this->input->post('title');
    $content['category_id']     = $this->input->post('category_id');
    $content['tags']            = $this->input->post('tags');
    $content['description']     = $this->input->post('description');
    $content['blog_image']     = $this->input->post('blog_image');
    $config = array(
       /* array(
            'field' => 'blog_image',
            'label' => 'Blog Image',
            'rules' => 'trim|callback_file_upload'
        ),*/
        array(
            'field' => 'title',
            'label' => 'Title',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required'
        )
    );
    $this->form_validation->set_rules($config);
    if ($this->form_validation->run() == FALSE){
        $content['layout'] = 'admin/property/add';
    }else{
        if(!empty($this->upload_data['blog_image'])){
            $image_name = $this->upload_data['blog_image']['file_name'];
        }else{
            $image_name = '';
        }
        $title = $this->input->post('title');
        $description = $this->input->post('description');
        $saveData = array(
            'blog_title' => $title,
            'org_id'       => 1,
            'category_id' => $content['category_id'],
            'tags' => $content['tags'],
            /*'blog_image' => $image_name,*/
            'blog_description' => stripslashes($description),
            'blog_created_date'  => $this->blogs->currentDateTime
        );
        if($this->input->post('hidden_profile_image')){
             $saveData['blog_image'] = $this->input->post('hidden_profile_image');
        }
        $this->blogs->save($this->blogs->table_blog,$saveData);
        $this->messages->add('You have successfully added property.','success');
        redirect('admin/property');
    }
    $this->templates->set_data('index',$index);
    $this->templates->set_data('content',$content);
    $this->templates->load();
}
public function edit($id = ''){
    if($id == ""){
       $this->messages->add('No direct access allowed.','error');
       redirect('admin/blog/index');
    }
    //check record exist or not
    $record = $this->blogs->checkRecord($this->blogs->table_blog, array('blog_id'=>$id));
    if(empty($record)){
       $this->messages->add('No blog found with given id.','error');
       redirect('admin/blog/index');
    }
    $index = array();
    $content = array();

    $this->privileges->check_privileges();
    $path = '../../../assets/js/ckfinder';
    $width = '1300px';
    parent::editor($path,$width);

    $cat_Array = $this->category->getCategoryData(array('category_parent_id'=>16));
    $content['blog_category_array'] = $cat_Array['data'];

    $where_array = array(
        'blog_id' => $id
    );
    #prd($where_array);
    $switch = $this->blogs->checkRecord($this->blogs->table_blog,$where_array);
    if((empty($id) && !is_numeric($id)) && ($switch == false)){
        $this->messages->add('Selected record does not exist in our database!','error');
        redirect(base_url('admin/blog/index'));
    }

    $email_array = array();
    $index['page_title'] = 'Blog Manager';
    $content['form_title'] = 'Edit blog';
    $content['button_text'] = 'Update blog';
    $this->templates->set('admin-layout');
    $where_array = array(
        'blog_id' => $id
    );
    $email_array = $this->blogs->getblogData($where_array,$select = '*');

    $email_array = $email_array['data'];
    $content['blog_id']           = $id;

    if(!empty($email_array)){
        $content['title']         = !empty($email_array[0]['blog_title'])?$email_array[0]['blog_title']:'';
        $content['category_id']   = !empty($email_array[0]['category_id'])?$email_array[0]['category_id']:'';
        $content['tags']          = !empty($email_array[0]['tags'])?$email_array[0]['tags']:'';
        $content['description']   = !empty($email_array[0]['blog_description'])?$email_array[0]['blog_description']:'';
        $content['blog_image']   = !empty($email_array[0]['blog_image'])?$email_array[0]['blog_image']:'';
    }

    if($_POST){
        $content['title']           = $this->input->post('title');
        $content['category_id']     = $this->input->post('category_id');
        $content['tags']            = $this->input->post('tags');
        $content['description']     = $this->input->post('description');
        $content['blog_image']     = $this->input->post('blog_image');
    }

    $config = array(
        array(
            'field' => 'title',
            'label' => 'Title',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required'
        )
    );
    $this->form_validation->set_rules($config);
    if ($this->form_validation->run() == FALSE){
        $content['layout'] = 'admin/blog/add';
    }else{
        if($_FILES['blog_image']['name']){
            $new_image_name = time() .'_'. str_replace(str_split(' ()\\/,:*?"<>|'), '',$_FILES['blog_image']['name']);
            $config['upload_path'] = 'uploads/blog/';
            $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
            $config['file_name'] = $new_image_name;
            $config['max_size']  = '0';
            $config['max_width']  = '0';
            $config['max_height']  = '0';
            $config['$min_width'] = '0';
            $config['min_height'] = '0';
            #$this->load->library('upload', $config);
            $this->upload->initialize($config);
            if(!$this->upload->do_upload('blog_image')) {
                $this->data['error'] = $this->upload->display_errors();
                $this->messages->add($this->data['error']);
                redirect('admin/blog/add');
            }
        }
        $title = $this->input->post('title');
        $description = $this->input->post('description');
        $saveData = array(
            'blog_title' => $title,
            'category_id' => $content['category_id'],
            'tags' => $content['tags'],
            'blog_description' => stripslashes($description),
        );

         if($this->input->post('hidden_profile_image')){
             $saveData['blog_image'] = $this->input->post('hidden_profile_image');
        }
      /*  if($_FILES['blog_image']['name']){
                $saveData['blog_image'] = $new_image_name;
            }*/
        $this->blogs->save($this->blogs->table_blog,$saveData,array('blog_id'=>$id));
        $this->messages->add('You have successfully updated selected blog.','success');
        redirect('admin/blog');
    }
    $this->templates->set_data('index',$index);
    $this->templates->set_data('content',$content);
    $this->templates->load();
}
    public function status(){
        $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray=array('flag'=>false);
        $where_array = array(
            'blog_id' => $sID
        );
        $page_update_array = array(
            'blog_is_active' => $this->input->post('sStatus')
        );
        if($this->blogs->save($this->blogs->table_blog,$page_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }

    public function delete(){
        $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray = array('flag' => false);
        $where_array = array(
           'blog_id' => $sID
       );
        $page_update_array = array(
            'blog_is_deleted' => '1'
        );
        if($this->blogs->save($this->blogs->table_blog,$page_update_array, $where_array)){
            #echo $this->db->last_query();die;
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }
    public function file_upload(){
        if(isset($_FILES['blog_image']['name']) && $_FILES['blog_image']['name'] != '') {
            $new_image_name = time() .'_'. str_replace(str_split(' ()\\/,:*?"<>|'), '',$_FILES['blog_image']['name']);
            $config['upload_path'] = 'uploads/blog/';
            $config['allowed_types'] = 'jpg|png|gif|bmp';
            $config['file_name'] = $new_image_name;
            $config['max_size']  = '0';
            $config['max_width']  = '0';
            $config['max_height']  = '0';
            $config['$min_width'] = '0';
            $config['min_height'] = '0';
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('blog_image')){
                $this->form_validation->set_message('file_upload',str_replace(array('<p>', '</p>'),'', $this->upload->display_errors()));
                return false;
            } else {
                $this->upload_data['blog_image'] = $this->upload->data();
                return true;
            }
        }
        if(!$this->input->post('blog_id')){
            $this->form_validation->set_message('file_upload',str_replace(array('<p>', '</p>'),'', "blog Image is required."));
        }
        return false;
    }
}
