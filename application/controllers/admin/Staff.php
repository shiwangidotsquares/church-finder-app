<?php
defined('BASEPATH') OR exit('No direct access allowed');

class Staff extends MY_Controller {
 	var $userData   = array();
    var $secret = '';
	public function __construct() {
        parent::__construct();

		$adminlogin = $this->session->userdata('is_admin');
        $this->load->model('staff_model','staff');
        $this->load->model('company_model','company');
        $this->load->model('Site_model');
        $this->load->model('users','obj_user');
        $this->load->library('googleauthenticator');
        $this->secret = $this->googleauthenticator->createSecret();
        $this->load->model('privileges','privilege');
    }

	public function index(){
	    $this->privileges->check_privileges();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$data = array();
		$layout = 'admin-layout';
        $where = array('U.user_is_active'=>'1','U.user_is_deleted'=>'0');
        $data_c = $this->company->getCompanyDataForCourse($where,$select = 'U.user_id,C.company_id,C.company_name');
        $userData = $data_c['data'];
        $content['dataComp'] = $userData;

		$view_file =  'admin/staff/index';
		$index['page_title'] = ':: View Staff ::';
		$content['form_title'] = 'View Staff';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}

	public function get_staff(){
        //$this->privilege->check_privileges();
		$order_by = array();
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        if(empty($length)){
            $length = 10;
            $start = 0;
        }
        $columnData = array(
            'sr_no',
            'staff_name',
         //   'company_name',
            'user_username',
			'user_email',
            'staff_created_time',
			'is_active'
        );
        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
        $searchData = $this->input->post('searchBox');
        $where = '';
        $where .= 'U.user_is_deleted = "0" AND U.role_id = "4"';
        $and=' and ';
       /* $company_user_id = $this->input->post('company_user_id');
        if($company_user_id){
           $where.= $and.'(S.company_id like "%'.$company_user_id.'%")';
        } */

        if($searchData){
            $where.= $and.'(U.first_name like "%'.$searchData.'%" OR U.last_name like "%'.$searchData.'%" OR S.staff_name like "%'.$searchData.'%" OR U.user_email like "%'.$searchData.'%" OR U.user_username like "%'.$searchData.'%")';
        }

        $List_Array = $this->staff->getStaffData($where,$select = 'S.staff_name,S.staff_created_time,S.staff_id,U.first_name,U.last_name,U.user_username,U.user_email,U.user_is_active,U.user_id,C.company_name',$order_by, $start, $length,$where_in = false,$where_not_in = false);

        $staffList = $List_Array['data'];

        $totalData = $List_Array['total'];
        $jsonArray=array(
            'draw'=>$this->input->post('draw'),
            'recordsTotal'=>$totalData,
            'recordsFiltered'=>$totalData,
            'data'=>array(),
        );
        foreach($staffList as $key => $val){

            $active = $val['user_is_active']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['user_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['user_id'].'" title="Active"></i></a>';
            $edit = '<a href="'.site_url('admin/staff/edit/'.$val['staff_id']).'" rel="'.$val['staff_id'].'"><i class="fa fa-edit" title="Edit"></i></a>';
            $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['user_id'].'" title="Delete"></i></a>';
			$login_as_user = '<a target="_blank" href="'.site_url('admin/staff/login_as_user/'.$val['user_id']).'" rel="'.$val['user_id'].'"><i class="fa fa-user" title="Login as user"></i></a>';
            $jsonArray['data'][] = array(
                'sr_no' => $start + $key + 1,
                'staff_name' => $val['staff_name']?ucfirst($val['staff_name']):'---',
                'company_name' => $val['company_name']?ucfirst($val['company_name']):'---',
                'user_username' => $val['user_username']?$val['user_username']:'---',
                'user_email' => $val['user_email']?$val['user_email']:'---',
				'staff_created_time'=>$val['staff_created_time'],
                'action' => $active.'&nbsp;'.$edit.'&nbsp;'.$delete
            );
        }

        echo json_encode($jsonArray); exit;
        echo $this->input->post('draw'); exit;

	}

	public function add(){

		$this->privileges->check_privileges();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));

		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$data = array();

		$layout = 'admin-layout';
		$view_file =  'admin/staff/add';
		$index['page_title'] = '::Add Staff ::';
		$content['form_title'] = 'Add Staff ';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
  
        $content['staff_fname']                = $this->input->post('staff_fname');
        $content['staff_lname']                = $this->input->post('staff_lname');
        $content['company_user_id']                = $this->input->post('company_user_id');
        $content['user_username']                = $this->input->post('user_username');
        $content['user_email']               = $this->input->post('user_email');



		$config = array(
			array(
                'field'   => 'staff_fname',
                'label'   => 'Staff First Name',
                'rules'   => 'trim|required'
            ),array(
                'field'   => 'staff_lname',
                'label'   => 'Staff Last Name',
                'rules'   => 'trim|required'
            ),
          /*  array(
                'field'   => 'company_user_id',
                'label'   => 'Choose Administrator',
                'rules'   => 'trim|required'
            ), */
            array(
    			'field'   => 'user_username',
    			'label'   => 'Staff Username',
    			'rules'   => 'trim|required|callback_check_username'
			),
			array(
    			'field'   => 'user_email',
    			'label'   => 'Staff Email',
    			'rules'   => 'trim|required|callback_check_email|valid_email'
			),
		    array(
                'field'   => 'user_password',
                'label'   => 'Password',
                'rules'   => 'trim|required|matches[confirm_password]|min_length[6]'
            ),
             array(
                'field'   => 'confirm_password',
                'label'   => 'Confirm Password',
                'rules'   => 'trim|required'
            ), array(
                'field'   => 'user_profile_image',
                'label'   => 'Staff Image',
                'rules'   => 'callback_image_upload|xss_clean'
            )
		);

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE){

            //print_r($_POST); die;
			$content['layout'] = $view_file;



		}else{
           
			$save_data = array(
                'first_name' => $content['staff_fname'],
                'last_name' => $content['staff_lname'],
				'user_username' => $content['user_username'],
                'role_id' => 4,
                'user_email' => $content['user_email'],
                'user_password' => generateHash($this->input->post('user_password')),
                'google_auth_code' => $this->secret,
                'created_time' => $this->staff->currentDateTime
            );
            //print_r($save_data); die;

             $last_id = $this->staff->save($this->staff->table_users,$save_data);

           if($last_id){
                $save_data = array(
                    'user_id' => $last_id
                );
                    
                    if(!empty($this->upload_data)){
                        if(!empty($this->upload_data['file_name'])){
                           $save_data['user_profile_image'] = $this->upload_data['file_name'];
                        }
                    }
                $last_id = $this->staff->save('user_info',$save_data);

                if($last_id){
                    $mail_to      = $content['user_email'];
                    $subject      = 'Welcome !! You have added as a individual to GHC eLearning';
                    $mail_extra   = '';
                    $mail_cc      = '';
                    $mail_bcc     = '';
                    if (!empty($mail_extra)) {
                        $mail_bcc = $mail_extra;
                    }

                    $sender_email = ADMIN_EMAIL;
                    $sender_name  = ADMIN_NAME;

                    $body = array('first_name' => $content['first_name'], 'user_email'=> $content['user_email'], 'username'=> $content['username'], 'password'=> $this->input->post('user_password'), 'login_url' => base_url().'account/login');
                    /* Send Inquiry in mail */
                    $message_data = get_email_body($template_id=4,$body);
                    $messagebody=$message_data['body'];

                    $this->sendmail($mail_to, $subject, $messagebody, $mail_cc, $mail_bcc, $sender_email, $sender_name, false);
                }

              
    			$this->messages->add('You have successfully added a Staff in your portal.','success');
                redirect('admin/staff/index');
            }else{
                $this->messages->add('error occurred.','error');
                redirect('admin/staff/index');
            } 
            $this->messages->add('You have successfully added a Staff in your portal.','success');
              redirect('admin/staff/index');
		}
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}

    public function edit($staff_id){
        $this->privileges->check_privileges();
        $data = array();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));

		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));

		$layout = 'admin-layout';
		$view_file =  'admin/staff/edit';
		$index['page_title'] = '::Edit Company::';
		$content['form_title'] = 'Edit Staff ';
		$this->templates->set($layout);
		$content['layout'] = $view_file;

		$where = array('staff_id' => $staff_id);

        #$data = $this->Site_model->getData($this->Site_model->table_users,$where);
        $data = $this->staff->getStaffData($where,$select = 'S.staff_name,S.company_id as company_user_id,U.user_id,U.first_name,U.last_name,U.user_username,U.user_email,C.company_id');

        $userData = $data['data'];

        #prd($userData);
		$swap_array_edit = array(0 => 1, 1 => 2);
        $content['staff_id']              = $staff_id;
        $content['user_id']               = $userData[0]['user_id'];
        $content['company_user_id']       = $userData[0]['company_user_id'];
        $content['staff_fname']           = $userData[0]['first_name'];
        $content['staff_lname']           = $userData[0]['last_name'];
        $content['user_username']         = $userData[0]['user_username'];
        $content['user_email']            = $userData[0]['user_email'];



		if($_POST){
            $content['company_user_id']         = $this->input->post('company_user_id');
            $content['staff_fname']             = $this->input->post('staff_fname');
            $content['staff_lname']             = $this->input->post('staff_lname');
            $content['user_username']           = $this->input->post('user_username');
			$content['user_email']              = $this->input->post('user_email');

		}

        /*$where = array('user_is_deleted' => '0', 'user_is_active' => '1', 'role_id' => 2);
        $dataC = $this->company->getRecords($this->obj_user->table,$where);
		#prd($dataC);
		$content['dataC']               = $dataC;*/

        $where = array('U.user_is_active'=>'1','U.user_is_deleted'=>'0');
        $data_c = $this->company->getCompanyDataForCourse($where,$select = 'U.user_id,C.company_id,C.company_name,U.user_email');
        $userData = $data_c['data'];
        $content['dataC'] = $userData;

		$config = array(
                array(
                    'field'   => 'company_user_id',
                    'label'   => 'Choose Administrator',
                    'rules'   => 'trim|required'
                ),
                array(
                    'field'   => 'staff_fname',
                    'label'   => 'Staff First Name',
                    'rules'   => 'trim|required'
                ),
                array(
                    'field'   => 'staff_lname',
                    'label'   => 'Staff Last Name',
                    'rules'   => 'trim|required'
                ),
                array(
                    'field'   => 'user_username',
                    'label'   => 'Staff Username',
                    'rules'   => 'trim|required|callback_check_username_edit'
                ),
                array(
                    'field'   => 'user_email',
                    'label'   => 'Staff Email',
                    'rules'   => 'trim|required|callback_check_email_edit|valid_email'
                ),
                array(
                    'field'   => 'user_password',
                    'label'   => 'Password',
                    'rules'   => 'trim|matches[confirm_password]|min_length[6]'
                ),
                 array(
                    'field'   => 'confirm_password',
                    'label'   => 'Confirm Password',
                    'rules'   => 'trim'
                )
            );


		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE){

			$content['layout'] = $view_file;

		}else{

			$where_update_array = array(
                'user_id' => $content['user_id']
            );
            $save_data = array(
                'first_name' => $content['staff_fname'],
                'last_name' => $content['staff_lname'],
                'user_username' => $content['user_username'],
                'user_email' => $content['user_email'],
            );

            if($this->input->post('user_password')){
                $save_data['user_password'] = generateHash($this->input->post('user_password'));
            }
            $last_id = $this->staff->save($this->staff->table_users,$save_data,$where_update_array);
			$swap_array = array(1 => 0, 2 => 1);

            $where_supdate_array = array(
                'staff_id' => $staff_id
            );
            if($last_id){
               $save_data = array(
                  'company_id' => $content['company_user_id'],
                  'staff_name' => $content['staff_fname'].' '.$content['staff_lname']

              );
              $last_id = $this->staff->save($this->staff->table_staff,$save_data,$where_supdate_array);

			  $this->messages->add('You have successfully updated staff.','success');
                  redirect('admin/staff/index');
              }else{
                  $this->messages->add('error occurred.','error');
                  redirect('admin/staff/index');
              }

		}

		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
    }
    public function status(){
        $this->privileges->check_privileges();

        $sID = $this->input->post('sID');
        $jsonArray=array('flag'=>false);
        $where_array = array(
            'user_id' => $sID
        );
        $page_update_array = array(
            'user_is_active' => $this->input->post('sStatus')
        );
        if($this->staff->save($this->staff->table_users,$page_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }

    public function delete(){
        $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray = array('flag' => false);
        $where_array = array(
           'user_id' => $sID
       );
        $page_update_array = array(
            'user_is_deleted' => '1'
        );
        if($this->staff->save($this->staff->table_users,$page_update_array, $where_array)){
            #echo $this->db->last_query();die;
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }

	function check_username($username){
        $where_array = array(
            'user_username' => $username,
            'user_is_deleted' => '0'
        );

         $result = $this->Site_model->getData($this->staff->table_users,$where_array);
       	if (count($result['data'])){
 			$this->form_validation->set_message('check_username', ' The %s field value already exist, please try another.');
 			return false;
  		}else{
 			return true;
  		}
    }

    function check_email($user_email){
        $where_array = array(
            'user_email' => $user_email,
            'user_is_deleted' => '0'
        );

       $result = $this->Site_model->getData($this->Site_model->table_users,$where_array);
        if (count($result['data'])){
            $this->form_validation->set_message('check_email', ' The %s fieldtt value already exist, please try another.');
            return false;
        }else{
            return true;
        }
    }

	function check_username_edit($username){
        $user_id = $this->input->post('user_id');
        $where_array = array(
            'user_username' => $username,
            'user_is_deleted' => '0',
            'user_id !=' => $user_id
        );

        $result = $this->Site_model->getData($this->Site_model->table_users,$where_array);
       if (count($result['data'])){
 			$this->form_validation->set_message('check_username_edit', ' The %s field value already exist, please try another.');
 			return false;
  		}else{
 			return true;
  		}
    }

    public function image_upload(){
      
        if(isset($_FILES['user_profile_image']) && $_FILES['user_profile_image']['name'] != '') {
            $this->load->library('upload');
            $new_name = time().'_'.$_FILES["user_profile_image"]['name'];
            $config['upload_path'] = './uploads/event_images/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['max_size']    = 1024*2; //2MB
            $config['file_name'] = $new_name;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('user_profile_image')){
                $this->form_validation->set_message('event_images',str_replace(array('<p>', '</p>'),'', $this->upload->display_errors()));
                return false;
            } else {
                $this->upload_data = $this->upload->data();
                return true;
            }
        }
        return true;
    }

    function check_email_edit($user_email){
	    $user_id = $this->input->post('user_id');
        $where_array = array(
            'user_email' => $user_email,
            'user_is_deleted' => '0',
            'user_id !=' => $user_id
        );

       $result = $this->Site_model->getData($this->Site_model->table_users,$where_array);
       if (count($result['data'])){
 			$this->form_validation->set_message('check_email_edit', ' The %s field value already exist, please try another');
 			return false;
  		}else{
 			return true;
  		}
    }


	  public function check_unique_user_username($username=''){
        $user_username = $this->input->post('user_username');
        $page_type = $this->input->post('page_type');
        $staff_id = $this->input->post('staff_id');

         if($page_type == 'edit'){
            $where_array = array(
                'user_username' => $user_username,
                'staff_id !=' => $staff_id
            );
         }else{
           $where_array = array(
                'user_username' => $user_username,
            );
         }

	   $result = $this->Site_model->getData($this->table_users,$where_array);

       	if (count($result['data'])){
 			$jsonArray = array('result' => 'failure','message' => 'Username already exit! Please choose another');
  		}else{
 			$jsonArray = array('result' => 'success','message' => '');
  		}

		echo json_encode($jsonArray);exit;
    }



	public function check_unique_user_email($useremail=''){
        $user_email = $this->input->post('user_email');
        $page_type = $this->input->post('page_type');
        $staff_id = $this->input->post('staff_id');

         if($page_type == 'edit'){
            $where_array = array(
                'user_email' => trim($user_email),
                'staff_id !=' => $staff_id
            );
         }else{
            $where_array = array(
                'user_email' => trim($user_email),
            );
         }

	   $result = $this->Site_model->getData($this->table_users,$where_array);

       	if (count($result['data'])){
 			$jsonArray = array('result' => 'failure','message' => 'Email already exit! Please choose another');
  		}else{
 			$jsonArray = array('result' => 'success','message' => '');
  		}

		echo json_encode($jsonArray);exit;
    }



}
