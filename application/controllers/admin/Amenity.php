<?php  defined('BASEPATH') OR exit('No direct access allowed');

class Amenity extends MY_Controller {
    public function __construct() {
        parent ::__construct();
        $this->load->library('upload');
        $adminlogin = $this->session->userdata('is_admin');
        //$this->load->model('feature','feature');
       $this->load->model('amenities');
    }

    public function index(){
        $session_array = $this->session->userdata('user_session_data');
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
        $data = array();
        $index = array();
        $content = array();
        $layout     = 'admin-layout';
        $view_file  = 'admin/amenity/index';
        $index['page_title'] = 'Amenity Manager';
        $index['form_title'] = 'View Amenity';
        $content['user_info']   = $session_array;
        $this->templates->set($layout);
        $content['layout']      = $view_file;
        $userData                = $this->amenities->getRecords($this->amenities->table_amenity);
        $content['userData']     = $userData;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }


public function get_all(){
            $order_by = array();
            $length = $this->input->post('length');
            $start = $this->input->post('start');
            if(empty($length)){
                $length = 10;
                $start = 0;
            }
            $columnData = array(
                'sr_no',
                'feature_name',
            );
            $sortData = $this->input->post('order');
            $order_by[0] = $columnData[$sortData[0]['column']];
            $order_by[1] = $sortData[0]['dir'];
            $searchData = $this->input->post('searchBox');
            $where = '';
            $and=' and ';
            if($searchData){
                $where.= $and.'(feature_name like "%'.$searchData.'%"   )';
            }
            $pageList_Array = $this->amenities->getData($where,$select =  '',$order_by, $start, $length,$where_in = false,$where_not_in = false);
            $pageList = $pageList_Array['data'];
            $totalData = $pageList_Array['total'];
            $jsonArray=array(
                'draw'=>$this->input->post('draw'),
                'recordsTotal'=>$totalData,
                'recordsFiltered'=>$totalData,
                'data'=>array(),
            );
            foreach($pageList as $key => $val){
                $edit = '<a href="'.site_url('admin/amenity/edit/'.$val['feature_id']).'" rel="'.$val['feature_id'].'"><i class="fa fa-edit" title="Edit"></i></a>';
                $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['feature_id'].'" title="Delete"></i></a>';
                $jsonArray['data'][] = array(
                    'sr_no' => $start + $key + 1,
                    'feature_name'=>$val['feature_name'],
                    'amenity_created_time' => $val['amenity_created_time'],
                    'action' =>$edit.'&nbsp;'.$delete
                );
            }
            echo json_encode($jsonArray); exit;
            echo $this->input->post('draw'); exit;
}
public function add(){
   
    $path = '../../../assets/js/ckfinder';
    $width = '1300px';
    parent::editor($path,$width);
    $index = array();
    $content = array();
    $email_array = array();
    $index['page_title'] = 'Amenity Manager';
    $content['form_title'] = 'Edit Amenity';
    $content['button_text'] = 'Update Amenity';
    $content['layout'] = 'admin/amenity/add';
    $this->templates->set('admin-layout');
    $config = array(
        array(
            'field' => 'feature_name',
            'label' => 'Title',
            'rules' => 'trim|required'
        )
    );
    $this->form_validation->set_rules($config);
    if ($this->form_validation->run() == FALSE){
        $content['layout'] = 'admin/amenity/add';
    }else{
        $feature_name = $this->input->post('feature_name');
        $saveData = array(
            'feature_name' => $feature_name
        );

        $this->amenities->save($this->amenities->table_amenity,$saveData);
        $this->messages->add('You have successfully added amenity.','success');
        redirect('admin/amenity');
    }
    $this->templates->set_data('index',$index);
    $this->templates->set_data('content',$content);
    $this->templates->load();
}
 public function edit($feature_id){
        $data = array();
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
        $layout = 'admin-layout';
        $view_file =  'admin/amenity/edit';
        $index['page_title'] = '::Edit Amenity ::';
        $content['form_title'] = 'Edit Amenity ';
        $this->templates->set($layout);
        $content['layout'] = $view_file;
        $path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);


        $where = array('feature_id' => $feature_id);
        $pageData = $this->amenities->getData($where,$select =  '',$order_by= NULL, $offset=0, $limit=NULL,$where_in = false,$where_not_in = false);
        $pages = $pageData['data'];

        $content['feature_id']                = $feature_id;
        $content['feature_name']             = $pages[0]['feature_name'];
        if($_POST){
            $content['feature_name']               = $this->input->post('feature_name');
        }
       $config = array(
            array(
                'field'   => 'feature_name',
                'label'   => 'Feature Name',
                'rules'   => 'required'
            ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE){
            $content['layout'] = $view_file;
        }else{
            $where_update_array = array(
                'feature_id' => $feature_id
            );
             $update_data = array(
                    'feature_name'       => $content['feature_name']
                );
            
            $membership_id = $this->amenities->save($this->amenities->table_amenity,$update_data,$where_update_array);
            /*
            if($membership_id && $news_id == 16){
                $update_term_data = array(
                    'is_term_accepted' => '0'
                );
                $where_array = array('user_is_deleted' => '0');
                $this->users->save($this->users->table,$update_term_data,$where_array);
            }
            */
            $this->messages->add('You have successfully added a amenity in your portal.','success');
            redirect('admin/amenity/index');

        }
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }
    public function status(){
        $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray=array('flag'=>false);
        $where_array = array(
            'blog_id' => $sID
        );
        $page_update_array = array(
            'blog_is_active' => $this->input->post('sStatus')
        );
        if($this->blogs->save($this->blogs->table_blog,$page_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }

    public function delete(){
        $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray = array('flag' => false);
        $where_array = array(
           'blog_id' => $sID
       );
        $page_update_array = array(
            'blog_is_deleted' => '1'
        );
        if($this->blogs->save($this->blogs->table_blog,$page_update_array, $where_array)){
            #echo $this->db->last_query();die;
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }
    public function file_upload(){
        if(isset($_FILES['blog_image']['name']) && $_FILES['blog_image']['name'] != '') {
            $new_image_name = time() .'_'. str_replace(str_split(' ()\\/,:*?"<>|'), '',$_FILES['blog_image']['name']);
            $config['upload_path'] = 'uploads/blog/';
            $config['allowed_types'] = 'jpg|png|gif|bmp';
            $config['file_name'] = $new_image_name;
            $config['max_size']  = '0';
            $config['max_width']  = '0';
            $config['max_height']  = '0';
            $config['$min_width'] = '0';
            $config['min_height'] = '0';
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('blog_image')){
                $this->form_validation->set_message('file_upload',str_replace(array('<p>', '</p>'),'', $this->upload->display_errors()));
                return false;
            } else {
                $this->upload_data['blog_image'] = $this->upload->data();
                return true;
            }
        }
        if(!$this->input->post('blog_id')){
            $this->form_validation->set_message('file_upload',str_replace(array('<p>', '</p>'),'', "blog Image is required."));
        }
        return false;
    }
}
