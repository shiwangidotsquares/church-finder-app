<?php
defined('BASEPATH') OR exit('No direct access allowed');

class Job extends MY_Controller {
 	var $userData   = array();
	public function __construct() {
        parent::__construct();
		$this->load->model('privileges','privilege');
		$adminlogin = $this->session->userdata('is_admin');
        $this->load->model('job_model','job');
        $this->load->model('customers');
		$this->load->model('users');
        $this->load->model('employees');
          $this->load->model('roles');
    }

	public function index(){
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$data = array();
		$layout = 'admin-layout';
		$view_file =  'admin/job/index';
		$index['page_title'] = ':: View Job ::';
		$content['form_title'] = 'View Job';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}




	public function add(){
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$data = array();

		$layout = 'admin-layout';
		$view_file =  'admin/job/add';
		$index['page_title'] = '::Add Job ::';
		$content['form_title'] = 'Add Job ';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
        $customer_Array = $this->customers->getRecords("customer");
        $content['customer_array'] = $customer_Array;

        $employee_Array = $this->employees->getRecords("employee");
        $content['employee_array'] = $employee_Array;

        $role_Array = $this->roles->getRecords("roles");
        $content['role_array'] = $role_Array;



        $path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);
        $content['job_title']               = $this->input->post('job_title');
		$content['job_description']            = $this->input->post('job_description');
		$content['client_id']         = $this->input->post('client_id');
        $content['employee_id']         = $this->input->post('employee_id');
        $content['role']         = $this->input->post('role');
        $content['start_date']         = $this->input->post('start_date');
        $content['end_date']         = $this->input->post('end_date');

		$config = array(
			array(
    			'field'   => 'job_title',
    			'label'   => 'Job Title',
    			'rules'   => 'required'
			),
			array(
    			'field'   => 'job_description',
    			'label'   => 'Job Description',
    			'rules'   => 'required'
			),
            array(
                'field'   => 'start_date',
                'label'   => 'Start Date',
                'rules'   => 'required'
            ),array(
                'field'   => 'end_date',
                'label'   => 'End Date',
                'rules'   => 'required'
            )

		);

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE){
			$content['layout'] = $view_file;
		}else{

			$save_data = array(
                'job_title'       => $content['job_title'],
                'job_description'        => $content['job_description'],
                'client_id' => $content['client_id'],
                 'employee_id' => $content['employee_id'],
                 'role' => $content['role'],
                 'start_date' => $content['start_date'],
                 'end_date' => $content['end_date']
            );
           
            $product_id = $this->job->save($this->job->table_job,$save_data);
			$this->messages->add('You have successfully added a Job in your portal.','success');
            redirect('admin/job/index');
		}
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}


    public function edit($id = ''){
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));

           if($id == ""){
              $this->messages->add('No direct access allowed.','error');
              redirect('admin/job/index');
           }
           //check record exist or not
           $record = $this->job->checkRecord($this->job->table_job, array('job_id'=>$id));
           if(empty($record)){
              $this->messages->add('No job found with given id.','error');
              redirect('admin/job/index');
           }
           $index = array();
           $content = array();

           $where_array = array(
               'job_id' => $id
           );
           #prd($where_array);
           $switch = $this->job->checkRecord($this->job->table_job,$where_array);
           if((empty($id) && !is_numeric($id)) && ($switch == false)){
               $this->messages->add('Selected record does not exist in our database!','error');
               redirect(base_url('admin/job/index'));
           }

           $email_array = array();
           $index['page_title'] = 'Employee Job';
           $content['form_title'] = 'Edit Job';
           $content['button_text'] = 'Update employee';
           $this->templates->set('admin-layout');
           $where_array = array(
               'job_id' => $id
           );

           $list = $this->job->getRecords('job',$where_array);
          $email_array = $list;
           $content['job_id']     = $id;
           $path = '../../../assets/js/ckfinder';
           $width = '1300px';
           parent::editor($path,$width);
            
           if(!empty($email_array)){
               $content['job_title']         = !empty($email_array[0]['job_title'])?$email_array[0]['job_title']:'';
               $content['job_description']   = !empty($email_array[0]['job_description'])?$email_array[0]['job_description']:'';
               $content['client_id']          = !empty($email_array[0]['client_id'])?$email_array[0]['client_id']:'';
               $content['employee_id']   = !empty($email_array[0]['employee_id'])?$email_array[0]['employee_id']:'';
               $content['role']   = !empty($email_array[0]['role'])?$email_array[0]['role']:'';
               $content['start_date']   = !empty($email_array[0]['start_date'])?$email_array[0]['start_date']:'0';
               $content['end_date']   = !empty($email_array[0]['end_date'])?$email_array[0]['end_date']:'';
           }

           if($_POST){
               $content['job_title']     = $this->input->post('job_title');
               $content['job_description']   = $this->input->post('job_description');
               $content['client_id']   = $this->input->post('client_id');
               $content['employee_id']   = $this->input->post('employee_id');
               $content['role']   = $this->input->post('role');
               $content['start_date']   = $this->input->post('start_date');
               $content['end_date']     = $this->input->post('end_date');
           }

           $config = array(
               array(
                   'field' => 'job_title',
                   'label' => 'Job Title',
                   'rules' => 'trim|required'
               )
           );
           $this->form_validation->set_rules($config);
           if ($this->form_validation->run() == FALSE){
               $content['layout'] = 'admin/job/add';
           }else{
               $saveData['job_title'] =$content['job_title'];
               $saveData['job_description'] =$content['job_description'];
               $saveData['client_id'] =$content['client_id'];
               $saveData['employee_id'] =$content['employee_id'];
               $saveData['role'] =$content['role'];
               $saveData['start_date'] =$content['start_date'];
               $saveData['end_date'] =$content['end_date'];
               $this->job->save('job',$saveData,array('job_id'=>$id));
               $this->messages->add('You have successfully updated selected job.','success');
               redirect('admin/job');
           }

           $customer_Array = $this->customers->getRecords("customer");
           $content['customer_array'] = $customer_Array;

           $employee_Array = $this->employees->getRecords("employee");
           $content['employee_array'] = $employee_Array;

           $role_Array = $this->roles->getRecords("roles");
           $content['role_array'] = $role_Array;
           $this->templates->set_data('index',$index);
           $this->templates->set_data('content',$content);
           $this->templates->load();
       }
    public function status(){
        $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray=array('flag'=>false);
        $where_array = array(
            'product_id' => $sID
        );
        $product_update_array = array(
            'product_is_active' => $this->input->post('sStatus')
        );
        if($this->product->save($this->product->table_product,$product_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }

    public function delete(){
        $sID = $this->input->post('sID');
        $jsonArray = array('flag' => false);
        $where_array = array(
            'job_id' => $sID
        );
        $product_update_array = array(
            'is_deleted' => '1'
        );
        if($this->job->save($this->job->table_job,$product_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }
    public function image_upload(){
        if(isset($_FILES['product_image']) && $_FILES['product_image']['name'] != '') {
            $this->load->library('upload');
            $new_name = time().'_'.$_FILES["product_image"]['name'];
            $config['upload_path'] = './uploads/product_images/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['max_size']    = 1024*2; //2MB
            $config['file_name'] = $new_name;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('product_image')){
                $this->form_validation->set_message('product_images',str_replace(array('<p>', '</p>'),'', $this->upload->display_errors()));
                return false;
            } else {
                $this->upload_data = $this->upload->data();
                return true;
            }
        }
        return true;
    }

    public function get_all(){
        $length     = $this->input->post('length');
        $start      = $this->input->post('start');
        $columnData = array(
            'sr_no',
            'job_title',
            'job_description',
            'client_name',
            'first_name',
            'role_title',
            'start_date',
            'end_date',
            'action'

        );
        $sortData       = $this->input->post('order');
        $order_by[0]    = $columnData[$sortData[0]['column']];
        $order_by[1]    = $sortData[0]['dir'];
        $searchData     = $this->input->post('searchBox');
        $where  = '';
        $where .= 'J.is_deleted = "0"';
        $and    = ' AND';
        if($searchData){
           $where.= $and.'(J.job_title like "%'.$searchData.'%")';
        }

   /*     $list = $this->job->getRecords($this->job->table_job, $where,'',$order_by, $length, $start);*/
         $list = $this->job->getJobData('',$where, $select = 'J.*', $order_by, $start, $length); 
         $total = $this->job->getRecords($this->job->table_job);
       $list_array    = $list['data'];
        $totalData          = $list;
        $jsonArray          = array(
            'draw'              => $this->input->post('draw'),
            'recordsTotal'      => count($total),
            'recordsFiltered'   => count($total),
            'data'              => array(),
        );
        foreach($list_array as $key => $val){
             $edit = '<a href="' . base_url('admin/job/edit/' . $val['job_id']) . '" rel="' . $val['job_id'] . '"><span class="fa fa-edit"></span></a>';
             $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['job_id'].'" title="Delete"></i></a>';
            $edit = '<a href="' . base_url('admin/job/edit/' . $val['job_id']) . '" rel="' . $val['job_id'] . '"><span class="fa fa-edit"></span></a>';
            
            
            $jsonArray['data'][] = array(
               'sr_no' => $start + $key + 1,
               'job_title' => $val['job_title'],
               'job_description' => $val['job_description'],
               'client_name' => $val['client_name'],
               'first_name' => $val['first_name'],
               'role_title' => $val['role_title'],
               'start_date' => $val['start_date'],
               'end_date' => $val['end_date'],
               'action' => $edit.'&nbsp;'.$delete
           );
        }
        echo json_encode($jsonArray); exit;
    }
}
