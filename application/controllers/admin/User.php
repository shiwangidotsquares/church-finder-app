<?php
defined('BASEPATH') OR exit('No direct access allowed');

class User extends MY_Controller {
    var $userData   = array();
    public function __construct() {
        parent::__construct();

        $adminlogin = $this->session->userdata('is_admin');
        $this->load->model('User_model','users');
        $this->load->model('category_model','category');
        $this->load->model('Users','check');
    }

    public function index(){
        
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
        $data = array();
        $layout = 'admin-layout';
        $view_file =  'admin/users/index';
        $index['page_title'] = ':: View Users ::';
        $content['form_title'] = 'View Users';
        $this->templates->set($layout);
        $content['layout'] = $view_file;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }

    public function view($user_id){
        
        $data = array();
        $index['page_title'] = ':: View User Details ::';
        $content['form_title'] = 'View User Details';
        $layout = 'admin-layout';
        $view_file =  'admin/users/view';
        $where = array('U.user_id' => $user_id);
        $data = $this->users->getUserAllData($where,$select = '*');

        $userData = $data['data'];

        $where = array('country_id' => $userData[0]['billing_country']);
        $dataC = $this->users->getRecords($this->users->table_country,$where);

        $content['country_name'] = $dataC[0]['name'];

        $content['userData'] = $userData;

        $this->templates->set($layout);
        $content['layout'] = $view_file;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }
    public function get_users(){
        
        $order_by = array();
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        if(empty($length)){
            $length = 10;
            $start = 0;
        }
        $columnData = array(
            'sr_no',
            'user_username',
            'user_email',
            'phone_number',
            'created_time',
            'user_is_active'
        );
        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
        $searchData = $this->input->post('searchBox');
        $where = '';
        $where .= 'user_is_deleted = "0"';
        $and=' and ';

        if($searchData){
            $searchData = trim($searchData);
            $where.= $and.'(user_username like "%'.$searchData.'%" OR user_email like "%'.$searchData.'%")';
        }
        
        $pageList_Array = $this->users->getUserData($where,$select =  '',$order_by, $start, $length,$where_in = false,$where_not_in = false);

        $pageList = $pageList_Array['data'];

        $totalData = $pageList_Array['total'];
        $jsonArray=array(
            'draw'=>$this->input->post('draw'),
            'recordsTotal'=>$totalData,
            'recordsFiltered'=>$totalData,
            'data'=>array(),
        );

        foreach($pageList as $key => $val){

            $active = $val['user_is_active']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['user_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['user_id'].'" title="Active"></i></a>';
            $view = '<a href="'.site_url('admin/user/view/'.$val['user_id']).'"><i class="fa fa-info-circle viewRecord" rel="'.$val['user_id'].'" title="View Details"></i></a>';
            $login_as_user = '<a target="_blank" href="'.site_url('admin/user/login_as_user/'.$val['user_id']).'" rel="'.$val['user_id'].'"><i class="fa fa-user" title="Login as user"></i></a>';
           /* $edit = '<a href="'.site_url('admin/user/edit/'.$val['user_id']).'" rel="'.$val['user_id'].'"><i class="fa fa-edit" title="Edit"></i></a>';*/

            $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['user_id'].'" title="Delete"></i></a>';
            $full_name = ($val['user_username'])?ucfirst($val['user_username']):'---';
            $jsonArray['data'][] = array(
                'sr_no' => $start + $key + 1,
                'user_username' => $full_name,
                'user_email' => $val['user_email']?$val['user_email']:'---',
                'phone_number' => $val['phone_number'],
                'created_time'=>$val['created_time'],
                'user_is_active'=>($val['user_is_active'])?'Unblocked':'Blocked',
                'action' => $active.'&nbsp;' .'&nbsp;'.$delete
            );
        }

        echo json_encode($jsonArray); exit;
        echo $this->input->post('draw'); exit;

    }

    public function add(){
       
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));

        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
        $data = array();
        $country_data = getContries();
        $content['country_data'] = $country_data['data'];
        $layout = 'admin-layout';
        $view_file =  'admin/users/add';
        $index['page_title'] = '::Add User ::';
        $content['form_title'] = 'Add User ';
        $this->templates->set($layout);
        $content['layout'] = $view_file;
       $path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);

        $config = array(

            array(
                'field'   => 'user_email',
                'label'   => 'Email',
                'rules'   => 'trim|required|callback_check_email|valid_email'
            ),

             array(
                'field'   => 'username',
                'label'   => 'Username',
                'rules'   => 'trim|required|callback_check_username'
            )
        );

 $this->form_validation->set_rules($config);
 if ($this->form_validation->run() == FALSE){
            $content['layout'] = $view_file;

}
else{

   if($_POST){
      $content['user_username']= $this->input->post('username');
      $content['user_email']= $this->input->post('user_email');
      $content['user_password']= $this->input->post('user_password');
      $content['phone_number'] = $this->input->post('phone_number');
      

      $save_data = array(
          'user_username'=> $content['user_username'],
          'user_email' => $content['user_email'],
          'user_password' => $content['user_password'],
          'phone_number' => $content['phone_number'],
          'user_is_active' => '1',
          'created_time' => $this->category->currentDateTime
      );

    $parishes_id=$this->users->save($this->users->table_users,$save_data);
    if($parishes_id){
        $this->messages->add('You have successfully added a user in your portal.','success');
        redirect('admin/user/index');
      }
  }

   }
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }

    public function edit($user_id){
       $data = array();
      add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));

        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
        $country_data = getContries();
        $content['country_data'] = $country_data['data'];
        $layout = 'admin-layout';
        $view_file =  'admin/users/edit';
        $index['page_title'] = '::Edit User ::';
        $content['form_title'] = 'Edit User ';
        $this->templates->set($layout);
        $content['layout'] = $view_file;
        $path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);
        $where = array('category_is_deleted' => '0', 'category_is_active' => '1','category_parent_id'=>'4' );
        $datacategory = $this->category->getRecords('categories',$where);

        $where = array('U.user_id' => $user_id);

        $data = $this->users->getUserAllData($where,$select = '*');

        #prd($data);
        $content['categories']               = $datacategory;


        $userData = $data['data'];
        
        $content['user_email']              = $userData[0]['user_email'];
        $content['username']                = $userData[0]['user_username'];
        $content['phone_number']            = $userData[0]['phone_number'];
        $content['user_profile_image']      = $userData[0]['user_profile_image'];
        //$content['user_password']         = $userData[0]['user_password'];

        if($_POST){
           $content['user_email']              = $this->input->post('user_email');
            $content['username']                = $this->input->post('username');
            $content['phone_number']            = $this->input->post('phone_number');
            
        }

        $config = array(
            array(
                'field'   => 'first_name',
                'label'   => 'First Name',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'last_name',
                'label'   => 'Last Name',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'username',
                'label'   => 'Username',
                'rules'   => 'trim|required|callback_check_username_edit'
            ),
            array(
                'field'   => 'user_email',
                'label'   => 'Email',
                'rules'   => 'trim|callback_check_email_edit|valid_email'
            ),
            array(
                'field'   => 'user_password',
                'label'   => 'Password',
                'rules'   => 'trim|matches[confirm_password]|min_length[6]'
            ),
            array(
                'field'   => 'confirm_password',
                'label'   => 'Confirm Password',
                'rules'   => 'trim'
            ),
            array(
                'field'   => 'phone_number',
                'label'   => 'Phone Number',
                'rules'   => 'trim|required|numeric'
            ),
            array(
                'field'   => 'user_profile_image',
                'label'   => 'Event Image',
                'rules'   => 'callback_image_upload|xss_clean'
            )
           /* array(
                'field'   => 'address_line1',
                'label'   => 'Address Line 1',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'user_city',
                'label'   => 'Town/City',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'region',
                'label'   => 'Region',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'postcode',
                'label'   => 'Postcode',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'billing_state',
                'label'   => 'Billing State / Province',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'billing_postcode',
                'label'   => 'Billing Postal / Zip Code',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'billing_country',
                'label'   => 'Billing Country',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'user_profile_image',
                'label'   => 'Event Image',
                'rules'   => 'callback_image_upload|xss_clean'
            )*/

        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE){
            $content['layout'] = $view_file;

        }else{

            $where_update_array = array(
                'user_id' => $user_id
            );
            $update_data = array(
                'first_name' => $content['first_name'],
                'last_name' => $content['last_name'],
                'user_username' => $content['username'],
                'role_id' => INDIVIDUAL_USER_ROLE_ID,
                'user_email' => $content['user_email'],
                'user_user_type' => $content['user_user_type'],
            );

            if(!empty($this->upload_data)){
               if(!empty($this->upload_data['file_name'])){
                $update_data['user_profile_image'] = $this->upload_data['file_name'];
            }
        }

        if($this->input->post('user_password')){
            $update_data['user_password'] = generateHash($this->input->post('user_password'));
        }
        $last_id = $this->users->save($this->users->table_users,$update_data,$where_update_array);

        if($last_id){
            $save_data = array(
                'user_id' => $user_id,
                'user_mobile_number' => $content['phone_number'],
                'address_line1' => $content['address_line1'],
                'address_line2' => $content['address_line2'],
                'city' => $content['user_city'],
                'region' => $content['region'],
                'postcode' => $content['postcode'],
                'billing_state' => $content['billing_state'],
                'billing_zipcode' => $content['billing_postcode'],
                'billing_country' => $content['billing_country']
            );
            $this->users->save($this->users->table_users_info,$save_data,$where_update_array);

            $this->messages->add('You have successfully Updated a User in your portal.','success');
            redirect('admin/user/index');

        }else{

            $this->messages->add('error occurred.','error');
            redirect('admin/user/index');
        }

    }
    $this->templates->set_data('index',$index);
    $this->templates->set_data('content',$content);
    $this->templates->load();
}
public function status(){
    //$this->privileges->check_privileges();
    $sID = $this->input->post('sID');
    $jsonArray=array('flag'=>false);
    $where_array = array(
        'user_id' => $sID
    );
    $page_update_array = array(
        'user_is_active' => $this->input->post('sStatus')
    );
    if($this->users->save($this->users->table_users,$page_update_array, $where_array)){
        $jsonArray['flag'] = true;
    }
    echo json_encode($jsonArray); exit;
}

public function delete(){
    //$this->privileges->check_privileges();
    $sID = $this->input->post('sID');
    $jsonArray = array('flag' => false);
    $where_array = array(
     'user_id' => $sID
 );
    $page_update_array = array(
        'user_is_deleted' => '1'
    );
    if($this->users->save($this->users->table_users,$page_update_array, $where_array)){
            
        $jsonArray['flag'] = true;
    }
    echo json_encode($jsonArray); exit;
}

function check_username($username){
    $where_array = array(
        'user_username' => $username,
        'user_is_deleted' => '0'
    );

    $result = $this->users->getRecords($this->users->table_users,$where_array);
    if ($result){
        $this->form_validation->set_message('check_username', 'The %s field value already exist, please try another.');
        return false;
    }else{
        return true;
    }
}

function check_email($user_email){
    $where_array = array(
        'user_email' => $user_email,
        'user_is_deleted' => '0'
    );

    $result = $this->users->getRecords($this->users->table_users,$where_array);
    //prd($result);
    if ($result){
        $this->form_validation->set_message('check_email', 'The %s field value already exist, please try another.');
        return false;
    }else{
        return true;
    }
}


public function image_upload(){

    if(isset($_FILES['user_profile_image']) && $_FILES['user_profile_image']['name'] != '') {
        $this->load->library('upload');
        $new_name = time().'_'.$_FILES["user_profile_image"]['name'];
        $config['upload_path'] = './uploads/profile-pic/';
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['max_size']    = 1024*2; //2MB
            $config['file_name'] = $new_name;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('user_profile_image')){
                $this->form_validation->set_message('user_profile_image',str_replace(array('<p>', '</p>'),'', $this->upload->display_errors()));
                return false;
            } else {
                $this->upload_data = $this->upload->data();
                return true;
            }
        }
        return true;
    }
}
