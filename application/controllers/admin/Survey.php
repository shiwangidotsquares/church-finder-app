<?php  defined('BASEPATH') OR exit('No direct access allowed');

class Survey extends MY_Controller {
    public function __construct() {
        parent ::__construct();
        $this->load->library('upload');
        $adminlogin = $this->session->userdata('is_admin');
        $this->load->model('survey_model','survey');
        $this->load->model('category_model','category');
        $this->load->model('privileges');
    }

    public function index(){
    //$this->privileges->check_privileges();
        $session_array = $this->session->userdata('user_session_data');
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
        $data = array();
        $index = array();
        $content = array();
        $layout     = 'admin-layout';
        $view_file  = 'admin/survey/index';
        $index['page_title'] = 'Survey Manager';
        $index['form_title'] = 'View Survey';
        $content['user_info']   = $session_array;
        $this->templates->set($layout);
        $content['layout']      = $view_file;
        $userData                = $this->survey->getRecords($this->survey->table_survey);
        $content['userData']     = $userData;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }
    public function get_result(){

            $length     = $this->input->post('length');
            $start      = $this->input->post('start');
            $columnData = array(
                'sr_no',
                'Survey_title',
                'first_name',
                'user_email',
                'score',
                'created'
            );
            $sortData       = $this->input->post('order');
            $order_by[0]    = $columnData[$sortData[0]['column']];
            $order_by[1]    = $sortData[0]['dir'];
            $searchData     = $this->input->post('searchBox');
            $where  = '';
            $where  = 'survey_is_deleted = 0';

            if($searchData){
                $where.= 'and (S.survey_title like "%'.$searchData.'%" OR U.first_name like "%'.$searchData.'%" OR U.last_name like "%'.$searchData.'%" OR U.user_email like "%'.$searchData.'%")';
            }
            $list = $this->survey->getDataResult($where, $select = 'SR.score,SR.created,U.first_name,U.last_name,U.user_email,S.survey_title', $order_by, $start, $length);

            $list_array    = $list['data'];

            $totalData          = $list['total'];
            $jsonArray          = array(
                'draw'              => $this->input->post('draw'),
                'recordsTotal'      => $totalData,
                'recordsFiltered'   => $totalData,
                'data'              => array(),
            );
            foreach($list_array as $key => $val){

                $originalDate = $val['created'];
                $created_date = date("d/m/Y", strtotime($originalDate));

                $jsonArray['data'][] = array(
                 'sr_no' => $start + $key + 1,
                 'survey_title' => $val['survey_title'],
                 'first_name' => $val['first_name']." ".$val['last_name'],
                 'user_email' => $val['user_email'],
                 'score' => $val['score'],
                 'created' => $created_date,
             );
            }
            echo json_encode($jsonArray); exit;

    }
    public function get_all(){
        $length     = $this->input->post('length');
        $start      = $this->input->post('start');
        $columnData = array(
            'sr_no',
            'survey_title',
            'survey_description',
            'survey_created_date',
            'survey_id'

        );
        $sortData       = $this->input->post('order');
        $order_by[0]    = $columnData[$sortData[0]['column']];
        $order_by[1]    = $sortData[0]['dir'];
        $searchData     = $this->input->post('searchBox');
        $where  = '';
        $where  = 'survey_is_deleted = 0';

        if($searchData){
            $where.= 'and (survey_title like "%'.$searchData.'%" OR survey_description like "%'.$searchData.'%")';
        }
        $list = $this->survey->getData('',$where, $select = '*', $order_by, $start, $length);
    #echo $this->db->last_query(); die;
        $list_array    = $list['data'];
        $totalData          = $list['total'];
        $jsonArray          = array(
            'draw'              => $this->input->post('draw'),
            'recordsTotal'      => $totalData,
            'recordsFiltered'   => $totalData,
            'data'              => array(),
        );
        foreach($list_array as $key => $val){
            $active = $val['survey_is_active']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['survey_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['survey_id'].'" title="Active"></i></a>';
            $edit = '<a href="' . base_url('admin/survey/edit/' . $val['survey_id']) . '" rel="' . $val['survey_id'] . '"><span class="fa fa-edit"></span></a>';
            $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['survey_id'].'" title="Delete"></i></a>';
            $edit = '<a href="' . base_url('admin/survey/edit/' . $val['survey_id']) . '" rel="' . $val['survey_id'] . '"><span class="fa fa-edit"></span></a>';
            $originalDate = $val['survey_created_date'];
            $survey_created_date = date("d/m/Y", strtotime($originalDate));
        //$survey_logo = '<img src=../uploads/survey/'. $val["survey_image"] .' width="80">';
            $jsonArray['data'][] = array(
             'sr_no' => $start + $key + 1,
             'survey_title' => $val['survey_title'],
             'survey_description' => $val['survey_description'],
             'survey_created_date' => $survey_created_date,
             'action' =>  $active.'&nbsp;'.$edit.'&nbsp;'.$delete
         );
        }
        echo json_encode($jsonArray); exit;
    }
    public function Add(){
    #prd($this->input->post());
        $this->privileges->check_privileges();
        $path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);
        $index = array();
        $content = array();
        $email_array = array();
        $index['page_title'] = 'Survey Manager';
        $content['form_title'] = 'Add Survey';
        $content['button_text'] = 'Update Survey';
        $this->templates->set('admin-layout');

        $cat_Array = $this->category->getCategoryData(array('category_parent_id'=>9));
        $content['survey_category_array'] = $cat_Array['data'];

        $content['survey_id']           = '';
        $content['title']             = $this->input->post('title');
        $content['survey_category']           = $this->input->post('survey_category');
        $content['description']     = $this->input->post('description');
        $config = array(
         array(
            'field' => 'title',
            'label' => 'Title',
            'rules' => 'trim|required'
        ),
         array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required'
        )
     );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE){
            $content['layout'] = 'admin/survey/add';
        }else{
            $title = $this->input->post('title');
            $description = $this->input->post('description');
            $saveData = array(
                'survey_title' => $title,
                'org_id'       => 1,
                'survey_description' => stripslashes($description),
                'survey_category' => $content['survey_category'],
                'survey_created_date'  => $this->survey->currentDateTime
            );
            $last_id = $this->survey->save($this->survey->table_survey,$saveData);

            if($last_id){

                foreach ($this->input->post('category_questions') as $key => $value) {

                    $saveQuestion = array(
                        'servey_id' => $last_id,
                        'question_id' => $value
                    );
                    $this->survey->save($this->survey->table_survey_question,$saveQuestion);
                }

                $this->messages->add('You have successfully added Survey.','success');
                redirect('admin/survey');
            }
        }
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }
    public function edit($id = ''){
        if($id == ""){
           $this->messages->add('No direct access allowed.','error');
           redirect('admin/survey/index');
        }
        //check record exist or not
        $record = $this->survey->checkRecord($this->survey->table_survey, array('survey_id'=>$id));
        if(empty($record)){
           $this->messages->add('No survey found with given id.','error');
           redirect('admin/survey/index');
        }
        $index = array();
        $content = array();
        $email_array = array();

        $this->privileges->check_privileges();

        $path = '../../../assets/js/ckfinder';

        $width = '1300px';
        parent::editor($path,$width);

        $cat_Array = $this->category->getCategoryData(array('category_parent_id'=>9));
        $content['survey_category_array'] = $cat_Array['data'];
        $where_array = array(
            'survey_id' => $id
        );

        $switch = $this->survey->checkRecord($this->survey->table_survey,$where_array);
        if((empty($id) && !is_numeric($id)) && ($switch == false)){
            $this->messages->add('Selected record does not exist in our database!','error');
            redirect(base_url('admin/survey/index'));
        }

        $index['page_title'] = 'Survey Manager';
        $content['form_title'] = 'Edit Survey';
        $content['button_text'] = 'Update Survey';
        $this->templates->set('admin-layout');
        $where_array = array(
            'survey_id' => $id
        );
        $email_array = $this->survey->getData('',$where_array,$select = '*');

        $email_array = $email_array['data'];
        $content['survey_id']           = $id;

        if(!empty($email_array)){
            $queData = $this->survey->getRecords($this->survey->table_questions, array('question_is_deleted' => 0,'question_is_active'=>1,'question_category'=>$email_array[0]['survey_category']));


            $content['title']         = !empty($email_array[0]['survey_title'])?$email_array[0]['survey_title']:'';
            $content['survey_category']         = !empty($email_array[0]['survey_category'])?$email_array[0]['survey_category']:'';
            $content['description']   = !empty($email_array[0]['survey_description'])?$email_array[0]['survey_description']:'';
            $content['queData']   = $queData;
        }
        if($_POST){
            $content['title']           = $this->input->post('title');
            $content['survey_category'] = $this->input->post('survey_category');
            $content['description']     = $this->input->post('description');
        }


        $config = array(
            array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'trim|required'
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE){
            $content['layout'] = 'admin/survey/add';
        }else{
            if($_FILES['survey_image']['name']){
                $new_image_name = time() .'_'. str_replace(str_split(' ()\\/,:*?"<>|'), '',$_FILES['survey_image']['name']);
                $config['upload_path'] = 'uploads/survey/';
                $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
                $config['file_name'] = $new_image_name;
                $config['max_size']  = '0';
                $config['max_width']  = '0';
                $config['max_height']  = '0';
                $config['$min_width'] = '0';
                $config['min_height'] = '0';
            #$this->load->library('upload', $config);
                $this->upload->initialize($config);
                if(!$this->upload->do_upload('survey_image')) {
                    $this->data['error'] = $this->upload->display_errors();
                    $this->messages->add($this->data['error']);
                    redirect('admin/survey/add');
                }
            }
            $title = $this->input->post('title');
            $description = $this->input->post('description');
            $saveData = array(
                'survey_title' => $title,
                'survey_description' => stripslashes($description),
                'survey_category' => $content['survey_category'],
            );

            $last_id = $this->survey->save($this->survey->table_survey,$saveData,array('survey_id'=>$id));
            if($last_id){
                $this->db->where('servey_id', $id);
                $this->db->delete('lss_survey_question');
                foreach ($this->input->post('category_questions') as $key => $value) {

                    $saveQuestion = array(
                        'servey_id' => $id,
                        'question_id' => $value
                    );
                    $this->survey->save($this->survey->table_survey_question,$saveQuestion);
                }

                $this->messages->add('You have successfully updated selected survey.','success');
                redirect('admin/survey');
            }
        }
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }
    public function load_questions(){
        $html = '';
        $cat_id = $this->input->post('cat_id');
        $queData = $this->survey->getRecords($this->survey->table_questions, array('question_is_deleted' => 0,'question_is_active'=>1,'question_category'=>$cat_id));

        foreach ($queData as $key => $value) {

            $html .='<option value="'.$value['question_id'].'">'.$value['question_title'].'</option>';
        }
        echo $html;

    }
    public function status(){
        $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray=array('flag'=>false);
        $where_array = array(
            'survey_id' => $sID
        );
        $page_update_array = array(
            'survey_is_active' => $this->input->post('sStatus')
        );
        if($this->survey->save($this->survey->table_survey,$page_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }
    public function delete(){
        $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray = array('flag' => false);
        $where_array = array(
         'survey_id' => $sID
     );
        $page_update_array = array(
            'survey_is_deleted' => '1'
        );

        if($this->survey->save($this->survey->table_survey,$page_update_array, $where_array)){
        #echo $this->db->last_query();die;
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }
    public function survey_result(){
        $session_array = $this->session->userdata('user_session_data');
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
        $data = array();
        $index = array();
        $content = array();
        $layout     = 'admin-layout';
        $view_file  = 'admin/survey/survey_result';
        $index['page_title'] = 'Survey Result';
        $index['form_title'] = 'Survey Result';
        $content['user_info']   = $session_array;
        $this->templates->set($layout);
        $content['layout']      = $view_file;

        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }
}
