<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Parking_operative extends MY_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('users');
	}

    /**
     * this is a generic function to show html for Parking operative in admin area
     * function having no parameter
     * function using template admin-login
     * @access public
    */
    public function index(){
    	add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
    	add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
    	$data = array();
    	$layout = 'admin-layout';
    	$view_file =  'admin/parking_operative/index';
    	$index['page_title'] = ':: View Parking Operative ::';
    	$content['form_title'] = 'View Parking Operative';
    	$this->templates->set($layout);
    	$content['layout'] = $view_file;
    	$this->templates->set_data('index',$index);
    	$this->templates->set_data('content',$content);
    	$this->templates->load();
    }
    /**
     * this is a generic function to get Parking operative for datatable
     * function using template admin-login
     * @access public
    */
    public function get_parking_operative(){

    	$order_by = array();
    	$length = $this->input->post('length');
    	$start = $this->input->post('start');
    	if(empty($length)){
    		$length = 10;
    		$start = 0;
    	}
    	$columnData = array(
    		'sr_no',
    		'user_name',
    		'user_email',
    		'created_time',
    		'action'
    	);
    	$sortData = $this->input->post('order');
    	$order_by[0] = $columnData[$sortData[0]['column']];
    	$order_by[1] = $sortData[0]['dir'];
    	$searchData = $this->input->post('searchBox');
    	$where = '';
        //getting data from user table for parking operative which have role id 3 and user not deleted
    	$where .= 'user_is_deleted = "0" and role_id = 3';
    	$and=' and ';

    	if($searchData){
    		$searchData = trim($searchData);
    		$where.= $and.'(user_name like "%'.$searchData.'%" OR user_email like "%'.$searchData.'%")';
    	}
        //getting data from user table for parking operative which have role id 3
    	$parking_operative_data = $this->users->getDataTableRecords($this->users->table_users, $where, $select = 'user_name,user_email,created_time,is_active',$order_by, $start, $length,$where_in = false,$where_not_in = false);

    	$user_data = $parking_operative_data['data'];
    	$total_data = $parking_operative_data['total'];
    	$jsonArray=array(
    		'draw'=>$this->input->post('draw'),
    		'recordsTotal'=>$total_data,
    		'recordsFiltered'=>$total_data,
    		'data'=>array(),
    	);

    	foreach($user_data as $key => $val){

    		$active = $val['user_is_active']?'<a href="javascript:void(0)" class="tableIcon"><i class="fa fa-check-square activeRecord" rel="'.$val['user_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)" class="tableIcon"><i class="fa fa-ban deactiveRecord" rel="'.$val['user_id'].'" title="Active"></i></a>';

    		$edit = '<a href="'.site_url('admin/parking_operative/edit/'.$val['user_id']).'" rel="'.$val['user_id'].'" class="tableIcon"><i class="fa fa-edit" title="Edit"></i></a>';

    		$delete = '<a href="javascript:void(0)" class="tableIcon"><i class="fa fa-trash-o deleteRecord" rel="'.$val['user_id'].'" title="Delete"></i></a>';

    		$jsonArray['data'][] = array(
    			'sr_no'         => $start + $key + 1,
    			'user_name'     => $val['user_name']?$val['user_name']:'---',
    			'user_email'    => $val['user_email']?$val['user_email']:'---',
    			'created_time'  =>$val['created_time'],
    			'action' 		=> $active.'&nbsp;'.$edit.'&nbsp;'.$delete
    		);
    	}
    	echo json_encode($jsonArray); exit;
    	echo $this->input->post('draw'); exit;
    }
    /**
     * this is a generic function to add Parking operative
     * function using template admin-login
     * @access public
    */
    public function add(){
    	$data = array();

    	$layout = 'admin-layout';
    	$view_file =  'admin/parking_operative/add';
    	$index['page_title'] = '::Add Parking Operative ::';
    	$content['form_title'] = 'Add Parking Operative ';
    	$this->templates->set($layout);
    	$content['layout'] = $view_file;

    	$content['user_id']                =  "";
    	$content['user_name']              = $this->input->post('user_name');
    	$content['user_email']             = strtolower($this->input->post('user_email'));

    	$config = array(
    		array(
    			'field'   => 'user_name',
    			'label'   => 'Name',
    			'rules'   => 'trim|required'
    		),
    		array(
    			'field'   => 'user_email',
    			'label'   => 'Email',
    			'rules'   => 'trim|required|callback_check_email|valid_email'
    		),
    		array(
    			'field'   => 'user_password',
    			'label'   => 'Password',
    			'rules'   => 'trim|required|matches[confirm_password]|min_length[6]'
    		),
    		array(
    			'field'   => 'confirm_password',
    			'label'   => 'Confirm Password',
    			'rules'   => 'trim|required'
    		)
    	);
    	//Form validation checking
    	$this->form_validation->set_rules($config);
    	if ($this->form_validation->run() == FALSE){
    		$content['layout'] = $view_file;

    	}else{
    		//Data which need to save in db for parking operative
    		$save_data = array(
    			'user_name' => $content['user_name'],
    			'role_id' => PARKING_OPERATIVE_ROLE_ID,
    			'user_email' => $content['user_email'],
                'show_user_password' => $this->input->post('user_password'),
    			'user_password' => generateHash($this->input->post('user_password')),
    			'created_time' => $this->users->currentDateTime
    		);
    		//save data in user table
    		$last_id = $this->users->save($this->users->table_users,$save_data);

    		if($last_id){
                //sending emails while added user as a Parking Operative to PCM Virtual Permit Portal
    			$mail_to      = $content['user_email'];
    			$subject      = 'Welcome !! You have added as a Parking Operative to PCM Virtual Permit Portal';
    			$mail_extra   = '';
    			$mail_cc      = '';
    			$mail_bcc     = '';
    			if (!empty($mail_extra)) {
    				$mail_bcc = $mail_extra;
    			}
    			$sender_email = ADMIN_EMAIL;
    			$sender_name  = ADMIN_NAME;

    			$message_data = '<p>Dear '.$content['user_name'].',</p>
    			<p>You are added as a Parking Operative to PCM Virtual Permit Portal, Below are your login details::</p>
    			<p>Email: <b>'.$content['user_email'].'</b></p>
    			<p>Password: <b>'.$this->input->post('user_password').'</b></p>';

    			$body           = array('email_content'=>$message_data);

    			$messagebody    = $this->parser->parse('emailer/email', $body, true);

    			/* Sending parking operative info in mail */
    			$this->sendmail($mail_to, $subject, $messagebody, $mail_cc, $mail_bcc, $sender_email, $sender_name, false);
    		}

    		$this->messages->add('You have successfully added a Parking Operative in your portal.','success');
    		redirect('admin/Parking_operative/index');
    	}
    	$this->templates->set_data('index',$index);
    	$this->templates->set_data('content',$content);
    	$this->templates->load();
    }
    /**
     * this is a generic function to edit Parking operative
     * function using template admin-login
     * @access public
    */
    public function edit($user_id = ''){
    	if($user_id == ''){
    		$this->messages->add('No direct script access allowed.','error');
    		redirect('admin/Parking_operative/index');
    	}
    	$data = array();
    	$layout = 'admin-layout';
    	$view_file =  'admin/parking_operative/edit';
    	$index['page_title'] = '::Edit Parking Operative ::';
    	$content['form_title'] = 'Edit Parking Operative ';
    	$this->templates->set($layout);
        //getting user records according to provided user id
    	$where = array(
    		'user_id' => $user_id
    	);
    	$userData = $this->users->getRecords($this->users->table_users,$where,$select = 'user_name,user_email,show_user_password');
        $content['show_user_password'] = '';
    	if (!empty($userData)) {
    		$content['user_id']                 = $user_id;
    		$content['user_name']               = $userData[0]['user_name'];
            $content['user_email']              = $userData[0]['user_email'];
    		$content['show_user_password']      = isset($userData[0]['show_user_password'])?$userData[0]['show_user_password']:'';
    	}else{
    		$this->messages->add('No records found.','error');
    		redirect('admin/Parking_operative/index');
    	}
    	if($_POST){
    		$content['user_name']              = $this->input->post('user_name');
            $content['user_email']             = $this->input->post('user_email');
    	}
    	$config = array(
    		array(
    			'field'   => 'user_name',
    			'label'   => 'Name',
    			'rules'   => 'trim|required'
    		),
    		array(
    			'field'   => 'user_email',
    			'label'   => 'Email',
    			'rules'   => 'trim|required|callback_check_email_edit|valid_email'
    		),
    		array(
    			'field'   => 'user_password',
    			'label'   => 'Password',
    			'rules'   => 'trim|matches[confirm_password]|min_length[6]'
    		),
    		array(
    			'field'   => 'confirm_password',
    			'label'   => 'Confirm Password',
    			'rules'   => 'trim'
    		)
    	);
        //checking validation
    	$this->form_validation->set_rules($config);
    	if ($this->form_validation->run() == FALSE){
    		$content['layout'] = $view_file;
    	}else{
    		//Data which need to save in db for parking operative
    		$where_update_array = array(
    			'user_id' => $user_id
    		);
    		$update_data = array(
    			'user_name' => $content['user_name'],
    			'role_id' => PARKING_OPERATIVE_ROLE_ID,
    			'user_email' => $content['user_email'],
    		);
    		if($this->input->post('user_password')){
                $update_data['user_password']      = generateHash($this->input->post('user_password'));
    			$update_data['show_user_password'] = $this->input->post('user_password');
    		}
    		//save data in user table
    		$last_id = $this->users->save($this->users->table_users,$update_data,$where_update_array);

    		if($last_id){
    			$this->messages->add('You have successfully updated Parking Operative in your portal.','success');
    			redirect('admin/Parking_operative/index');

    		}else{
    			$this->messages->add('error occurred.','error');
    			redirect('admin/Parking_operative/index');
    		}

    	}
    	$this->templates->set_data('index',$index);
    	$this->templates->set_data('content',$content);
    	$this->templates->load();
    }
    /**
     * this is a generic function to add change Parking operative status
     * function using template admin-login
     * @access public
    */
    public function status(){

    	$sID = $this->input->post('sID');
    	$jsonArray=array('flag'=>false);

    	if($this->input->post('sStatus') == '0'){
	    	$where_array = array(
	    		'parking_operative_id' => $sID
	    	);
	       //checking email already exist or not in while edit records
	    	$result = $this->users->checkRecord($this->users->table_business,$where_array);

	    	if(!empty($result)){
	    		$jsonArray['flag'] = false;
	    		$jsonArray['status'] = 'pending';
	    		echo json_encode($jsonArray); exit;
	    	}
    	}
    	$where_array = array(
    		'user_id' => $sID
    	);
    	$user_update_array = array(
    		'user_is_active' => $this->input->post('sStatus')
    	);
        //updating the status
    	if($this->users->save($this->users->table_users,$user_update_array, $where_array)){
    		$jsonArray['flag'] = true;
    	}
    	echo json_encode($jsonArray); exit;
    }

    /**
     * this is a generic function to add delete Parking operative
     * function using template admin-login
     * @access public
    */
    public function delete(){
    	$sID = $this->input->post('sID');
    	$jsonArray = array('flag' => false);

    	$where_array = array(
    		'parking_operative_id' => $sID
    	);
        //checking email already exist or not in while edit records
    	$result = $this->users->checkRecord($this->users->table_business,$where_array);

    	if(!empty($result)){
    		$jsonArray['flag'] = false;
    		$jsonArray['status'] = 'pending';
    		echo json_encode($jsonArray); exit;
    	}

    	$where_array = array(
    		'user_id' => $sID
    	);
    	$user_update_array = array(
    		'user_is_deleted' => '1'
    	);
    	if($this->users->save($this->users->table_users,$user_update_array, $where_array)){
    		$jsonArray['flag'] = true;
    	}
    	echo json_encode($jsonArray); exit;
    }
    /**
     * this is a generic function to check email already exist or not
     * function using template admin-login
     * @access public
    */
    function check_email($user_email){
    	$where_array = array(
    		'user_email' => $user_email,
    		'user_is_deleted' => '0'
    	);
        //checking email already exist or not
    	$result = $this->users->checkRecord($this->users->table_users,$where_array);
    	if ($result){
    		$this->form_validation->set_message('check_email', 'The %s field value already exist, please try another.');
    		return false;
    	}else{
    		return true;
    	}
    }

    /**
     * this is a generic function to check email already exist or not while record edit
     * function using template admin-login
     * @access public
    */

    function check_email_edit($user_email){
    	$user_id = $this->input->post('user_id');
    	$where_array = array(
    		'user_email' => $user_email,
    		'user_is_deleted' => '0',
    		'user_id !=' => $user_id
    	);
        //checking email already exist or not in while edit records
    	$result = $this->users->checkRecord($this->users->table_users,$where_array);

    	if (count($result['data'])){
    		$this->form_validation->set_message('check_email_edit', 'The %s field value already exist, please try another');
    		return false;
    	}else{
    		return true;
    	}
    }

}
?>