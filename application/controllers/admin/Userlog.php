<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Userlog extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('users','obj_user');
        $this->load->model('staff_model','staff');
        $this->load->model('privileges','privilege');
    }


    public function index(){
        $this->privileges->check_privileges();
        $layout = 'admin-layout';
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
        $index = array();
        $footer = array();
        $content = array();
        $this->templates->set($layout);
        $view_file =  'admin/userlog/account_history';
        $content['layout'] = $view_file;

        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }
    public function get_user_log(){
        //$this->privilege->check_privileges();
        $order_by = array();
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        if(empty($length)){
            $length = 10;
            $start = 0;
        }
        $columnData = array(
            'sr_no',
			'U.first_name',
            'UL.login_ip',
			'UL.user_event_log_id',
            'UL.user_login_count',
            'UL.user_last_login'
        );
        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
        $searchData = $this->input->post('searchBox');

		$user_type = $this->input->post('user_type');
		$company_user_id = $this->input->post('company_user_id');
		$indidual_user_id = $this->input->post('indidual_user_id');
		$staff_user_id = $this->input->post('staff_user_id');

        $where = '';
        $and='';
		if($user_type && $staff_user_id){
			$where.= $and.'(U.role_id = "4")';
			$and=' AND ';
		}elseif($user_type){
			$where.= $and.'(U.role_id = "'.$user_type.'")';
			$and=' AND ';
		}

		if($company_user_id && $staff_user_id){
			$where.= $and.'(U.role_id = "4" AND U.user_id = "'.$staff_user_id.'")';
			$and=' AND ';
		}elseif($company_user_id){
			$where.= $and.'(U.user_id = "'.$company_user_id.'")';
			$and=' AND ';
		}

		if($indidual_user_id){
			$where.= $and.'(U.user_id = "'.$indidual_user_id.'")';
			$and=' AND ';
		}

        if($searchData){
            $where.= $and.'(UL.login_ip like "%'.$searchData.'%" OR UL.user_login_count like "%'.$searchData.'%" OR UL.user_last_login like "%'.$searchData.'%" OR U.last_name like "%'.$searchData.'%" OR U.first_name like "%'.$searchData.'%")';
        }

        $List_Array = $this->obj_user->get_user_logs($where,$select = 'UL.*,U.first_name,U.last_name',$order_by, $start, $length,$where_in = false,$where_not_in = false);

        $staffList = $List_Array['data'];

        $totalData = $List_Array['total'];
        $jsonArray=array(
            'draw'=>$this->input->post('draw'),
            'recordsTotal'=>$totalData,
            'recordsFiltered'=>$totalData,
            'data'=>array(),
        );
        #prd($staffList);
		$user_event = $this->config->item('user_event');
        foreach($staffList as $key => $val){
			$full_name = '';
			if(!empty($val['first_name']) && !empty($val['last_name'])){
				$full_name = $val['first_name'].' '.$val['last_name'];
			}else if(!empty($val['first_name'])){
                $full_name = $val['first_name'];
            }
            $original = $val['user_last_login'];
            $user_last_login = date("d/m/Y", strtotime($original));

            $jsonArray['data'][] = array(
                'sr_no' => $start + $key + 1,
				'user_name' => $full_name.'',
                'login_ip' => $val['login_ip']?$val['login_ip']:'--',
				'log_event_name' => isset($user_event[$val['user_event_log_id']])?$user_event[$val['user_event_log_id']]:'--',
                'user_login_count' => $val['user_login_count']?$val['user_login_count']:'--',
                'user_last_login'=> $user_last_login
            );
        }

        echo json_encode($jsonArray); exit;
        echo $this->input->post('draw'); exit;
    }


	public function get_company_user(){
		$where_array = array(
			'user_is_active' => '1',
			'user_is_deleted' => '0',
			'role_id' => 2
		);
		$company_user_array = $this->obj_user->getRecords($this->obj_user->table,$where_array);
		$jsonArray = array();
        foreach ($company_user_array as $key => $val) {
            $jsonArray[] = array
                (
                'user_id' => $val['user_id'],
                'full_name' => $val['first_name'].' '.$val['last_name'],
            );
        }
        echo json_encode($jsonArray);
        exit;
	}

	public function get_individual_user(){
		$where_array = array(
			'user_is_active' => '1',
			'user_is_deleted' => '0',
			'role_id' => 3
		);
		$individual_user_array = $this->obj_user->getRecords($this->obj_user->table,$where_array);
		$jsonArray = array();
        foreach ($individual_user_array as $key => $val) {
            $jsonArray[] = array
                (
                'user_id' => $val['user_id'],
                'full_name' => $val['first_name'].' '.$val['last_name'],
            );
        }
        echo json_encode($jsonArray);
        exit;
	}

	public function get_stff_user(){
		$company_user_id = $this->input->post('company_user_id');
        $where = 'U.user_is_deleted = "0" AND U.user_is_active = "1" AND S.company_id='.$company_user_id;
        $List_Array = $this->staff->getallStaff($where,$select = 'S.staff_name,S.staff_id,U.first_name,U.last_name,U.user_id',$order_by = array('S.staff_name','ASC'), false, false,$where_in = false,$where_not_in = false);
        $staffList = $List_Array['data'];
		$jsonArray = array();
        foreach ($staffList as $key => $val) {
            $jsonArray[] = array
                (
                'user_id' => $val['user_id'],
                'full_name' => $val['staff_name']
            );
        }
        echo json_encode($jsonArray);
        exit;
	}
}

/* End of file Dashboard.php */
/* Location: ./application/controllers/admin/userlog.php */