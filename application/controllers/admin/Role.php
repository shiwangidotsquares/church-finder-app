<?php
defined('BASEPATH') OR exit('No direct access allowed');

class Role extends MY_Controller {
 	var $userData   = array();
	public function __construct() {
        parent::__construct();
		$this->load->model('privileges','privilege');
		$this->load->model(array('actions','controllers'));
		$adminlogin = $this->session->userdata('is_admin');
        $this->load->model('roles');
		$this->load->model('users');
    }

	public function index(){
        #$this->privilege->check_privileges();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$data = array();
		$layout = 'admin-layout';
		$view_file =  'admin/role/index';
		$index['role_title'] = ':: Manage Roles ::';
		$content['form_title'] = 'Manage Roles';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
    }

	public function get_role(){
        #$this->privilege->check_privileges();
		$order_by = array();
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        if(empty($length)){
            $length = 10;
            $start = 0;
        }
        $columnData = array(
            'sr_no',
            'role_title',
			'role_description',
			'role_is_active'
        );
        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
        $searchData = $this->input->post('searchBox');
        $where = '';
        $where .= 'role_is_deleted = "0"';
        $and=' and ';
        if($searchData){
            $where.= $and.'(role_title like "%'.$searchData.'%"   )';
		}
		$where_not_in = array(0 => 'role_id',array(1,2,3,4));
        $RoleList_Array = $this->roles->getData($where,$select =  '',$order_by, $start, $length,$where_in = false,$where_not_in);
		#prd($RoleList_Array);
		$RoleList = $RoleList_Array['data'];
        $totalData = $RoleList_Array['total'];
        $jsonArray=array(
            'draw'=>$this->input->post('draw'),
            'recordsTotal'=>$totalData,
            'recordsFiltered'=>$totalData,
            'data'=>array(),
        );
        foreach($RoleList as $key => $val){
            $active = $val['role_is_active']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['role_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['role_id'].'" title="Active"></i></a>';
            $edit = '<a href="'.site_url('admin/role/edit/'.$val['role_id']).'" rel="'.$val['role_id'].'"><i class="fa fa-edit" title="Edit"></i></a>';
            $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['role_id'].'" title="Delete"></i></a>';
            $jsonArray['data'][] = array(
                'sr_no' => $start + $key + 1,
                'role_title' => $val['role_title']?ucfirst($val['role_title']):'-----',
				'role_description'=>$val['role_description'],
				'role_is_active'=>($val['role_is_active'])?'Active':'Inactive',
                'action' => $active.'&nbsp;'.$edit.'&nbsp;'.$delete
            );
        }
        echo json_encode($jsonArray); exit;
        echo $this->input->post('draw'); exit;
	}

	public function add(){
		#$this->privilege->check_privileges();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$data = array();
		$layout = 'admin-layout';
		$view_file =  'admin/role/add';
		$index['role_title'] = '::Add Role ::';
		$content['form_title'] = 'Add Role ';
		$this->templates->set($layout);
		$content['layout'] = $view_file;

        $content['role_title']               = $this->input->post('role_title');
		$content['role_description']         = $this->input->post('role_description');
		$config = array(
			array(
    			'field'   => 'role_title',
    			'label'   => 'Role Title',
    			'rules'   => 'required|trim|xss_clean|callback_check_role_title'
			),
			array(
    			'field'   => 'role_description',
    			'label'   => 'Role Description',
    			'rules'   => 'required|trim|xss_clean'
			)
		);

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE){
			$content['layout'] = $view_file;

		}else{
			$save_data = array(
                'role_title' => $content['role_title'],
                'role_description' => $content['role_description']
            );

            $role_id = $this->roles->save($this->roles->table,$save_data);
			$this->messages->add('You have successfully added a Page in your portal.','success');
            redirect('admin/role/index');
		}
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}

    public function edit($role_id){
        #$this->privilege->check_privileges();
        $data = array();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$layout = 'admin-layout';
		$view_file =  'admin/role/add';
		$index['role_title'] = '::Edit Page ::';
		$content['form_title'] = 'Edit Page ';
		$this->templates->set($layout);
		$content['layout'] = $view_file;

		$where = array('role_id' => $role_id);
		$where_not_in = array(0 => 'role_id',array(1,2,3,4));
        $role_data = $this->roles->getData($where,$select =  '',$order_by= NULL, $offset=0, $limit=NULL,$where_in = false,$where_not_in);
        $roles = $role_data['data'];
        $content['role_id']                = $role_id;
        $content['role_title']             = $roles[0]['role_title'];
        $content['role_description']              = $roles[0]['role_description'];
		if($_POST){
			$content['role_title']               = $this->input->post('role_title');
			$content['role_description']         = $this->input->post('role_description');
		}
		$config = array(
			array(
    			'field'   => 'role_title',
    			'label'   => 'Role Title',
    			'rules'   => 'required|trim|xss_clean|callback_check_edited_role_title'
			),
			array(
    			'field'   => 'role_description',
    			'label'   => 'Role Description',
    			'rules'   => 'required|trim|xss_clean'
			)
		);
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE){
			$content['layout'] = $view_file;
		}else{
			$where_update_array = array(
                'role_id' => $role_id
            );
			$update_data = array(
                'role_title' => $content['role_title'],
                'role_description' => $content['role_description']
            );
            $this->roles->save($this->roles->table,$update_data,$where_update_array);

			$this->messages->add('You have successfully updated a role in your portal.','success');
            redirect('admin/role/index');
		}
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
    }
    public function status(){
        #$this->privilege->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray= array('flag'=>false);
        $where_array = array(
            'role_id' => $sID
        );
        $page_update_array = array(
            'role_is_active' => $this->input->post('sStatus')
        );
        if($this->roles->save($this->roles->table,$page_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }

    public function delete(){
        #$this->privilege->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray = array('flag' => false);
        $where_array = array(
            'role_id' => $sID
        );
        $role_update_array = array(
            'role_is_deleted' => '1'
        );
        if($this->roles->save($this->roles->table,$role_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }

	function check_role_title($role_title){
        $where_array = array(
            'role_title' => $role_title,
            'role_is_deleted' => '0'
        );
        $result = $this->roles->getRecords($this->roles->table,$where_array);
       	if (!empty($result[0])){
 			$this->form_validation->set_message('check_role_title', 'The %s field value already exist, please try another.');
 			return false;
  		}else{
 			return true;
  		}
	}

	function check_edited_role_title($role_title){
        $where_array = array(
            'role_title' => $role_title,
			'role_is_deleted' => '0',
			'role_id !=' => $this->input->post('edited_role_id')
        );
        $result = $this->roles->getRecords($this->roles->table,$where_array);
       	if (!empty($result[0])){
 			$this->form_validation->set_message('check_edited_role_title', 'The %s field value already exist, please try another.');
 			return false;
  		}else{
 			return true;
  		}
	}

	public function assign(){
		#$this->privilege->check_privileges();
	    $this->templates->set('admin-layout');
		$assigned_privileges = $this->privileges->get_all_role_actions();
		$role_privileges = array();
		foreach($assigned_privileges as $assigns){
			$role_privileges[$assigns['role_id']][] = $assigns['action_id'];
		}
		$middle['assigned_privileges'] = $role_privileges;
		$middle['result'] = $this->actions->get_all_actions();
		$middle['groups'] = $this->roles->show_perticular_active_roles();
        #prd($middle['groups']);
		$controllers = array();
		$Pcontrollers = $this->controllers->get_all_controllers();
		foreach($Pcontrollers as $controller){
			$controllers[ $controller['controller_id'] ] = $controller;
			$controllers[ $controller['controller_id'] ]['actions'] = $this->actions->get_all_action_by_controller($controller['controller_id']);
		}
		$middle['controllers'] = $controllers;
		$middle['formTitle'] = 'Privilege Manager';
        $middle['layout'] = 'admin/privilege/index';
        $this->templates->set_data('content',$middle);

		if( is_array( $this->input->post('privileges') ) && count ($this->input->post('privileges')) > 0){
			#$this->db->query("TRUNCATE ".$this->table_privileges);
			$role_array = $middle['groups'];
			if(!empty($role_array)){
				foreach($role_array as $key => $value){
					$where_array = array(
						'role_id' => $value['role_id']
					);
					$this->roles->deleteRecord($this->roles->table_privileges,$where_array);
				}
			}
			$privileges = $this->input->post('privileges');
			$count_privilege = count($privileges);
			$i = 1 ;
			foreach($privileges as $role_id => $privilege ){
				foreach($privilege as $pri ){
					$data['role_id']  = $role_id ;
					$controller_action = explode('_',$pri );

					$data['controller_id'] = $controller_action[0];
					$data['action_id'] = $controller_action[1];
					if( (isset($data['role_id']) && !empty($data['role_id'])) && (isset($data['controller_id']) && !empty($data['controller_id'])) && (isset($data['action_id']) && !empty($data['action_id'])) ){
						$this->privileges->add_role_action($data['controller_id'],$data['role_id'],$data['action_id']);
						$this->session->set_flashdata('message','Privileges assigned successfully.');
					}
				}
				if($i == $count_privilege){
					redirect('admin/role/assign');
				}
				$i ++;
			}
		}
        $this->templates->load();
	}

}
