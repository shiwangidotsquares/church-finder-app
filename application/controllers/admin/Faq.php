<?php
defined('BASEPATH') OR exit('No direct access allowed');

class Faq extends MY_Controller {
 	var $data   = array();
	public function __construct() {
        parent::__construct();
		$adminlogin = $this->session->userdata('is_admin');
        $this->load->model('faqs');
        $this->load->model('privileges','privilege');
    }

	public function index(){
        #$this->privileges->check_privileges();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));

		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));

        //$this->privilege->check_privileges();
		$data = array();
		$layout = 'admin-layout';
		$view_file =  'admin/faq/index';
		$index['page_title'] = ':: View FAQs ::';
		$content['form_title'] = 'View FAQs';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}


	public function get_faqs(){
		$order_by = array();
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        if(empty($length)){
            $length = 10;
            $start = 0;
        }
        $columnData = array(
            'sr_no',
            'faq_title',
            'faq_description',
            'faq_created_date',
			'action'
        );
        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
        $searchData = $this->input->post('searchBox');
        $where = '';
        $where .= 'faq_is_deleted = "0"';
        $and=' and ';

        if($searchData){
            $where.= $and.'(faq_title like "%'.$searchData.'%" OR faq_description like "%'.$searchData.'%" OR faq_created_date like "%'.$searchData.'%")';
        }

        $List_Array = $this->faqs->getFaqsData($where,$select = '*',$order_by, $start, $length,$where_in = false,$where_not_in = false);

        $companyList = $List_Array['data'];

        $totalData = $List_Array['total'];
        $jsonArray=array(
            'draw'=>$this->input->post('draw'),
            'recordsTotal'=>$totalData,
            'recordsFiltered'=>$totalData,
            'data'=>array(),
        );
        foreach($companyList as $key => $val){

           $active = $val['faq_is_active']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['faq_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['faq_id'].'" title="Active"></i></a>';

            $edit = '<a href="'.site_url('admin/faq/edit/'.$val['faq_id']).'" rel="'.$val['faq_id'].'"><i class="fa fa-edit" title="Edit"></i></a>';

            $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['faq_id'].'" title="Delete"></i></a>';

            $originalDate = $val['faq_created_date'];
            $created_date = date("d/m/Y", strtotime($originalDate));
            $jsonArray['data'][] = array(
                'sr_no'         	=> $start + $key + 1,
                'faq_title'         => $val['faq_title']?ucfirst($val['faq_title']):'---',
                'faq_description'   => $val['faq_description'],
				'faq_ordering'      => $val['faq_ordering'],
				'faq_created_date'  => $created_date,
                'action'            => $active.'&nbsp;'.$edit.'&nbsp;'.$delete
            );
        }

        echo json_encode($jsonArray); exit;

	}

	public function add(){
        #$this->privileges->check_privileges();
		$data = array();

		$layout = 'admin-layout';
		$view_file =  'admin/faq/add';
		$index['page_title'] = '::Add FAQ ::';
		$content['form_title'] = 'Add FAQ ';
		$this->templates->set($layout);
		$content['layout'] = $view_file;

        $path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);

        $content['faq_title']          = $this->input->post('faq_title');
        $content['faq_description']    = $this->input->post('faq_description');
        $content['faq_ordering']       = $this->input->post('faq_ordering');

		$config = array(

            array(
                'field'   => 'faq_title',
                'label'   => 'faq Name',
                'rules'   => 'trim|required|callback_check_faq_title'
            ),
            array(
                'field'   => 'faq_description',
                'label'   => 'faq Description',
                'rules'   => 'trim|required'
            )

		);

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE){
			$content['layout'] = $view_file;
		}else{
			$save_data = array(
                'faq_title'         => $content['faq_title'],
                'org_id'       => 1,
                'faq_description'   => $content['faq_description'],
                'faq_ordering'   	=> $content['faq_ordering'],
                'faq_created_date'  => $this->faqs->currentDateTime
            );

            $last_id = $this->faqs->save($this->faqs->table_faq,$save_data);

            if($last_id){
    			$this->messages->add('You have successfully added a FAQ in your portal.','success');
                redirect('admin/faq/index');
            }else{
                $this->messages->add('error occurred.','error');
                redirect('admin/faq/index');
            }


		}
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}

    public function edit($faq_id =''){
        if($faq_id == ""){
           $this->messages->add('No direct access allowed.','error');
           redirect('admin/faq/index');
        }
        //check record exist or not
        $record = $this->faqs->checkRecord($this->faqs->table_faq, array('faq_id'=>$faq_id));
        if(empty($record)){
           $this->messages->add('No faq found with given id.','error');
           redirect('admin/faq/index');
        }
        #$this->privileges->check_privileges();
        $data = array();

        $layout = 'admin-layout';
        $view_file =  'admin/faq/edit';
        $index['page_title'] = '::Edit FAQ ::';
        $content['form_title'] = 'Edit FAQ ';
        $this->templates->set($layout);
        $content['layout'] = $view_file;

        $path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);

        $where = array('faq_id'=>$faq_id);
        $data_result =  $this->faqs->getRecords($this->faqs->table_faq,$where);

        if($data_result){
            $content['faq_id']                     = $faq_id;
            $content['faq_title']                  = $data_result[0]['faq_title'];
            $content['faq_ordering']                  = $data_result[0]['faq_ordering'];
            $content['faq_description']            = $data_result[0]['faq_description'];
        }
        if($_POST){
            $content['faq_id']                     = $faq_id;
            $content['faq_title']                  = $this->input->post('faq_title');
            $content['faq_ordering']                  = $this->input->post('faq_ordering');
            $content['faq_description']            = $this->input->post('faq_description');
        }

        $config = array(

            array(
               'field'   => 'faq_title',
               'label'   => 'faq Name',
               'rules'   => 'trim|required|callback_check_faq_title_edit'
           ),
           array(
               'field'   => 'faq_description',
               'label'   => 'faq Description',
               'rules'   => 'trim|required'
           )
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE){
            $content['layout'] = $view_file;

        }else{



            $save_data = array(
                'faq_title'         => $content['faq_title'],
                'faq_description'   => $content['faq_description'],
                'faq_ordering'   	=> $content['faq_ordering'],
                'faq_created_date'  => $this->faqs->currentDateTime
            );


            $last_id = $this->faqs->save($this->faqs->table_faq,$save_data,$where);

            if($last_id){
                $this->messages->add('You have successfully added a FAQ in your portal.','success');
                redirect('admin/faq/index');
            }else{
                $this->messages->add('error occurred.','error');
                redirect('admin/faq/index');
            }
        }
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }
    public function status(){
        #$this->privileges->check_privileges();

        $sID = $this->input->post('sID');
        $jsonArray=array('flag'=>false);
        $where_array = array(
            'faq_id' => $sID
        );
        $page_update_array = array(
            'faq_is_active' => $this->input->post('sStatus')
        );
        if($this->faqs->save($this->faqs->table_faq,$page_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }

    public function delete(){
        #$this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray = array('flag' => false);
        $where_array = array(
           'faq_id' => $sID
       );
        $page_update_array = array(
            'faq_is_deleted' => '1'
        );
        if($this->faqs->save($this->faqs->table_faq,$page_update_array, $where_array)){
            #echo $this->db->last_query();die;
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }

    function check_faq_title($faq_title){

        $where_array = array(
           'faq_title' => $faq_title,
           'faq_is_deleted' => '0'
        );
        $result = $this->faqs->checkRecord($this->faqs->table_faq,$where_array);

        if ($result){
            $this->form_validation->set_message('check_faq_title', 'The %s field value already exist, please try another.');
            return false;
        }else{
            return true;
        }

    }

    function check_faq_title_edit($faq_title){
        $faq_id = $this->input->post('faq_id');
        $where_array = array(
            'faq_title' => $faq_title,
            'faq_is_deleted' => '0',
            'faq_id !=' => $faq_id
        );
        $result = $this->faqs->checkRecord($this->faqs->table_faq,$where_array);
       if ($result){
            $this->form_validation->set_message('check_faq_title_edit', 'The %s field value already exist, please try another.');
            return false;
        }else{
            return true;
        }
    }

	function check_faq_code($faq_code){
        $where_array = array(
            'faq_code' => $faq_code,
            'faq_is_deleted' => '0'
        );
          $result = $this->faqs->checkRecord($this->faqs->table_faq,$where_array);
       	if ($result){
 			$this->form_validation->set_message('check_faq_code', 'The %s field value already exist, please try another.');
 			return false;
  		}else{
 			return true;
  		}
    }

	function check_faq_code_edit($faq_code){
        $faq_id = $this->input->post('faq_id');
        $where_array = array(
            'faq_code' => $faq_code,
            'faq_is_deleted' => '0',
            'faq_id !=' => $faq_id
        );
        $result = $this->faqs->checkRecord($this->faqs->table_faq,$where_array);
       if ($result){
 			$this->form_validation->set_message('check_faq_code_edit', 'The %s field value already exist, please try another.');
 			return false;
  		}else{
 			return true;
  		}
    }
    public function file_upload(){

        if(isset($_FILES['faq_image']['name']) && $_FILES['faq_image']['name'] != '') {
            $new_image_name = time() .'_'. str_replace(str_split(' ()\\/,:*?"<>|'), '',$_FILES['faq_image']['name']);
            $config['upload_path'] = 'uploads/faqs/';
            $config['allowed_types'] = 'jpg|png|gif|bmp';
            $config['file_name'] = $new_image_name;
            $config['max_size']  = '0';
            $config['max_width']  = '0';
            $config['max_height']  = '0';
            $config['$min_width'] = '0';
            $config['min_height'] = '0';

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('faq_image')){
                $this->form_validation->set_message('file_upload',str_replace(array('<p>', '</p>'),'', $this->upload->display_errors()));
                return false;
            } else {
                $this->upload_data['faq_image'] = $this->upload->data();
                return true;
            }
        }
            $this->form_validation->set_message('file_upload',str_replace(array('<p>', '</p>'),'', "faq Image is required."));
            return false;

    }

}
