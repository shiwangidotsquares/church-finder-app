<?php
defined('BASEPATH') OR exit('No direct access allowed');

class Permission extends MY_Controller {
	var $userData   = array();
	public function __construct() {
		parent::__construct();
		$sessionData= $this->session->userdata(); 
		/*print_r($sessionData['admin_session_data']['role_id']);
		die('jai');*/
		 if(($sessionData['admin_session_data']['role_id'] != 1)){
            $this->messages->add('You do not have permission to access.','error');
            redirect('admin/dashboard');
        }
		$this->load->model('users');
		$this->load->model('Organisation_model','Organisation');
		$this->load->model('Controllers','Controllers');
	}

	public function index(){
		$session_array = $this->session->userdata('user_session_data');
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));

          if(!empty($_POST['message']) && isset($_POST['sendType'])){
        if(!empty($_POST['usersEmail'])){
            if($_POST['sendType']=='email'){
                /* call function into custom helper */   
               send_email($_POST['usersEmail'],$_POST['message']); 
           }  if($_POST['sendType']=='sms'){
               send_sms($_POST['usersEmail'],$_POST['message']);
            }  } else {
             $this->messages->add('Please select atleast one record.','error');
        } }
		$data = array();
		$layout = 'admin-layout';
		$view_file =  'admin/permission/index';
		$index['page_title'] = ':: View Permission Manager ::';
		$content['form_title'] = 'View Permission Manager';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}


	public function add(){

		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js','admin/js/jquery.magicsearch.js'));
        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css','admin/css/jquery.magicsearch.css'));

		$data = array();

		$layout = 'admin-layout';
		$view_file =  'admin/permission/add';
		$index['page_title'] = '::Add Permission Manager ::';
		$content['form_title'] = 'Add Permission Manager ';
		$swhere = array('user_is_active' => '1','user_is_deleted' => '0');
		$content['Organisationdata'] = $this->Organisation->getRecords($this->Organisation->table_organisation,$swhere);
		$this->templates->set($layout);
		$content['layout'] = $view_file;
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
        $content['org_id'] = $this->input->post('org_id');
        $content['assignMethod'] = $this->input->post('assignMethod');
        $where=array('org_id' =>$content['org_id']);

         if(is_array($content['assignMethod'])){
         foreach ($content['assignMethod'] as $key => $value) {
         	$delete_record=$this->Organisation->deleteRecord($this->Organisation->table_permission,$where);
            $save_data=array('org_id' =>$content['org_id'] , 
         					  'controller_id'=>$value	
                );
        }
        $save_record=$this->Organisation->save($this->Organisation->table_permission,$save_data);
             $this->messages->add('You have successfully added price.','success');
            redirect('admin/permission');
             $this->templates->set_data('index',$index);
             $this->templates->set_data('content',$content);
             $this->templates->load();

       }

	}


	public function getOrganisationList(){
     	$jsonArray = array('flag' => false);
        $swhere = array('user_is_active' => '1','user_is_deleted' => '0');
        $jsonArray = $this->Organisation->getRecords($this->Organisation->table_organisation,$swhere,'','','','','','',$select = 'org_id,user_username');
		echo json_encode($jsonArray); exit;
	}

	public function organizationSubscriptionData(){
		$jsonArray=array('flag'=>false);
		$where = '';
    	
    	$where = array('org_id' => $this->input->post('org_id'));
        $Subscribe_Array = $this->Organisation->getRecords($this->Organisation->table_organisation,$where,'','','','','','','org_id,user_username,user_email,organisation_name,user_profile_image');

       if($Subscribe_Array){
    			$cwhere = 'P.org_id='.$this->input->post('org_id');
    			$ControllerArray_selected1 = $this->Organisation->getsubcriptionData($cwhere,'C.controller_id');

				$ControllerArray_selected = array();
				
    			foreach ($ControllerArray_selected1 as $key => $value) {
    				$ControllerArray_selected[] = $value['controller_id'];			  
    			}
    			
    			 $ControllerArray = $this->Organisation->getRecords($this->Organisation->table_controllers);
        
		}
    	
		if(count($Subscribe_Array) > 0){
    		$jsonArray['flag'] = true;
    		$jsonArray['Subscribe_Array']=$Subscribe_Array[0];
    		$jsonArray['ControllerArray']=$ControllerArray;
    		$jsonArray['ControllerArray_selected']=$ControllerArray_selected;
    	}
    	echo json_encode($jsonArray); exit;
	}
}