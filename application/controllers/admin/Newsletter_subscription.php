<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Newsletter_subscription extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('users');
        $this->load->model('newsletters');
        $this->load->model('privileges','privilege');
    }


    /**
     * this is a generic function to get data from model home
     * function having no parameter
     * function using index view at location admin/dashboard
     * @access public
    */

	public function index(){
        $this->privileges->check_privileges();
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));

        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));

        //$this->privilege->check_privileges();
        $data = array();
        $layout = 'admin-layout';
        $view_file =  'admin/newsletter/index';
        $index['page_title'] = ':: View Newsletter Subscription ::';
        $content['form_title'] = 'View Newsletter Subscription';
        $this->templates->set($layout);
        $content['layout'] = $view_file;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }


    public function get_subscription(){
        //$this->privilege->check_privileges();
        $order_by = array();
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        if(empty($length)){
            $length = 10;
            $start = 0;
        }
        $columnData = array(
            'sr_no',
            'first_name',
            'user_email',
            'company_name',
            'information_about',
            'receive_information_by',
            'sbuscribe_status',
            'subscription_created_time'
        );
        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
        $searchData = $this->input->post('searchBox');
        $where = '';
        $and=' and ';

        if($searchData){
            $where.= '(first_name like "%'.$searchData.'%" OR last_name like "%'.$searchData.'%" OR company_name like "%'.$searchData.'%" OR user_email like "%'.$searchData.'%" OR information_about like "%'.$searchData.'%" OR receive_information_by like "%'.$searchData.'%" OR key_code like "%'.$searchData.'%" OR sbuscribe_status like "%'.$searchData.'%")';
        }

        $List_Array = $this->newsletters->getNewslettersData($where,$select = '*',$order_by, $start, $length,$where_in = false,$where_not_in = false);

        $companyList = $List_Array['data'];

        $totalData = $List_Array['total'];
        $jsonArray=array(
            'draw'=>$this->input->post('draw'),
            'recordsTotal'=>$totalData,
            'recordsFiltered'=>$totalData,
            'data'=>array(),
        );
        foreach($companyList as $key => $val){
            $information_about      = json_decode($val['information_about']);
            $receive_information_by = json_decode($val['receive_information_by']);
            $full_name = $val['first_name']?ucfirst($val['first_name'])." ".ucfirst($val['last_name']):'';
            $jsonArray['data'][] = array(
                'sr_no'                     => $start + $key + 1,
                'first_name'                => $full_name,
                'user_email'                => $val['user_email']?$val['user_email']:'',
                'company_name'             => $val['company_name']?$val['company_name']:'',
                'information_about'         => $information_about,
                'receive_information_by'    => $receive_information_by,
                'sbuscribe_status'          => ($val['sbuscribe_status'] == '1')?'Active':'Inactive',
                'subscription_created_time' => $val['subscription_created_time']?$val['subscription_created_time']:''
            );
        }

        echo json_encode($jsonArray); exit;

    }
    public function send_email(){
        $this->privileges->check_privileges();
        $data = array();
        $customers_array = array();
        $layout = 'admin-layout';
        $view_file =  'admin/newsletter/send_email';
        $index['page_title'] = ':: Send Email ::';
        $content['form_title'] = 'Send Email';
        $this->templates->set($layout);
        $content['layout'] = $view_file;

        $path = '../../../assets/js/ckfinder';
        $width = '1300px';
        parent::editor($path,$width);
        $customers_array = $this->newsletters->getRecords($this->newsletters->table_newsletter);
        $content['customers_array'] = $customers_array;
        $content['userlist']               = $this->input->post('userlist');
        $content['message_body']              = $this->input->post('message_body');

        $config = array(

            array(
                'field'   => 'userlist',
                'label'   => 'User',
                'rules'   => 'required'
            ),
            array(
                'field'   => 'message_body',
                'label'   => 'Message Body',
                'rules'   => 'required'
            )

        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE){
            $content['layout'] = $view_file;

        }else{

            foreach ($content['userlist'] as $key => $val) {
                $where_arr = array('newsletter_id' => $val);
                $newsletter_array = $this->newsletters->getRecords($this->newsletters->table_newsletter,$where_arr);

                if($newsletter_array){
                    $message = $content['message_body'];
                    $mail_to = $newsletter_array[0]['user_email'];
                    $subject = 'Newsletter Subscription';
                    $mail_cc = '';
                    $mail_bcc = '';
                    $sender_email = ADMIN_EMAIL;
                    $sender_name = ADMIN_NAME;
                    $attachUrl = '';

                    $body = array('message' => $message,'name' => $newsletter_array[0]['first_name']);
                    /* Send Inquiry in mail */
                    #$messagebody = $this->parser->parse('emailer/newslatter-email', $body, true);

                    $message_data = get_email_body($template_id=13,$body);
                    $messagebody=$message_data['body'];

                    $custom_mail_queue_array[$key] = array(
                        'mail_to' => $mail_to,
                        'subject' => $subject,
                        'messagebody' => $messagebody,
                        'mail_cc' => json_encode($mail_cc),
                        'mail_bcc' => json_encode($mail_bcc),
                        'sender_email' => $sender_email,
                        'sender_name' => $sender_name,
                        'attachUrl' => json_encode($attachUrl)
                    );
                }
            }

            if(!empty($custom_mail_queue_array)){
               $switch = $this->users->save_batch($this->users->table_custom_mail_queue,$custom_mail_queue_array);
            }

            if($switch){
                $this->messages->add('Email sent to selected users successfully.','success');
                redirect('admin/newsletter_subscription/index');
            }else{
                $this->messages->add('error occered.','error');
                redirect('admin/newsletter_subscription/index');
            }
        }

        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }

}

/* End of file newsletter.php */
/* Location: ./application/controllers/newsletter.php */