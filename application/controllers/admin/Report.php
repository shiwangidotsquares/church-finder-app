<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Report extends MY_Controller {

	function __construct(){
        parent::__construct();
        if(!$this->session->userdata('admin_session_data')){
            redirect(site_url('admin/login'));
        }
        $this->load->model('users','obj_user');
        $this->load->model('course_model','course');
        $this->load->model('questions_model','questions');
		$this->load->model('staff_model','staff');
        $this->load->model('quiz_model','quiz');
		$this->load->model('reports','obj_report');
    }

	public function index(){
	    $this->privileges->check_privileges();
		$layout = 'admin-layout';
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));

        $index = array();
        $footer = array();
        $content = array();
        $this->templates->set($layout);

		$where = 'C.course_is_deleted = "0" AND O.order_status = 1'; #AND UC.user_id='.$this->session_array['user_id']
        $order_by = array('C.course_name','ASC');
        $List_Array = $this->course->get_user_course($where,$select = 'C.course_name,C.course_id',$order_by, '', '',$where_in = false,$where_not_in = false);
        $courseList = $List_Array['data'];

        $where = 'U.user_is_deleted = "0" ';#AND S.company_id='.$this->session_array['user_id']
        $order_by = array('staff_name','ASC');
        $List_Array = $this->staff->getallStaff($where,$select = 'S.staff_name,S.user_id',$order_by, '', '',$where_in = false,$where_not_in = false);
        $staffList = $List_Array['data'];
		$path = '../../../assets/js/ckfinder';
        $width = '800px';
        parent::editor($path,$width);
        $view_file =  'admin/report/quiz-reports';
        $content['courseList'] = $courseList;
		$content['staffList'] = $staffList;
        $content['layout'] = $view_file;
		#save_user_log('13',$this->session_array['user_id']);
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}

    public function get_quiz_reports(){
        //$this->privilege->check_privileges();
		$order_by = array();
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        if(empty($length)){
            $length = 10;
            $start = 0;
        }
        $columnData = array(
            'sr_no',
			'staff_name',
            'course_name',
            'video_title',
            'obtained_marks',
            'order_created_time',
			'order_valid_till',
			'action'
        );
        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
		$course_id = $this->input->post('course_id');
		#$user_id = $this->input->post('user_id');
        $searchData = $this->input->post('searchBox');

		$user_type = $this->input->post('user_type');
		$company_user_id = $this->input->post('company_user_id');
		$indidual_user_id = $this->input->post('indidual_user_id');
		$staff_user_id = $this->input->post('staff_user_id');

        $where = '';
        $where .= 'CO.course_is_deleted = "0" AND O.order_status = 1 ';#AND C.user_id='.$this->session_array['user_id']
        $and=' AND ';
		if($course_id){
			$where.= $and.'M.course_id ="'.$course_id.'"';
		}
		/*
		if($user_type && $staff_user_id){
			$where.= $and.'(U.role_id = "4")';
			$and=' AND ';
		}elseif($user_type){
			$where.= $and.'(U.role_id = "'.$user_type.'")';
			$and=' AND ';
		}
		*/
		if($user_type){
			if($company_user_id && $staff_user_id){
				$where.= $and.'(M.user_id = "'.$staff_user_id.'")';
				$and=' AND ';
			}

			if($indidual_user_id){
				$where.= $and.'(M.user_id = "'.$indidual_user_id.'")';
				$and=' AND ';
			}
		}


		/*
		if($user_id){
			$where.= $and.'M.user_id ="'.$user_id.'"';
		}
		*/

        if($searchData){
            $where.= $and.'(CO.course_name like "%'.$searchData.'%" OR S.staff_name like "%'.$searchData.'%" OR M.video_title like "%'.$searchData.'%")';
        }
		$select = 'false';

		$List_Array = $this->obj_report->get_quiz_reports($where,$select,$order_by, $start, $length,$where_in = false,$where_not_in = false, $is_admin = true);
        #prd($List_Array);
        $staffList = $List_Array['data'];

        $totalData = $List_Array['total'];
        $jsonArray=array(
            'draw'=>$this->input->post('draw'),
            'recordsTotal'=>$totalData,
            'recordsFiltered'=>$totalData,
            'data'=>array(),
        );
        foreach($staffList as $key => $val){
			$obtained_marks = 0;
            $email = '<a class="action email" data-toggle="modal" href="#myModalemail" user_id="'.$val['user_id'].'" user_email="'.$val['user_email'].'" staff_name="'.$val['staff_name'].'" title="Send Eamil"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>';
			$restore = '<a class="action restoreQuiz" href="javascript:void(0);" user_id="'.$val['user_id'].'" quiz_id="'.$val['quiz_id'].'" course_id="'.$val['course_id'].'" miniquiz_report_id="'.$val['miniquiz_report_id'].'" title="Restore Progress"><i class="fa fa-window-restore restoreQuiz" aria-hidden="true"></i></a>';

			$final_test = 'N/A';
			$download_cert = 'N/A';
			$where_check_array = array(
                'user_id' => $val['user_id'],
                'quiz_id' => $val['quiz_id']
            );
            $switch_final = $this->obj_user->checkRecord($this->obj_user->table_quiz_reports,$where_check_array);
            if($switch_final){
				$where_get_array = array(
					'user_id' => $val['user_id'],
					'quiz_id' => $val['quiz_id']
				);
				$final_report_data = $this->obj_user->getRecords($this->obj_user->table_quiz_reports,$where_check_array);
				if(!empty($final_report_data)){
					$final_test = isset($final_report_data[0]['total_mark_obtained'])?$final_report_data[0]['total_mark_obtained'].'%':'N/A';
					if(isset($final_report_data[0]['result']) && $final_report_data[0]['result'] == 'Pass'){
						$restore = '';
						$cerificate_pdf_name = isset($final_report_data[0]['cerificate_pdf_name'])?$final_report_data[0]['cerificate_pdf_name']:'';
						$download_cert = '<a href="'.base_url().'admin/report/download_certificate/'.$cerificate_pdf_name.'"><i class="fa fa-cloud-download" aria-hidden="true"></i></a>';
					}
				}
			}
			if($val['is_completed'] == 0 && $val['attach_quiz_out']){
					$restore = '';
			}
			if($val['attach_quiz_out'] && $val['is_completed']){
				$obtained_marks = '100%';
			}else{
				$obtained_marks = $val['obtained_marks'].'%';
				if($val['obtained_marks'] == 0){
					$restore = '';
				}
			}
            $jsonArray['data'][] = array(
                'sr_no' => $start + $key + 1,
                'staff_name' => $val['staff_name']?ucfirst($val['staff_name']):'---',
                'course_name' => $val['course_name']?$val['course_name']:'---',
                'video_title' => $val['video_title'],
				'obtained_marks' => $obtained_marks,
				'final_test' => $final_test,
				'download_cert' => $download_cert,
				'order_created_time' => show_datetime($val['order_created_time']),
				'order_valid_till' => show_datetime($val['order_valid_till']),
                'action' => $email.'&nbsp'.$restore
            );
        }

        echo json_encode($jsonArray); exit;
        echo $this->input->post('draw'); exit;
    }

    public function download_certificate($file){
        $filename = $file;
        $path = "uploads/certificate/";
        if(file_exists($path.$filename))
        {
            $ext= explode('.',$filename);
            $ext = $ext[count($ext)-1]; #echo $ext; exit;
            header('Content-Transfer-Encoding: binary');  // For Gecko browsers mainly
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($path)) . ' GMT');
            header('Accept-Ranges: bytes');  // Allow support for download resume
            header('Content-Length: ' . filesize($path.$filename));  // File size
            header('Content-Encoding: none');
            header('Content-Type: application/'.$ext);  // Change the mime type if the file is not PDF
            header('Content-Disposition: attachment; filename='.$filename);  // Make the browser display the Save As dialog
            #ob_clean(); #THIS!
            flush();
            readfile($path.$filename);  // This is necessary in order to get it to actually download the file, otherwise it will be 0Kb
            //@unlink($filename);
            exit;
        }
    }

	public function restote_all(){
		$user_id = $this->input->post('user_id');
		$course_id = $this->input->post('course_id');
		$quiz_id = $this->input->post('quiz_id');
		$miniquiz_report_id = $this->input->post('miniquiz_report_id');
		$update_progress_data = array(
			'obtained_marks' => 0,
			'is_completed' => 0,
			'updated_time' => ''
		);
		$where_array = array(
			'user_id' => $user_id,
			'course_id' => $course_id,
			'quiz_id' => $quiz_id
		);
		$flag = $this->obj_user->save($this->obj_user->table_miniquiz_report,$update_progress_data,$where_array);
		if($flag){
			echo 'true';
		}
	}

	public function restote_perticular(){
		$user_id = $this->input->post('user_id');
		$course_id = $this->input->post('course_id');
		$quiz_id = $this->input->post('quiz_id');
		$miniquiz_report_id = $this->input->post('miniquiz_report_id');
		$update_progress_data = array(
			'obtained_marks' => 0,
			'is_completed' => 0,
			'updated_time' => ''
		);
		$where_array = array(
			'miniquiz_report_id' => $miniquiz_report_id
		);
		$flag = $this->obj_user->save($this->obj_user->table_miniquiz_report,$update_progress_data,$where_array);
		if($flag){
			echo 'true';
		}
	}

	public function send_email(){
		$to_email 			= $this->input->post('to_email');
		$cc_email 			= $this->input->post('cc_email');
		$bcc_email			= $this->input->post('bcc_email');
		$subject_email 		= $this->input->post('subject_email');
		$editor1 			= $this->input->post('editor1');
		$staff_user_id 		= $this->input->post('staff_user_id');
		$staff_name 		= $this->input->post('staff_name');
		$msg_text = trim($editor1);
		if(!empty($to_email) && !empty($subject_email) && !empty($msg_text)){
			$mail_to = $to_email;
			$subject = $subject_email;
			$mail_cc = $cc_email;
			$mail_bcc = $bcc_email;
			$sender_email = $this->session_array['user_email'];
			$sender_name = $this->session_array['first_name'];

			$body = array('user-message' => trim($msg_text), 'name' => $sender_name);
			/* Send Inquiry in mail */
			#$messagebody = $this->parser->parse('emailer/report-email', $body, true);
            $message_data = get_email_body_user($template_id=12,$body);
            $messagebody=$message_data['body'];
			$flag = $this->sendmail($mail_to, $subject, $messagebody, $mail_cc, $mail_bcc, $sender_email, $sender_name, $attachement_array = '');
			$this->messages->add('You have successfully sent an email to '.$staff_name,'success');
            redirect('admin/report/index');
		}else{
			$this->session->set_flashdata('error_message', 'Email not sent! Please provide all the details except CC or BCC');
            redirect('admin/report/index');
		}
	}

	public function courseprogress(){
		$this->privileges->check_privileges();
		$layout = 'admin-layout';
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));

        $index = array();
        $footer = array();
        $content = array();
        $this->templates->set($layout);

		$where = 'C.course_is_deleted = "0" AND O.order_status = 1';#AND UC.user_id='.$this->session_array['user_id']
        $order_by = array('C.course_name','ASC');
        $List_Array = $this->course->get_user_course($where,$select = 'C.course_name,C.course_id',$order_by, '', '',$where_in = false,$where_not_in = false);
        $courseList = $List_Array['data'];

        $where = 'U.user_is_deleted = "0"';#AND S.company_id='.$this->session_array['user_id']
        $order_by = array('staff_name','ASC');
        $List_Array = $this->staff->getallStaff($where,$select = 'S.staff_name,S.user_id',$order_by, '', '',$where_in = false,$where_not_in = false);
        $staffList = $List_Array['data'];

		$path = '../../../assets/js/ckfinder';
        $width = '800px';
        parent::editor($path,$width);

        $view_file =  'admin/report/course-reports';
        $content['courseList'] = $courseList;
		$content['staffList'] = $staffList;
        $content['layout'] = $view_file;
		#save_user_log('14',$this->session_array['user_id']);
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}

	public function get_course_reports(){
        //$this->privilege->check_privileges();
		$order_by = array();
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        if(empty($length)){
            $length = 10;
            $start = 0;
        }
        $columnData = array(
            'sr_no',
			'staff_name',
            'course_name',
            'video_title',
            'obtained_marks',
            'order_created_time',
			'order_valid_till',
			'action'
        );
        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
		$course_id = $this->input->post('course_id');
		$user_id = $this->input->post('user_id');
        $searchData = $this->input->post('searchBox');
        $where = '';
        $where .= 'CO.course_is_deleted = "0" AND O.order_status = 1 ';#AND UC.company_user_id = '.$this->session_array['user_id'].' AND C.user_id='.$this->session_array['user_id']
        $and=' AND ';
		if($course_id){
			$where.= $and.'UC.course_id ="'.$course_id.'"';
		}

		if($user_id){
			$where.= $and.'S.user_id ="'.$user_id.'"';
		}

        if($searchData){
            $where.= $and.'(CO.course_name like "%'.$searchData.'%" OR S.staff_name like "%'.$searchData.'%")';
        }
		$select = 'false';

		$List_Array = $this->obj_report->get_course_reports($where,$select,$order_by, $start, $length,$where_in = false,$where_not_in = false, $is_admin = true);

        $staffList = $List_Array['data'];
		#prd($staffList);
        $totalData = $List_Array['total'];
        $jsonArray=array(
            'draw'=>$this->input->post('draw'),
            'recordsTotal'=>$totalData,
            'recordsFiltered'=>$totalData,
            'data'=>array(),
        );
        foreach($staffList as $key => $val){
			$email = '';
			$email = '<a class="action email" data-toggle="modal" href="#myModalemail" user_id="'.$val['user_id'].'" user_email="'.$val['user_email'].'" staff_name="'.$val['staff_name'].'" title="Send Eamil"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>';
			$download_cert = 'N/A';
			$where_check_array = array(
                'user_id' => $val['user_id'],
                'quiz_id' => $val['quiz_id']
            );
            $switch_final = $this->obj_user->checkRecord($this->obj_user->table_quiz_reports,$where_check_array);
            if($switch_final){
				$where_get_array = array(
					'user_id' => $val['user_id'],
					'quiz_id' => $val['quiz_id']
				);
				$final_report_data = $this->obj_user->getRecords($this->obj_user->table_quiz_reports,$where_check_array);
				if(!empty($final_report_data)){
					$final_test = isset($final_report_data[0]['total_mark_obtained'])?$final_report_data[0]['total_mark_obtained'].'%':'N/A';
					if(isset($final_report_data[0]['result']) && $final_report_data[0]['result'] == 'Pass'){
						$restore = '';
						$cerificate_pdf_name = isset($final_report_data[0]['cerificate_pdf_name'])?$final_report_data[0]['cerificate_pdf_name']:'';
						$download_cert = '<a href="'.base_url().'admin/report/download_certificate/'.$cerificate_pdf_name.'"><i class="fa fa-cloud-download" aria-hidden="true"></i></a>';
					}
				}
			}


			$avg_progress = 0;
			if($val['avg_progress']){
				$avg_progress = $val['avg_progress'];
			}
            $jsonArray['data'][] = array(
                'sr_no' => $start + $key + 1,
                'staff_name' => $val['staff_name']?ucfirst($val['staff_name']):'---',
                'course_name' => $val['course_name']?$val['course_name']:'---',
				'avg_progress' => $avg_progress."%",
				'download_cert' => $download_cert,
				'order_created_time' => show_datetime($val['order_created_time']),
				'order_valid_till' => show_datetime($val['order_valid_till']),
                'action' => $email
            );
        }

        echo json_encode($jsonArray); exit;
        echo $this->input->post('draw'); exit;
    }

	public function send_reminder_email(){
		$this->privileges->check_privileges();
		$to_email 			= $this->input->post('to_email');
		$cc_email 			= $this->input->post('cc_email');
		$bcc_email			= $this->input->post('bcc_email');
		$subject_email 		= $this->input->post('subject_email');
		$editor1 			= $this->input->post('editor1');
		$staff_user_id 		= $this->input->post('staff_user_id');
		$staff_name 		= $this->input->post('staff_name');
		$msg_text = trim($editor1);
		if(!empty($to_email) && !empty($subject_email) && !empty($msg_text)){
			$mail_to = $to_email;
			$subject = $subject_email;
			$mail_cc = $cc_email;
			$mail_bcc = $bcc_email;
			$sender_email = $this->session_array['user_email'];
			$sender_name = $this->session_array['first_name'];

			$body = array('user-message' => trim($msg_text), 'name' => $sender_name);
			/* Send Inquiry in mail */

			#$messagebody = $this->parser->parse('emailer/report-email', $body, true);

            $message_data = get_email_body_user($template_id=12,$body);
            $messagebody=$message_data['body'];

			$flag = $this->sendmail($mail_to, $subject, $messagebody, $mail_cc, $mail_bcc, $sender_email, $sender_name, $attachement_array = '');
			$this->messages->add('You have successfully sent an email to '.$staff_name,'success');
            redirect('admin/report/courseprogress');
		}else{
			$this->session->set_flashdata('error_message', 'Email not sent! Please provide all the details except CC or BCC');
            redirect('admin/report/courseprogress');
		}
	}

	public function get_company_user(){
		$where_array = array(
			'user_is_active' => '1',
			'user_is_deleted' => '0',
			'role_id' => 2
		);
		$company_user_array = $this->obj_user->getRecords($this->obj_user->table,$where_array);
		$jsonArray = array();
        foreach ($company_user_array as $key => $val) {
            $jsonArray[] = array
                (
                'user_id' => $val['user_id'],
                'full_name' => $val['first_name'].' '.$val['last_name'],
            );
        }
        echo json_encode($jsonArray);
        exit;
	}

	public function get_individual_user(){
		$where_array = array(
			'user_is_active' => '1',
			'user_is_deleted' => '0',
			'role_id' => 3
		);
		$individual_user_array = $this->obj_user->getRecords($this->obj_user->table,$where_array);
		$jsonArray = array();
        foreach ($individual_user_array as $key => $val) {
            $jsonArray[] = array
                (
                'user_id' => $val['user_id'],
                'full_name' => $val['first_name'].' '.$val['last_name'],
            );
        }
        echo json_encode($jsonArray);
        exit;
	}

	public function get_stff_user(){
		$company_user_id = $this->input->post('company_user_id');
        $where = 'U.user_is_deleted = "0" AND U.user_is_active = "1" AND S.company_id='.$company_user_id;
        $List_Array = $this->staff->getallStaff($where,$select = 'S.staff_name,S.staff_id,U.first_name,U.last_name,U.user_id',$order_by = array('S.staff_name','ASC'), false, false,$where_in = false,$where_not_in = false);
        $staffList = $List_Array['data'];
		$jsonArray = array();
        foreach ($staffList as $key => $val) {
            $jsonArray[] = array
                (
                'user_id' => $val['user_id'],
                'full_name' => $val['staff_name']
            );
        }
        echo json_encode($jsonArray);
        exit;
	}
}

/* End of file report.php */
/* Location: ./application/controllers/report.php */