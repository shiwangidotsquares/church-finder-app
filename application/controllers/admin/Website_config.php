<?php
defined('BASEPATH') OR exit('No direct access allowed');

class Website_config extends MY_Controller {

 	var $userData   = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('site_model');
        $this->load->model('privileges');
    }

	public function index(){
	   $this->privileges->check_privileges();
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css',
            'admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
		$data = array();
		$layout = 'admin-layout';
		$view_file =  'admin/website-config/index';
		$index['page_title'] = ':: Website Configurations ::';
		$content['form_title'] = 'Website Configurations';
		$this->templates->set($layout);
		$content['layout'] = $view_file;
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}

	public function get_website_config(){
		$order_by = array();
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        $columnData = array(
            'sr_no',
            'config_key',
            'config_label',
            'action'
        );
        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
        $searchData = $this->input->post('searchBox');
        $where = '';
        $and=' AND ';
        $where.= 'config_is_deleted = 0 ';
        if($searchData){
            $where.= $and.'(config_label like "%'.$searchData.'%" OR config_key like "%'.$searchData.'%")';
        }
        $countryList_Array = $this->site_model->getData($this->site_model->table_site_config,$where,"",$order_by,"","",$length,$start);

        $countryList = $countryList_Array['data'];
        $totalData = $countryList_Array['total'];
        $jsonArray=array(
            'draw'=>$this->input->post('draw'),
            'recordsTotal'=>$totalData,
            'recordsFiltered'=>$totalData,
            'data'=>array(),
        );

        foreach($countryList as $key => $val){

            $active = $val['config_status']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['config_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['config_id'].'" title="Active"></i></a>';

            $edit = '<a href="'.site_url('admin/website_config/edit/'.$val['config_id']).'" rel="'.$val['config_id'].'"><i class="fa fa-edit" title="Edit"></i></a>';

            $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['config_id'].'" title="Delete"></i></a>';


            $jsonArray['data'][] = array(
                'sr_no' => $start + $key + 1,
                'config_key' => $val['config_key']?$val['config_key']:'-----',
                'config_label' => $val['config_label'],

                'action' => $active.'&nbsp;'.$edit.'&nbsp;'.$delete
            );
        }

        echo json_encode($jsonArray); exit;
        echo $this->input->post('draw'); exit;
	}

	public function add(){
        $this->privileges->check_privileges();
		$postData = $this->input->post();
		if(!empty($postData)){
		     $config = array(
                array(
                     'field'   => 'config_key',
                     'label'   => 'Config Key',
                     'rules'   => 'required|callback_check_is_uniqe_edit'
                  ),
                array(
                     'field'   => 'config_value',
                     'label'   => 'Config value',
                     'rules'   => 'required'
                  ),
                array(
                     'field'   => 'config_label',
                     'label'   => 'Config Label',
                     'rules'   => 'required'
                  ),
                );
            $this->form_validation->set_rules($config);

			if ($this->form_validation->run() != FALSE)
	        {
	        	$lastinsertId =  $this->site_model->insertData($this->site_model->table_site_config,$postData);
	        	if(!empty($lastinsertId)){
	        		$this->messages->add("You have successfully added site config.",'success');
                	redirect('admin/website_config/index');
	        	}
	        }
		}

		$layout = 'admin-layout';
        $view_file =  'admin/website-config/add';
        $index['page_title'] = ':: Add website config::';
        $content['form_title'] = 'Add website config';
        $this->templates->set($layout);
        $content['layout'] = $view_file;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
	}

	public function edit($id){
        $this->privileges->check_privileges();
		$postData = $this->input->post();
		if(!empty($postData)){
            $config = array(
                array(
                     'field'   => 'config_key',
                     'label'   => 'Config Key',
                     'rules'   => 'required|callback_check_is_uniqe_edit'
                  ),
                array(
                     'field'   => 'config_value',
                     'label'   => 'Config Value',
                     'rules'   => 'required'
                  ),
                array(
                     'field'   => 'config_label',
                     'label'   => 'Config Label',
                     'rules'   => 'required'
                  ),
                );

            $this->form_validation->set_rules($config);

			if ($this->form_validation->run() != FALSE)
	        {
	        	$lastinsertId =  $this->site_model->updateData($this->site_model->table_site_config,$postData,"config_id = $id");
	        	if(!empty($lastinsertId)){
	        		$this->messages->add("You have successfully updated site config.",'success');
                	redirect('admin/website_config/index');
	        	}
	        }
		}


		$content['olddata'] = $this->site_model->getDataRow($this->site_model->table_site_config,"config_id = $id");

		$layout = 'admin-layout';
        $view_file =  'admin/website-config/edit';
        $index['page_title'] = ':: Edit website config ::';
        $content['form_title'] = ' Edit website config ';
        $this->templates->set($layout);
        $content['layout'] = $view_file;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
	}

	public function delete(){
        $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray=array('flag'=>false);

        $user_update_array = array(
            'config_is_deleted' => 1
        );
        if($this->site_model->updateData($this->site_model->table_site_config,$user_update_array,"config_id = $sID")){
            $jsonArray['flag'] = true;
        }

        echo json_encode($jsonArray); exit;
	}

	public function status(){
        $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray=array('flag'=>false);
        $user_update_array = array(
            'config_status' => $this->input->post('sStatus')
        );
        if($this->site_model->updateData($this->site_model->table_site_config,$user_update_array,"config_id = $sID")){
            $jsonArray['flag'] = true;
        }

        echo json_encode($jsonArray); exit;
	}


    public  function check_is_uniqe_edit(){
        $postData = $this->input->post();
        //pr($postData);
        if(!empty($postData['config_id'])){
        $where = "config_key = '".$postData['config_key']."' and (config_is_deleted = 0) and (config_id != ".$postData['config_id'].")";
        }else{
            $where = "config_key = '".$postData['config_key']."' and (config_is_deleted = 0)";
        }

        $check_already = $this->site_model->getDataCount($this->site_model->table_site_config,$where);

        if($check_already > 0)
        {
            $this->form_validation->set_message('check_is_uniqe_edit', "Please check config key it's already used ");
            return false;
        }
       else{
        return true;
        }
    }
}
