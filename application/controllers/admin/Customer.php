<?php  defined('BASEPATH') OR exit('No direct access allowed');

class Customer extends MY_Controller {
    public function __construct() {
        parent ::__construct();
        $this->load->library('upload');
        $adminlogin = $this->session->userdata('is_admin');
        $this->load->model('customers');
        $this->load->model('category_model','category');
        $this->load->model('privileges','privilege');
    }

    public function index(){
        $session_array = $this->session->userdata('user_session_data');
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
        $data = array();
        $index = array();
        $content = array();
        $layout     = 'admin-layout';
        $view_file  = 'admin/customer/index';
        $index['page_title'] = 'Customer Manager';
        $index['form_title'] = 'View Customer';
        $content['user_info']   = $session_array;
        $this->templates->set($layout);
        $content['layout']      = $view_file;
        $userData    = $this->customers->getRecords($this->customers->table_customer);
        $content['userData']     = $userData;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }


public function get_all(){
    $length     = $this->input->post('length');
    $start      = $this->input->post('start');
    $columnData = array(
           'sr_no',
        'client_name',
        'email_address',
        'address',
        'city',
        'state',
        'pin_code',
        'company_name',
        'company_address',
        'company_url',
        'phone',
        'fax',
        'skype_id',

    );
    $sortData       = $this->input->post('order');
    $order_by[0]    = $columnData[$sortData[0]['column']];
    $order_by[1]    = $sortData[0]['dir'];
    $searchData     = $this->input->post('searchBox');
    $where  = '';
    $where .= 'is_deleted = "0"';
    $and    = ' AND';
    if($searchData){
        $where.= $and.'(client_name like "%'.$searchData.'%" OR email_address like "%'.$searchData.'%")';
    }
    $list = $this->customers->getcustomerData($where, $select = 'A.*', $order_by, $start, $length);
    #echo $this->db->last_query(); die;
    $list_array    = $list['data'];
    $totalData          = $list['total'];
    $jsonArray          = array(
        'draw'              => $this->input->post('draw'),
        'recordsTotal'      => $totalData,
        'recordsFiltered'   => $totalData,
        'data'              => array(),
    );
    foreach($list_array as $key => $val){
    
         $edit = '<a href="' . base_url('admin/customer/edit/' . $val['customer_id']) . '" rel="' . $val['customer_id'] . '"><span class="fa fa-edit"></span></a>';
         $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['customer_id'].'" title="Delete"></i></a>';
        $edit = '<a href="' . base_url('admin/customer/edit/' . $val['customer_id']) . '" rel="' . $val['customer_id'] . '"><span class="fa fa-edit"></span></a>';
        $jsonArray['data'][] = array(
           'sr_no' => $start+$key + 1,
           'client_name'      =>  $val['client_name'],
           'email_address'    =>  $val['email_address'],
           'address'    =>  $val['address'],
           'city'    =>  $val['city'],
           'state'    =>  $val['state'],
           'pin_code'    =>  $val['pin_code'],
           'company_name'      =>  $val['company_name'],
           'company_address'   =>  $val['company_address'],
           'company_url'      =>  $val['company_url'],
           'phone'      =>  $val['phone'],
           'fax'      =>  $val['fax'],
           'skype_id'      =>  $val['skype_id'],
           'action'  => $edit.'&nbsp;'.$delete
       );
    }
    echo json_encode($jsonArray); exit;
}
public function Add(){
    $path = '../../../assets/js/ckfinder';
    $width = '1300px';
    parent::editor($path,$width);
    $index = array();
    $content = array();
    $email_array = array();
    $index['page_title'] = 'Customer Manager';
    $content['form_title'] = 'Edit Customer';
    $content['button_text'] = 'Update Customer';
    $this->templates->set('admin-layout');
    $content['client_name']     = $this->input->post('client_name');
    $content['email_address']   = $this->input->post('email_address');
    $content['address']   = $this->input->post('address');
    $content['city']   = $this->input->post('city');
    $content['state']   = $this->input->post('state');
    $content['pin_code']   = $this->input->post('pin_code');
    $content['company_name']     = $this->input->post('company_name');
    $content['company_address']  = $this->input->post('company_address');
    $content['company_url']     = $this->input->post('company_url');
    $content['phone']     = $this->input->post('phone');    
    $content['fax']     = $this->input->post('fax');
    $content['skype_id']     = $this->input->post('skype_id'); 
    $content['status']     = $this->input->post('status');
    $config = array(
        array(
            'field' => 'client_name',
            'label' => 'Client Name',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'email_address',
            'label' => 'Email Address',
            'rules' => 'trim|required'
        )
    );
    $this->form_validation->set_rules($config);
    if ($this->form_validation->run() == FALSE){
        $content['layout'] = 'admin/customer/add';
    }else{
       
        $saveData = array(
            'client_name' => $content['client_name'],
            'email_address'  => $content['email_address'],
            'address'  => $content['address'],
            'city'  => $content['city'],
            'state'  => $content['state'],
            'pin_code'  => $content['pin_code'],
            'company_name' => $content['company_name'],
            'company_address' => $content['company_address'],
            'company_url' => $content['company_url'],
            'phone' => $content['phone'],
            'fax' => $content['fax'],
            'skype_id' => $content['skype_id']

        );
   
        $this->customers->save($this->customers->table_customer,$saveData);
        $this->messages->add('You have successfully added customer.','success');
        redirect('admin/customer');
    }
    $this->templates->set_data('index',$index);
    $this->templates->set_data('content',$content);
    $this->templates->load();
}
public function edit($id = ''){
    if($id == ""){
       $this->messages->add('No direct access allowed.','error');
       redirect('admin/customer/index');
    }
    $index = array();
    $content = array();
    $path = '../../../assets/js/ckfinder';
    $width = '1300px';
    parent::editor($path,$width);
    $where_array = array(
        'customer_id' => $id
    );
    #prd($where_array);
    $switch = $this->customers->checkRecord($this->customers->table_customer,$where_array);
    if((empty($id) && !is_numeric($id)) && ($switch == false)){
        $this->messages->add('Selected record does not exist in our database!','error');
        redirect(base_url('admin/customer/index'));
    }

    $customer_array = array();
    $index['page_title'] = 'Customer Manager';
    $content['form_title'] = 'Edit customer';
    $content['button_text'] = 'Update Customer';
    $this->templates->set('admin-layout');
    $where_array = array(
        'customer_id' => $id
    );

      $customer_array = $this->customers->getcustomerData($where_array,$select = '*');
      $customer_array = $customer_array['data'];
      $content['customer_id']     = $id;

      if(!empty($customer_array)){
          $content['client_name']         = !empty($customer_array[0]['client_name'])?$customer_array[0]['client_name']:'';
          $content['email_address']         = !empty($customer_array[0]['email_address'])?$customer_array[0]['email_address']:'';
          $content['address']         = !empty($customer_array[0]['address'])?$customer_array[0]['address']:'';
          $content['city']         = !empty($customer_array[0]['city'])?$customer_array[0]['city']:'';
          $content['state']         = !empty($customer_array[0]['state'])?$customer_array[0]['state']:'';
          $content['pin_code']         = !empty($customer_array[0]['pin_code'])?$customer_array[0]['pin_code']:'';
          $content['company_name']         = !empty($customer_array[0]['company_name'])?$customer_array[0]['company_name']:'';
          $content['company_address']         = !empty($customer_array[0]['company_address'])?$customer_array[0]['company_address']:'';
          $content['company_url']         = !empty($customer_array[0]['company_url'])?$customer_array[0]['company_url']:'';
          $content['phone']         = !empty($customer_array[0]['phone'])?$customer_array[0]['phone']:'';
          $content['fax']         = !empty($customer_array[0]['fax'])?$customer_array[0]['fax']:'';
          $content['skype_id']         = !empty($customer_array[0]['skype_id'])?$customer_array[0]['skype_id']:'';
      }
    if($_POST){
        $content['client_name']           = $this->input->post('client_name');
        $content['email_address']     = $this->input->post('email_address');
        $content['address']            = $this->input->post('address');
        $content['city']     = $this->input->post('city');
        $content['state']     = $this->input->post('state');
        $content['pin_code']     = $this->input->post('pin_code');
        $content['company_name']     = $this->input->post('company_name');
        $content['company_address']     = $this->input->post('company_address');
        $content['company_url']     = $this->input->post('company_url');
        $content['phone']     = $this->input->post('phone');
        $content['fax']     = $this->input->post('fax');
        $content['skype_id']     = $this->input->post('skype_id');
    }

    $config = array(
        array(
            'field' => 'client_name',
            'label' => 'Client Name',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'email_address',
            'label' => 'Email Address',
            'rules' => 'trim|required'
        )
    );
    $this->form_validation->set_rules($config);
    if ($this->form_validation->run() == FALSE){
        $content['layout'] = 'admin/customer/add';
    }else{
       
        $saveData = array(
            'client_name' => $content['client_name'],
            'email_address'  => $content['email_address'],
            'address'  => $content['address'],
            'city'  => $content['city'],
            'state'  => $content['state'],
            'pin_code'  => $content['pin_code'],
            'company_name' => $content['company_name'],
            'company_address' => $content['company_address'],
            'company_url' => $content['company_url'],
            'phone' => $content['phone'],
            'fax' => $content['fax'],
            'skype_id' => $content['skype_id']
        );

        $this->customers->save($this->customers->table_customer,$saveData,array('customer_id'=>$id));
        $this->messages->add('You have successfully updated selected customer.','success');
        redirect('admin/customer');
    }
    $this->templates->set_data('index',$index);
    $this->templates->set_data('content',$content);
    $this->templates->load();
}
    public function status(){
        $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray=array('flag'=>false);
        $where_array = array(
            'blog_id' => $sID
        );
        $page_update_array = array(
            'blog_is_active' => $this->input->post('sStatus')
        );
        if($this->blogs->save($this->blogs->table_blog,$page_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }

    public function delete(){
        $sID = $this->input->post('sID');
        $jsonArray = array('flag' => false);
        $where_array = array(
           'customer_id' => $sID
       );
        $page_update_array = array(
            'is_deleted' => '1'
        );
        if($this->customers->save($this->customers->table_customer,$page_update_array, $where_array)){
            #echo $this->db->last_query();die;
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }
    public function file_upload(){
        if(isset($_FILES['blog_image']['name']) && $_FILES['blog_image']['name'] != '') {
            $new_image_name = time() .'_'. str_replace(str_split(' ()\\/,:*?"<>|'), '',$_FILES['blog_image']['name']);
            $config['upload_path'] = 'uploads/blog/';
            $config['allowed_types'] = 'jpg|png|gif|bmp';
            $config['file_name'] = $new_image_name;
            $config['max_size']  = '0';
            $config['max_width']  = '0';
            $config['max_height']  = '0';
            $config['$min_width'] = '0';
            $config['min_height'] = '0';
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('blog_image')){
                $this->form_validation->set_message('file_upload',str_replace(array('<p>', '</p>'),'', $this->upload->display_errors()));
                return false;
            } else {
                $this->upload_data['blog_image'] = $this->upload->data();
                return true;
            }
        }
        if(!$this->input->post('blog_id')){
            $this->form_validation->set_message('file_upload',str_replace(array('<p>', '</p>'),'', "blog Image is required."));
        }
        return false;
    }
}
