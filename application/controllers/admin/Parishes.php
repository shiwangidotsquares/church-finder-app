<?php
defined('BASEPATH') OR exit('No direct access allowed');

class Parishes extends MY_Controller {
  var $userData   = array();
  public function __construct() {
    parent::__construct();
    $this->load->model('privileges');
    $adminlogin = $this->session->userdata('is_admin');
    $this->load->model('parishes_model','parishes');
    $this->load->model('category_model','category');
    $this->load->model('users');
}

public function index(){
  add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
  add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
  $data = array();
  $layout = 'admin-layout';
  $view_file =  'admin/parishes/index';
  $index['page_title'] = ':: View Parishes Directory::';
  $content['form_title'] = 'View Parishes Directory';
  $this->templates->set($layout);
  $content['layout'] = $view_file;
  $this->templates->set_data('index',$index);
  $this->templates->set_data('content',$content);
  $this->templates->load();
}

// Get record from parishes
public function get_parishes(){
      $order_by = array();
      $length = $this->input->post('length');
      $start = $this->input->post('start');
      if(empty($length)){
        $length = 10;
        $start = 0;
    }
$columnData = array(
    'sr_no',
    'parish_name',
    'phone_number',
    'website_url',
    'location',
    'parishes_open_time',
    'parishes_close_time',
    'action',

);
$sortData = $this->input->post('order');
$order_by[0] = $columnData[$sortData[0]['column']];
$order_by[1] = $sortData[0]['dir'];
$searchData = $this->input->post('searchBox');
$where = '';
$where .= 'is_deleted = "0"';
$and=' and ';
if($searchData){
    $where.= $and.'(parish_name like "%'.$searchData.'%"   )';
}
$pageList_Array = $this->parishes->getData('',$where,$select =  '',$order_by, $start, $length,$where_in = false,$where_not_in = false);
$pageList = $pageList_Array['data'];
$totalData = $pageList_Array['total'];
$jsonArray=array(
    'draw'=>$this->input->post('draw'),
    'recordsTotal'=>$totalData,
    'recordsFiltered'=>$totalData,
    'data'=>array(),
);
foreach($pageList as $key => $val){
    

   $active = $val['is_active']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['parishes_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['parishes_id'].'" title="Active"></i></a>';
   $edit = '<a href="'.site_url('admin/parishes/edit/'.$val['parishes_id']).'" rel="'.$val['parishes_id'].'"><i class="fa fa-edit" title="Edit"></i></a>';
   $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['parishes_id'].'" title="Delete"></i></a>';
   $jsonArray['data'][] = array(
    'sr_no' => $start + $key + 1,
    'parish_name' => $val['parish_name']?ucfirst($val['parish_name']):'-----',
    'phone_number' => $val['phone_number'],
    'website_url' => $val['website_url'],
    'location' => $val['location'],
    'parishes_open_time'=>$val['parishes_open_time'],
    'parishes_close_time'=>$val['parishes_close_time'],
    'action' => $active.'&nbsp;'.$edit.'&nbsp;'.$delete
);
}
echo json_encode($jsonArray); exit;
echo $this->input->post('draw'); exit;
}

// Add parishes 
public function add(){

  add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
  add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
  $data = array();

  $layout = 'admin-layout';
  $view_file =  'admin/parishes/add';
  $index['page_title'] = '::Add Parishes Directory ::';
  $content['form_title'] = 'Add Parishes Directory ';
  $this->templates->set($layout);
  $content['layout'] = $view_file;

  $cat_Array = $this->category->getCategoryData(array('category_parent_id'=>2));
  $content['business_directories_category_array'] = $cat_Array['data'];
  
  $path = '../../../assets/js/ckfinder';
  $width = '1300px';
  parent::editor($path,$width);          
  if($_POST){
  
      $content['parish_name']= $this->input->post('parish_name');
      $content['parishes_location']= $this->input->post('parishes_location');
      $content['parishes_phone_number']= $this->input->post('parishes_phone_number');
      $content['parishes_website_url'] = $this->input->post('parishes_website_url');
      $content['opening_time'] = $this->input->post('opening_time');
      $content['closing_time'] = $this->input->post('closing_time');

      $save_data = array(
          'parish_name'=> $content['parish_name'],
          'phone_number' => $content['parishes_phone_number'],
          'website_url' => $content['parishes_website_url'],
          'location' => $content['parishes_location'],
          'is_active' => '1',
          'parishes_open_time' =>$content['opening_time'],
          'parishes_close_time' => $content['closing_time']
      );

     $parishes_id = $this->parishes->save($this->parishes->table_parishes,$save_data);

     if($parishes_id){

        if(isset($_FILES['user_profile_image']) && $_FILES['user_profile_image']['name'] != '') {
        $this->load->library('upload');
        $new_name = time().'_'.$_FILES["user_profile_image"]['name'];
        $config['upload_path'] = './uploads/profile-pic/';
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['max_size']    = 1024*2; //2MB
            $config['file_name'] = $new_name;
            $this->upload->initialize($config);
             $uploadData['file_name'] = $new_name;
             $uploadData['created'] = date("Y-m-d H:i:s");
             $uploadData['modified'] = date("Y-m-d H:i:s");
              $uploadData['parishes_id']=$parishes_id;
             if (!$this->upload->do_upload('user_profile_image')){
                $this->form_validation->set_message('user_profile_image',str_replace(array('<p>', '</p>'),'', $this->upload->display_errors()));
                return false;
            } else {
                 $this->upload_data = $this->upload->data();
                 $this->parishes->save($this->parishes->table_gallery,$uploadData);
                 $this->messages->add('You have successfully added a Parishes in your portal.','success');
                  redirect('admin/parishes/index');
               
            }
        }
       
}


 else{

  $this->messages->add('Error occured during process.','success');
        redirect('admin/parishes/add');
 }

}

$this->templates->set_data('index',$index);
$this->templates->set_data('content',$content);
$this->templates->load();

}



// edit parishes 
public function edit($parishes_id =''){

    if(empty($parishes_id)){
       $this->messages->add('No direct access allowed.','error');
       redirect('admin/parishes/index');
    }
    //check record exist or not
    $record = $this->parishes->getRecords($this->parishes->table_parishes, array('parishes_id'=>$parishes_id));

   if(empty($record)){
       $this->messages->add('No directory found with given id.','error');
       redirect('admin/parishes/index');
    }
    /*$this->privileges->check_privileges();*/
    $data = array();
    add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
    add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
    $layout = 'admin-layout';
    $view_file =  'admin/parishes/edit';
    $index['page_title'] = '::Edit Parishes  ::';
    $content['form_title'] = 'Edit Parishes ';
    $this->templates->set($layout);
    $content['layout'] = $view_file;
    $path = '../../../assets/js/ckfinder';
    $width = '1300px';
    parent::editor($path,$width);
    $where = array('parishes_id' => $parishes_id);

    $userData = $this->users->getRecords($this->parishes->table_parishes,$where);

    //Get files data from database
     $content['gallery'] = $this->parishes->getRows($parishes_id);
     $content['parish_name']= $userData[0]['parish_name'];
      $content['phone_number'] = $userData[0]['phone_number'];
      $content['website_url'] = $userData[0]['website_url'];
      $content['location']= $userData[0]['location'];
      $content['parishes_open_time']= $userData[0]['parishes_open_time'];
      $content['parishes_close_time']= $userData[0]['parishes_close_time'];

    if($_POST){
     $content['parish_name']= $this->input->post('parish_name');
     $content['parishes_phone_number']= $this->input->post('parishes_phone_number');
      $content['parishes_location']= $this->input->post('parishes_location');
      $content['parishes_website_url'] = $this->input->post('parishes_website_url');
      $content['opening_time'] = $this->input->post('opening_time');
      $content['closing_time'] = $this->input->post('closing_time');

      $save_data = array(
          'parish_name'=> $content['parish_name'],
          'phone_number' => $content['parishes_phone_number'],
          'website_url' => $content['parishes_website_url'],
           'parishes_open_time' =>$content['opening_time'],
          ' parishes_close_time' => $content['closing_time']
      );
     
     $where = array('parishes_id' => $parishes_id);
     $update_id = $this->parishes->save($this->parishes->table_parishes,$save_data, $where);


    if($update_id){
            $data = array();
            $filesCount = count($_FILES['upload_Files']['name']);
            $_FILES['upload_File']['name'] = $_FILES['upload_Files']['name']; 
            $_FILES['upload_File']['type'] = $_FILES['upload_Files']['type'];  
            $_FILES['upload_File']['tmp_name'] = $_FILES['upload_Files']['tmp_name']; 
            $_FILES['upload_File']['error'] = $_FILES['upload_Files']['error']; 
            $_FILES['upload_File']['size'] = $_FILES['upload_Files']['size']; 
            $uploadPath = 'uploads/files/'; 
            $config['upload_path'] = $uploadPath; $config['allowed_types'] = 'gif|jpg|png'; 
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->data();
            $file_name=implode(',', $_FILES['upload_Files']['name']);
            $uploadData['file_name'] = $file_name.time();
            $uploadData['created'] = date("Y-m-d H:i:s");
            $uploadData['modified'] = date("Y-m-d H:i:s");
              

               if(!empty($uploadData)){

                //Insert file information into the database
                $insert = $this->parishes->save($this->parishes->table_gallery,$uploadData,$parishes_id);
                $this->messages->add('You have successfully updated a Parishes in your portal.','success');
                 redirect('admin/parishes/index');

                  /*//Get files data from database
                  $data['gallery'] = $this->parishes->getRows();

                 //Pass the files data to view
                 $this->load->view('admin/parishes/index', $data);*/

            } 
  
      }
  }

$this->templates->set_data('index',$index);
$this->templates->set_data('content',$content);
$this->templates->load();
}
public function status(){
    $sID = $this->input->post('sID');
    $jsonArray=array('flag'=>false);
    $where_array = array(
        'parishes_id' => $sID
    );
    $parishes_is_update_array = array(
        'is_active' => $this->input->post('sStatus')
    );
    if($this->parishes->save($this->parishes->table_parishes,$parishes_is_update_array, $where_array)){
        $jsonArray['flag'] = true;
    }
    echo json_encode($jsonArray); exit;
}

public function delete(){
    
    $sID = $this->input->post('sID');
    $jsonArray = array('flag' => false);
    $where_array = array(
     'parishes_id' => $sID
 );
    $page_update_array = array(
        'is_deleted' => '1'
    );
    if($this->parishes->save($this->parishes->table_parishes,$page_update_array, $where_array)){
            
        $jsonArray['flag'] = true;
    }
    echo json_encode($jsonArray); exit;
}



public function image_upload(){
    if(isset($_FILES['business_directories_logo']) && $_FILES['business_directories_logo']['name'] != '') {
        $this->load->library('upload');
        $new_name = time().'_'.$_FILES["business_directories_logo"]['name'];
        $config['upload_path'] = './uploads/business_directories_logos/';
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['max_size']    = 1024*2; //2MB
            $config['file_name'] = $new_name;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('business_directories_logo')){
                $this->form_validation->set_message('business_directories_logos',str_replace(array('<p>', '</p>'),'', $this->upload->display_errors()));
                return false;
            } else {
                $this->upload_data = $this->upload->data();
                return true;
            }
        }
        return true;
    }
}
