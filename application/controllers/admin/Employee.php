<?php  defined('BASEPATH') OR exit('No direct access allowed');

class Employee extends MY_Controller {
    public function __construct() {

        parent ::__construct();
        $this->load->library('upload');
        $adminlogin = $this->session->userdata('is_admin');
        $this->load->model('employees');
      
        $this->load->model('privileges','privilege');
        
    }

    public function index(){
        
        $session_array = $this->session->userdata('user_session_data');
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
        $data = array();
        $index = array();
        $content = array();
        $layout     = 'admin-layout';
        $view_file  = 'admin/employee/index';
        $index['page_title'] = 'Employee Manager';
        $index['form_title'] = 'View Employee';
        $content['user_info']   = $session_array;
        $this->templates->set($layout);
        $content['layout']      = $view_file;
        $userData                = $this->employees->getRecords($this->employees->table_employee);
        $content['userData']     = $userData;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }


public function get_all(){
    $length     = $this->input->post('length');
    $start      = $this->input->post('start');
    $columnData = array(
        'sr_no',
        /*'employee_image',*/
        'first_name',
        'last_name',
        'email',
        'phone_number',
        'department',
        'designation',
        'action'

    );
    $sortData       = $this->input->post('order');
    $order_by[0]    = $columnData[$sortData[0]['column']];
    $order_by[1]    = $sortData[0]['dir'];
    $searchData     = $this->input->post('searchBox');
    $where  = '';
    $where .= 'is_deleted = "0"';
    $and    = ' AND';
    if($searchData){
        $where.= $and.'(first_name like "%'.$searchData.'%" OR last_name like "%'.$searchData.'%" OR email like "%'.$searchData.'%" OR  phone_number like "%'.$searchData.'%" OR department like "%'.$searchData.'%" OR designation like "%'.$searchData.'%")';
    }
    $total = $this->employees->getRecords('employee');

    $list = $this->employees->getRecords('employee',$where,'',$order_by, $length, $start);
    $department_list=array('0'=>'Development','1'=>'Testing','2'=>'Business Analyst','3'=>'Ui Development','4'=>'Human Resource');
    $designation_list=array('0'=>'Leader','1'=>'Manager','2'=>'Project-Cordinator','3'=>'Software-engineer');

    $education_qualification_list=array('0'=>'Post Graduation','1'=>'Under Graduation','2'=>'SSLC','3'=>'PUC II');
    $list_array    = $list;
    $totalData          = $list;
    $jsonArray          = array(
        'draw'              => $this->input->post('draw'),
        'recordsTotal'      => count($total),
        'recordsFiltered'   =>count($total),
        'data'              => array(),
    );
    foreach($list_array as $key => $val){
        $active = $val['status']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['id'].'" title="Active"></i></a>';
         $edit = '<a href="' . base_url('admin/employee/edit/' . $val['id']) . '" rel="' . $val['id'] . '"><span class="fa fa-edit"></span></a>';
         $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['id'].'" title="Delete"></i></a>';
        $edit = '<a href="' . base_url('admin/employee/edit/' . $val['id']) . '" rel="' . $val['id'] . '"><span class="fa fa-edit"></span></a>';
        
        
        $jsonArray['data'][] = array(
           'sr_no' => $start + $key + 1,
           /*'employee_image' => $employee_logo,*/
           'first_name' => $val['first_name'],
           'last_name' => $val['last_name'],
           'email' => $val['email'],
           'department' => $department_list[$val['department']],
           'designation' => $designation_list[$val['designation']],
           'phone_number' => $val['phone_number'],
          
           'action' =>  $active.'&nbsp;'.$edit.'&nbsp;'.$delete
       );
    }
    echo json_encode($jsonArray); exit;
}
public function Add(){
    $path = '../../../assets/js/ckfinder';
    $width = '1300px';
    parent::editor($path,$width);
    $index = array();
    $content = array();
    $email_array = array();
    $index['page_title'] = 'Employee Manager';
    $content['form_title'] = 'Edit Employee';
    $content['button_text'] = 'Update Employee';
    $this->templates->set('admin-layout');
    $content['first_name']     = $this->input->post('first_name');
    $content['last_name']   = $this->input->post('last_name');
    $content['email']   = $this->input->post('email');
    $content['phone_number']   = $this->input->post('phone_number');
    $content['date_of_birth']   = $this->input->post('date_of_birth');
    $content['marital_status']   = $this->input->post('marital_status');
    $content['education_qualification']     = $this->input->post('education_qualification');
    $content['permanent_address']     = $this->input->post('permanent_address');
    $content['temporary_address']  = $this->input->post('temporary_address');
    $content['date_of_joining']     = $this->input->post('date_of_joining');
    $content['department']     = $this->input->post('department');    
    $content['designation']     = $this->input->post('designation');
    $config = array(
        array(
            'field' => 'first_name',
            'label' => 'First Name',
            'rules' => 'trim|required'
        ),
         array(
            'field' => 'last_name',
            'label' => 'Last Name',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'email',
            'label' => 'Email Address',
            'rules' => 'trim|required'
        )
    );
    $this->form_validation->set_rules($config);
    if ($this->form_validation->run() == FALSE){
        $content['layout'] = 'admin/employee/add';
    }else{
        $saveData = array(
            'first_name' => $content['first_name'],
            'last_name'  => $content['last_name'],
            'email'  => $content['email'],
            'phone_number'  => $content['phone_number'],
            'date_of_birth'  => $content['date_of_birth'],
            'marital_status'  => $content['marital_status'],
            'education_qualification' => $content['education_qualification'],
            'permanent_address' => $content['permanent_address'],
            'temporary_address' => $content['temporary_address'],
            'date_of_joining' => $content['date_of_joining'],
            'department' => $content['department'],
            'designation' => $content['designation'],
        );
   
        $this->employees->save($this->employees->table_employee,$saveData);
        $this->messages->add('You have successfully added employee.','success');
        redirect('admin/employee');
    }
    $this->templates->set_data('index',$index);
    $this->templates->set_data('content',$content);
    $this->templates->load();
}
public function edit($id = ''){

    if($id == ""){
       $this->messages->add('No direct access allowed.','error');
       redirect('admin/employee/index');
    }
    //check record exist or not
    $record = $this->employees->checkRecord($this->employees->table_employee, array('id'=>$id));
    if(empty($record)){
       $this->messages->add('No employee found with given id.','error');
       redirect('admin/employee/index');
    }
    $index = array();
    $content = array();

    $where_array = array(
        'id' => $id
    );
    #prd($where_array);
    $switch = $this->employees->checkRecord($this->employees->table_employee,$where_array);
    if((empty($id) && !is_numeric($id)) && ($switch == false)){
        $this->messages->add('Selected record does not exist in our database!','error');
        redirect(base_url('admin/employee/index'));
    }

    $email_array = array();
    $index['page_title'] = 'Employee Manager';
    $content['form_title'] = 'Edit employee';
    $content['button_text'] = 'Update employee';
    $this->templates->set('admin-layout');
    $where_array = array(
        'id' => $id
    );

    $list = $this->employees->getRecords('employee',$where_array);


    $email_array = $list;
    $content['id']     = $id;

    if(!empty($email_array)){
        $content['first_name']         = !empty($email_array[0]['first_name'])?$email_array[0]['first_name']:'';
        $content['last_name']   = !empty($email_array[0]['last_name'])?$email_array[0]['last_name']:'';
        $content['email']          = !empty($email_array[0]['email'])?$email_array[0]['email']:'';
        $content['phone_number']   = !empty($email_array[0]['phone_number'])?$email_array[0]['phone_number']:'';
        $content['date_of_birth']   = !empty($email_array[0]['date_of_birth'])?$email_array[0]['date_of_birth']:'';
        $content['marital_status']   = !empty($email_array[0]['marital_status'])?$email_array[0]['marital_status']:'0';
        $content['education_qualification']   = !empty($email_array[0]['education_qualification'])?$email_array[0]['education_qualification']:'';
        $content['permanent_address']   = !empty($email_array[0]['permanent_address'])?$email_array[0]['permanent_address']:'';
        $content['temporary_address']   = !empty($email_array[0]['temporary_address'])?$email_array[0]['temporary_address']:'';

        $content['date_of_joining']   = !empty($email_array[0]['date_of_joining'])?$email_array[0]['date_of_joining']:'';
        $content['department']   = !empty($email_array[0]['department'])?$email_array[0]['department']:'';
        $content['designation']   = !empty($email_array[0]['designation'])?$email_array[0]['designation']:'';
    }

    if($_POST){
        $content['first_name']     = $this->input->post('first_name');
        $content['last_name']   = $this->input->post('last_name');
        $content['email']   = $this->input->post('email');
        $content['phone_number']   = $this->input->post('phone_number');
        $content['date_of_birth']   = $this->input->post('date_of_birth');
        $content['marital_status']   = $this->input->post('marital_status');
        $content['education_qualification']     = $this->input->post('education_qualification');
        $content['permanent_address']     = $this->input->post('permanent_address');
        $content['temporary_address']  = $this->input->post('temporary_address');
        $content['date_of_joining']     = $this->input->post('date_of_joining');
        $content['department']     = $this->input->post('department');    
        $content['designation']     = $this->input->post('designation');
    }

    $config = array(
        array(
            'field' => 'first_name',
            'label' => 'First Name',
            'rules' => 'trim|required'
        ),
         array(
            'field' => 'last_name',
            'label' => 'Last Name',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required'
        )
    );
    $this->form_validation->set_rules($config);
    if ($this->form_validation->run() == FALSE){
        $content['layout'] = 'admin/employee/add';
    }else{

        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
       
        $saveData['first_name'] =$first_name;
        $saveData['last_name'] =$last_name;
        $saveData['email'] =$email;
        $saveData['phone_number'] =$content['phone_number'];
        $saveData['date_of_birth'] =$content['date_of_birth'];
        $saveData['marital_status'] =$content['marital_status'];
        $saveData['education_qualification'] =$content['education_qualification'];
        $saveData['permanent_address'] =$content['permanent_address'];
        $saveData['temporary_address'] =$content['temporary_address'];
        $saveData['date_of_joining'] =$content['date_of_joining'];
        $saveData['department'] =$content['department'];
        $saveData['designation'] =$content['designation'];
        $this->employees->save('employee',$saveData,array('id'=>$id));
        $this->messages->add('You have successfully updated selected employee.','success');
        redirect('admin/employee');
    }
    $this->templates->set_data('index',$index);
    $this->templates->set_data('content',$content);
    $this->templates->load();
}
    public function status(){
       // $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray=array('flag'=>false);
        $where_array = array(
            'id' => $sID
        );
        $page_update_array = array(
            'status' => $this->input->post('sStatus')
        );
        if($this->employees->save($this->employees->table_employee,$page_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }

    public function delete(){
       // $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray = array('flag' => false);
        $where_array = array(
           'id' => $sID
       );
        $page_update_array = array(
            'is_deleted' => '1'
        );
        if($this->employees->save($this->employees->table_employee,$page_update_array, $where_array)){
            #echo $this->db->last_query();die;
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }
    public function file_upload(){
        if(isset($_FILES['employee_image']['name']) && $_FILES['employee_image']['name'] != '') {
            $new_image_name = time() .'_'. str_replace(str_split(' ()\\/,:*?"<>|'), '',$_FILES['employee_image']['name']);
            $config['upload_path'] = 'uploads/employee/';
            $config['allowed_types'] = 'jpg|png|gif|bmp';
            $config['file_name'] = $new_image_name;
            $config['max_size']  = '0';
            $config['max_width']  = '0';
            $config['max_height']  = '0';
            $config['$min_width'] = '0';
            $config['min_height'] = '0';
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('employee_image')){
                $this->form_validation->set_message('file_upload',str_replace(array('<p>', '</p>'),'', $this->upload->display_errors()));
                return false;
            } else {
                $this->upload_data['employee_image'] = $this->upload->data();
                return true;
            }
        }
        if(!$this->input->post('id')){
            $this->form_validation->set_message('file_upload',str_replace(array('<p>', '</p>'),'', "employee Image is required."));
        }
        return false;
    }
}
