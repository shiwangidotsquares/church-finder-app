<?php
defined('BASEPATH') OR exit('No direct access allowed');

class Dashboard extends My_Controller {

	    public function __construct() {
            parent::__construct();
			//$this->load->model('Privileges','privilege');
            $adminlogin = $this->session->userdata('is_admin');
            $this->load->model('users');
            $this->load->model('Site_model');
        }

	public function index(){
		//$this->privileges->check_privileges();

        $role_title_array = array();
		$data = array();
		$layout = 'admin-layout';
		$view_file =  'admin/dashboard';

		$index['page_title'] = ':: Dashboard ::';

		$content['form_title'] = 'Dashboard';

		add_js(array('admin/js/loadingoverlay.min.js'));

		$this->templates->set($layout);

		$content['layout'] = $view_file;
		
        $this->templates->set_data('index',$index);

		$this->templates->set_data('content',$content);

		$this->templates->load();
	}

}