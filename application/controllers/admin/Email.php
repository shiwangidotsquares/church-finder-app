<?php

defined('BASEPATH') OR exit('No direct access allowed');

class Email extends MY_Controller {

    public function __construct() {
        parent ::__construct();
        $adminlogin = $this->session->userdata('is_admin');
        $this->load->model('emails');
		$this->load->model('company_model','company');
		$this->load->model('privileges','privilege');
    }

    public function index(){
		$this->privileges->check_privileges();
        $session_array = $this->session->userdata('user_session_data');
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));

        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));

        $data = array();
        $index = array();
        $content = array();

		$where = array('U.user_is_active'=>'1','U.user_is_deleted'=>'0');
        $data_c = $this->company->getCompanyDataForCourse($where,$select = 'U.user_id,C.company_id,C.company_name,U.user_email');
        $userData = $data_c['data'];
        $content['dataC'] = $userData;

        $layout     = 'admin-layout';
        $view_file  = 'admin/email/index';
        $index['page_title'] = 'Email Template Manager';
        $index['form_title'] = 'View Template';
        $content['user_info']   = $session_array;
        $this->templates->set($layout);
        $content['layout']      = $view_file;
        $userData                = $this->emails->getRecords($this->emails->table_email);
        $content['userData']     = $userData;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();

    }


public function get_all(){
    $length     = $this->input->post('length');
    $start      = $this->input->post('start');
    $columnData = array(
        'sr_no',
		'id',
        'tpl_name',
        'subject',
        'action'

    );
    $sortData       = $this->input->post('order');

    $order_by[0]    = $columnData[$sortData[0]['column']];
    $order_by[1]    = $sortData[0]['dir'];

    $searchData     = $this->input->post('searchBox');
	$company_user_id     = $this->input->post('company_user_id');

    $where  = '';
    $and    = '';
	if($company_user_id){
		$where.= $and.'company_user_id ="'.$company_user_id.'"';
		$and    .= ' AND ';
	}
    if($searchData){
        $where.= $and.'(subject like "%'.$searchData.'%" OR tpl_name like "%'.$searchData.'%")';
    }

    $list = $this->emails->get_all_temp($where, $select = '*', $order_by, $start, $length);
    #echo $this->db->last_query(); die;
    $list_array    = $list['data'];
    $totalData          = $list['total'];
    $jsonArray          = array(
        'draw'              => $this->input->post('draw'),
        'recordsTotal'      => $totalData,
        'recordsFiltered'   => $totalData,
        'data'              => array(),
    );

    foreach($list_array as $key => $val){

        $edit = '<a href="' . base_url('admin/email/edit/' . $val['id']) . '" rel="' . $val['id'] . '"><span class="fa fa-edit"></span></a>';

        $jsonArray['data'][] = array(
           'sr_no' => $start + $key + 1,
		   'tpl_id' => $val['id'],
           'tpl_name' => $val['tpl_name'],
           'subject' => $val['subject'],
           'action' =>  $edit ,
       );
    }

    echo json_encode($jsonArray); exit;

}

	public function edit($id){
		$this->privileges->check_privileges();
		$path = '../../assets/js/ckfinder';
		$width = '1300px';
		parent::editor($path,$width);

		$where_array = array(
			'id' => $id,
			'is_delete' => 0
		);
		#prd($where_array);
		$switch = $this->emails->checkRecord($this->emails->table_email,$where_array);
		if((empty($id) && !is_numeric($id)) && ($switch == false)){
			$this->messages->add('Selected record does not exist in our database!','error');
			redirect(base_url('admin/email/index'));
		}
		$index = array();
		$content = array();
		$email_array = array();
		$index['page_title'] = 'Email Template Manager';
		$content['form_title'] = 'Edit Template';
		$content['button_text'] = 'Update Email Template';
		$this->templates->set('admin-layout');

		$where = array('U.user_is_active'=>'1','U.user_is_deleted'=>'0');
        $data_c = $this->company->getCompanyDataForCourse($where,$select = 'U.user_id,C.company_id,C.company_name,U.user_email');
        $userData = $data_c['data'];
        $content['dataC'] = $userData;

		$where_array = array(
			'id' => $id
		);
		$email_array = $this->emails->get_all_temp($where_array,$select = '*');

		$email_array = $email_array['data'];
		$content['id']           = $id;

		if($_POST){
			$content['company_user_id'] = $this->input->post('company_user_id');
			$content['tpl_name']    	= $this->input->post('tpl_name');
			$content['subject']     	= $this->input->post('subject');
			$content['body']     		= $this->input->post('body');
		}else{
			if(!empty($email_array)){
				$content['company_user_id'] = !empty($email_array[0]['company_user_id'])?$email_array[0]['company_user_id']:'';
				$content['tpl_name']        = !empty($email_array[0]['tpl_name'])?$email_array[0]['tpl_name']:'';
				$content['subject']         = !empty($email_array[0]['subject'])?$email_array[0]['subject']:'';
				$content['body']            = !empty($email_array[0]['body'])?$email_array[0]['body']:'';
			}
		}

		$config = array(
			array(
				'field' => 'tpl_name',
				'label' => 'Template Name',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'subject',
				'label' => 'Email Subject',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'body',
				'label' => 'Email Body',
				'rules' => 'trim|required'
			)
		);

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE){
			$content['layout'] = 'admin/email/add';
		}else{
			$company_user_id = $this->input->post('company_user_id');
			$tpl_name = $this->input->post('tpl_name');
			$subject = $this->input->post('subject');
			$body = $this->input->post('body');

			$saveData = array(
				'company_user_id' => !empty($company_user_id)?$company_user_id:'1',
				'tpl_name' => $tpl_name,
				'subject' => $subject,
				'body' => stripslashes($body),
			);
			$this->emails->save($this->emails->table_email,$saveData,array('id'=>$id));

			$this->messages->add('You have successfully updated selected email template.','success');
			redirect('admin/email');
		}

		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}


	public function add(){
		#$this->privileges->check_privileges();
		$path = '../../assets/js/ckfinder';
		$width = '1300px';
		parent::editor($path,$width);
		$index = array();
		$content = array();
		$email_array = array();
		$index['page_title'] = 'Add Template Manager';
		$content['form_title'] = 'Add Template';
		$content['button_text'] = 'Add Email Template';
		$this->templates->set('admin-layout');

		$where = array('U.user_is_active'=>'1','U.user_is_deleted'=>'0');
        $data_c = $this->company->getCompanyDataForCourse($where,$select = 'U.user_id,C.company_id,C.company_name,U.user_email');
        $userData = $data_c['data'];
        $content['dataC'] = $userData;

		if($_POST){
			$content['company_user_id'] = $this->input->post('company_user_id');
			$content['tpl_name']    = $this->input->post('tpl_name');
			$content['subject']     = $this->input->post('subject');
			$content['body']     	= $this->input->post('body');
		}

		$config = array(
			array(
				'field' => 'tpl_name',
				'label' => 'Template Name',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'subject',
				'label' => 'Email Subject',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'body',
				'label' => 'Email Body',
				'rules' => 'trim|required'
			)
		);

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE){
			$content['layout'] = 'admin/email/add';
		}else{
			$company_user_id = $this->input->post('company_user_id');
			$tpl_name = $this->input->post('tpl_name');
			$subject = $this->input->post('subject');
			$body = $this->input->post('body');

			$saveData = array(
				'company_user_id' => !empty($company_user_id)?$company_user_id:'1',
				'tpl_name' => $tpl_name,
				'subject' => $subject,
				'body' => stripslashes($body),
			);
			$this->emails->save($this->emails->table_email,$saveData);

			$this->messages->add('You have successfully added email template.','success');
			redirect('admin/email');
		}

		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}


}
