<?php  defined('BASEPATH') OR exit('No direct access allowed');

class Recipes extends MY_Controller {
    public function __construct() {
        parent ::__construct();
        $this->load->library('upload');
        $adminlogin = $this->session->userdata('is_admin');
        $this->load->model('model_recipe','recipe');
        $this->load->model('category_model','category');
        $this->load->model('privileges','privilege');
    }

    public function index(){
        #$this->privileges->check_privileges();
        $session_array = $this->session->userdata('user_session_data');
        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
        $data = array();
        $index = array();
        $content = array();
        $layout     = 'admin-layout';
        $view_file  = 'admin/recipes/index';
        $index['page_title'] = 'Recipe Manager';
        $index['form_title'] = 'View Recipe';
        $content['user_info']   = $session_array;
        $this->templates->set($layout);
        $content['layout']      = $view_file;
        $userData                = $this->recipe->getRecords($this->recipe->table_recipe);
        $content['userData']     = $userData;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }


public function get_all(){
    $length     = $this->input->post('length');
    $start      = $this->input->post('start');
    $columnData = array(
        'sr_no',
        'recipe_title',
        'recipe_created_date',
        'action'

    );
    $sortData       = $this->input->post('order');
    $order_by[0]    = $columnData[$sortData[0]['column']];
    $order_by[1]    = $sortData[0]['dir'];
    $searchData     = $this->input->post('searchBox');
    $where  = '';
    $where .= 'recipe_is_deleted = "0"';
    $and    = ' AND';
    if($searchData){
        $where.= $and.'(recipe_title like "%'.$searchData.'%" OR recipe_description like "%'.$searchData.'%")';
    }
    $list = $this->recipe->getRecipeData($where, $select = '', $order_by, $start, $length);
    #echo $this->db->last_query(); die;
    $list_array    = $list['data'];
    $totalData          = $list['total'];
    $jsonArray          = array(
        'draw'              => $this->input->post('draw'),
        'recordsTotal'      => $totalData,
        'recordsFiltered'   => $totalData,
        'data'              => array(),
    );
    foreach($list_array as $key => $val){
        $active = $val['recipe_is_active']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['recipe_id'].'" title="Inactive"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['recipe_id'].'" title="Active"></i></a>';
         $edit = '<a href="' . base_url('admin/recipes/edit/' . $val['recipe_id']) . '" rel="' . $val['recipe_id'] . '"><span class="fa fa-edit"></span></a>';
         $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['recipe_id'].'" title="Delete"></i></a>';
        $edit = '<a href="' . base_url('admin/recipes/edit/' . $val['recipe_id']) . '" rel="' . $val['recipe_id'] . '"><span class="fa fa-edit"></span></a>';
        $originalDate = $val['recipe_created_date'];
        $recipe_created_date = date("d/m/Y", strtotime($originalDate));
        $jsonArray['data'][] = array(
           'sr_no' => $start + $key + 1,
           'recipe_title' => $val['recipe_title'],
           'recipe_created_date' => $recipe_created_date,
           'action' =>  $active.'&nbsp;'.$edit.'&nbsp;'.$delete
       );
    }
    echo json_encode($jsonArray); exit;
}
public function Add(){

    #$this->privileges->check_privileges();
    $path = '../../../assets/js/ckfinder';
    $width = '1300px';
    parent::editor($path,$width);
    $index = array();
    $content = array();
    $email_array = array();
    $index['page_title'] = 'Recipe Manager';
    $content['form_title'] = 'Edit Recipe';
    $content['button_text'] = 'Update Recipe';
    $this->templates->set('admin-layout');

    $cat_Array = $this->category->getCategoryData(array('category_parent_id'=>16));
    $content['recipe_category_array'] = $cat_Array['data'];

    $ingredients = array();
    $ingredients_quentity = array();
    $ingredients_quentity = $this->input->post('ingredients_quentity');
    if(!empty($ingredients_quentity)){
        $counting = 0;
        foreach ($this->input->post('ingredients_quentity') as $key => $value) {
            $ingredients[$counting]['title'] = $this->input->post('ingredients_title')[$key];
            $ingredients[$counting]['quentity'] = $value;
            $counting++;
        }

    }


    $content['recipe_id']         = '';
    $content['recipe_title']    = $this->input->post('recipe_title');
    $content['recipe_ingredients']     = json_encode($ingredients);
    $content['recipe_prep_time']      = $this->input->post('recipe_prep_time');
    $content['recipe_no_serving']     = $this->input->post('recipe_no_serving');
    $content['recipe_description']     = $this->input->post('recipe_description');
    $content['hidden_recipe_image']     = $this->input->post('hidden_recipe_image');

    $config = array(
        array(
            'field' => 'recipe_title',
            'label' => 'Recipe Title',
            'rules' => 'trim|required'
        )
    );
    $this->form_validation->set_rules($config);
    if ($this->form_validation->run() == FALSE){
        $content['layout'] = 'admin/recipes/add';
    }else{
        $saveData = array(
            'recipe_title' => $content['recipe_title'],
            'org_id'       => 1,
            'recipe_ingredients' => $content['recipe_ingredients'],
            'recipe_no_serving' => $content['recipe_no_serving'],
            'recipe_description' => $content['recipe_description'],
            'recipe_prep_time' => $content['recipe_prep_time'],
            'recipe_no_serving' => $content['recipe_no_serving'],
            'recipe_created_date'  => $this->recipe->currentDateTime
        );
         if($this->input->post('hidden_recipe_image')){
             $saveData['recipe_image'] = $this->input->post('hidden_recipe_image');
        }
        #prd($saveData);
        $this->recipe->save($this->recipe->table_recipe,$saveData);
        $this->messages->add('You have successfully added Recipe.','success');
        redirect('admin/recipes');
    }
    $this->templates->set_data('index',$index);
    $this->templates->set_data('content',$content);
    $this->templates->load();
}
public function edit($id = ''){
    if($id == ""){
       $this->messages->add('No direct access allowed.','error');
       redirect('admin/recipes/index');
    }
    //check record exist or not
    $record = $this->recipe->checkRecord($this->recipe->table_recipe, array('recipe_id'=>$id));
    if(empty($record)){
       $this->messages->add('No recipe found with given id.','error');
       redirect('admin/recipes/index');
    }
    $index = array();
    $content = array();

    #$this->privileges->check_privileges();
    $path = '../../../assets/js/ckfinder';
    $width = '1300px';
    parent::editor($path,$width);

    $cat_Array = $this->category->getCategoryData(array('category_parent_id'=>16));
    $content['recipe_category_array'] = $cat_Array['data'];

    $where_array = array(
        'recipe_id' => $id
    );
    #prd($where_array);
    $switch = $this->recipe->checkRecord($this->recipe->table_recipe,$where_array);
    if((empty($id) && !is_numeric($id)) && ($switch == false)){
        $this->messages->add('Selected record does not exist in our database!','error');
        redirect(base_url('admin/recipes/index'));
    }

    $email_array = array();
    $index['page_title'] = 'Recipe Manager';
    $content['form_title'] = 'Edit Recipe';
    $content['button_text'] = 'Update Recipe';
    $this->templates->set('admin-layout');
    $where_array = array(
        'recipe_id' => $id
    );
    $recipeData = $this->recipe->getRecipeData($where_array,$select = '*');
    #prd($recipeData);
    $recipeData = $recipeData['data'];

    $content['recipe_id']           = $id;

    if(!empty($recipeData)){
        $content['recipe_title']           = $recipeData[0]['recipe_title'];
        $content['recipe_ingredients']     = $recipeData[0]['recipe_ingredients'];
        $content['recipe_prep_time']       = $recipeData[0]['recipe_prep_time'];
        $content['recipe_no_serving']      = $recipeData[0]['recipe_no_serving'];
        $content['recipe_description']     = $recipeData[0]['recipe_description'];
        $content['hidden_recipe_image']    = $recipeData[0]['recipe_image'];
    }

    if($_POST){
        $ingredients = array();
        $ingredients_quentity = $this->input->post('ingredients_quentity');
        if(!empty($ingredients_quentity)){
            $counting = 0;
            foreach ($this->input->post('ingredients_quentity') as $key => $value) {
                $ingredients[$counting]['title'] = $this->input->post('ingredients_title')[$key];
                $ingredients[$counting]['quentity'] = $value;
                $counting++;
            }

        }
        $content['recipe_title']            = $this->input->post('recipe_title');
        $content['recipe_ingredients']     = json_encode($ingredients);
        $content['recipe_prep_time']        = $this->input->post('recipe_prep_time');
        $content['recipe_no_serving']       = $this->input->post('recipe_no_serving');
        $content['recipe_description']      = $this->input->post('recipe_description');
        $content['hidden_recipe_image']     = $this->input->post('hidden_recipe_image');

    }
    $config = array(
        array(
           'field' => 'recipe_title',
           'label' => 'Recipe Title',
           'rules' => 'trim|required'
       )
    );
    $this->form_validation->set_rules($config);
    if ($this->form_validation->run() == FALSE){
        $content['layout'] = 'admin/recipes/add';
    }else{

        $saveData = array(
            'recipe_title' => $content['recipe_title'],
            'recipe_ingredients' => $content['recipe_ingredients'],
            'recipe_no_serving' => $content['recipe_no_serving'],
            'recipe_description' => $content['recipe_description'],
            'recipe_prep_time' => $content['recipe_prep_time'],
            'recipe_no_serving' => $content['recipe_no_serving'],
            'recipe_created_date'  => $this->recipe->currentDateTime
        );
         if($this->input->post('hidden_recipe_image')){
             $saveData['recipe_image'] = $this->input->post('hidden_recipe_image');
        }
        $this->recipe->save($this->recipe->table_recipe,$saveData,array('recipe_id'=>$id));
        $this->messages->add('You have successfully updated selected recipe.','success');
        redirect('admin/recipes');
    }
    $this->templates->set_data('index',$index);
    $this->templates->set_data('content',$content);
    $this->templates->load();
}
    public function status(){
        #$this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray=array('flag'=>false);
        $where_array = array(
            'recipe_id' => $sID
        );
        $page_update_array = array(
            'recipe_is_active' => $this->input->post('sStatus')
        );
        if($this->recipe->save($this->recipe->table_recipe,$page_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }

    public function delete(){
        #$this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray = array('flag' => false);
        $where_array = array(
           'recipe_id' => $sID
       );
        $page_update_array = array(
            'recipe_is_deleted' => '1'
        );
        if($this->recipe->save($this->recipe->table_recipe,$page_update_array, $where_array)){
            #echo $this->db->last_query();die;
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }
    public function file_upload(){
        if(isset($_FILES['recipe_image']['name']) && $_FILES['recipe_image']['name'] != '') {
            $new_image_name = time() .'_'. str_replace(str_split(' ()\\/,:*?"<>|'), '',$_FILES['recipe_image']['name']);
            $config['upload_path'] = 'uploads/recipes/';
            $config['allowed_types'] = 'jpg|png|gif|bmp';
            $config['file_name'] = $new_image_name;
            $config['max_size']  = '0';
            $config['max_width']  = '0';
            $config['max_height']  = '0';
            $config['$min_width'] = '0';
            $config['min_height'] = '0';
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('recipe_image')){
                $this->form_validation->set_message('file_upload',str_replace(array('<p>', '</p>'),'', $this->upload->display_errors()));
                return false;
            } else {
                $this->upload_data['recipe_image'] = $this->upload->data();
                return true;
            }
        }
        if(!$this->input->post('recipe_id')){
            $this->form_validation->set_message('file_upload',str_replace(array('<p>', '</p>'),'', "blog Image is required."));
        }
        return false;
    }
}
