<?php
defined('BASEPATH') OR exit('No direct access allowed');

class Referal extends MY_Controller {
 	var $data   = array();
	public function __construct() {
        parent::__construct();

		$adminlogin = $this->session->userdata('is_admin');
        $this->load->model('referals');
        $this->load->model('privileges','privilege');
    }

	public function index(){
        $this->privileges->check_privileges();
		add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));

		add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));

        //$this->privilege->check_privileges();
		$data = array();
		$layout = 'admin-layout';
		$view_file =  'admin/referal/index';
		$index['page_title'] = ':: View Referals ::';
		$content['form_title'] = 'View Referals';

        $where = 'R.is_partner = 2 AND R.referral_is_deleted = 0';
        $referr_users_list = $this->referals->getReferralUser($where,$select = '*');

        if(!empty($referr_users_list['data'])){
            $content['referr_users_list'] = $referr_users_list['data'];
        }else{

            $content['referr_users_list'] = array();
        }

        $this->templates->set($layout);
		$content['layout'] = $view_file;
		$this->templates->set_data('index',$index);
		$this->templates->set_data('content',$content);
		$this->templates->load();
	}


	public function get_referal(){
        //$this->privilege->check_privileges();
		$order_by = array();
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        if(empty($length)){
            $length = 10;
            $start = 0;
        }
        $columnData = array(
            'sr_no',
            'refer_by_user_id',
            'refer_to_user_id',
            'refer_amount',
            'is_paid',
            'referral_created_time',
            'action'
        );
        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
        $searchData = $this->input->post('searchBox');
        $where = 'R.is_partner = 2 AND R.referral_is_deleted = 0';
        $and=' and ';

        $referal_status = $this->input->post('referal_status');

        if($referal_status){

            $where.= $and.'(R.refer_by_user_id = "'.$referal_status.'")';

        }

        if($searchData){
            $where.= $and.'(R.refer_amount like "%'.$searchData.'%" OR CONCAT(first_name," ",last_name) LIKE "'.$searchData.'")';
        }

        $List_Array = $this->referals->getReferralData($where,$select = '*',$order_by, $start, $length,$where_in = false,$where_not_in = false);

        $companyList = $List_Array['data'];

        $totalData = $List_Array['total'];
        $jsonArray=array(
            'draw'=>$this->input->post('draw'),
            'recordsTotal'=>$totalData,
            'recordsFiltered'=>$totalData,
            'data'=>array(),
        );
        foreach($companyList as $key => $val){

            $active = $val['is_paid']?'<a href="javascript:void(0)"><i class="fa fa-check-square activeRecord" rel="'.$val['referral_id'].'" title="Mark Unpaid"></i></a>':'<a href="javascript:void(0)"><i class="fa fa-ban deactiveRecord" rel="'.$val['referral_id'].'" title="Mark Paid"></i></a>';

             #$edit = '<a href="'.site_url('admin/referal/edit/'.$val['referral_id']).'" rel="'.$val['referral_id'].'"><i class="fa fa-edit" title="Edit"></i></a>';


             $delete = '<a href="javascript:void(0)"><i class="fa fa-trash-o deleteRecord" rel="'.$val['referral_id'].'" title="Delete"></i></a>';

            $where_to = array('user_id'=>$val['refer_to_user_id']);
            $data_to = $this->referals->getRecords($this->referals->table_users,$where_to);
            $refer_to_user_name = ($data_to)?$data_to[0]['first_name']." ".$data_to[0]['last_name']:'';
            $where_by = array('user_id'=>$val['refer_by_user_id']);
            $data_by = $this->referals->getRecords($this->referals->table_users,$where_by);
            $refer_by_user_name = ($data_by)?$data_by[0]['first_name']." ".$data_by[0]['last_name']:'';

            $jsonArray['data'][] = array(
                'sr_no'                     => $start + $key + 1,
                'refer_by_user_id'          => $refer_by_user_name,
                'refer_to_user_id'          => $refer_to_user_name,
                'refer_amount'              => $val['refer_amount']?$val['refer_amount']:'0',
                'is_paid'                   => $val['is_paid']?'Paid':'Unpaid',
                'referral_created_time'     => $val['referral_created_time']?$val['referral_created_time']:'',
                'action'                    => $active.'&nbsp;'.$delete
            );
        }

        echo json_encode($jsonArray); exit;

	}
    public function export_referral() {
        $where = 'R.is_partner = 2 AND R.referral_is_deleted = 0';
        $List_Array = $this->referals->getReferralData($where,$select = '*',$order_by = NULL, $start = 0, $length = 0);

        if ($List_Array) {
            $List_Array = $List_Array['data'];
            $this->load->library('PHPExcel');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setTitle("title")->setDescription("Report");

            // Assign cell values
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Refer By');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Refer To');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Referal Amount');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Referal Date/Time');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Payment Status');

            foreach ($List_Array as $key => $val) {
                $row = $key + 2;

                $where_to = array('user_id'=>$val['refer_to_user_id']);
                $data_to = $this->referals->getRecords($this->referals->table_users,$where_to);
                $refer_to_user_name = ($data_to)?$data_to[0]['first_name']." ".$data_to[0]['last_name']:'';
                $where_by = array('user_id'=>$val['refer_by_user_id']);
                $data_by = $this->referals->getRecords($this->referals->table_users,$where_by);
                $refer_by_user_name = ($data_by)?$data_by[0]['first_name']." ".$data_by[0]['last_name']:'';

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, ucfirst($refer_by_user_name));
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, ucfirst($refer_to_user_name));
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $val['refer_amount']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $val['referral_created_time']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $val['is_paid']);
            }
            header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition:attachment;filename="referralReport.xls"');
            header('Cache-Control: max-age=0');
            //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            //$objWriter->save("nameoffile.xls");
        } exit;
    }
    public function delete(){
        $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray = array('flag' => false);
        $where_array = array(
           'referral_id' => $sID
       );
        $page_update_array = array(
            'referral_is_deleted' => '1'
        );
        if($this->referals->save($this->referals->table_referral,$page_update_array, $where_array)){
            #echo $this->db->last_query();die;
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }
    public function status(){
        $this->privileges->check_privileges();
        $sID = $this->input->post('sID');
        $jsonArray=array('flag'=>false);
        $where_array = array(
            'referral_id' => $sID
        );
        $page_update_array = array(
            'is_paid' => $this->input->post('sStatus')
        );
        if($this->referals->save($this->referals->table_referral,$page_update_array, $where_array)){
            $jsonArray['flag'] = true;
        }
        echo json_encode($jsonArray); exit;
    }


}
