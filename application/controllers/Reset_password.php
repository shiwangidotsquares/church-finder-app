<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Reset_password extends MY_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('users');
    }

    /**
     * this is a generic function to show html to reset password page
     * @param  auth_key
     * @access public
    */
	public function index($auth_key = ''){
        //auth key exist or not
        $where_key = array(
            'auth_key'=>$auth_key,
        );
        $user_details = $this->users->getRecords($this->users->table_users,$where_key);

        if(empty($user_details)){
            $this->messages->add("Link Expired, Please try again!", "error");
            redirect(base_url('admin'));
        }

        $this->templates->set('reset-password-layout');

        $config = array(
            array(
              'field'   => 'new_password',
              'label'   => 'Password',
              'rules'   => 'trim|required|xss_clean'
            ),
            array(
              'field'   => 'confirm_password',
              'label'   => 'Confirm Password',
              'rules'   => 'trim|required|xss_clean|matches[new_password]|min_length[6]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == false){

            $this->templates->load();

        }else{
            $new_password   =   $this->input->post('new_password');
            //cheking user status
            $where_key = array(
                'auth_key'=>$auth_key,
            );
            $user_details = $this->users->getRecords($this->users->table_users,$where_key);

            if(empty($user_details)){
                $this->messages->add("Link Expired, Please try again!", "error");
                redirect(base_url('admin'));
            }

            $response['password'] = $new_password;
            $response['auth_key'] = 0;

            $save_user_data = array(
                'user_password' => generateHash($new_password),
                'auth_key' => 0
            );
            //save function is used to save or update records in a given table
            $last_id = $this->users->save($this->users->table_users,$save_user_data,$where_key);

            if($last_id){

                $this->messages->add('Your password has been changed successfully!.','success');
                redirect(base_url('admin'));

            }else{

                $this->messages->add('error occurred.','error');
                redirect(base_url('admin'));
            }
        }
	}
}
/* End of file Reset_password.php */
/* Location: ./application/controllers/Reset_password.php */