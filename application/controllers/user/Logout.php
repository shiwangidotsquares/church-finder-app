<?php

if (!defined('BASEPATH'))
    exit('No direct access allowed');

/**
 * Logout controller
 */
class Logout extends CI_Controller {

    /**
     * class constructor.
     */
    function __construct() {
        parent::__construct();
        $this->load->model('users', 'user');
    }

    /**
     * Logout controller.
     * unset all session and redirect to login page.
     */
    public function index() {
        $company_slug = 'user/login';
        $this->session->unset_userdata('organization_session_data');
        #$this->session->sess_destroy();
        redirect(site_url($company_slug), 'refresh');
    }

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */