<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Login extends MY_Controller {

    function __construct(){
        parent::__construct();
        if($this->session->userdata('organization_session_data')){
            redirect(base_url('user/dashboard'));
        }
        $this->load->model('users','obj_users');
    }


    /**
     * this is a generic function to show html for login to admin area
     * function having no parameter
     * function using template crm-login
     * function use to login area after successful field validation with captcha
     * function also check block ip address and login attemp before successful login to admin area
     * @access public
    */


    public function index(){
        $this->templates->set('user-login-layout');
        $this->form_validation->set_rules('email', 'Username', 'required|trim|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'required|trim|xss_clean');
        if ($this->form_validation->run() == false)
        {
           
            $this->templates->load();
        }
        else{
            $result = false;
            $login_remember = '';
            $email   =   $this->input->post('email');
            $password   =   $this->input->post('password');
            $users_failed_login_count = 0;
            $result = $this->obj_users->checkOrganization($email,$password,1);
            
            if($result){
                $this->_authenticate($email,$password,$login_remember);
            }else{
                $this->messages->add("Provided credentials does not match! Please try again!", "error");
                redirect(base_url('user/login'));
            }
        }
    }


    /**
     * this is a generic function to check captch if entered captcha by user is valid or not
     * function having single parameter , which is randomly generated captcha value, which have to match with
     * word provided by user in captch field
     * @access public
     * @param  string
     * @return boolean
    */


    public function check_captch($str){
        $word = $this->session->userdata('captchaWord');
        if(strcmp(strtoupper($str),strtoupper($word)) == 0){
            return true;
        }else{
            $this->form_validation->set_message('check_captch', 'Please enter valid security key!');
            return false;
        }
    }


    /**
     * this is a generic function to check for aunthentication to login
     * first parameter having information about user id
     * second parameter having information about password
     * third parameter having information if login information already remembered
     * @access public
     * @param  integer
     * @param  string
     * @param  string
     * @return boolean
    */

    public function _authenticate($users_uid = '',$users_password = '',$login_remember = ''){
        $res = false;
        if(isset($login_remember) && $login_remember != ''){
            setCookies($users_uid,$users_password);
        }
        if(isset($users_uid) && $users_uid != '' && $users_password != '' && isset($users_password)){
            
            $users_password = md5($users_password);
            $whereOR = '';
            $data = $this->obj_users->getRecords($this->obj_users->table_organisation, array('user_username' => $users_uid,'user_password' => $users_password),array('user_username','user_email'));
           

            if(!empty($data)){
              $this->session->set_userdata('organization_session_data',$data[0]['org_id']);
              
                $this->session->set_userdata('last_login_time',strtotime(date('Y-m-d H:i:s',time())));
                $users_last_login = strtotime(date('Y-m-d H:i:s',time()));

                $login_count = $data[0]['user_login_count'];
                $login_count = $login_count + 1;
                $redirect_url = base_url('user/dashboard');
                
               
                 $logindata = array(
                    'user_last_login' => $users_last_login,
                    'user_login_count' => $login_count
                 );
                  $this->obj_users->save($this->obj_users->table_organisation,$logindata,array('user_email' => $this->session->userdata['organization_session_data']['user_email'],'user_password' => $users_password));

                 redirect($redirect_url);
            }else{
                
                $this->messages->add("You do not have permission to access.", "error");
                redirect(base_url('user/login'));
            }

        }else{
             
            $this->messages->add("You do not have permission to access.", "error");
            redirect(base_url('user/login'));
        }

    }
}


/* End of file login.php */
/* Location: ./application/controllers/admin/login.php */
?>