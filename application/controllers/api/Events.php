<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
// Load the Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';


class Events extends REST_Controller {
    public function __construct() {
        parent::__construct();
        // Load the user model
        $this->load->model('api');
        // Load these helper to create JWT tokens
        $this->load->helper(['jwt', 'authorization']);
    }

    
    /**
    * Get Events.
    * @return json encode array that contain flag,message
    * @param  Page ,pageSize
    */

    public function getEvents_post(){
        $userIDArr = $this->verify_request();
        $org_id = $userIDArr->org_id;
        $getEvents = $this->post();
        $required_arr = array('Page','PageSize');

           //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $getEvents)) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val.' field is not valid',
                    'statusCode' => REST_Controller::HTTP_BAD_REQUEST
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$getEvents[$val]) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val . ' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }

        $chk_array = array(
           'org_id' => $org_id
        );
        $getEvents_data = $this->api->getRecords($this->api->table_events,$chk_array);
        $total_getEvents = $this->api->count_record($this->api->table_events,$chk_array);
        if (!empty($getEvents_data)) {
                // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Success',
                'total'=>$total_getEvents,
                'responsePacket' => $getEvents_data
            ], REST_Controller::HTTP_OK);
        } else {
                // Set the response and exit
            $this->response([
                'status' => False,
                'message' =>'No record found',
            ], REST_Controller::HTTP_BAD_REQUEST);

        }
    }

    
    /**
    * Get Events Detail.
    * @return json encode array that contain flag,message
    * @param  eventId
    */

    public function getEventsDetail_post(){
        $userIDArr = $this->verify_request();
        $org_id = $userIDArr->org_id;
        $getEventsDetail = $this->post();
        $required_arr = array('eventId');

           //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $getEventsDetail)) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val.' field is not valid',
                    'statusCode' => REST_Controller::HTTP_BAD_REQUEST
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$getEventsDetail[$val]) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val . ' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }

        $chk_array = array(
           'org_id' => $org_id,
           'event_id' => $getEventsDetail['eventId']
        );

        $getEventsDetail_data = $this->api->getRecords($this->api->table_events,$chk_array);
        
        if (!empty($getEventsDetail_data)) {
                // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Success',
               'responsePacket' => $getEventsDetail_data
            ], REST_Controller::HTTP_OK);
        } else {
                // Set the response and exit
            $this->response([
                'status' => False,
                'message' =>'No record found',
            ], REST_Controller::HTTP_BAD_REQUEST);

        }
    }

}

