<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
// Load the Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';


class Survey extends REST_Controller {
    public function __construct() {
        parent::__construct();
        // Load the user model
        $this->load->model('api');
        // Load these helper to create JWT tokens
        $this->load->helper(['jwt', 'authorization']);
    }


    /**
    * Get Survey.
    * @return json encode array that contain flag,message
    * @param  CategoryId,Page,PageSize
    */

    public function getSurvey_post(){

        $userIDArr = $this->verify_request();
        $org_id = $userIDArr->org_id;
        $getSurvey = $this->post();
        $required_arr = array("CategoryId","Page","PageSize");

       //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $getSurvey)) {
                $returnArray = array(
                     'status' => false,
                    'message' => $val.'field is not valid',
                    'statusCode' => REST_Controller::HTTP_BAD_REQUEST
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }

        //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$getSurvey[$val]) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val .'field is required.',
                    'statusCode' => REST_Controller::HTTP_BAD_REQUEST
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        $chk_array = array(
           'org_id' => $org_id,
           'survey_category' => $getSurvey['CategoryId']
        );

        $Page = $getSurvey['Page'];
        $PageSize = $getSurvey['PageSize'];
        $offset=($Page-1)*($PageSize);

        $Survey_data = $this->api->getRecords($this->api->table_survey,$chk_array,$whereOR =NULL,$order_by=NULL,$PageSize,$offset);
         $total_Survey = $this->api->count_record($this->api->table_survey,$chk_array);

        if (!empty($Survey_data)) {
                // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Success',
                'total'=>$total_Survey,
                'responsePacket' => $Survey_data
            ], REST_Controller::HTTP_OK);
        } else {
                // Set the response and exit
            $this->response([
                'status' => False,
                'message' =>'No record found',
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    /**
    * Get SurveyDetail.
    * @return json encode array that contain flag,message
    * @param  jobId
    */

    public function getSurveyDetail_post(){
        $userIDArr = $this->verify_request();
        $org_id = $userIDArr->org_id;
        $getSurveyDetail = $this->post();
        $required_arr = array('SurveyId');

           //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $getSurveyDetail)) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val.' field is not valid',
                    'statusCode' => REST_Controller::HTTP_BAD_REQUEST
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$getSurveyDetail[$val]) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val . ' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }

        $chk_array = array(
           'org_id' => $org_id,
           'survey_id' => $getSurveyDetail['SurveyId']

        );
        $getSurveyDetail_data = $this->api->getRecords($this->api->table_survey,$chk_array);

        if (!empty($getSurveyDetail_data)) {
                // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Success',
                'responsePacket' => $getSurveyDetail_data
            ], REST_Controller::HTTP_OK);
        } else {
                // Set the response and exit
            $this->response([
                'status' => False,
                'message' =>'No record found',
            ], REST_Controller::HTTP_BAD_REQUEST);

        }
    }



    /**
    * Submit Survey.
    * @return json encode array that contain flag,message
    * @param  userId
    */

    public function submitSurvey_post(){
        $userIDArr = $this->verify_request();
        $SurveyDetail = $this->post();
        $required_arr = array('survey_id','user_id','selected_options');

        //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $SurveyDetail)) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val.' field is not valid',
                    'statusCode' => REST_Controller::HTTP_BAD_REQUEST
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$SurveyDetail[$val]) {
                $returnArray = array(   
                    'status' => false,
                    'message' => $val . ' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        $selected_options=json_encode($SurveyDetail['selected_options']);
        $insert = array(
           'survey_id' => $SurveyDetail['user_id'],
           'user_id' => $SurveyDetail['survey_id'],
           'result'=> $selected_options

        );
        $getResult_id = $this->db->insert('lss_survey_result', $insert);
        if (!empty($getResult_id)) {
           // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Success',
                'responsePacket' => $getResult_id
            ], REST_Controller::HTTP_OK);
        } else {
           // Set the response and exit
            $this->response([
                'status' => False,
                'message' =>'No record found',
            ], REST_Controller::HTTP_BAD_REQUEST);

        }
    

    }


}

