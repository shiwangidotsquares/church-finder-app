<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
// Load the Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';


class BusinessDirectories extends REST_Controller {
    public function __construct() {
        parent::__construct();
        // Load the user model
        $this->load->model('api');
        // Load these helper to create JWT tokens
        $this->load->helper(['jwt', 'authorization']);
    }


    /**
    * Get BusinessDirectories.
    * @return json encode array that contain flag,message
    * @param  Page ,pageSize
    */

    public function getBusinessDirectories_post(){
        $userIDArr = $this->verify_request();
        $org_id = $userIDArr->org_id;
        $BusinessDirectories = $this->post();
        $required_arr = array('Page','PageSize');
        //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $BusinessDirectories)) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val.' field is not valid',
                    'statusCode' => REST_Controller::HTTP_BAD_REQUEST
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$BusinessDirectories[$val]) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val . ' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        $chk_array = array(
           'org_id' => $org_id
        );
        $BusinessDirectories_data = $this->api->getRecords($this->api->table_business_directories,$chk_array);
        $total_BusinessDirectories = $this->api->count_record($this->api->table_business_directories,$chk_array);
        if (!empty($BusinessDirectories_data)) {
                // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Success',
                'total'=>$total_BusinessDirectories,
                'responsePacket' => $BusinessDirectories_data
            ], REST_Controller::HTTP_OK);
        } else {
                // Set the response and exit
            $this->response([
                'status' => False,
                'message' =>'No record found',
            ], REST_Controller::HTTP_BAD_REQUEST);

        }
    }
}

