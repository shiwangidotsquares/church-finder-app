<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
// Load the Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Blogs extends REST_Controller {
    public function __construct() {

        parent::__construct();
        // Load the user model
        $this->load->model('api');

        // Load these helper to create JWT tokens
        $this->load->helper(['jwt', 'authorization']);
    }
    /**
    * Get Blog.
    * @return json encode array that contain flag,message
    * @param  CategoryId,Page,PageSize
    */
    public function getBlogs_post(){
        $userIDArr = $this->verify_request();
        $org_id = $userIDArr->org_id;
        $getBlogs = $this->post();
        $required_arr = array("CategoryId","Page","PageSize");

       //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $getBlogs)) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val.' field is not valid',
                    'statusCode' => REST_Controller::HTTP_BAD_REQUEST
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }

          //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$getBlogs[$val]) {
                $returnArray = array(
                    'status' => false,
                    'message' =>  $val .' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        $chk_array = array(
            'org_id' => $org_id,
            'category_id' => $getBlogs['CategoryId']
        );

        $Page=$getBlogs['Page'];
        $PageSize= $getBlogs['PageSize'];
        $offset=($Page-1)*($PageSize);
        $getBlogs_data = $this->api->getRecords($this->api->table_blog,$chk_array,$whereOR =NULL,$order_by=NULL,$PageSize,$offset);
        $total_Blogs = $this->api->count_record($this->api->table_product,$chk_array);
        if (!empty($getBlogs_data)) {
            $this->response([
            'status' => TRUE,
            'message' => 'Success',
            'total'=>$total_Blogs,
            'responsePacket' => $getBlogs_data,

            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
            'status' => False,
            'message' =>'No record found',
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }


    /**
    * get BlogDetail Detail.
    * @return json encode array that contain flag,message
    * @param  parentId
    */

    public function getBlogDetail_post(){
        $userIDArr = $this->verify_request();
        $org_id = $userIDArr->org_id;
        $getProductDetail = $this->post();
        $required_arr = array('BlogId');

       //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $getProductDetail)) {
                $returnArray = array(
                    'status' => false,
                    'message' => 'Key field is not valid',
                    'statusCode' => REST_Controller::HTTP_BAD_REQUEST
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }

          //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$getProductDetail[$val]) {
                $returnArray = array(
                    'status' => false,
                    'message' => str_replace("_", " ", $val) . ' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }

        $chk_array = array(
            'org_id' => $org_id,
            'blog_id' => $getProductDetail['BlogId']
        );
        $getProductDetail = $this->api->getRecords($this->api->table_blog,$chk_array);
            if (!empty($getProductDetail)) {
            // Set the response and exit
            $this->response([
            'status' => TRUE,
            'message' => 'Success',
            'responsePacket' => $getProductDetail
            ], REST_Controller::HTTP_OK);
        } else {
            // Set the response and exit
            $this->response([
            'status' => False,
            'message' =>'No record found',
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}

