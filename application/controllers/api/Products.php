<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
// Load the Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';


class Products extends REST_Controller {
    public function __construct() {
        parent::__construct();
        // Load the user model
        $this->load->model('api');
        // Load these helper to create JWT tokens
        $this->load->helper(['jwt', 'authorization']);
    }

    /**
    * Get Product.
    * @return json encode array that contain flag,message
    * @param  parentId
    */

    public function getProducts_post(){

        $userIDArr = $this->verify_request();
        $org_id = $userIDArr->org_id;
        $getProduct = $this->post();
        $required_arr = array("CategoryId","Page","PageSize");

       //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $getProduct)) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val.' field is not valid',
                    'statusCode' => REST_Controller::HTTP_BAD_REQUEST
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }

        //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$getProduct[$val]) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val .' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        $chk_array = array(
           'org_id' => $org_id,
           'category_id' => $getProduct['CategoryId']
        );

        $Page = $getProduct['Page'];
        $PageSize = $getProduct['PageSize'];
        $offset=($Page-1)*($PageSize);

        $Product_data = $this->api->getRecords($this->api->table_product,$chk_array,$whereOR =NULL,$order_by=NULL,$PageSize,$offset);
        $total_products = $this->api->count_record($this->api->table_product,$chk_array);

        if (!empty($Product_data)) {
                // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Success',
                'total'=>$total_products,
                'responsePacket' => $Product_data
            ], REST_Controller::HTTP_OK);
        } else {
                // Set the response and exit
            $this->response([
                'status' => False,
                'message' =>'No record found',
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    /**
    * Get Product Detail.
    * @return json encode array that contain flag,message
    * @param  ProductId
    */

    public function getProductDetail_post(){
        $userIDArr = $this->verify_request();
        $org_id = $userIDArr->org_id;
        $getProductDetail = $this->post();
        $required_arr = array('ProductId');

           //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $getProductDetail)) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val.' field is not valid',
                    'statusCode' => REST_Controller::HTTP_BAD_REQUEST
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$getProductDetail[$val]) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val . ' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }

        $chk_array = array(
           'org_id' => $org_id,
           'product_id' => $getProductDetail['ProductId']
        );
        $Product_data = $this->api->getRecords($this->api->table_product,$chk_array);
        if (!empty($Product_data)) {
                // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Success',
                'responsePacket' => $Product_data
            ], REST_Controller::HTTP_OK);
        } else {
                // Set the response and exit
            $this->response([
                'status' => False,
                'message' =>'No record found',
            ], REST_Controller::HTTP_BAD_REQUEST);

        }
    }
}

