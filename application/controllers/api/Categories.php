<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
// Load the Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Categories extends REST_Controller {
    public function __construct() {
        parent::__construct();
        // Load the user model
        $this->load->model('api');

        // Load these helper to create JWT tokens
        $this->load->helper(['jwt', 'authorization']);
    }
    /**
    * Get Categories.
    * @return json encode array that contain flag,message
    * @param  parentId
    */
    public function getCategories_post(){
        $userIDArr = $this->verify_request();
        $org_id = $userIDArr->org_id;
        $getCategories_post = $this->post();
        $required_arr = array('parentId');

       //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $getCategories_post)) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val.' field is not valid',
                    'statusCode' => REST_Controller::HTTP_BAD_REQUEST
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        $chk_array = array(
            'org_id' => $org_id,
            'category_parent_id' => $getCategories_post['parentId']
        );
        $category_data = $this->api->getRecords($this->api->table_categories,$chk_array);
        $total_category_data = $this->api->count_record($this->api->table_categories,$chk_array);
            if (!empty($category_data)) {
            // Set the response and exit
            $this->response([
            'status' => TRUE,
            'message' => 'Success',
            'total'=>$total_category_data,
            'responsePacket' => $category_data
            ], REST_Controller::HTTP_OK);
        } else {
            // Set the response and exit
            $this->response([
            'status' => False,
            'message' =>'No record found',
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

}

