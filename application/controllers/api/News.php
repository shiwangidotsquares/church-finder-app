<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
// Load the Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';


class News extends REST_Controller {
    public function __construct() {
        parent::__construct();
        // Load the user model
        $this->load->model('api');
        // Load these helper to create JWT tokens
        $this->load->helper(['jwt', 'authorization']);
    }

    
    /**
    * Get News.
    * @return json encode array that contain flag,message
    * @param  Page ,pageSize
    */

    public function getNews_post(){
        $userIDArr = $this->verify_request();
        $org_id = $userIDArr->org_id;
        $getNews = $this->post();
        $required_arr = array('Page','PageSize');

           //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $getNews)) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val.' field is not valid',
                    'statusCode' => REST_Controller::HTTP_BAD_REQUEST
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$getNews[$val]) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val . ' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }

        $chk_array = array(
           'org_id' => $org_id
        );
        $getNews_data = $this->api->getRecords($this->api->table_news,$chk_array);
        $total_getNews = $this->api->count_record($this->api->table_news,$chk_array);
        if (!empty($getNews_data)) {
                // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Success',
                'total'=>$total_getNews,
                'responsePacket' => $getNews_data
            ], REST_Controller::HTTP_OK);
        } else {
                // Set the response and exit
            $this->response([
                'status' => False,
                'message' =>'No record found',
            ], REST_Controller::HTTP_BAD_REQUEST);

        }
    }

    
    /**
    * Get NewsDetail.
    * @return json encode array that contain flag,message
    * @param  NewsId
    */

    public function getNewsDetail_post(){
        $userIDArr = $this->verify_request();
        $org_id = $userIDArr->org_id;
        $getNewsDetail = $this->post();
        $required_arr = array('NewsId');

           //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $getNewsDetail)) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val.' field is not valid',
                    'statusCode' => REST_Controller::HTTP_BAD_REQUEST
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$getNewsDetail[$val]) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val . ' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }

        $chk_array = array(
           'org_id' => $org_id,
           'news_id' => $getNewsDetail['NewsId']
        );

        $getNewsDetail_data = $this->api->getRecords($this->api->table_news,$chk_array);
        $total_NewsDetai = $this->api->count_record($this->api->table_news,$chk_array);
        if (!empty($getNewsDetail_data)) {
                // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Success',
               'responsePacket' => $getNewsDetail_data
            ], REST_Controller::HTTP_OK);
        } else {
                // Set the response and exit
            $this->response([
                'status' => False,
                'message' =>'No record found',
            ], REST_Controller::HTTP_BAD_REQUEST);

        }
    }



}

