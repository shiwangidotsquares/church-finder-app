<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
// Load the Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';


class Authentication extends REST_Controller {
    public function __construct() {

        parent::__construct();
        // Load the user model
        $this->load->model('api');

        // Load these helper to create JWT tokens
        $this->load->helper(['jwt', 'authorization']);
    }


     /*Register API*/
    public function registration_post()
    {
        $register_list = $this->post();
        $required_arr = array('name','email','password');


        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $register_list)) {
                $returnArray = array(
                    'status' => 201,
                    'message' => str_replace("_", " ", $val) . ' field is required.'
                );
                
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$register_list[$val]) {
                $returnArray = array(
                    'status' => 201,
                    'message' => str_replace("_", " ", $val) . ' field is required.'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        
        $name =$this->post('name');
       $email  = strtolower($this->post('email'));

        $condition_array = array(
            'user_email' => $email,
            'user_is_deleted'=>'0'
        );

        $email_exist = $this->api->getRecords($this->api->table_users, $condition_array);

        if ($email_exist) {
            $error = 1;
            $message = "This email already exists in the system, please try with another email.";
            $returnArray = array(
                'status' => false,
                'message' => $message,
                'statusCode' => '500'
            );
            echo json_encode($returnArray, JSON_NUMERIC_CHECK);
            die;
        }

       else {

              $password = md5($register_list['password']);
         // Insert user data
            $userData = array(
                'user_username'=>$name, 
                'user_email' => $email,
                'user_password' => $password,
                'user_is_active'=>'1',
                /*'device_token' => $register_list['deviceToken'],
                'device_type' => $register_list['deviceType'],*/
                'created_time' => date("Y-m-d H:i:s")
            );

             //save function is used to save or update records in a given table
            $insert = $this->api->save($this->api->table_users, $userData);
            
            // Check if the user data is inserted
            if($insert){

            ////////////////////////////////Send Activation Email///////////////

                $signup_token  = mt_rand(100000, 999999);

                $save_user_data = array(
                    'auth_key' => $signup_token.'_'.$insert
                );
                
                //save registration token
                $where_id = array(
                    'user_id'=>$insert,
                );
                $updated_row = $this->api->save($this->api->table_users,$save_user_data,$where_id);
                $signToken = $signup_token.'_'.$insert;
                $email = strtolower($email);
                
                //sending email if user sign up
                if ($updated_row){
                    $mail_to      = $email;
                    $subject      = 'New Registration!';
                    $mail_extra   = '';
                    $mail_cc      = '';
                    $mail_bcc     = '';
                    $sender_email = ADMIN_EMAIL;
                    $sender_name  = ADMIN_NAME;

                    $url = base_url().'activatethanks/updateSignUpToken_get/'. urlencode($signToken);

                    $message_data = '<p>Dear '.$this->post('name').',</p>
                    <p>Welcome to the Church Finder App! Click on the link below to confirm your details.</p>
                    <p><a href="'.$url.'">'.$url.'</a></p>';
                    $body = array('email_content'=>$message_data);

                    $messagebody = $this->parser->parse('emailer/email', $body, true);

                   $this->api->sendmail($email, $subject, $messagebody, $mail_cc, $mail_bcc, $sender_email, $sender_name, false);
                    
                    $con= array(
                        '   user_id' => $insert
                    );

                    $userRow = $this->api->checkRecord($this->api->table_users,$con);
                    $userData = array();
                   $userData['userId'] = $userRow['user_id'];
                    
            
                    // Create a token from the user data and send it as reponse
                    $token = AUTHORIZATION::generateToken(array('user_id' => $userData['userId']));

                    $returnArray = array(
                        'status'=> true,
                        'message'=> 'A verification email has been sent to your email address, Please verify your email to enjoy our services.',
                        'statusCode'=>'200',
                        'token'=> $token
                    );
                    echo json_encode($returnArray,JSON_NUMERIC_CHECK); die;


                    }
                ////////////////////////////End Registration Email //////////////////////////

                    $returnArray = array(
                    'status' => true,
                    'message' => 'Registration successful! Welcome to the Church Finder App!',
                    'statusCode' => '200',
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;

                
            }else{
                // Set the response and exit
                $this->response([
                    'status' => 201,
                    'message' => 'Error occurred.',
                ], REST_Controller::HTTP_OK);
                
            }
        }
        
    }
    
  
        /*
    Get Profile Data
    */
    public function getprofiledata_post()
    {
        //verify request
        $userIDArr = $this->verify_request();
        $userId = $userIDArr->user_id;
        $where_user = array(
            'users.id' => $userId
        );
        $join = 1;
        // Get user details if the user data exists
        $userdetails = $this->users->getUserDetails($this->users->table_users, $join, $where_user);
        // Set the response and exit
        if ($userdetails) {
            $userData['userId'] = $userdetails['user_id'];
            $userData['FirstName'] = $userdetails['first_name'];
            $userData['LastName'] = $userdetails['last_name'];
            $userData['emailId'] = $userdetails['user_email'];
            $userData['profileImage'] = $userdetails['profile_image'];
            $userData['phoneNumber'] = $userdetails['contact_number'];
            $userData['gender'] = $userdetails['gender'];
            $userData['isPaidUser'] = $userdetails['is_subscribed'];
            $this->response([
                'status' => 200,
                'message' => GLOBAL_SUCCESS,
                'responseData' => $userdetails
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => 201,
                'message' => NO_USER_FOUND
            ], REST_Controller::HTTP_OK);
        }
    }
    
    /*
    Update Profile Data
    */
    public function updateProfileData_post()
    {
        $userIDArr = $this->verify_request();
        $id = $userIDArr->user_id;
        
        // Get the post data
        $first_name = strip_tags($this->post('firstName'));
        $last_name = strip_tags($this->post('lastName'));
        $email = strip_tags($this->post('emailId'));
        $profile_image_url = $this->post('profile_image_url');
        $image = $_FILES['profileImage'];
        $phone = strip_tags($this->post('phoneNumber'));
        $gender = $this->post('gender');
        
        // Validate the post data
        if (!empty($id) && (!empty($first_name) || !empty($last_name) || !empty($email) || !empty($image) || !empty($phone) || !empty($gender) )) {
            $error = array();
            // Update user's account data
            $userReturnData = array();
            if (!empty($first_name)) {
                $userDetailData['first_name'] = $first_name;
            }
            if (!empty($last_name)) {
                $userDetailData['last_name'] = $last_name;
            }
            if (!empty($email)) {
                $userData['user_email'] = $email;
            }
            if (!empty($image)) {
                if (!empty($_FILES['profileImage']['name'])) {
                    $profilePath = $this->image_config_path['profile'];
                    $config = $this->setConfigImage($profilePath);
                    $file_ext = pathinfo($_FILES['profileImage']['name'], PATHINFO_EXTENSION);
                    $image_file_name = time(). '.' . $file_ext;
                    $config['file_name'] = $image_file_name;
                    $this->load->library('upload', $config, 'imageupload');
                    $this->imageupload->initialize($config);

                    if (!$this->imageupload->do_upload('profileImage')) {
                        $error = $this->imageupload->display_errors();
                        // Set the response and exit if error in uploading image
                        $this->response([
                            'status' => 201,
                            'message' => strip_tags($error)
                        ], REST_Controller::HTTP_OK);
                    } else {
                        $userDetailData['profile_image'] = base_url() . $profilePath . $image_file_name;
                        $profile_image_url = $userDetailData['profile_image'];

                    }
                }
            }
            if (!empty($phone)) {
                $userDetailData['contact_number'] = $phone;
            }
            if (!empty($gender)) {
                $userDetailData['gender'] = $gender;
            }
            $userReturnData = $userDetailData;
            $userReturnData['user_email'] = $userData['user_email'];
            //prd($userData);
            $update = $this->users->update($userData, $id);
            $detailsInsert = $this->UserDetails->save($this->UserDetails->table, $userDetailData, $where = ['user_id' => $id]);
            // Check if the user data is updated
            if ($update) {
                $userReturnData['profile_image_url'] = $profile_image_url;
                // Set the response and exit
                $this->response([
                    'status' => 200,
                    'message' => UPDATED_SUCCESS,
                    'responseData' => $userReturnData
                ], REST_Controller::HTTP_OK);
            } else {
                // Set the response and exit
                $this->response([
                    'status' => 201,
                    'message' => GLOBAL_ERROR
                ], REST_Controller::HTTP_OK);
            }
        } else {
            // Set the response and exit
            $this->response([
                'status' => 201,
                'message' => 'Provide at least one user info to update.'
            ], REST_Controller::HTTP_OK);
        }
    }

    /*
    Social login
    */
    function userSocialLogin_post(){
        $social_id_existed = true;
        $register_list = $this->post();
        $required_arr = array('socialLoginId');
        //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $register_list)) {
                $returnArray = array(
                    'status' => false,
                    'message' => str_replace("_", " ", $val) . ' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$register_list[$val]) {
                $returnArray = array(
                    'status' => false,
                    'message' => str_replace("_", " ", $val) . ' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }

        $con['returnType'] = 'single';
        $con['conditions'] = array(
            'social_id'=>$this->post('socialLoginId'),
            'is_deleted' => NOT_DELETED,
        );
        $roleId = 2;
        $userdata = $this->users->getRows($con, $join = true, $roleId);
        if (!empty($userdata)) {
            if ($userdata['is_active'] == '0') {
                $returnArray = array(
                    'status'  => 201,
                    'message' => ACCOUNT_NOT_ACTIVE
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
            $where = array(
                'id' => $userdata['user_id']
            );
            $save_user_data = array(
                'device_token' => $this->post('deviceToken'),
                'device_type'  => $this->post('deviceType'),
                'social_id'    => $this->post('socialLoginId'),
            );

            if ($this->post('email')!="") {
                $save_user_data['user_email'] = $this->post('email');
            }
            $this->users->save($this->users->table_users, $save_user_data, $where);

            $todayDate = date("Y-m-d");
            $userdata['is_profilecomplete'] = true;
        } else {
            $social_id_existed = false;
            // Insert user data
            $email  = strtolower($this->post('email'));
            $userData = array(
                'user_email' => $email,
                'device_token' => $register_list['deviceToken'],
                'device_type' => $register_list['deviceType'],
                'created_time' => date("Y-m-d H:i:s"),
                'is_subscribed'  => $this->post('isPaidUser'),
                'social_id'  => $this->post('socialLoginId'),
            );
            $insert = $this->users->insert($userData);
            // Check if the user data is inserted
            if ($insert) {
                //save roles and user details
                $roleCreateData = array(
                    'role_id' => CUSTOMER_ROLE_ID,
                    'user_id' => $insert,
                );
                $userDetailData = array(
                    'first_name' => $this->post('FirstName'),
                    'last_name' => $this->post('LastName'),
                    'contact_number' => $this->post('phone'),
                    'gender' => $this->post('gender'),
                    'profile_image' => $this->post('profileImage'),
                    'created_time' => date("Y-m-d H:i:s"),
                    'user_id' => $insert,
                );
                $detailsInsert = $this->UserDetails->save($this->UserDetails->table, $userDetailData);
                $rolesInsert = $this->UserRoles->save($this->UserRoles->table, $roleCreateData);
            }
            $where_user = array(
                'id' => $insert
            );
            $userdata = $this->users->getRows($con, $join = true, $roleId);
        }

        $userData = [];
        $userData['userId'] = $userdata['user_id'];
        $userData['FirstName'] = $userdata['first_name'];
        $userData['LastName'] = $userdata['last_name'];
        $userData['email'] = $userdata['user_email'];
        $userData['profileImage'] = $userdata['profile_image'];
        $userData['contact_number'] = $userdata['contact_number'];
        $userData['gender'] = $userdata['gender'];
        $userData['role_id'] = $userdata['role_id'];
        $userData['isPaidUser'] = ($userRow['is_subscribed']) ? "true" : "false";
        $userData['deviceToken'] = $this->post('deviceToken');
        $userData['deviceType'] = $this->post('deviceType');
        $userData['privacy_policy'] = ($userdata['privacy_policy']) ? "true" : "false";
        $token = AUTHORIZATION::generateToken(array('user_id' => $userData['userId']));
        $returnArray = array(
            'status' => 200,
            'message' => LOGIN_SUCCESSFUL,
            'token'=> $token,
            'data' => $userData,
            'social_id_existed' => $social_id_existed,
        );
        echo json_encode($returnArray, JSON_NUMERIC_CHECK);
        die;
    }
    

    /**
     * Login
     * @return json encode array that contain jwt token and orgnization data.
     * @param  email password
     */
    public function login_post(){
        // Get the post data
        $login_post = $this->post();
        $required_arr = array('email','password');
        //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $login_post)) {
                $returnArray = array(
                    'status' => false,
                    'message' => str_replace("_", " ", $val) . ' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$login_post[$val]) {
                $returnArray = array(
                    'status' => false,
                    'message' => str_replace("_", " ", $val) . ' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        //condition to get orgnization.
        $conndition = array(
            'user_email' => $login_post['email'],
            'user_password' => md5($login_post['password']),
            'user_is_active' => 1,
        );

        $user_data = $this->api->getRecords($this->api->table_users,$conndition);
        $user_data = $user_data[0];

        if (!empty($user_data)) {
             // Create a token from the user data and send it as reponse
            $token = AUTHORIZATION::generateToken(array('user_id' => $user_data['user_id']));

            unset($user_data['user_id']);
            // Set the response and exit
             $returnArray = array(
                    'status'=>TRUE,
                    'message'=> REST_Controller::LOGIN_SUCCESS,
                     'token'=> $token,
                     //'data' => $user_data,
                    'statusCode'=>REST_Controller::HTTP_OK
                );
                echo json_encode($returnArray);

        } else {
            // Set the response and exit
             $returnArray = array(
                    'status'=>False,
                    'message'=> REST_Controller::LOGIN_FAILED,
                    'statusCode'=>REST_Controller::HTTP_NOT_FOUND
                );
                echo json_encode($returnArray);


        }
    }
    /**
    * Forgot Password.
    * @return json encode array that contain flag,message
    * @param  email
    */
    function forgot_post(){

        $userIDArr = $this->verify_request();
        $user_id = $userIDArr->user_id;
        
        $forgot_post = $this->post();
        $required_arr = array('email');
        //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $forgot_post)) {
                $returnArray = array(
                    'status' => false,
                    'message' => str_replace("_", " ", $val) . ' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$forgot_post[$val]) {
                $returnArray = array(
                    'status' => false,
                    'message' => str_replace("_", " ", $val) . ' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }

        $chk_array = array(
            'user_email' => $forgot_post['email']
        );
        $user_data = $this->api->getRecords($this->api->table_users,$chk_array);
        $user_data = $user_data[0];


        if(!empty($user_data)){
            $mail_to = $forgot_post['email'];
            $From = $forgot_post['email'];

            $subject = 'Your reset password link ';

            $uniqid=uniqid();
            $update_password_field  = array(
                'verification_code' => $uniqid,
            );
            $where_user   = array(
                'user_id' => $user_data['user_id']
            );
            $this->api->save($this->api->table_users,$update_password_field, $where_user);
                 
            $messagebody='
            <p>Hi '.$user_data['user_username'].',</p><br />
            <p>Please click on the link below to reset your password. If you didn’t request for new password, please ignore this mail.</p><br />
            <a href="'.base_url().'Authentication/resetpassword?uniq_id='.$uniqid.'" target="_blank">Click here to reset your password</a><br />
            <p>Thanks,<p>';

            $switch = $this->api->sendmail(strtolower($mail_to), $subject, $messagebody, '', '', $From, 'test', false);

            if($switch){
                $returnArray = array(
                    'status'=> true,
                    'message'=> 'Reset password link sent to your registered email.',
                    'statusCode'=>'200'
                );
                    //$this->response($returnArray, 200);
                echo json_encode($returnArray);
                    //die;
            }else{
                $returnArray = array(
                    'status'=>TRUE,
                    'message'=> 'Sent mail failed',
                    'statusCode'=>'200'
                );
                echo json_encode($returnArray);

            }

        }else{

            $returnArray = array(
                'status'=> true,
                'message'=> 'This email is not registered with us. Please check the email and try again',
                'statusCode'=>'500'
            );
            echo json_encode($returnArray);
            die;
        }
    }


    /**
    * Change password.
    * @return json encode array that contain flag,message
    * @param  Old password, new_password, confirm password
    */
    function changepassword_post(){
        $userIDArr = $this->verify_request();
        $org_id = $userIDArr->org_id;
        $change_psw_post = $this->post();
        $required_arr = array('old_password','new_password','confirm_password');
        //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $change_psw_post)) {
                $returnArray = array(
                    'status' => false,
                    'message' => str_replace("_", " ", $val) . ' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
            //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$change_psw_post[$val]) {
                $returnArray = array(
                    'status' => false,
                    'message' => str_replace("_", " ", $val) . ' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }


        $old_password =md5($change_psw_post['old_password']);
        $where_user = array(
            'org_id'=>$org_id,
            'user_password' => $old_password
        );
                //checking old password for security
        $result = $this->api->checkRecord($this->api->table_orgnization,$where_user);


        if ($result) {
            $where_user = array(
                'org_id' => $org_id
            );
            $save_data = array(
                'user_password' => md5($change_psw_post['new_password'])
            );
                        //updating password
            $result = $this->api->save($this->api->table_orgnization, $save_data, $where_user);
            $this->response([
                'status' => True,
                'Message' => REST_Controller::CHANGE_PASSWORD_SUCCEFULLY,
            ], REST_Controller::HTTP_OK);

        }
        else{
            $this->response([
                'status' => False,
                'Message' => REST_Controller::OLD_PASSWORD_ERROR,
            ], REST_Controller::HTTP_OK);
        }
    }

     
   /**
    * get Media .
    * @return json encode array that contain flag,message
    * @param  media_type
    */

     public function getMedia_post(){

        $userIDArr = $this->verify_request();
        $org_id = $userIDArr->org_id;
        $getMedia = $this->post();
        $required_arr = array('media_type');
           //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $getMedia)) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val.' field is not valid',
                    'statusCode' => REST_Controller::HTTP_BAD_REQUEST
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$getMedia[$val]) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val . ' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }

        $chk_array = array(
           'org_id' => $org_id,
           'type' => $getMedia['media_type']
        );

        $getMedia_data = $this->api->getRecords($this->api->table_media,$chk_array);
        $getMedia_data['base_url'] = base_url().'uploads/mediamanager/';
        echo "<pre>";
        print_r($getMedia_data); die;
        if (!empty($getMedia_data)) {
                // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Success',
               'responsePacket' => $getMedia_data
            ], REST_Controller::HTTP_OK);
        } else {
                // Set the response and exit
            $this->response([
                'status' => False,
                'message' =>'No record found',
            ], REST_Controller::HTTP_BAD_REQUEST);

        }
    }

}

