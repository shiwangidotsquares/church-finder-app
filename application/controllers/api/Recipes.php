<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
// Load the Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';


class Recipes extends REST_Controller {
    public function __construct() {
        parent::__construct();
        // Load the user model
        $this->load->model('api');
        // Load these helper to create JWT tokens
        $this->load->helper(['jwt', 'authorization']);
    }


    /**
    * Get Recipes.
    * @return json encode array that contain flag,message
    * @param  Page,PageSize
    */

    public function getRecipes_post(){

        $userIDArr = $this->verify_request();
        $org_id = $userIDArr->org_id;
        $getRecipes = $this->post();
        $required_arr = array("Page","PageSize");

       //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $getRecipes)) {
                $returnArray = array(
                     'status' => false,
                    'message' => $val.'field is not valid',
                    'statusCode' => REST_Controller::HTTP_BAD_REQUEST
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }

        //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$getRecipes[$val]) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val .'field is required.',
                    'statusCode' => REST_Controller::HTTP_BAD_REQUEST
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }   
        $chk_array = array(
           'org_id' => $org_id
           
        );
        
        $Page = $getRecipes['Page'];
        $PageSize = $getRecipes['PageSize'];
        $offset=($Page-1)*($PageSize);

        $getRecipes_data = $this->api->getRecords($this->api->table_recipe,$chk_array,$whereOR =NULL,$order_by=NULL,$PageSize,$offset);
         $total_Recipes_data = $this->api->count_record($this->api->table_recipe,$chk_array);
         
        if (!empty($getRecipes_data)) {
                // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Success',
                'total'=>$getRecipes_data,
                'responsePacket' => $total_Recipes_data
            ], REST_Controller::HTTP_OK);
        } else {
                // Set the response and exit
            $this->response([
                'status' => False,
                'message' =>'No record found',
            ], REST_Controller::HTTP_BAD_REQUEST);
        }  
    }


    /**
    * Get Events Detail.
    * @return json encode array that contain flag,message
    * @param  recipe_id
    */

    public function getRecipeDetail_post(){
        $userIDArr = $this->verify_request();
        $org_id = $userIDArr->org_id;
        $getrecipeDetail = $this->post();
        $required_arr = array('recipeId');

           //Checking required fields key exists or not
        foreach ($required_arr as $key => $val) {
            if (!array_key_exists($val, $getrecipeDetail)) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val.' field is not valid',
                    'statusCode' => REST_Controller::HTTP_BAD_REQUEST
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }
        //Checking required fields value exists or not
        foreach ($required_arr as $key => $val) {
            if (!$getrecipeDetail[$val]) {
                $returnArray = array(
                    'status' => false,
                    'message' => $val . ' field is required.',
                    'statusCode' => '500'
                );
                echo json_encode($returnArray, JSON_NUMERIC_CHECK);
                die;
            }
        }

        $chk_array = array(
           'org_id' => $org_id,
           'recipe_id' => $getrecipeDetail['recipeId']
        );

        $getrecipeDetail_data = $this->api->getRecords($this->api->table_recipe,$chk_array);
        
        if (!empty($getrecipeDetail_data)) {
                // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Success',
               'responsePacket' => $getrecipeDetail_data
            ], REST_Controller::HTTP_OK);
        } else {
                // Set the response and exit
            $this->response([
                'status' => False,
                'message' =>'No record found',
            ], REST_Controller::HTTP_BAD_REQUEST);

        }
    }


}

