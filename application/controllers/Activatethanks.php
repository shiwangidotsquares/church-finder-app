<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Activatethanks extends MY_Controller {
    var $secret = '';
	function __construct(){
        parent::__construct();
        $this->load->model('users');
        $this->load->model('api');
    }

	public function updateSignUpToken_get($singuptoken){
        //prd($singuptoken);
        $where_key = array(
                'auth_key'=>$singuptoken,
        );
        $details = $this->api->getRecords($this->api->table_users,$where_key);

        if(empty($details)){
            $index['message'] = "This user detail does not exist in the system, please try again.";
            $index['msg_class'] = 'alert alert-error nor-space';
            $index['msg_title'] = 'Oops !!';
        }else{
            $save_data = array(
                'signup_status' => 1
            );

            $where_id = array(
                'auth_key'=>$singuptoken,
            );
            $signToken=$this->api->save($this->api->table_users,$save_data,$where_id);
            $index['message'] = "Activate Account Successfully.";
            $index['msg_class'] = 'alert alert-success nor-space';
            $index['msg_title'] = 'Thanks';
        }

        $this->templates->set('activate-thanks');
        $this->templates->set_data('index',$index);
        $this->templates->load();
    }
}

/* End of file Home.php */
