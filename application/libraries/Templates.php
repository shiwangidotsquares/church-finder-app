<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');
 /**
  * Template
  *
  * WordPress like template for CodeIgniter
  *
  * @package        Template
  * @version        0.1.0
  * @author        WebInOne
  * @link            http://www.webduos.com/codeigniter-template-library/
  * @copyright        Copyright (c) 2011, WebInOne
  * @license        http://opensource.org/licenses/mit-license.php MIT Licensed
  *
  */

  class Templates
  {
      private $ci;
      private $tp_name;
      private $data = array();

      public function __construct() {
          $this->ci = &get_instance();
      }

      public function set($name='') {
          $this->tp_name = $name;
      }

      public function load($name = 'index') {
          $this->load_file($name);
      }

      public function get_top($name) {
          if(isset($name)) {
              $file_name = "header-{$name}.php";
              $this->load_file($file_name);
          }
          else {
              $this->load_file('top');
          }
      }

	  public function get_header($name) {
          if(isset($name)) {
              $file_name = $name;
              $this->load_file($file_name);
          }
          else {
              $file_name = "header.php";
              $this->load_file($file_name);
          }
      }

      public function get_slideshow($name) {
          if(isset($name)) {
              $file_name = $name;
              $this->load_file($file_name);
          }
          else {
              $file_name = "slideshow.php";
              $this->load_file($file_name);
          }
      }

      public function get_left($name) {
          if(isset($name)) {
              $file_name = "sidebar-{$name}.php";
              $this->load_file($file_name);
          }
          else {
              $this->load_file('left');
          }
      }

      public function get_content($name) {
          if(isset($name)) {
              $file_name = "content-{$name}.php";
              $this->load_file($file_name);
          }
          else {
              $this->load_file('content');
          }
      }


      public function get_right($name) {
          if(isset($name)) {
              $file_name = "right-{$name}.php";
              $this->load_file($file_name);
          }
          else {
              $this->load_file('right');
          }
      }


      public function get_breadcrumbs($name) {
          if(isset($name)) {
              $file_name = "breadcrumbs-{$name}.php";
              $this->load_file($file_name);
          }
          else {
              $this->load_file('breadcrumbs');
          }
      }

       public function get_banner($name) {
          if(isset($name)) {
              $file_name = "banner-{$name}.php";
              $this->load_file($file_name);
          }
          else {
              $this->load_file('banner');
          }
      }


      public function get_sidebar($name) {
          if(isset($name)) {
              $file_name = "sidebar-{$name}.php";
              $this->load_file($file_name);
          }
          else {
              $this->load_file('sidebar');
          }
      }

      public function get_footer($name) {
          if(isset($name)) {
              $file_name = "footer-{$name}.php";
              $this->load_file($file_name);
          }
          else {
              $this->load_file('footer');
          }
      }

      public function get_template_part($slug, $name) {
          if(isset($name)) {
              $file_name = "{$slug}-{$name}.php";
              $this->load_file($file_name);
          }
          else{
              $this->load_file($slug);
          }
      }

      public function load_file($name)
      {
          if($this->get_data($name))
          {
              $data = $this->get_data($name);
              $this->ci->load->view($this->tp_name.'/'.$name,$data);
          }
          else {
              $this->ci->load->view($this->tp_name.'/'.$name);
          }
      }

      public function set_data($key, $data) {
          $this->data[$key] = $data;
      }

      public function get_data($key) {
          if(isset($this->data[$key])) {
              return $this->data[$key];
          }
          else {
              return false;
          }
      }

      public function checkSession() {

          if(!$this->ci->session->userdata("admin_session_data")) {
              $this->ci->session->set_flashdata('message' ,'Your login session expired! Please login again.');
			  redirect(base_url(), 'refresh');
          }
          else {
              return true;
          }
      }
  }
?>